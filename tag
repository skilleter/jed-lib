#!/bin/bash

ctags --recurse=yes --slang-types=+f+n --if0=yes --totals=yes --format=2 -otags $(find -name '*.rc' -o -name '*.sl' -type f)
