% Puppet mode based on JSON mode (just as a starting point).

$1 = "puppet";

create_syntax_table ($1);
define_syntax ("/*", "*/", '%', $1);
define_syntax ("#", "", '%', $1);
define_syntax ("([{", ")]}", '(', $1);
define_syntax ('"', '"', $1);
define_syntax ('\'', '\'', $1);
define_syntax ('\\', '\\', $1);
define_syntax ("0-9a-zA-Z_", 'w', $1);        % words
define_syntax ("-+0-9a-fA-F.xXL", '0', $1);   % Numbers
define_syntax (",;.?:", ',', $1);
define_syntax ("$%-+/&*=<>|!~^", '+', $1);
set_syntax_flags ($1, 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache("puppetmode.dfa", name);
   dfa_define_highlight_rule("#.*", "comment", name);
   dfa_define_highlight_rule("/\\*.*\\*/", "Qcomment", name);
   dfa_define_highlight_rule("^([^/]|/[^\\*])*\\*/", "Qcomment", name);
   dfa_define_highlight_rule("/\\*.*", "comment", name);
   dfa_define_highlight_rule("^[ \t]*\\*+([ \t].*)?$", "comment", name);
   dfa_define_highlight_rule("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
   dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?", "number", name);
   dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*[LU]*", "number", name);
   dfa_define_highlight_rule("[0-9]+[LU]*", "number", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"", "string", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*'", "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule("[ \t]+", "normal", name);
   dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
   dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);
   dfa_build_highlight_table(name);
}
dfa_set_init_callback (&setup_dfa_callback, "puppet");
%%% DFA_CACHE_END %%%
#endif

append_keywords("puppet", 0,
                "false", "FALSE", "nil", "NIL", "undef", "UNDEF", "true", "TRUE", "yes", "YES", "no", "NO");

append_keywords("puppet", 1,
                "String", "Integer", "Float", "Numeric", "Boolean", "default", "Array", "Hash",
                "Regexp", "Undef", "Default", "File", "Enum");

%append_keywords("puppet", 1,
%                "absent", "aclinherit", "aclmode", "action_url", "active_checks_enabled",
%                "address", "address1", "address2", "address3", "address4", "address5", "address6",
%                "admin", "adminfile", "age", "alias", "allowcdrom", "allowdupe", "allowed_trunk_vlans",
%                "allow_root", "arguments", "atboot", "atime", "attribute_membership", "attributes",
%                "auth_class", "authenticate_user", "auth_membership", "auths", "auth_type", "autoboot",
%                "backup", "baseurl", "binary", "blockdevice", "canmount", "can_submit_commands", "category",
%                "changes", "check_command", "check_freshness", "check_interval", "check_period", "checksum",
%                "clone", "command", "command_line", "command_name", "comment", "compression", "configfiles",
%                "contactgroup_members", "contactgroup_name", "contactgroups", "contact_groups", "contact_name",
%                "contacts", "content", "context", "control", "copies", "cost", "create_args", "creates", "ctime",
%                "cwd", "dataset", "dependency_period", "dependent_hostgroup_name", "dependent_host_name",
%                "dependent_service_description", "descr", "description", "device", "devices", "device_url",
%                "disk", "display_name", "ds_name", "ds_type", "dump", "duplex", "email", "enable", "enabled",
%                "enablegroups", "en_address", "encapsulation", "ensure", "escalation_options", "escalation_period",
%                "etherchannel", "event_handler", "event_handler_enabled", "exclude", "execution_failure_criteria",
%                "expiry", "failovermethod", "failure_prediction_enabled", "first_notification", "first_notification_delay",
%                "flap_detection_enabled", "flap_detection_options", "flavor", "force", "freshness_threshold", "friday",
%                "fstype", "gid", "gpgcheck", "gpgkey", "group", "groups", "hasrestart", "hasstatus", "high_flap_threshold",
%                "home", "host_aliases", "hostgroup_members", "hostgroup_name", "hostgroups", "host_name",
%                "host_notification_commands", "host_notification_options", "host_notification_period",
%                "host_notifications_enabled", "hour", "http_caching", "ia_load_module", "icon_image", "icon_image_alt",
%                "id", "ignore", "incl", "includepkgs", "inherit", "inherits_parent", "initial_state", "install_args",
%                "install_options", "instance", "ip", "ipaddress", "ip_address", "iptype", "is_volatile", "keepalive",
%                "key", "key_membership", "keys", "k_of_n", "last_notification", "lens", "links", "load_path", "log",
%                "logbias", "logoutput", "low_flap_threshold", "mailserver", "managehome", "manifest", "matches",
%                "max_check_attempts", "mechanisms", "members", "membership", "message", "metadata_expire", "minute",
%                "mirror", "mirrorlist", "mode", "monday", "month", "monthday", "mountpoint", "mtime", "_naginator_name",
%                "name", "native_vlan", "nbmand", "normal_check_interval", "notes", "notes_url", "notification_failure_criteria",
%                "notification_interval", "notification_options", "notification_period", "notifications_enabled",
%                "obsess_over_host", "obsess_over_service", "onlyif", "options", "owner", "pager", "parallelize_check",
%                "parents", "pass", "passive_checks_enabled", "password", "password_max_age", "password_min_age", "path",
%                "pattern", "period", "periodmatch", "persistent", "platform", "pool", "port", "primarycache", "principals",
%                "priority", "process_perf_data", "profile_membership", "profiles", "project", "protect", "provider", "proxy",
%                "proxy_password", "proxy_username", "purge", "quota", "raid_parity", "raidz", "range", "readonly", "realhostname",
%                "realname", "recipient", "recordsize", "recurse", "recurselimit", "refquota", "refreservation", "refresh", "refreshonly",
%                "register", "remounts", "repeat", "replace", "require", "notify", "reservation", "responsefile", "restart", "retain_nonstatus_information",
%                "retain_status_information", "retry_check_interval", "retry_interval", "returns", "rmdirs", "role_membership", "roles",
%                "root", "rule", "saturday", "secondarycache", "selinux_ignore_defaults", "selmoduledir", "selmodulepath", "selrange",
%                "selrole", "seltype", "seluser", "server", "service_description", "servicegroup_members", "servicegroup_name",
%                "servicegroups", "service_notification_commands", "service_notification_options", "service_notification_period",
%                "service_notifications_enabled", "session_owner", "setuid", "shared", "shareiscsi", "sharenfs", "shares",
%                "sharesmb", "shell", "size", "snapdir", "source", "sourceselect", "spare", "special", "speed", "stalking_options",
%                "start", "status", "statusmap_image", "stop", "sunday", "syncversion", "sysidcfg", "system", "target", "thursday",
%                "timeout", "timeperiod_name", "tries", "trigger", "try_sleep", "tuesday", "type", "type_check", "uid", "unless",
%                "unless_system_user", "url", "use", "user", "value", "vendor", "version", "volsize", "vrml_image", "vscan",
%                "webserver", "wednesday", "weekday", "withpath", "working_dir", "xattr", "zoned");

append_keywords("puppet", 2,
                "alias", "audit", "before", "check", "consume", "export", "loglevel", "noop",
                "schedule", "stage", "subscribe", "tag");

append_keywords("puppet", 3,
                "command", "onlyif");

append_keywords("puppet", 4,
                "augeas", "computer", "cron", "exec", "file", "filebucket", "group", "host", "interface", "k5login",
                "macauthorization", "mailalias", "maillist", "mcx", "mount", "nagios_command", "nagios_contact",
                "nagios_contactgroup", "nagios_host", "nagios_hostdependency", "nagios_hostescalation",
                "nagios_hostextinfo", "nagios_hostgroup", "nagios_service", "nagios_servicedependency",
                "nagios_serviceescalation", "nagios_serviceextinfo", "nagios_servicegroup", "nagios_timeperiod",
                "notify", "class", "package", "resources", "router", "schedule", "scheduled_task", "selboolean", "selmodule",
                "service", "ssh_authorized_key", "sshkey", "stage", "tidy", "user", "vlan", "yumrepo", "zfs", "zone",
                "zpool");

append_keywords("puppet", 4,
                "alert", "assert_type", "contain", "create_resources", "crit", "debug", "defined", "digest",
                "each", "emerg", "epp", "err", "extlookup", "fail", "file", "filter", "fqdn_rand", "generate",
                "hiera", "hiera_array", "hiera_hash", "hiera_include", "include", "info", "inline_epp", "inline_template",
                "lookup", "map", "match", "md5", "notice", "realize", "reduce", "regsubst", "search", "scanf",
                "sha1", "shellquote", "slice", "split", "sprintf", "tag", "tagged", "template", "versioncmp", "warning",
                "with", "class", "package");

append_keywords("puppet", 5,
                "and", "case", "default", "else", "elsif", "if", "in", "or", "Exec");

append_keywords("puppet", 6,
                "application", "attr", "consumes", "environment", "function", "import", "private", "produces", "type");

append_keywords("puppet", 7,
                "absent", "define", "directory", "include", "inherits", "installed", "latest", "link", "node",
                "on_failure", "present", "running");

append_keywords("puppet", 0,
                "file_line", "abs", "any2array", "base64", "basename", "bool2num", "bool2str", "capitalize", "ceiling",
                "chomp", "chop", "clamp", "concat", "convert_base", "count", "defined_with_params", "delete", "delete_at",
                "delete_values", "delete_undef_values", "difference", "dirname", "dos2unix", "downcase", "empty", "ensure_packages",
                "ensure_resource", "flatten", "floor", "fqdn_rand_string", "fqdn_rotate", "get_module_path", "getparam", "getvar",
                "grep", "has_interface_with", "has_ip_address", "has_ip_network", "has_key", "hash", "intersection", "is_a",
                "is_absolute_path", "is_array", "is_bool", "is_domain_name", "is_float", "is_function_available", "is_hash",
                "is_integer", "is_ip_address", "is_mac_address", "is_numeric", "is_string", "join", "join_keys_to_values",
                "keys", "loadyaml", "load_module_metadata", "lstrip", "max", "member", "merge", "min", "num2bool", "parsejson",
                "parseyaml", "pick", "pick_default", "prefix", "assert_private", "pw_hash", "range", "reject", "reverse", "rstrip",
                "seeded_rand", "shuffle", "size", "sort", "squeeze", "str2bool", "str2saltedsha512", "strftime", "strip", "suffix",
                "swapcase", "time", "to_bytes", "try_get_value", "type3x", "type_of", "union", "unique", "unix2dos", "upcase",
                "uriescape", "validate_absolute_path", "validate_array", "validate_augeas", "validate_bool", "validate_cmd",
                "validate_hash", "validate_integer", "validate_ip_address", "validate_numeric", "validate_re", "validate_slength",
                "validate_string", "values", "values_at", "zip");

define puppet_mode ()
{
   variable puppet = "puppet";
   USE_TABS = 0;
   TAB = 2;
   set_mode (puppet, 2);
   use_syntax_table (puppet);
   set_comment_info(puppet, "//", "/*", "*/", 0);
   run_mode_hooks("puppet_mode_hook");
}
