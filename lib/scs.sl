% ******************************************************************************
% Source control functions
% ******************************************************************************

%******************************************************************************

static define scs_command(cmdline, currentfile)
{
        variable currentbuf, currentline, currentcol, currentflags, msg;

   (, , currentbuf, currentflags) = getbuf_info();

        flush("Executing '"+cmdline+"'");
        usleep(100);

   save_buffer();

        % We put the output into the scratch buffer

   sw2buf("*scratch*");
        erase_buffer ();

        insert(cmdline);
        newline();
        newline();

        % Run the command

        () = run_shell_cmd(cmdline);

        % Extract the response

        eob();
   go_up_1();
   bol();
        push_mark();
        eol();
        msg = bufsubstr();
        pop_mark(0);

        % Re-read the file

        sw2buf(currentbuf);

        flush("Reloading "+currentfile);

   currentline = what_line();
   currentcol  = what_column();

   set_buffer_modified_flag(0);
   delbuf(currentbuf);
   () = find_file(currentfile);

   goto_line(currentline);
   goto_column(currentcol);

        % Display the result message from mks

        flush(msg);
}

%******************************************************************************
%******************************************************************************

static define scs_checkout(currentfile)
{
         scs_command("cleartool co -nc " + currentfile, currentfile);
}

%******************************************************************************
%******************************************************************************

static define scs_checkin(currentfile)
{
         variable message;

         message = read_mini("Description: ", "", "");

         scs_command("cleartool ci -c '" + message + "' " + currentfile, currentfile);
}

%******************************************************************************
%******************************************************************************

static define scs_diff(currentfile)
{
        error("diff not implemented!");
}

%******************************************************************************
%******************************************************************************

static define scs_uncheckout(currentfile)
{
        scs_command("cleartool unco -keep "+currentfile, currentfile);
}

%******************************************************************************
%******************************************************************************

define source_control_menu()
{
        variable c, currentbuf, ccroot;
        variable menu, file, dir;

        % Save information relating to the current buffer

   (file, dir , currentbuf,  ) = getbuf_info();

   % Build the menu

   ccroot = getenv("CLEARCASE_ROOT");

   if(ccroot == NULL)
      ccroot = "";
   else
      ccroot = ccroot + " : ";

        menu = ccroot + "check-[O]ut, check-[I]n,  [D]iff,  [U]ncheckout" + " : " + currentbuf + " >";

        % Prompt the user

        !if (input_pending(1)) flush (menu);
   c = toupper (getkey());

   file = dir+file;

        % Whaddya wannado?

   switch (c)
        {       case 'O' : scs_checkout(file);                  }
        {  case 'I' : scs_checkin(file);                                }
        {  case 'D' : scs_diff(file);                                   }
        {  case 'U' : scs_uncheckout(file);       }
   {                      beep(); clear_message ();     }
}
