%==========================================================================
% Visual Basic (and other Basics) editing mode
% ----------------------------------------------------------------------------

define_word ("0-9A-Za-z_");

!if(keymap_p ("BAS"))
   make_keymap ("BAS");

% -------------------------------------------------------------------------
% Now create and initialize the syntax tables.
% -------------------------------------------------------------------------

create_syntax_table ("BAS");

define_syntax ("'", "", '%', "BAS");
% define_syntax("REM",             "",     '%', "BAS");
define_syntax ("(", ")", '(', "BAS");
define_syntax ('"', '"', "BAS");
define_syntax ("0-9a-zA-Z_", 'w', "BAS"); % words

define_syntax ("-+0-9.", '0', "BAS");  % Numbers

define_syntax (",;.?:_", ',', "BAS");
define_syntax ('#', '#', "BAS");
define_syntax ("%-+/&*=<>|!~^\\", '+', "BAS");

set_syntax_flags ("BAS", 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache ("basmode.dfa", name);

   dfa_define_highlight_rule ("^[ \t]*#", "PQpreprocess", name);
   dfa_define_highlight_rule ("%.*", "comment", name);
   dfa_define_highlight_rule ("/\\*.*\\*/", "Qcomment", name);
   dfa_define_highlight_rule ("^([^/]|/[^\\*])*\\*/", "Qcomment", name);
   dfa_define_highlight_rule ("/\\*.*", "comment", name);
   dfa_define_highlight_rule ("^[ \t]*\\*+([ \t].*)?$", "comment", name);
   dfa_define_highlight_rule ("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
   dfa_define_highlight_rule ("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?", "number", name);
   dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\"", "string", name);
   dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*'", "string", name);
   dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule ("[ \t]+", "normal", name);
   dfa_define_highlight_rule ("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
   dfa_define_highlight_rule ("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);

   dfa_build_highlight_table (name);
}
dfa_set_init_callback (&setup_dfa_callback, "BAS");
%%% DFA_CACHE_END %%%
#endif

% Type 1 keywords

() = define_keywords_n ("BAS", "", 1, 1);
() = define_keywords_n ("BAS", "FVPV", 2, 1);
() = define_keywords_n ("BAS", "AbsAscAtnChrCosDayDDBDirEOFExpFixHexIIfIntIRRLenLocLOFLogMidNowNPVOctPmtRGBRndSgnSinSLNSpcSqrStrSYDTabTanValXor", 3, 1);
() = define_keywords_n ("BAS", "AscBCCurCDblCIntCLngCSngCStrCVarChrBDateHourIPmtLeftMIRRMidBNPerPPmtRateSeekTimeTrimTrueYear", 4, 1);
() = define_keywords_n ("BAS", "ArrayCBoolCByteCDateCVErrErrorFalseInputInStrLCaseLTrimMonthRTrimRightShellSpaceTimerUCase", 5, 1);
() = define_keywords_n ("BAS", "ChooseCurDirFormatIsDateIsNullLBoundMinuteMsgBoxRandomSecondStringSwitchUBound", 6, 1);
() = define_keywords_n ("BAS", "CommandDateAddEnvironFileLenGetAttrIsArrayIsEmptyIsErrorQBColorStrCompStrConvVarTypeWeekday", 7, 1);
() = define_keywords_n ("BAS", "DateDiffDatePartDoEventsFileAttrFreeFileIsObjectTypeName", 8, 1);
() = define_keywords_n ("BAS", "DateValueGetObjectIMEStatusIsMissingIsNumericPartitionTimeValue", 9, 1);
() = define_keywords_n ("BAS", "DateSerialGetSettingTimeSerial", 10, 1);
() = define_keywords_n ("BAS", "LoadPictureLoadResData", 11, 1);
() = define_keywords_n ("BAS", "CreateObjectFileDateTimeOpenDatabaseReadProperty", 12, 1);
() = define_keywords_n ("BAS", "LoadResString", 13, 1);
() = define_keywords_n ("BAS", "CreateDatabaseGetAllSettingsLoadResPicture", 14, 1);

% Type 0 keywords

() = define_keywords_n ("BAS", "", 1, 0);
() = define_keywords_n ("BAS", "AsDoEBIfInOnOrTo", 2, 0);
() = define_keywords_n ("BAS", "AndDimEndForGetGetLetNotPutRemSetSub", 3, 0);
() = define_keywords_n ("BAS", "BeepByteCallCaseDateEachElseExitGoToKillLoadLongLoopLSetMemoNameNextOpenRSetSeekStopTextThenTimeTypeWendWith", 4, 0);
() = define_keywords_n ("BAS", "BeginByRefByValChDirCloseConstEraseErrorGoSubMkDirReDimResetRmDirWhileWidthWrite", 5, 0);
() = define_keywords_n ("BAS", "DefCurDefDblDefIntDefLngDefObjDefSngDefStrDefVarDoubleElseIfInput#ObjectOnGotoOptionPrint#PublicResumeReturnSelectSingleStaticStringUnloadUnlock", 6, 0);
() = define_keywords_n ("BAS", "BooleanChDriveDeclareDefBoolDefByteDefDateIntegerOnErrorOnGoSubPrivateSetAttrVERSIONVariant", 7, 0);
() = define_keywords_n ("BAS", "CurrencyFileCopyFunctionOptionalPropertyRollbackSendKeys", 8, 0);
() = define_keywords_n ("BAS", "AttributeFreeLocksRandomize", 9, 0);
() = define_keywords_n ("BAS", "BeginTransLineInput#OptionBaseSelectCase", 10, 0);
() = define_keywords_n ("BAS", "AppActivateCommitTransSavePictureSaveSetting", 11, 0);
() = define_keywords_n ("BAS", "DeleteSetting", 13, 0);
() = define_keywords_n ("BAS", "RepairDatabase", 14, 0);
() = define_keywords_n ("BAS", "CompactDatabase", 15, 0);
() = define_keywords_n ("BAS", "RegisterDatabase", 16, 0);
() = define_keywords_n ("BAS", "SetDataAccessOptionSetDefaultWorkspace", 19, 0);

variable BAS_TAB_STOPS = 4;

define space_tabs ()
{
   variable col = what_column ();
   variable tabstop;

   tabstop = (col / BAS_TAB_STOPS + 1) * BAS_TAB_STOPS;

   while(what_column () <= tabstop)
      insert_char (' ');
}

define bas_mode ()
{
   set_abbrev_mode (1);

   set_mode ("BAS", 2);
   use_keymap ("BAS");
   use_syntax_table ("BAS");
   set_comment_info("BAS", "REM ", NULL, NULL, 0);

   definekey ("space_tabs", "^I", "BAS");

   %      set_buffer_hook ("par_sep", "c_paragraph_sep");
   %      set_buffer_hook ("indent_hook", "c_indent_line");
   %      set_buffer_hook ("newline_indent_hook", "c_newline_and_indent");
   run_mode_hooks ("bas_mode_hook");
}

