%%
%%  utility functions which Must be available (e.g., autoload)
%%

% ******************************************************************************
% strip_control_codes() - Remove ANSI control code sequences from the current
% buffer.
% ******************************************************************************

define strip_control_codes()
{
   variable repcount=0;

   push_spot();

   ERROR_BLOCK
   {
      pop_spot();
   }

   bob();

   while(re_fsearch("\[.[0-9;()?]+[A-Za-z]") > 0)
   {
      if(replace_match("", 1) == 0)
      {
         error("Error replacing control code!");
      }

      repcount+=1;
   }

   ifnot (BATCH) flush(sprintf("Replaced %d code sequences", repcount));

   EXECUTE_ERROR_BLOCK;
}

%%
%%  A routine that trims the buffer by:  removing excess whitespace at the
%%  end of lines and removing excess newlines
%% TODO: Python mode should preseve pairs of blank % lines between functions
%%

define trim_buffer()
{
   % Save position and go to the top of the buffer

   push_spot();
   bob();

   % Trim trailing whitespace

   do
   {
      eol_trim();
   }
   while (down_1 ());

   % Merge adjacent blank lines

   bob();

   do
   {
      % If current and following line are blank, delete
      % the following line, otherwise move down and stop
      % when we reach the end of the buffer

      if(eolp() and bolp() and not eobp())
      {
         go_down_1();
         if(eolp() and bolp() and not eobp())
         {
            go_up_1();
            del();
         }
      }
      else
      {
         go_down_1();
         eol();
      }
   }
   while(not eobp());

   % Return from whence we came

   pop_spot();

   ifnot (BATCH) message ("done.");
}

%% Trim the buffer but also remove ANSI sequences

define clean_buffer()
{
   trim_buffer();
   strip_control_codes();
}

% ******************************************************************************
% Case-independent string comparison for the sort
% ******************************************************************************

public define strcmpi(a, b)
{
   return strcmp(strup(a), strup(b));
}

% ******************************************************************************
% Given a path to a file and a base directory, return path as a relative path
% relative to the base directory, if it is possible to do so.
% ******************************************************************************

public define get_relative_path(full_path, base_dir)
{
   variable full_dir = path_dirname(full_path);
   variable file_name = path_basename(full_path);
   variable full_list, base_list;

   % Strip trailing '/' from the paths

   if(full_dir[-1] == '/')
   {
      full_dir = full_dir[[:-2]];
   }

   if(base_dir[-1] == '/')
   {
      base_dir = base_dir[[:-2]];
   }

   % Split both paths into a list of directories

   full_list = strchop(full_dir, '/', 0);
   base_list = strchop(base_dir, '/', 0);

   % If the path is shorter than the base path, it can't be a
   % subdirectory

   if(length(full_list) < length(base_list))
   {
      return full_path;
   }

   % Check that the full path has the same root as the base path

   variable i;
   for(i=0; i<length(base_list); i++)
   {
      if(full_list[i] != base_list[i])
      {
         return full_path;
      }
   }

   if(length(full_list) == length(base_list))
   {
      return file_name;
   }
   else
   {
      return strjoin(full_list[[length(base_list):]], "/") + "/" + file_name;
   }
}

%

public define insert_wildcard()
{
   variable wildcard = cursor_text(0);
   variable files;

   if(wildcard == "")
   {
      error("No wildcard to expand");
   }

   % Get the list of matching files

   files = glob(wildcard);

   % If there are matches, delete the wildcard text and replace it
   % with the list of matches

   if(length(files) > 0)
   {
      () = cursor_text(1);

      foreach(files)
      {
         insert(());
         insert(" ");
      }
   }
   else
   {
      error("No matches for '"+wildcard+"'");
   }
}
