define_word ("0-9A-Za-z_#");

!if(keymap_p ("notes"))
        make_keymap ("notes");

% ******************************************************************************
% Insert the specified character and reformat the current paragraph, maintaining
% the cursor position. Assigned to various keys to provide semi-automatic
% reformatting.
% ******************************************************************************

define notes_format_paragraph(ch)
{
   insert(ch);

   if (WRAP > 0)
   {
      push_spot();

      eol();
      if(what_column() > WRAP)
      {
         format_paragraph_down();
      }
      pop_spot();
   }
}


% -------------------------------------------------------------------------
% Now create and initialize the syntax tables.
% -------------------------------------------------------------------------

create_syntax_table ("notes");

define_syntax ("~",             "",     '%', "notes");
define_syntax ("^",             "^",    '%', "notes");
define_syntax ("({[",           "]})",  '(', "notes");
define_syntax ('"',                     '"', "notes");
define_syntax ("0-9a-zA-Z_#*",           'w', "notes"); % words
define_syntax ("-+x0-9A-Fa-f.",                '0', "notes");   % Numbers
define_syntax (",;.?:_",                ',', "notes");
define_syntax ('#',                     '#', "notes");
define_syntax ("%-+/&*#=<>|!~^\\",       '+', "notes");

set_syntax_flags ("notes", 4);

definekey ("notes_format_paragraph(\" \")", " ", "notes");
definekey ("notes_format_paragraph(\",\")", ",", "notes");
definekey ("notes_format_paragraph(\".\")", ".", "notes");
definekey ("notes_format_paragraph(\":\")", ":", "notes");
definekey ("notes_format_paragraph(\";\")", ";", "notes");
definekey ("notes_format_paragraph(\")\")", ")", "notes");

append_keywords("notes", 1, "#define");
append_keywords("notes", 2, "**##**");
append_keywords("notes", 3, "*");
append_keywords("notes", 2, "JMS");

define notes_mode ()
{
        set_abbrev_mode (1);

        set_mode ("notes", 2);
        use_keymap ("notes");
        use_syntax_table ("notes");

        run_mode_hooks ("notes_mode_hook");
}

provide("notesmode");
