%==========================================================================
% SQL editing mode
% ----------------------------------------------------------------------------

define_word ("0-9A-Za-z_");

!if(keymap_p ("SQL"))
        make_keymap ("SQL");

% -------------------------------------------------------------------------
% Now create and initialize the syntax tables.
% -------------------------------------------------------------------------

create_syntax_table ("SQL");

define_syntax("/*", "*/", '%', "SQL");
define_syntax ("--", "", '%', "SQL");
define_syntax ("(", ")", '(', "SQL");
define_syntax ('`', '"', "SQL");
define_syntax ('\'', '"', "SQL");
define_syntax ("0-9a-zA-Z_", 'w', "SQL");       % words

define_syntax ("-+0-9.", '0', "SQL");   % Numbers

define_syntax (",;.?:_", ',', "SQL");
define_syntax ("#", "", '%', "SQL");
define_syntax ("%-+/&*=<>|!~^\\", '+', "SQL");
define_syntax('\\', '\\', "SQL");

set_syntax_flags ("SQL", 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
        dfa_enable_highlight_cache ("sqlmode.dfa", name);

        dfa_define_highlight_rule ("^[ \t]*#", "PQpreprocess", name);
        dfa_define_highlight_rule ("%.*", "comment", name);
        dfa_define_highlight_rule ("/\\*.*\\*/", "Qcomment", name);
        dfa_define_highlight_rule ("^([^/]|/[^\\*])*\\*/", "Qcomment", name);
        dfa_define_highlight_rule ("/\\*.*", "comment", name);
        dfa_define_highlight_rule ("^[ \t]*\\*+([ \t].*)?$", "comment", name);
        dfa_define_highlight_rule ("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
        dfa_define_highlight_rule ("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?", "number", name);
        dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\"", "string", name);
        dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*'", "string", name);
        dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule ("[ \t]+", "normal", name);
        dfa_define_highlight_rule ("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
        dfa_define_highlight_rule ("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);

        dfa_build_highlight_table (name);
}
dfa_set_init_callback (&setup_dfa_callback, "SQL");
%%% DFA_CACHE_END %%%
#endif

append_keywords("SQL", 0, "DROP", "CREATE", "LOCK", "INSERT", "UNLOCK", "ADD", "CONSTRAINT", "ALTER", "COLUMN", "BACKUP", "DATABASE", "DELETE", "REPLACE",
                          "SELECT", "EXEC", "TRUNCATE", "UNION");
append_keywords("SQL", 1, "IF", "EXISTS", "DEFAULT", "CHARACTER", "SET", "ON", "UPDATE", "COLLATE", "WHERE", "THEN", "ELSE", "END", "FROM", "LIMIT");
append_keywords("SQL", 2, "TABLE", "TABLES", "WRITE", "INTO", "VALUES", "PRIMARY", "KEY", "ENGINE", "CHARSET", "AUTO_INCREMENT", "UNIQUE",
                          "VIEW", "FOREIGN", "FULL", "OUTER", "JOIN", "GROUP", "BY", "HAVING", "INNER", "LEFT","ORDER", "RIGHT", "ROWNUM", "TOP",
                          "BETWEEN", "CASE", "CHECK", "DATABASE", "INDEX", "TABLE", "PROCEDURE", "DESC", "DISTINCT", "WHEN", "AS");
append_keywords("SQL", 3, "NULL", "TIMESTAMP", "CURRENT_TIMESTAMP", "ALL", "AND", "WHY", "ASC", "OR", "IS", "LIKE", "IN", "NOT");
append_keywords("SQL", 4, "int", "varchar", "char", "unsigned", "bigint", "text", "timestamp");
append_keywords("SQL", 5, "latin1", "latin1_bin", "MyISAM");
append_keywords("SQL", 6, "");
define sql_mode ()
{
        set_abbrev_mode (1);

        set_mode ("SQL", 2);
        use_keymap ("SQL");
        use_syntax_table ("SQL");
   set_comment_info("SQL", "--", "/*", "*/", 0);

        run_mode_hooks ("sql_mode_hook");
}
