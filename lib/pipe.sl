variable Last_Process_Command = Null_String;

% ******************************************************************************
% Pipe the current region, or the whole buffer, through the specified
% command, back into the buffer/region from whence it came.

define pipe_region_through(cmd)
{
   variable end_mark, start_mark, pos, tmpfile, tmpfile_out, tmpfile_err, result;

   if(is_readonly())
   {
      error("Buffer is read-only");
   }

   ifnot(strlen(cmd))
   {
      cmd = read_mini ("Pipe to command:", Last_Process_Command, Null_String);
   }

   ifnot (strlen (cmd))
      return;

   % Writing the region to a file seems to unmark the region
   % so we save the start/end/current pos and recreate the
   % region when we've done.

   if (markp())
   {
      end_mark = create_user_mark();
      pos = end_mark;
      pop_mark(1);
      start_mark = create_user_mark();
      push_mark();
      goto_user_mark(end_mark);
   }
   else
   {
      pos = create_user_mark();
      bob();
      start_mark = create_user_mark();
      push_mark();

      eob();
      end_mark = create_user_mark();
   }

   % Write the buffer out to a temporary file, crete temporary files
   % for stdout and stderr as well.

   tmpfile = make_tmp_file("/tmp/jed_json");
   tmpfile_out = make_tmp_file("/tmp/jed_json_out");
   tmpfile_err = make_tmp_file("/tmp/jed_json_err");

   () = write_region_to_file(tmpfile);

   % Run the command redirecting output

   variable shell_cmd = cmd + " " + tmpfile + " > " + tmpfile_out + " 2> " + tmpfile_err;

   flush("Running: " + cmd);
   result = system(shell_cmd);
   flush("");

   % On success, read tmpfile_out back into the buffer
   % On failure get the first line of stderr and output as an error

   if(result == 0)
   {
      % Recreate the region, delete it, insert the corrected file

      goto_user_mark(start_mark);
      push_mark();
      goto_user_mark(end_mark);

      del_region();
      () = insert_file(tmpfile_out);

      goto_user_mark(pos);
   }
   else
   {
      variable msg;

      setbuf(COMPILE_BUFFER);
      erase_buffer();
      insert_file(tmpfile_err);

      bob();
      push_mark();
      eol();
      msg = bufsubstr();
      pop_mark();

      error(msg);
   }

   % Tidy up on exit

   EXIT_BLOCK {
      () = delete_file(tmpfile);
      () = delete_file(tmpfile_out);
      () = delete_file(tmpfile_err);
   }

   pop_spot();
}
