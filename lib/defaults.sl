%==========================================================================
% Jed extension functions
%==========================================================================

_debug_info = 1;
_traceback = 1;

require("keydefs");
%require("bufhist");
require("keystrokes");
require("pipe");
require("loadfile");

Help_File = "emacs.hlp";

KILL_LINE_FEATURE = 0;
IGNORE_BEEP=1;

#ifdef UNIX
   enable_flow_control(0);  %turns off ^S/^Q processing (Unix only)
#endif

%==========================================================================
% Log parsing stuff
%==========================================================================

variable PARSE_GREP    = 0;
variable PARSE_COMPILE = 1;
variable PARSE_TAG     = 2;
variable PARSE_CSCOPE  = 3;

variable last_parse_dir   = 0;

variable parse_type       = PARSE_COMPILE;

variable grep_pattern     = "";              % The pattern to search for
variable grep_wildcard    = "";
variable grep_fixed_string = 0;

%============================================================================
% Create the bookmark array
%============================================================================

!if (is_defined("Book_Marks"))
{
   % user marks are of type 128

   variable Book_Marks = create_array (128, 10, 1);
}

% ******************************************************************************
% Crude hacks - "|"-separated list of directory/file regexps where we ignore
% build errors, grep results and tags and a similar one for benign error
% messages.
% ******************************************************************************

variable parse_ignore_file_list = "tags";

variable parse_ignore_error_list = "Possible error: >= 10 lines of macro arguments|Static 'DBUG_CURRENT' declared but not used";

% ******************************************************************************
% By the time we get to the exit hook we seem always to be in the scratch
% buffer, so we save the last valid current buffer here before we exit so we
% can write it to the state file
% ******************************************************************************

variable jed_last_current_buffer = "";

% ******************************************************************************
% Support routines in other modules
% ******************************************************************************

% The 8086 version of jed does not suppot yank-pop whereas jed386 does.  So,
% I do it this way so both executables can execute this file.

if (is_defined ("KILL_ARRAY_SIZE"))
   () = evalfile ("yankpop");
else
   () = evalfile ("yank16");

autoload("compile_select_compiler",    "compile");
autoload("compile_set_callback",       "compile");
autoload("compile_parse_ignore_dirs",  "compile");
autoload("compile_parse_ignore_errs",  "compile");
autoload("compile_reset_parser",       "compile");

autoload("hlp_mode",                   "hlpmode");
autoload("occur_mode",                 "occurmode");

autoload("c_mark_function",            "cmisc");
autoload("c_insert_comment_block",     "cmisc");

autoload("search_last_match",          "search");
autoload("search_command",             "search");
autoload("search_command_2",           "search");
autoload("replace_command",            "search");
autoload("search_set_defaults",        "search");
autoload("search_get_defaults",        "search");
autoload("search_set_parameters",      "search");
autoload("search_set_text",            "search");
autoload("search_get_text",            "search");
autoload("search_menu",                "search");

autoload("re_search_again",            "regexp");

autoload("search_again",               "search");

autoload("search_forward_global",      "search");

autoload("search_generic_search",      "search");

autoload("find_tag_text",              "ctags");

autoload("cscope",                     "cscope");
autoload("cscope_parse",               "cscope");

autoload("grep",                       "grep");
autoload("grep_parse",                 "grep");

autoload("strip_control_codes",        "util");

autoload("test_function",              "testfunction");

% ******************************************************************************
% Insert or overwrite text depending on the current mode (insert or
% overwrite).
% ******************************************************************************

define ins_ovr_text(text)
{
   if(is_overwrite_mode())
   {
      variable i;

      for(i=0; i<strlen(text); i++)
         del();

      insert(text);
   }
   else
   {
      insert(text);
   }
}

% ******************************************************************************
% Return the current line in the specified buffer
% ******************************************************************************

define what_line_buf(bufname)
{
   variable line, buf;

   buf = whatbuf();
   setbuf(bufname);
   line = what_line();
   setbuf(buf);
   return line;
}

%==========================================================================
% Emergency exit function - for use when things go horribly wrong!
%==========================================================================

define coredump()
{
   if (get_yes_no("Core dump - are you sure (no files will be saved)") > 0)
   {
      core_dump("Emergency exit from Jed!", 1);
   }
}

setkey("coredump",              "^X^Q");

%******************************************************************************
% Saved blocks of text (a bit like 10 simple clipboards).
%******************************************************************************

variable saved_region = String_Type[10];

%==========================================================================
% Defaults that were previously (and wrongly) set in site.sl
%==========================================================================

REPLACE_PRESERVE_CASE  = 1;

SAVE_STATE             = 1;

%---------------------------------------------------------------------------
% Run 'filename' as a Jed file, if it exists and do nothing if it doesn't
% Return the 1 on success, 0 on file not found

define runhookfile (filename)
{
   variable fullname;

   fullname = expand_jedlib_file (filename);

   if( strlen (fullname) > 0)
   {
      if(evalfile (fullname))
      {
         message(Sprintf("Running: %s", fullname, 1));
         return (1);
       }
   }

   return (0);
}

%============================================================================
% Mark the entire buffer
%============================================================================

define emacs_mark_buffer()
{
   mark_buffer ();
   exchange_point_and_mark ();
}

%============================================================================
% Split a line at the cursor position
%============================================================================

define emacs_open_line()
{
   newline();
   go_left_1 ();
}

%============================================================================
% Center the current line on the screen
%============================================================================

define emacs_recenter()
{
   recenter(window_info('r')/2);
%  update_sans_update_hook(1);
}

% ******************************************************************************
% If the current line is in the top or bottom section of the screen,
% recenter a bit up or down to stop it being too close to the top or the
% bottom.

define roughly_recenter()
{
   variable rows, pos;

   rows = window_info('r');
   pos = window_line();

   if(pos < rows/5)
   {
      recenter(rows/5);
   }
   else if(pos > 4*rows/5)
   {
      recenter(4*rows/5);
   }
}

%******************************************************************************
%******************************************************************************

define insert_date()
{
   variable now;

   now = time();

   insert(substr(now, 9, 2));
   insert("-");
   insert(substr(now, 5, 3));
   insert("-");
   insert(substr(now, 21, 4));
}

%============================================================================
% Swap the current and following characters
%============================================================================

define transpose_chars ()
{
   variable c, err;
   err = "Top of Buffer";

   if (eolp())
      go_left_1 ();

   !if (left(1))
      error(err);

   c = what_char();
   del();
   go_right_1 ();
   insert_char(c);
}

%==========================================================================
% 0 9 1 { "^U" exch string strcat "digit_arg" exch setkey } _for
%
%  Emacs Universal argument--- bound to ^U
%
%==========================================================================

define universal_argument ()
{
   variable n, key, count, msg, cu, force;

   n = 4;
   count = 0;
   cu = "C-u";
   msg = cu;
   force = 0;

   forever
   {
      !if (force)
         !if(input_pending(10))
            force = 1;

      if (force)
      {
         message(strcat (msg, "-"));
         update(0);
      }

      msg = strcat(msg, " ");
      key = getkey();

      switch(key)
      {
      isdigit(char(())) :

         key = key - '0';
         count = 10 * count + key;
         msg = strcat(msg, string(key));
      }
      {
      case 21 :              %  ^U

         !if (count) n = 4 * n;
         count = 0;
         msg = strcat (msg, cu);
      }
      {
         ungetkey(());

         !if (count)
            count = n;

         count = string(count);
         n = strlen(count);

         _for (n, 1, -1)
         {
            count; exch();
            ungetkey(int (substr((), (), 1)));
         }

         ungetkey(27);
         return;
      }
   }
}

%******************************************************************************

static variable last_buf = "";

define toggle_buffer(buf)
{
   variable currbuf;

   (, , currbuf, ) = getbuf_info();

   if(currbuf == buf)
   {
      if(last_buf == "")
      {
         error("You already ARE in the "+buf+" window");
      }

      sw2buf(last_buf);
   }
   else
   {
      ( , , last_buf, ) = getbuf_info();

      sw2buf(buf);
   }
}

% ******************************************************************************
% Switch to the traceback buffer

define show_traceback()
{
   toggle_buffer(TRACE_BUFFER);
}

%******************************************************************************
% switch_to_output_buffer()
%
% Switch to the shell output buffer for the last command/grep/tag/cscope
% command
%******************************************************************************

define switch_to_output_buffer()
{
   switch(parse_type)
   { case PARSE_COMPILE: toggle_buffer(COMPILE_BUFFER); }
   { case PARSE_CSCOPE : toggle_buffer(GREP_BUFFER);    }
   { case PARSE_TAG    : toggle_buffer(TAGS_BUFFER);    }
   { case PARSE_GREP   : toggle_buffer(GREP_BUFFER);    }
}

% ******************************************************************************

define switch_to_debug_buffer()
{
   toggle_buffer(DEBUG_BUFFER);
}

% ******************************************************************************

define backspace()
{
   call("backward_delete_char");
}

%==========================================================================
% Cut to the clipboard
%==========================================================================

define x_cut_region_to_cutbuffer()
{
#ifdef XWINDOWS MSWINDOWS
   x_copy_region_to_cutbuffer();

   del_region();
#else
   error("x_copy_region_to_cutbuffer not supported on this platform");
#endif
}

% ******************************************************************************
% Get the name of the previous buffer to be accessed
% ******************************************************************************

define get_previous_buffer()
{
   variable n, buf, pbuf;

   n = buffer_list ();             % Get list of buffers
   pbuf = "";

   loop (n - 1)                    % Go to penultimate entry
   {
      buf = ();                   % Get name

      if( (buf[0] == ' ')         % Ignore unnamed or internal buffers
       or (buf[0] == '*') )
         continue;

      pbuf = buf;                 % save name
   }

   pop();                          % remove this buffer

   pbuf;
}

% ******************************************************************************

static define _jms_next_buffer()
{
   variable buf, n;

   n = buffer_list();

   loop(n)
   {
      buf = ();
      n--;

      if(buf[0] == ' ' or buf[0] == '*')
         continue;

      sw2buf(buf);
      _pop_n(n);
      return;
   }
}

%==========================================================================

define jms_next_buffer ()
{
   variable cbuf;

   cbuf = whatbuf();

   _jms_next_buffer();

   if(cbuf == whatbuf())
   {
      error("No other buffers");
   }
}

%==========================================================================

define jms_prev_buffer ()
{
   variable pbuf;

   pbuf = get_previous_buffer();

   if(strcmp(pbuf, "") == 0)
   {
      error("No other buffers");
   }
   else
   {
      sw2buf(pbuf);
   }
}

%==========================================================================
% Kill current buffer

define jms_kill_buffer ()
{
   variable n, buf, cbuf;

   delbuf(whatbuf());   % Zap the current buffer

   % kill_buffer doesn't do a sw2buf()

   cbuf = whatbuf();
   sw2buf(cbuf);

   % If we are left in an internal buffer, try and find the
   % next user buffer

   if( (cbuf[0] == ' ')
    or (cbuf[0] == '*') )
   {
      _jms_next_buffer();
   }
}

% ******************************************************************************
% The move_cursor_xxx() functions try and keep the cursor in the same
% column when moving up and down a buffer
% ******************************************************************************

% ******************************************************************************
% Called from line and page up/down functions to move the cursor by a
% given number of lines up (distance < 0) or down (distance > 0).
%
% If the previous command run from the keyboard was was one of the
% up/down move commands then try and keep the cursor in the same
% column

variable last_col = -1;

define move_cursor(distance)
{
   variable col, last_cmd;

   % Check the previous keystroke

   last_cmd = string(get_keystroke(1));

   if(last_col >= 0 &&
      (last_cmd == "move_cursor_page_down" or last_cmd == "move_cursor_page_up" or
       last_cmd == "move_cursor_down_1" or last_cmd == "move_cursor_up_1"))
   {
      col = last_col;
   }
   else
   {
      last_col = what_column();
   }

   % Move up or down

   if(distance > 0)
   {
      loop(distance)
         go_down_1();
   }
   else
   {
      loop(-distance)
         go_up_1();
   }

   % Try to keep to the correct column

   col = goto_column_best_try(last_col);

   % redraw() seems to be needed to ensure that syntax highlighting is
   % consistent - update(1) is not sufficient. Unfortunately it can
   % cause the display to flicker.

   %call("redraw");
   update(1);
}

%==========================================================================
% Move up one line unless we're already at the top
%==========================================================================

define move_cursor_up_1()
{
   move_cursor(-1);
}

%==========================================================================
% Move up down line unless we're already at the bottom
%==========================================================================

define move_cursor_down_1()
{
   move_cursor(1);
}

%==========================================================================
% [JMS] Page up and centre current line (avoids eye-bending scrolling)
%==========================================================================

define move_cursor_page_up()
{
   variable rows, pos;

   rows = window_info('r');
   pos = window_line();

   if(rows >= what_line())
      rows = what_line() - 1;

   move_cursor(-rows);

   if(rows > 0)
      recenter(pos);
}

%==========================================================================
% [JMS] Page down and centre current line (avoids eye-bending scrolling)
%==========================================================================

define move_cursor_page_down()
{
   variable rows, max_row, pos;

   push_spot();
   eob();
   max_row = what_line();
   pop_spot();

   rows = window_info('r');
   pos = window_line();

   if( (what_line() + rows) > max_row)
      rows = max_row - what_line();

   move_cursor(rows);

   if(rows > 0)
      recenter(pos);
}

%==========================================================================
% Save buffers and exit

define save_and_exit()
{
   jed_last_current_buffer = whatbuf();

   save_buffers ();
   exit_jed ();
}

%==========================================================================

define jms_skip_word ()
{
   skip_word ();
   skip_word ();
   bskip_word ();
}

% ******************************************************************************
% Return TRUE if the character under the cursor is a word character, FALSE
% if it not.
% ******************************************************************************

define is_word_char ()
{
   variable c;

   push_spot ();

   c = what_column ();
   skip_word_chars ();
   c = (what_column () != c);

   pop_spot ();

   return c;
}

%==========================================================================
% Extract the word under the cursor and return it as a string
%==========================================================================

define cursor_word ()
{
   variable edge = 0, cursword;

   push_spot();

   if(is_word_char())
   {
      while(is_word_char())
      {
         if(left(1) != 1)
         {
            edge=1;
            break;
         }
      }

      if(edge==0)
      {
         go_right_1();
      }
   }

   push_mark();

   while(is_word_char())
   {
      if(right(1) != 1)
      {
         edge = 1;
         break;
      }
   }

   cursword = bufsubstr();

   % Sticking a long input string into the minibuffer can
   % kill Jed with a GPF (!)

   if(strlen(cursword) > 1024)
   {
      cursword = "";
   }

   pop_spot();
   return cursword;
}

%==========================================================================
% Extract the whitespace-delimited text under the cursor and return it
% as a string, optionally deleting it from under the cursor
%==========================================================================

define cursor_text (delete)
{
   variable cursword;

   push_spot();

   if(looking_at_char(' ') or looking_at_char('\t'))
      bskip_white();

   bskip_chars("^ \t\n");
   push_mark();
   skip_chars("^ \t\n");

   if(delete)
      cursword = bufsubstr_delete();
   else
      cursword = bufsubstr();

   pop_spot();

   return cursword;
}

%******************************************************************************

define fixup_lint_output()
{
   % Rejoin any lines that were split at column 80

   setbuf("*shell-output*");
   bob();

   do
   {
      % Go to the start of the line

      bol();

      % If the current line hasn't been indented and the following one has

      !if(looking_at(" "))
      {
         if(down(1))
         {
            if(looking_at(" "))
            {
               % and the end-character of the line isn't "_"

               eol();
               if(left(1))
               {
                  !if(looking_at("_"))
                  {
                     % Go back to the current line, delete the EOL, remove
                     % the indentation and insert a space

                     () = up(1);
                     eol();
                     call("delete_char_cmd");
                     trim();
                     insert(" ");
                  }
               }
            }
         }
      }
   }
   while(down(1));

   % Go back to the top of the buffer
   bob();
}

%==========================================================================
% Find the current cscope/grep/compile error
%==========================================================================

variable lint_fixed = 1;

define parse_current_output()
{
   variable i, j, dir;

   if(lint_fixed == 0)
   {
      fixup_lint_output();
      lint_fixed = 1;
   }

   % If we parsed upwards last time, we need to step forwards again

   if(last_parse_dir == 1)
      dir = -1;
   else
      dir=0;

   switch(parse_type)
   { case PARSE_COMPILE : compile_parse_errors(0, dir);        }
   { case PARSE_GREP    : grep_parse (dir);                           }
   { case PARSE_CSCOPE  : cscope_parse(dir);                          }
   { error("Undefined parse type in parse_current_output()");             }

   last_parse_dir = -1;

   emacs_recenter();
}

%==========================================================================
% Find the previous cscope/grep/compile error
%==========================================================================

define _jms_parse_prev (err_flag)
{
   variable i, j, dir;

   if(lint_fixed == 0)
   {
      fixup_lint_output();
      lint_fixed = 1;
   }

   % If we parsed downwards last time, parsing backwards will find the
   % current error first, so we need to step back twice.

   if(last_parse_dir == 1)
   {
      i=2;
   }
   else
   {
      i=1;
   }

   for(j=0; j<i; j++)
   {
      switch(parse_type)
      { case PARSE_COMPILE : compile_parse_errors(err_flag, -1);        }
      { case PARSE_GREP    : grep_parse (-1);                           }
      { case PARSE_CSCOPE  : cscope_parse(-1);                          }
      { error("Undefined parse type in _jms_parse_prev()");             }
   }

   last_parse_dir = -1;

   emacs_recenter();
}

%==========================================================================
% Find the next cscope/grep/compile error.
%==========================================================================

define _jms_parse_next (err_flag)
{
   variable i, j, dir;

   if(lint_fixed == 0)
   {
      fixup_lint_output();
      lint_fixed = 1;
   }

   % If we parsed upwards last time, parsing downwards will find the
   % current error first, so we need to step on twice.

   if(last_parse_dir == -1)
   {
      i=2;
   }
   else
   {
      i=1;
   }

   for(j=0; j<i; j++)
   {
      switch(parse_type)
      { case PARSE_COMPILE : compile_parse_errors (err_flag, 1);        }
      { case PARSE_GREP    : grep_parse (1);                            }
      { case PARSE_CSCOPE  : cscope_parse(1);                           }
      { error("Undefined parse_type in _jms_parse_next()"); }
   }

   last_parse_dir = 1;

   emacs_recenter();
}

% ******************************************************************************
% Set the type of parse to use with jms_parse_next/prev()
% Useful if you grep after compiling and want to go back to the
% compiler output
% ******************************************************************************

define parse_output_menu()
{
   variable current_parse, c, done=0;

   while(done == 0)
   {
      switch(parse_type)
      { case PARSE_CSCOPE  : current_parse = "cscope";      }
      { case PARSE_GREP    : current_parse = "grep";        }
      { case PARSE_COMPILE : current_parse = "compilation"; }

      flush("Currently parsing " + current_parse + " output: Select c[S]cope, [G]rep or [C]ompilation output (press Q to leave unchanged) -->");

      c = toupper (getkey());

      switch (c)
      { case 'S' : parse_type = PARSE_CSCOPE;  }
      { case 'G' : parse_type = PARSE_GREP;    }
      { case 'C' : parse_type = PARSE_COMPILE; }
      { case 'Q' : done=1; }
      { case '\r': done=1; }
   }

   clear_message();
}

% ******************************************************************************
% Parse the next entry in the compile/grep/cscope log
% ******************************************************************************

define parse_next_output()
{
   _jms_parse_next(0);
}

% ******************************************************************************
% Parse the next _significant_ entry in the compile/grep/cscope log
% ******************************************************************************

define parse_next_error()
{
   _jms_parse_next(1);
}

% ******************************************************************************
% Parse the next previous in the compile/grep/cscope log
% ******************************************************************************

define parse_previous_output()
{
   _jms_parse_prev(0);
}

% ******************************************************************************
% Parse the previous _significant_ entry in the compile/grep/cscope log
% ******************************************************************************

define parse_previous_error()
{
   _jms_parse_prev(1);
}

%==========================================================================
% Select the compiler type
%==========================================================================

variable current_compiler_name="GNU", current_compiler="gcc";

define select_compiler_menu()
{
   variable done = 0;

   while(not done)
   {
      variable c;

      flush("{{3}}"+current_compiler_name+
            ": {{2}}B{{-}}orland, {{2}}A{{-}}rm Ultri{{2}}x{{-}}, {{2}}H{{-}}P, S{{2}}u{{-}}n, A{{2}}I{{-}}X, {{2}}G{{-}}NU, {{2}}W{{-}}atcon,"+
            " {{2}}J{{-}}ava, {{2}}V{{-}}C++, {{2}}S{{-}}T, {{2}}T{{-}}urbo C, {{2}}L{{-}}int, L{{2}}C{{-}}lint, {{2}}P{{-}}ylint, {{2}}R{{-}}uff");

      c = toupper(getkey());

      done = 1;

      switch(c)
      { case 'A' : current_compiler_name = "ARM";     current_compiler = "arm";        }
      { case 'B' : current_compiler_name = "Borland"; current_compiler = "bcc";        }
      { case 'C' : current_compiler_name = "LCLint";  current_compiler = "lclint";     }
      { case 'G' : current_compiler_name = "GNU";     current_compiler = "gcc";        }
      { case 'H' : current_compiler_name = "HP";      current_compiler = "hp_cc";      }
      { case 'I' : current_compiler_name = "AIX";     current_compiler = "aix";        }
      { case 'J' : current_compiler_name = "Java";    current_compiler = "javac";      }
      { case 'L' : current_compiler_name = "Lint";    current_compiler = "lint";       }
      { case 'P' : current_compiler_name = "Pylint";  current_compiler = "pylint";     }
      { case 'R' : current_compiler_name = "Ruff";    current_compiler = "ruff";     }
      { case 'S' : current_compiler_name = "ST20";    current_compiler = "icc_cc";     }
      { case 'T' : current_compiler_name = "Turbo C"; current_compiler = "tcc";        }
      { case 'U' : current_compiler_name = "Sun";     current_compiler = "sun_acc";    }
      { case 'V' : current_compiler_name = "Visual C";current_compiler = "vc";         }
      { case 'W' : current_compiler_name = "Watcom";  current_compiler = "wcc";        }
      { case 'X' : current_compiler_name = "Ultrix";  current_compiler = "ultrix_cc";  }
      { case '\e': }
      {            done = 0; beep();                                                   }
   }

   message("Now using "+current_compiler_name+" as current compiler");
}

%==========================================================================
%==========================================================================

variable lint_root_dir = "";

define load_lint_file()
{
   if(lint_root_dir == "")
   {
      error("Lint has not been run yet!");
   }

   setbuf(COMPILE_BUFFER);
   insert_file(lint_root_dir + "lint.txt");
}

% ******************************************************************************
% ******************************************************************************

define jms_lint ()
{
   variable dir;

   parse_type = PARSE_COMPILE;
   last_parse_dir = 0;
   lint_fixed = 0;

%   compile_set_callback(&load_lint_file);

   (, dir, , ) = getbuf_info();

   lint_root_dir = find_up_dir("makefile.mak");

   chdir(lint_root_dir);

   error("Running lint in "+lint_root_dir);
   compile("make "+current_compiler);
}

%==========================================================================
% Set the 'next' type to 'compile' and call compile
%==========================================================================

define jms_compile ()
{
   parse_type = PARSE_COMPILE;
   last_parse_dir = 0;

   save_buffers();

   compile_select_compiler(current_compiler);

   compile_parse_ignore_dirs(parse_ignore_file_list);
   compile_parse_ignore_errs(parse_ignore_error_list);

   if(current_compiler == "lint" or current_compiler == "lclint")
   {
      variable src_dir;

      (,src_dir,,) = getbuf_info ();

      compile ("lnt "+src_dir+DIR_STR+"*.c");
   }
   else if(current_compiler == "arm")
   {
      compile ("domake");
   }
   else if(current_compiler == "icc_cc")
   {
      compile ("pmu");
   }
   else if(current_compiler == "bcc")
   {
      compile("m");
   }
   else if(current_compiler == "pylint")
   {
      compile("pylint --output-format parseable "+buffer_filename());
   }
   else if (current_compiler == "ruff")
   {
      compile("ruff check "+buffer_filename());
   }
   else
   {
      compile ("make");
   }
}

% ******************************************************************************
% ******************************************************************************

variable build_log_file = "";

define jms_load_build_log()
{
   last_parse_dir = 0;

   compile_select_compiler(current_compiler);
   compile_parse_ignore_dirs(parse_ignore_file_list);
   compile_parse_ignore_errs(parse_ignore_error_list);

   setbuf(COMPILE_BUFFER);
   build_log_file = read_mini("Build log file: ", build_log_file, "");
   insert_file("");

   parse_type = PARSE_COMPILE;
   compile_reset_parser();
}

%==========================================================================
% Run an external program in the foreground
%==========================================================================

variable run_command = "/bin/sh";

define jms_run ()
{
   run_command = read_mini("Command line: ", run_command, "");
   system(run_command);
   pop ();
}

%==========================================================================
% jms_c_help
%==========================================================================

define jms_c_help ()
{
#ifdef MSWINDOWS
   variable keyword;

   keyword = cursor_word();

   message("Finding help for "+keyword);

   () = run_shell_cmd("start winhlp32 -k" + keyword + " c:\\bc5\\help\\bcpp.hlp");

#else

   variable keyword, buf, msg, htmlfile;
   variable Lynx_Pid, found = 0, tmp_file, cmd;

   % Get the current word under the cursor, pass it to 'man', filter
   % control codes from the output using 'col', redirect it into a
   % temporary file, read the file into a buffer, delete the file and
   % view the buffer in 'most' mode. Phew!

   buf = whatbuf();

   keyword = cursor_word();

   message("Finding help for "+keyword);

   tmp_file = make_tmp_file ("/tmp/jedrlog");
   cmd = sprintf ("man %s 2>&1 | col -b > %s", keyword, tmp_file);

   if (0 != system (cmd))
     verror ("Error running '"+cmd+"' command!");

   call ("one_window");
   splitwindow();
   sw2buf ("*help*");
   set_readonly(0);
   erase_buffer ();
   insert_file (tmp_file);
   bob();
   set_readonly(1);
   delete_file (tmp_file);
   most_mode ();

#endif
}

%==========================================================================
% Void show_help_popup();
% Pop up a window containing a help file.  The help file that is read
% in is given by the variable help_file
%==========================================================================

private variable help_page = 1;

private variable help_pages =
[
  "cursor.hlp", "function1.hlp", "function2.hlp", "alt.hlp", "control.hlp",
  "metakey1.hlp", "metakey2.hlp", "metakey3.hlp", "metakey4.hlp", "prefix.hlp",
  "prefix2.hlp", "prefix3.hlp", "misc.hlp", "folding.hlp", "html.hlp", "regex.hlp",
  ""
];

define show_help_popup()
{
   variable buf, rows;

   buf = whatbuf();
   onewindow();
   rows = window_info('r');

   setbuf(HELP_BUFFER);
   set_readonly(0);
   erase_buffer();

   if(strlen(help_pages[help_page]) > 0)
   {
      variable help_file = expand_jedlib_file(help_pages[help_page]);

      () = insert_file(help_file);

      pop2buf(HELP_BUFFER);

      eob();
      bskip_chars("\n");

      rows = rows / 2 - (what_line() + 1);
      bob();

      hlp_mode();

      set_buffer_modified_flag(0);
      set_readonly(1);

      pop2buf(buf);
      loop (rows) enlargewin();

      help_page = (help_page + 1) mod length(help_pages);
   }
   else
   {
      help_page = 1;
   }

   update(1);

   if(help_page == 1)
      clear_message ();
   else
     message("Press F1 or F5 to cycle through the help pages, shift+F5 to remove the help display");
}

%==========================================================================
% Remove online help
%==========================================================================

define hide_help_popup()
{
   help_page = 0;
   show_help_popup();
}

%============================================================================
% Cut the current line to the kill buffer
%============================================================================

define cut_line()
{
   variable eobflag;

   bol();
   push_mark();

   eobflag = (down(1) == 0);

   if(eobflag)
   {
      eob();
   }

   yp_kill_region ();

   if(eobflag)
   {
      push_mark();
      insert("\n");
      yp_append_region();
   }
}

%******************************************************************************
%******************************************************************************

define yp_copy_text_as_kill (txt)
{
   variable buf;

   buf = whatbuf();

   setbuf("*scratch*");
   set_mark_cmd();
   insert(txt);
   yp_kill_region();

   setbuf(buf);
}

%============================================================================
% Deletes text from the cursor to the beginning of the next word,
% Does NOT store the text in the kill buffer.
%============================================================================

define del_word()
{
   push_mark();

   % If we are on whitespace then delete up to the start of the next
   % word, otherwise, delete to the next non-word character

   if(looking_at("\t") or looking_at(" "))
   {
      skip_white();
   }
   else
   {
      right(1);
      skip_word_chars();
      skip_white();
   }

   del_region();
}

%============================================================================
% Move a word left or right
%============================================================================

define new_jms_skip_word(dir)
{
   if(dir == 1)
   {
      if (eolp())
         return (go_right_1 ());

      skip_word_chars();
      skip_non_word_chars();
   }
   else
   {
      if (bolp())
         return (go_left_1 ());

      bskip_non_word_chars();
      bskip_word_chars();
   }
}

%******************************************************************************
%******************************************************************************

define jms_copy_word()
{
   variable w;

   w = cursor_word();

   yp_copy_text_as_kill(w);
   message("Copied '"+w+"'");
}

%******************************************************************************
%******************************************************************************

define jms_copy_line()
{
   variable w;

   push_spot();
   bol();
   push_mark();
   eol();
   w = bufsubstr();
   yp_copy_text_as_kill(w);
   pop_spot();
   message("Copied '"+w+"'");
}

%============================================================================
% Get a digit from the keyboard
%============================================================================

define bkmrk_get_number ()
{
   variable n;

   flush ("Bookmark number:");
   n = getkey () - '0';

   if ((n < 0) or (n > 9))
      error ("Number must be less than 10.");

   n;
}

%============================================================================
% Indent the region
%============================================================================

define indent_region()
{
   variable i;

   check_region(1);
   narrow();
   bob();

   do
   {
      bol();

      % Indent non-blank lines (a line with only whitespace is not
      % considered to be blank).

      if (not eolp())
      {
         if(USE_TABS)
         {
            insert("\t");
         }
         else
         {
            whitespace(TAB);
         }
      }

   }
   while(down_1());

   bob();
   smart_set_mark_cmd();

   widen();
   pop_spot();
}

%============================================================================
% Unindent the region by removing a leading tab or spaces to the size
% of a tab.
%============================================================================

define unindent_region()
{
   variable i, spaces;

   check_region(1);

   narrow();
   bob();

   % Build a tab-sized string of spaces

   spaces = sprintf("%*s", TAB, " ");

   do
   {
      bol();
      push_mark();

      if(looking_at("\t"))
      {
         go_right_1();
      }
      else
      {
         if(looking_at(spaces))
         {
            () = right(TAB);
         }
      }

      del_region();
   }
   while(down_1());

   bob();
   smart_set_mark_cmd();

   widen();
   pop_spot();
}

%============================================================================
%============================================================================

variable c_marked = 0;

define jms_narrow_fn()
{
   if(c_marked)
   {
      widen();
      flush("Widened to file");
   }
   else
   {
      c_mark_function();
      narrow();
      flush("Narrowed to function");
   }

   c_marked = 1-c_marked;
}

%============================================================================
%!%S-Lang user function (0 args)
%!%Prototype: Void getpwd()
%!%This function returns the current working directory where Jed was
%!%started from.
%============================================================================

define getpwd()
{
   variable pwd;

   setbuf("*scratch*");
   (,pwd,,) = getbuf_info();
   return(pwd);
}

%============================================================================
% Untab an entire buffer
%============================================================================

define untab_buffer()
{
   push_spot();
   mark_buffer ();
   untab();
   pop_spot();

   !if (BATCH)
      message("Buffer untabbed.");
}

% ******************************************************************************
% Tab a buffer
% ******************************************************************************

define tab_buffer()
{
   push_spot();
   mark_buffer();
   entab();
   pop_spot();

   !if (BATCH)
      message("Buffer tabbed..");
}

% ******************************************************************************
% tab_menu()
% Manipulate tab settings
% ******************************************************************************

define tab_menu()
{
   variable default_use_tabs, default_tab_size;
   variable done=0;

   default_use_tabs = USE_TABS;
   default_tab_size = TAB;

   while(done==0)
   {
      variable c, prompt, do_redraw;

      do_redraw = 1;

      prompt = "Tab key inserts:";

      if(USE_TABS == 1)
         prompt += " {{2}}T{{-}}ABS/{{3}}s{{-}}paces";
      else
         prompt += " {{3}}t{{-}}abs/{{2}}S{{-}}PACES";

      prompt += sprintf(", Tab size={{3}}%d{{-}}. Set tab size {{2}} 1 {{-}}-{{2}} 9 {{-}}. {{2}}U{{-}}ntab buffer, {{2}}R{{-}}e-tab buffer,  -->", TAB);

      flush(prompt);
      c = toupper (getkey());

      if(c=='\r')
         done=1;
      else if(c=='Q')
         done=-1;
      else if(c=='T')
         USE_TABS = 1;
      else if(c=='S')
         USE_TABS = 0;
      else if(c=='U')
      {
         untab_buffer();
         flush("Buffer untabbed");
         usleep(1000);
      }
      else if(c=='R')
      {
         tab_buffer();
         flush("Buffer tabbed");
         usleep(1000);
      }
      else if(c>='1' and c<='9')
         TAB = int(c)-int('0');
      else
      {
         flush("Invalid command");
         usleep(1000);
         do_redraw = 0;
      }

      if(do_redraw)
         call("redraw");
   }

   if(done==1)
   {
      variable tabkey;

      if(USE_TABS==1)
         tabkey="tabs";
      else
         tabkey="spaces";

      flush(sprintf("Tab key inserts %s, tab size set to %d", tabkey, TAB));

      C_INDENT=TAB;
   }
   else
   {
      USE_TABS=default_use_tabs;
      TAB = default_tab_size;
      C_INDENT=default_tab_size;

      error("Tab settings not changed");
   }
}

% ******************************************************************************
% ******************************************************************************

define get_jed_state_file()
{
   variable jed_state_file;

   jed_state_file = getenv("HOME");

   if(jed_state_file == NULL)
   {
      jed_state_file = getenv("JED_HOME");

      if(jed_state_file == NULL)
      {
         jed_state_file = getenv("JED_ROOT");

         if(jed_state_file == NULL)
         {
            jed_state_file = getenv("TEMP");

            if(jed_state_file == NULL)
            {
               jed_state_file = ".";
            }
         }
      }
   }

   if(jed_state_file == NULL)
   {
      flush("no state file!");
      usleep(250);
   }
   else
   {
      flush("get_jed_state_file(): " + jed_state_file);
      usleep(100);
   }

#ifdef UNIX
   % Original (?) code refers to wd, stat_struct, uid & gid, which are undefined,
   % so changed it to be more similarer to windows version of code (below)

#ifdef __OLD_CODE__
   jed_state_file = dircat(jed_state_file, strcat(".", strcat(str_replace_all(wd, "/", "_"),".sl")));

   if (stat_file(jed_state_file) == 0)
   {
      % [JMS] ???

      uid = stat_struct("uid");
      gid = stat_struct("gid");
   }
#endif
   jed_state_file = path_concat(jed_state_file, ".jed_state.sl");
#else
   jed_state_file = path_concat(jed_state_file, "$jed_state$.sl");
#endif

   return jed_state_file;
}

%============================================================================
%!%S-Lang user function (0 args)
%!%Prototype: Void write_state()
%!%This function saves the state information as a executable s-lang file.
%!%This includes inforemation about all files, their modes and cursor
%!%positions.
%!%write_state is normaly called bei exit_hook()
%!%See also: read_state
%============================================================================

define write_state(errorabort)
{
   variable file, gid, buf, flags, name, dir, fname, mode, str = "";
   variable uid = 0, i;
   variable wd = getpwd();
   variable jed_state_file;
   variable s_dir, s_case, s_type, s_global, s_words, search_defaults;
   variable s_txt, s_rpl;

   ( s_dir, s_case, s_type, s_global, s_words ) = search_get_defaults();
   ( s_txt, s_rpl) = search_get_text();

   if(jed_last_current_buffer == "")
   {
      jed_last_current_buffer = whatbuf();
   }

   jed_state_file = get_jed_state_file();

   if(errorabort)
   {
      ERROR_BLOCK
      {
         exit_jed();
      }
   }

   save_abbrevs();

   flush("Saving current state to '"+jed_state_file+"'...");
   usleep(10);

   str =  "% Jed Auto-Restore File\n";
   str += "% ---------------------\n";
   str += "\n";

   % Save variable variables

   str += "% Configuration variables\n\n";

   str += "Shell_Last_Shell_Command = \"" + str_quote_string (Shell_Last_Shell_Command, "\\\"", '\\') + "\";\n";
   str += "Shell_Last_Build_Command = \"" + str_quote_string (Shell_Last_Build_Command, "\\\"", '\\') + "\";\n";
   str += "\n";
   str += "Compile_Last_Compile_Cmd = \"" + str_quote_string (Compile_Last_Compile_Cmd, "\\\"", '\\') + "\";\n";
   str += "\n";
   str += "grep_pattern             = \"" + str_quote_string (grep_pattern,  "\\\"", '\\') + "\";\n";
   str += "grep_wildcard            = \"" + str_quote_string (grep_wildcard, "\\\"", '\\') + "\";\n";
   str += "grep_fixed_string        = " + string(grep_fixed_string) + ";\n";
   str += "\n";
   str += "current_compiler         = \"" + current_compiler + "\";\n";
   str += "current_compiler_name    = \"" + current_compiler_name +"\";\n";
   str += "build_log_file           = \"" + build_log_file + "\";\n";
   str += "\n";
   str += "search_set_parameters("+string(s_dir)+","+string(s_case)+","+string(s_type)+","+string(s_global)+","+string(s_words)+");\n";
   str += "search_set_text(\"" + str_quote_string(s_txt, "\n\"", '\\') + "\", \"" + str_quote_string(s_rpl, "\n\"", '\\')  +"\");\n";
   str += "\n";
   str += "REPLACE_PRESERVE_CASE    = " + string(REPLACE_PRESERVE_CASE) + ";\n";
   str += "\n";

   str += "% Saved regions (if any)\n\n";

   for(i=0; i<length(saved_region); i++)
   {
      variable saved = saved_region[i];
      if(typeof(saved) == String_Type)
      {
         saved = str_quote_string(saved, "\\\"", '\\');
         saved = strreplace(saved, "\n", "\\n");
         str += sprintf("saved_region[%d] = \"%s\";\n", i, saved);
      }
   }

#ifdef MSWINDOWS
   str += "\n% Windows font configuration\n\n";
   str += "Jed_Font_Name            = \"" + Jed_Font_Name + "\";\n";
   str += "Jed_Font_Size            = " + string(Jed_Font_Size) + ";\n";
   str += "Jed_Font_Bold            = " + string(Jed_Font_Bold) + ";\n";
   str += "\n";
   str += "w32_select_font(Jed_Font_Name, Jed_Font_Size, Jed_Font_Bold);\n";
#endif

   str += "\n";

   % Define a temporary function to restore a file and editing position

   str += "% Temporary function to restore a file and current position\n\n";

   str += "define restoref(f,n,c)\n";
   str += "{\n";
   str += "   if(file_status(f) == 1)\n";
   str += "   {\n";
   str += "      flush(strcat(\"Restoring \", f));\n";
   str += "      ()= find_file(f);\n";
   str += "      goto_line(n);\n";
   str += "      goto_column_best_try(c);\n";
   str += "      1;\n";
   str += "   }\n";
   str += "   else\n";
   str += "   {\n";
   str += "      flush(strcat(\"Cannot restore \", f));\n";
   str += "      0;\n";
   str += "   }\n";
   str += "}\n";
   str += "\n";

   % Restore files previously being edited, if nothing specified on the Jed command line

   str += "% Restore files that were being edited, unless something specified on the command line\n\n";

   str += "if(__argc <= 1)\n";
   str += "{\n";

   % Append the commands to the state file to restore current files being edited

   loop (buffer_list())
   {
      buf = ();

      % skip internal/special buffers

      if ((buf[0] == ' ') or (buf[0] == '*'))
         continue;

      setbuf (buf);
      (file, dir, name, flags) = getbuf_info ();

      !if (strlen (file))
         continue;   % no file associated

      if (flags & 0x200)
         continue;    % binary file

      fname = dircat (dir, file);

#ifdef MSDOS OS2 MSWINDOWS
      % quote backslash char & quotes in path names
      fname = str_quote_string (fname, "\\", '\\');
#endif

      (mode,) = what_mode ();
      mode = strlow (mode);

      % SLang commands for the state file

      if(mode != "")
         str += Sprintf ("\n\tif (restoref(\"%s\", %d, %d))\n\t\t%s_mode();\n",
                             fname, what_line(), what_column() ,mode, 4);
      else
         str += Sprintf ("\n\t()=restoref(\"%s\", %d, %d);\n",
                             fname, what_line(), what_column() ,3);
   }

   % Switch to the current buffer

   str += "\n\n\tsw2buf(\"" + jed_last_current_buffer + "\");\n";

   % End the conditional restoration

   str += "}\n";

   % delete the temporary restore function

   str += "\n.()restoref";

   i = write_string_to_file(str, jed_state_file);

   if(i < 0)
   {
      flush("Error in writing to the state file");
   }
   else
   {
      flush("State file saved ("+string(i)+" lines)");
   }

#ifdef UNIX
   if (uid > 0)
      () = chown(jed_state_file, uid, gid);
#endif

% #ifdef MSDOS MSWINDOWS
%    setbuf("*scratch*");
%    () = run_shell_cmd("attrib +h " + jed_state_file);
% #endif

   exit_jed();
}

%============================================================================
%!%S-Lang user function (0 args)
%!%Prototype: Void read_state()
%!%This function looks for a state file for the current working directory and
%!%evaluates it. In this case all files from the last editing session will be
%!%reload.
%!%read_state is normaly called bei start_hook()
%!%See also: write_state
%============================================================================

define read_state()
{
   variable jed_state_file = get_jed_state_file();

   flush("Restoring previous edit state from '"+jed_state_file+"'");
   usleep(50);

   if (1 == file_status(jed_state_file))
   {
      () = evalfile(jed_state_file);
      return 1;
   }
   else
   {
      return 0;
   }
}

%============================================================================
%!%S-Lang user function (0 args)
%!%Prototype: Void terminate_buffer()
%!%This function saves the current buffer and then deletes it.
%============================================================================

define terminate_buffer()
{
   variable buf = whatbuf;

   save_buffer();
   call("delete_window");
   delbuf(buf);
}

%============================================================================
% Display PC key binding
%============================================================================

define pc_showkey()
{
   variable c, ch, key = Null_String;
   variable timeout = 50;  % 5 seconds

   flush ("Press key:");

   while (input_pending(timeout))
   {
      timeout = 0;
      c = getkey();

      if (c == 0)
      {
         ch = "\\0";
      }
      else
      {
         if (c == 224) ch = "\\xE0";
         else if (c > 127) ch = strcat ("\\d", string (c));
         else ch = char (c);
      }

      key = strcat(key, ch);
   }

   % insert (key);
   message (strcat(strcat("Key code(s): '", key),"'"));
}

% ******************************************************************************
% Undefine a range of keys, optionally, with a prefix sequence.
% ******************************************************************************

define unset_keyrange(prefix, first, last)
{
   while(first <= last)
   {
      unsetkey(prefix + first);

      first = char(first[0]+1);
   }
}

%==========================================================================
% Align a series of assignments putting the '=' in the same column
% Similar to align_symbols("=") but checks for compound '==', '>=', '<='
% symbols and ignores them.
%==========================================================================

define align_assign ()
{
   variable assign_str = "[\?+-*/:&|^]?=[>=]?";
   variable c, maxc = -1, maxmatch=-1, i;

   check_region(1);
   narrow();
   bob();

   % Search the region for the furthest-right assignment operator
   % so that we know where we'll be aligning things

   while(1)
   {
      variable matchsize;

      matchsize = re_fsearch(assign_str);
      if(matchsize == 0)
         break;

      if(matchsize > maxmatch)
         maxmatch = matchsize;

      c = what_column();

      if(c > 0)
      {
         () = left(1);

         % Remove any existing whitespace before the operator and just
         % add a single space

         if(looking_at(" ") or looking_at("\t"))
         {
            trim();
            c = what_column();
         }

         () = right(matchsize);

         trim();
      }

      % If this is the furthest-right, set maxc
      if(c > maxc)
      {
         maxc = c;
      }

      eol();
   }

   % Now align everything to the right-most operator we found

   bob();

   while(1)
   {
      matchsize = re_fsearch(assign_str);
      if(matchsize == 0)
         break;

      % Line up the operator

      while(what_column() <= maxc)
      {
         insert_single_space();
      }

      % Add spaces after it, unless there's one there already

      if(fsearch("="))
      {
         () = right(matchsize);
         if(not (looking_at(" ") or looking_at("\t")))
         {
            insert_spaces(maxmatch - matchsize + 1);
         }
      }

      eol();
   }

   widen();
   pop_spot();
}

%==========================================================================
% Align a series of symbols to the furthest possible left position
%
% If all the symbols are the first thing on a line, we align them with
% the position of the left-most one.
%
% If not, we move each one as far left as it will go then line them all
% up with the right-most one of them + 1 tab stop.
%==========================================================================

define align_symbols_left(symbol)
{
   variable first_thing  = 1,
            left_edge    = 0,
            left_column  = 9999,
            right_column = -1,
            align_column,
            c;

   bob();

   while(fsearch(symbol))
   {
      c = what_column();

      bskip_white();

      % Something preceding, so clear the first column flag

      if(what_column() > 1)
      {
         first_thing = 0;
      }

      % Check for a new left edge, far left or far right position

      if(c < left_column)
      {
         left_column = c;
      }

      if(c > right_column)
      {
         right_column = what_column();
      }

      if(what_column() > left_edge)
      {
         left_edge = what_column();
      }

      % Hit the end of the buffer ?

      !if(down(1))
      {
         break;
      }
   }

   % Calculate the alignment column

   if(first_thing)
   {
      align_column = left_column;
   }
   else
   {
      align_column = left_edge;
   }

   % Tab-align the column

   if(first_thing)
   {
      align_column = ((align_column-1)/TAB) * TAB + 1;
   }
   else
   {
      align_column = ((align_column + TAB*2 - 1)/TAB) * TAB + 1;
   }

   % Now align all the symbols on the correct column

   bob();
   while(fsearch(symbol))
   {
      if(what_column() > 1)
      {
         () = left(1);
         if(looking_at("\t") or looking_at(" "))
         {
            trim();
         }
         else
         {
            () = right(1);
         }
      }

      while(what_column() < align_column)
      {
         insert_char(' ');
      }

      () = right(1);
   }
}

%==========================================================================
% Align a series of symbols to the nearest right position
%==========================================================================

define align_symbols_right(symbol)
{
   variable col,
            align_column = -1;

   %  Remove as much leading space before each occurance of each symbol
   %  as we can,

   bob();

   while(fsearch(symbol))
   {
      col = what_column();

      % If not at the start of the line, trim leading whitespace

      if(col > 1)
      {
         () = left(1);
         if(looking_at("\t") or looking_at(" "))
         {
            trim();
         }
         else
         {
            () = right(1);
         }

         col = what_column();
      }

      % If this is the most indented line so far, remember it

      if(col > align_column)
      {
         align_column = col;
      }

      % If we can't move past the symbol, we've hit the end of the buffer

      !if(right(1))
      {
         break;
      }
   }

   % Go back to the start of the buffer to line everything up

   bob();

   % If the align column is > 1 then tab-align it

   if(align_column > 1)
   {
      align_column = ((align_column + TAB*1 - 1)/TAB) * TAB + 1;
   }

   while( fsearch(symbol) )
   {
      % If it isn't indented far enough, indent it.

      while(what_column() < align_column)
      {
         insert_char(' ');
      }

      % If we can't move right, then we've hit the end of the buffer

      !if(right(1))
      {
         break;
      }
   }
}

%==========================================================================
% Align a series of symbols to the right in a region
%==========================================================================

define align_comments ()
{
   variable current_mode;

   (current_mode, ) = what_mode ();

   check_region(1);
   narrow();

   if (strcmp (current_mode, "C") == 0)
   {
      align_symbols_left("/*");
      align_symbols_right("*/");
      align_symbols_left("//");
   }
   else if (strcmp (current_mode, "SLang") == 0)
   {
      align_symbols_left("%");
   }
   else
   {
      error("Comment align not implemented in " + current_mode + " mode");
   }

   widen();
   pop_spot();
}

%******************************************************************************
%******************************************************************************

define make_bakname(bakname, baknum, prefix)
{
   variable i;
   variable filename, extname;

   % Pad the backup field to be 4 characters

   while(strlen(baknum)<4)
   {
      baknum = prefix + baknum;
   }

   filename = path_sans_extname(bakname);
   extname  = path_extname(bakname);

   if(extname == NULL)
   {
      extname = "";
   }

   % If there is an extension this will return filename#backup#.extension,
   % otherwise return filename#backup#

   filename + "#" + baknum + "#" + extname;
}

% ******************************************************************************
% jed_backup_directory() - Return the backup directory path with trailing '/'
% ******************************************************************************

define jed_backup_directory()
{
   variable bakdir;

   if(getenv("JED_BACKUP") == NULL)
   {
#ifdef MSWINDOWS
      bakdir = ".\\";
#else
      bakdir = "/tmp/";
#endif
   }
   else
   {
      bakdir = getenv("JED_BACKUP");

      % Add a backslash if required

      if(substr(bakdir, strlen(bakdir), 1) != "/")
      {
         bakdir = bakdir + "/";
      }
   }

   return bakdir;
}

% ******************************************************************************
% ******************************************************************************

define generate_backup_filename(name)
{
   variable newname;

   % If we are in a ClearCase view then prefix the backup filename with the name
   % of the view

   variable clearcase = getenv("CLEARCASE_ROOT");

   if(clearcase != NULL)
   {
      name = clearcase + name;
   }

#ifdef MSWINDOWS
   newname = strreplace(name,     ":",  "@");
   newname = strreplace(newname,  "\\", "^");
#else
   newname = strreplace(name,     "/",  "^");
#endif

   return newname;
}

%============================================================================
% Save hook - creates a backup before the save happens
%============================================================================

define buffer_save_hook(save_filename)
{
   variable backup_filename, backup_path,
            max_backup_files,
            bak_save_filename,
            num_backup_files,
            wildpath,
            i,
            num, min_backup, max_backup, numstr,
            filename,
            num_backups,
            delbak;

   ERROR_BLOCK
   {
      flush("Error making backup of " + save_filename);
      sleep(1);
      return;
   }

   % If the file being saved doesn't already exist, then
   % we don't need to make a backup copy of it!

   if(file_status(save_filename) != 1)
   {
      return;
   }

   flush("Saving "+save_filename);

   bak_save_filename = generate_backup_filename(save_filename);

   % Find the directory where the backup should be created

   backup_path = jed_backup_directory();

   % Find the number of backup files to preserve

   if(getenv("JED_NUM_BAK") == NULL)
   {
      max_backup_files = 32;
   }
   else
   {
      max_backup_files = integer(getenv("JED_NUM_BAK"));
   }

   % Create a wildcard mask for matching backup files

   wildpath = backup_path + make_bakname(bak_save_filename, "?", "?");

   % Create a list of backup files

   num_backup_files = directory(wildpath);

   % Find the lowest and highest-numbered backup files that
   % already exist.

   min_backup=0;
   max_backup=0;

   for(i=0; i<num_backup_files; i++)
   {
      filename=();

      % Extract the backup number (the backup directory path and filename should
      % not have '#' characters in them, or this would get upset).
      % **##** Fix this so that it only takes #nnnn# at the RHS of the filename

      numstr = extract_element(filename, 1, '#');

      % Remove any leading zeros, or it will look octalish

      while(substr(numstr, 1, 1) == "0" and strlen(numstr) > 1)
      {
         numstr=substr(numstr, 2, -1);
      }

      % See if this is the lowest/highest file encountered so far

      num = integer(numstr);

      if(i==0 or min_backup > num)
      {
         min_backup = num;
      }

      if(i==0 or max_backup < num)
      {
         max_backup = num;
      }
   }

   % See if there are excess backup files to delete

   if(max_backup>0)
   {
      num_backups = max_backup-min_backup+1;

      % Whilst there are too many backup files, delete them, starting
      % at the lowest-numbered and working upwards

      while(num_backups > max_backup_files and max_backup >= min_backup)
      {
         delbak = backup_path + make_bakname(bak_save_filename, string(min_backup), "0");

         % Check that the relevently numbered file exists (nothing to
         % prevent numbered files being manually deleted so that the list isn't
         % contiguous).

         if(file_status(delbak)==1)
         {
            ifnot(delete_file(delbak))
            {
               flush("Unable to delete backup file '"+delbak+"'");
               sleep(1);
            }
         }

         --num_backups;
         ++min_backup;
      }
   }

   % Work out what to call the backup file

   backup_filename = backup_path + make_bakname(bak_save_filename, string(max_backup+1), "0");

   % Copy the file on disk to be the backup

   if(copy_file(save_filename, backup_filename) != 0)
   {
      flush("Unable to make backup of " + save_filename + " to " + backup_filename);
      usleep(2000);
      return;
   }

   % Finished, the save routine can now get on and save the file - it _will_
   % still make a backup in the current directory, and we can do something
   % with that if the save is successful
}

% ******************************************************************************
% Post-Save hook - Copies the file to the backup directory without a
% numeric suffix.
% ******************************************************************************

define post_buffer_save_hook(save_filename)
{
   variable backup_filename, local_backup_filename;

   % Copy the current saved file to the backup directory without a
   % numeric suffix.

   backup_filename = jed_backup_directory() + generate_backup_filename(save_filename);

   if(copy_file(save_filename, backup_filename) != 0)
   {
      flush("Can't copy "+save_filename+" to "+backup_filename);
      usleep(1000);
   }

   % Save complete, so we can delete the local backup file if it exists
   % N.B. delete_file returns 0 on _failure_ (!)

   local_backup_filename = save_filename + "~";

   if(file_status(local_backup_filename) == 1)
   {
      if(delete_file(local_backup_filename) == 0)
      {
         flush("Can't delete local backup "+local_backup_filename+" after saving!");
         usleep(1000);
      }
   }
}

% ******************************************************************************
% Add the pre and post save hooks.
% ******************************************************************************

add_to_hook ("_jed_save_buffer_before_hooks", &buffer_save_hook);
add_to_hook ("_jed_save_buffer_after_hooks",  &post_buffer_save_hook);

%============================================================================
%============================================================================

define toggle_file_readwrite()
{
   variable cmd, file, dir, buf, line, col, flags, rwmode, opt;

   (file, dir, buf, flags) = getbuf_info();

   rwmode = (flags & 8);

   file = dir+file;
   line = what_line();
   col  = what_column();

#ifdef MSDOS MSWINDOWS
   cmd = "attrib";

   if(rwmode)
   {
      opt = "+r";
   }
   else
   {
      opt = "-r";
   }
#else
   cmd = "chmod";

   if(rwmode)
   {
      opt = "-w";
   }
   else
   {
      opt = "+w";
   }

#endif

   (, ) = run_shell_cmd_buf(cmd + " " + opt + " \"" + file +"\"", "*scratch*");

   delbuf(buf);
   () = find_file(file);

   goto_line(line);
   goto_column(col);

   if(rwmode)
   {
      flush(sprintf("%s set to read-only mode", file, 1));
   }
   else
   {
      flush(sprintf("%s set to read/write mode", file, 1));
   }
}

% ******************************************************************************
% Run an external command on the current buffer

define run_external_command_on_buffer(cmd)
{
   variable currentflags, file, dir;

   (file, dir, , currentflags) = getbuf_info();

   if(file=="")
   {
      error("Cannot run " + cmd + " without saving buffer to disk!");
   }

   % Save the buffer if it has been modified

   if((currentflags & 1) == 1)
   {
      save_buffer();
   }

   (, ) = run_shell_cmd_buf(cmd + " " + dircat(dir, file), COMPILE_BUFFER);

   reload_buffer();
}

% ******************************************************************************
% Run GNU Indent on the current buffer

define run_gnu_indent()
{
   variable currentmode;

   (currentmode, ) = what_mode();

   if(currentmode != "C")
   {
      error("Indent can only be run on C source files - current buffer is in '"+currentmode+"' mode!");
   }

   run_external_command_on_buffer("indent");
}

%******************************************************************************
%******************************************************************************

define align_function_call()
{
   bol();
   push_mark();

   !if(fsearch("("))
   {
      error("Can't find start of function call!");
   }

   call("goto_match");
   narrow();
   bob();

   while(fsearch(","))
   {
      () = right(1);
      del();
      call("newline_and_indent");
   }

   widen();
   pop_mark(1);
}

%******************************************************************************
%
%******************************************************************************

define evaluate_buffer()
{
   evalbuffer();
   flush("Buffer evaluated OK!");
}

%******************************************************************************
%******************************************************************************

define save_region_contents(buf, app)
{
   if(app)
   {
      saved_region[buf] = saved_region[buf] + bufsubstr();
      flush("Region appended to text cache #"+string(buf+1));
   }
   else
   {
      saved_region[buf] = bufsubstr();
      flush("Region copied to text cache #"+string(buf+1));
   }
}

%******************************************************************************
%******************************************************************************

define insert_saved_region(buf)
{
   if(saved_region[buf] == NULL)
   {
      error("Error: no text has been saved into text cache #"+string(buf+1));
   }
   else
   {
      insert(saved_region[buf]);
      flush("Inserted from text cache #"+string(buf+1));
   }
}

%******************************************************************************
% Reformat a C comment
%******************************************************************************

define reformat_c_comment()
{
   variable curr_mode;

   (curr_mode, ) = what_mode ();

   !if(curr_mode == "C")
   {
      error(sprintf("Uncomment function does not work in %s mode!", curr_mode));
   }

   push_mark();
   bol();

   ERROR_BLOCK
   {
      widen();
      pop_mark(1);
      return;
   }

   % Find the start of the comment

   if(fsearch("/*") == 0)
   {
      error("Can't find comment start");
   }

   % Find the end of a series of comment lines

   forever
   {
      if(fsearch("*/") == 0)
      {
         error("Can't find comment end");
      }

      % If the next line is the opening of another comment
      % then include it and loop to find the end of that
      % comment.

      !if(down(1))
      {
         break;
      }

      bol();
      skip_white();

      % Not a comment, go back up to the previous line and exit the loop

      !if(looking_at("/*"))
      {
         () = up(1);
         break;
      }
   }

   % Restrict ourselves to the commented area.

   narrow();

   flush("Narrowed editing to comment");

   % Remove the start and end of comment markers

   bob();
   replace("/*", "");

   replace("*/", "");
   bob();

   % Remove all leading '*',  '-' and space at the start of each line

   do
   {
      bol();
      trim();

      if(looking_at_char('*') or looking_at_char('-'))
      {
         while(looking_at_char('*') or looking_at_char('-'))
         {
            del();
            trim();
         }
      }
   }
   while(down(1));

   % Trim any unnecessary blank lines

   trim_buffer();

   % Put a box around the comment

   bob();

   insert("/******************************************************************************\n");

   do
   {
      insert("* ");
   }
   while(down(1));

   eob();
   if(what_column() > 1)
   {
      insert("\n");
      bol();
      del_eol();
   }

   insert("******************************************************************************/\n");

   % Revert back to editing everything and move the cursor back to where we started.

   widen();
   pop_mark(1);
}

%******************************************************************************
%******************************************************************************

define select_buff()
{
   bob();
   smart_set_mark_cmd();
   eob();
}

%******************************************************************************
%******************************************************************************

define pad_to_column()
{
   variable col;

   read_mini("Pad to column:", Null_String, Null_String);
   col = integer(());

   if(col < 1)
   {
      error("Invalid column!");
   }

   while(what_column() < col)
   {
      insert(" ");
   }
}

%******************************************************************************
%******************************************************************************

define delete_to_column()
{
   variable col;

   read_mini("Delete to column:", Null_String, Null_String);
   col = integer(());

   if(col < 1)
   {
      error("Invalid column!");
   }

   while(what_column() > col)
   {
      () = left(1);

      call("delete_char_cmd");
   }
}

%******************************************************************************
%******************************************************************************

define align_to_column()
{
   variable col;

   read_mini("Align to column:", Null_String, Null_String);
   col = integer(());

   if(col < 1)
   {
      error("Invalid column!");
   }

   while(what_column() < col)
   {
      insert(" ");
   }

   while(what_column() > col)
   {
      () = left(1);

      call("delete_char_cmd");
   }
}

%******************************************************************************
%******************************************************************************

define jms_skip_white()
{
   if(looking_at(" ") or looking_at("\t") or looking_at("\n") or looking_at("\r"))
   {
      skip_chars(" \t\n\r");
   }
   else
   {
      skip_chars("^ \t\n\r");
   }
}

%******************************************************************************
%******************************************************************************

define jms_bskip_white()
{
   if(looking_at(" ") or looking_at("\t") or looking_at("\n") or looking_at("\r"))
   {
      bskip_chars(" \t\n\r");
   }
   else
   {
      bskip_chars("^ \t\n\r");
   }
}

%******************************************************************************
% Formats the current paragraph from the cursor position down, rather
% than the whole paragraph.

define format_paragraph_down()
{
   % Slightly hacky - prevents format_paragraph from locating the top
   % of the current paragraph before starting formatting

   bol();
   insert("\n");
   push_spot();
   call("format_paragraph");
   pop_spot();
   call("backward_delete_char");

   while(1)
   {
      fsearch("\n");
      () = go_right_1();
      if(looking_at("\n") or eobp())
         break;
   }

   skip_chars(" \t\n\r");
}

%******************************************************************************
%******************************************************************************

define centre_bottom()
{
   variable n;

   n = window_info('r');

   recenter(n);
}

%******************************************************************************
%******************************************************************************

define centre_top()
{
   recenter(1);
}

%==========================================================================
% Extract text, delimited by textchrs from under the cursor and return it.
% - Looks backwards and forwards from the cursor for characters listed
%   in textchrs and returns the string between them.
%   Uses the skip_chars function so its rules apply to the interpretation
%   of textchrs, except that a '^' prefix cannot be used.
%==========================================================================

define get_cursor_text (textchrs)
{
   variable edge = 0, cursword;

   push_spot();

   bskip_chars("^"+textchrs);
   push_mark();
   skip_chars("^"+textchrs);

   cursword = bufsubstr();

   pop_spot();
   return cursword;
}

% ******************************************************************************
% ******************************************************************************

define load_file_under_cursor()
{
   variable fname = get_cursor_text("?* \t\r\n()[]");

   if(fname != "")
   {
      load_file(fname; new=0, hardfail=1);
   }
   else
   {
      error("No filename under the cursor!");
   }
}

% ******************************************************************************
% update_statusline()
%
% Update the status line (should be done if you change any of the settings
% (such as search defaults) that are displayed on the status line)
% ******************************************************************************

define update_statusline()
{
   variable s_dir, s_case, s_type, s_global, s_words, search_defaults;

   ( s_dir, s_case, s_type, s_global, s_words ) = search_get_defaults();

   if(s_global)
   {
      search_defaults = "Glb ";
   }
   else
   {
      search_defaults = "Loc ";
   }

   if(s_dir)
   {
      search_defaults += "Fwd ";
   }
   else
   {
      search_defaults += "Rev ";
   }

   if(s_case)
   {
      search_defaults += "Case ";
   }

   if(s_type)
   {
      search_defaults += "Regex ";
   }
   else
   {
      search_defaults += "Lit ";
   }

   if(s_words)
   {
      search_defaults += "Word ";
   }

   set_status_line  (" |%3C %b [%4l/%L] %-4c %2C| %m | Search: "+search_defaults+"| %W %T%a%n | %14C%t%2C | %F (%v) (%S)", 1);
}

%% ******************************************************************************
% Switch to an existing buffer
% ******************************************************************************

define switch_to_existing_buffer()
{
   variable buffer;

   buffer = read_with_completion("Switch to buffer: ", get_previous_buffer(), "", 'b');

   if(bufferp(buffer))
   {
      sw2buf(buffer);
   }
   else
   {
      error("Buffer '"+buffer+"' does not exist!");
   }
}

% ******************************************************************************
% Switch to the previously-used buffer
% ******************************************************************************

define jms_switch_to_last_buffer()
{
   variable previous_buffer = get_previous_buffer();

   if(bufferp(previous_buffer))
   {
      sw2buf(previous_buffer);
   }
   else
   {
      error("Buffer '"+previous_buffer+"' is no longer loaded!");
   }
}

% ******************************************************************************
% Switch to a new buffer
% ******************************************************************************

define jms_new_buffer()
{
   variable buffer;

   buffer = read_mini("New buffer: ", "", "");

   if(bufferp(buffer))
   {
      error("Buffer '"+buffer+"' already exists!");
   }
   else
   {
      sw2buf(buffer);
   }
}

% ******************************************************************************
% Convert the number under the cursor from one base to another and replace it
% with the result.
%
% Takes 0, 1 or 2 arguments as follows:
%       0 arguments - Attempts to determine the base of the number by assuming
%                     hex numbers prefixed with '0x', octal with '0' and binary
%                     with '0', defaulting to decimal otherwise.
%                     Decimal numbers are converted to hex and all others are
%                     converted to decimal.
%
%       1 argument -  The argument specifies the destination base, the source base
%                     is determined as for 0 arguments
%
%       2 arguments-  The source and destination bases are both specified and
%                     the number must not have any prefix.
%
% Specifying the source argument as 0 will cause it to be determined automatically
% and specifying the destination argument as 0 will cause it to default to 10 or
% 16 appropriately.
%
% Note that this function will handle numbers immediately followed by non-
% numeric characters, but not those _preceded_ by non-numeric characters without
% whitespace.
%
% ******************************************************************************

define cursor_num_conv ()
{
   variable num, src_base, dst_base, digits;
   variable ch, digit, result;

   % Handle variable arguments (see comment in header above)

   if(_NARGS > 2)
   {
      error("Too many arguments passed to cursor_num_conv!");
   }
   else if(_NARGS == 2)
   {
      (src_base, dst_base) = ();
   }
   else if(_NARGS == 1)
   {
      dst_base = ();
      src_base = 0;
   }
   else
   {
      dst_base = 0;
      src_base = 0;
   }

   % Move to the start of the number

   () = right(1);
   bskip_word();

   % Mark the start of the number

   push_mark();

   % Set the source base if it wasn't explicitly specified.

   if(src_base == 0)
   {
      if(looking_at("0x") or looking_at("0X"))
      {
         % Hex source number

         src_base = 16;

         % Skip the prefix

         if(right(2) != 2)
         {
            error("Invalid hex number!");
         }
      }
      else if(looking_at("0"))
      {
         % Octal (or decimal 0) value

         src_base = 8;
      }
      else if(looking_at("b") or looking_at("B"))
      {
         % Binary

         src_base = 2;

         % Skip the prefix

         if(right(1) != 1)
         {
            error("Invalid binary number!");
         }
      }
      else
      {
         % Default is decimal

         src_base = 10;
      }
   }

   % Set the destination base if it wasn't explicitly specified

   if(dst_base == 0)
   {
      % Destination defaults to hex if source is decimal
      % or is decimal otherwise.

      if(src_base == 10)
      {
         dst_base = 16;
      }
      else
      {
         dst_base = 10;
      }
   }

   % Parse through the number

   num = 0;
   digits = 0;

   forever
   {
      ch = what_char();

      % Convert the current character to a single digit.

      if(ch >= '0' and ch <= '9')
      {
         digit = int(ch) - '0';
      }
      else if(ch >= 'A' and ch <= 'Z')
      {
         digit = int(ch) - 'A' + 10;
      }
      else if(ch >= 'a' and ch <= 'z')
      {
         digit = int(ch) - 'a' + 10;
      }
      else
      {
         % Not a valid digit

         break;
      }

      % Out of range digit for the current base

      if(digit >= src_base)
      {
         break;
      }

      % Stick the digit on the end of the number

      num = num * src_base + digit;
      digits++;

      % Next digit please!

      if(right(1) != 1)
      {
         break;
      }
   }

   % No digits!

   if(digits == 0)
   {
      error("Invalid number!");
   }

   del_region();

   result = "";

   do
   {
      digit = num - (num/dst_base)*dst_base;
      num   /= dst_base;

      if(digit < 10)
      {
         result = char(digit+'0') + result;
      }
      else
      {
         result = char(digit+'A'-10) + result;
      }
   }
   while(num > 0);

   if(dst_base == 2)
   {
      result = "B" + result;
   }
   else if(dst_base == 8)
   {
      result = "0" + result;
   }
   else if(dst_base == 16)
   {
       result = "0x" + result;
   }

   insert(result);
}

% ******************************************************************************
% Function to be assigned to tab - does the following:
%  If a region is defined, it indents it.
%  Otherwise, it does a tab.
% Origininally intended it to do abbreviation expansion too, but I can't get
% it to work properly.
% ******************************************************************************

define tab_abbrev_expand(direction)
{
   if(markp())
   {
      % A region is defined

      if(direction)
      {
         indent_region();
      }
      else
      {
         unindent_region();
      }
   }
   else
   {
      insert_char('\t');
   }
}

% ******************************************************************************
% ******************************************************************************

define jms_jed_help()
{
#ifdef MSWINDOWS
   flush("Load HTML help files...");

   () = run_shell_cmd("start iexplore c:\\jed\\html\\index.html");
#else
   variable helpfile, keyword;

   keyword = cursor_word();

   onewindow();
   splitwindow();
   sw2buf ("*help*");
   set_readonly(0);
   erase_buffer ();

   helpfile = expand_jedlib_file("functions.hlp");
   () = insert_file(helpfile);

   bob();
   set_readonly(1);

   re_fsearch("^"+keyword);
   recenter(1);
   %most_mode ();
   hlp_mode();
#endif
}

% ******************************************************************************
% Create a list of editable buffers that are associated with files
% If 'writeable' is 1 then it only returns a list of files that are
% writeable
% The current buffer (if present) is always added to the end of the
% list.

define editable_buffers(writeable)
{
   variable startbuf = whatbuf ();
   variable nbufs = buffer_list ();
   variable buf, file, flags;
   variable buffers = {};

   while(nbufs)
   {
      buf = ();
      nbufs--;

      % skip special buffers

      if ((buf[0] == '*') or (buf[0] == ' '))
         continue;

      % Get the buffer filename & flags

      (file,,,flags) = getbuf_info (buf);

      % If writeable specified, skip read-only buffers

      if (writeable and (flags & 8))
         continue;

      % Add to the list (except for the current buffer)

      if(buf != startbuf)
         list_append(buffers, buf);
   }

   (file,,,flags) = getbuf_info (buf);

   % Add the current buffer to the end of the list (subject to the
   % writeable constraint).

   !if (writeable and (flags & 8))
   {
      list_append(buffers, startbuf);
   }

   return buffers;
}

% ******************************************************************************
% ******************************************************************************

define kill_word ()
{
   push_mark(); skip_word();
   del_region ();
}

% ******************************************************************************
% ******************************************************************************

define bkill_word ()
{
   push_mark(); bskip_word();
   del_region ();
}

% ******************************************************************************
% ******************************************************************************

#ifdef MSWINDOWS

variable Jed_Font_List =  "Monotype.com|Bitstream Vera Sans Mono|Bitstream Vera Sans Mono Bold|"+
                          "Consolas|Courier New|Crystal|"+
                          "Hyperfont|Hyperfont Dk.|Hyperfont Lt|Lucida Console|"+
                          "MS LineDraw|C++ Editor 8pt|Console8 347|DOSLike|Terminal|Z88 LoRes1|"+
                          "Sheldon|ProFontWindows|Kourier 9|Kourier Sans Serif 9|FixedSys OEM 14|Sprog 8x8|"+
                          "NetTerm ANSI (TrueType)|NetTerm OEM (TrueType)|Letter Gothic Line (TrueType)|"+
                          "Raize|SysMono|Dina 8|Monaco|Anonymous|DPCustomMono2";

variable Jed_Num_Fonts = length(strchop(Jed_Font_List, '|', 0));

variable Jed_Current_Font = 0;

define win_new_font()
{
   variable fontspec;

   w32_select_font(Jed_Font_Name, Jed_Font_Size, Jed_Font_Bold);

   fontspec = sprintf("%s %d", Jed_Font_Name, Jed_Font_Size);

   if(Jed_Font_Bold)
   {
      fontspec += " bold";
   }

   flush("Font set to "+fontspec);
}

define win_font_size(change)
{
   if(Jed_Font_Size+change > 2)
   {
      Jed_Font_Size += change;

      win_new_font();
   }
}

define win_font_select(change)
{
   if(change == 0)
   {
      Jed_Current_Font = 0;
   }
   else
   {
      Jed_Current_Font += change;

      if(Jed_Current_Font < 0)
      {
         Jed_Current_Font = Jed_Num_Fonts-1;
      }
      else if(Jed_Current_Font >= Jed_Num_Fonts)
      {
         Jed_Current_Font = 0;
      }
   }

   Jed_Font_Name = extract_element(Jed_Font_List, Jed_Current_Font, '|');

   if(Jed_Font_Name == NULL)
   {
      Jed_Font_Name = "Courier New";
   }

   win_new_font();
}

#endif

% ******************************************************************************
% ******************************************************************************

#ifdef MSWINDOWS

variable print_command = "*copy %s lpt1:";

define print_buffer()
{
   variable tempfile;

   flush("Printing current buffer...");

   tempfile = make_tmp_file(dircat(getenv("TEMP"), "jp"));

   push_spot ();
   mark_buffer ();
   () = write_region_to_file(tempfile);
   pop_spot();

   setbuf("*shell-output*");
   () = system(sprintf(print_command, tempfile));

   delete_file(tempfile);

   flush("Printed!");
}

#endif

% ******************************************************************************
% Scroll the current window up one line

define scroll_up()
{
   if(window_line() == window_info('r'))
   {
      () = up(1);
   }

   push_spot();
   go_up (window_line ());
   recenter(1);
   pop_spot();
}

% ******************************************************************************
% Scroll the current window down one line

define scroll_down()
{
   if(window_line() == 1)
   {
      () = down(1);
   }

   push_spot();
   go_down (window_info('r')-window_line ()+1);
   recenter(window_info('r'));
   pop_spot();
}

% ******************************************************************************
% ******************************************************************************

define jms_other_window()
{
   call("other_window");
}

% ******************************************************************************
% ******************************************************************************

define other_window_key(key)
{
   variable type, func;
   call("other_window");

   (type, func) = get_key_binding(key);

   if (type)
     call (func);
   else
     eval (func);

   call("other_window");
}

%******************************************************************************
%******************************************************************************

define other_prev()
{
   call("other_window");
   scroll_up();
   call("other_window");
}

%******************************************************************************
%******************************************************************************

define other_next()
{
   call("other_window");
   scroll_down();
   call("other_window");
}

%******************************************************************************
%******************************************************************************

define other_pgup()
{
   other_window_key(Key_PgUp);
}

% ******************************************************************************
% Remove all blank lines and trailing space from the current buffer

define trim_blank_lines()
{
   % Move to the top

   push_spot_bob();

   % Loop through the buffer

   do
   {
      % Move to the end of the line, trim whitespace, move to the next
      % character and break if we are are the end of the buffer

      eol_trim();
      if(not right(1))
         break;

      % Trim the line, move to the start of the line and if we are
      % looking at \n, delete it and repeat until we hit a non-blank
      % line or the end of the buffer.

      do
      {
         eol_trim();
         bol();

         if(not eolp())
            break;

         del();
      }
      while(not eobp());
   }
   while(not eobp());

   % Return from whence we came

   pop_spot();
}

%******************************************************************************
%******************************************************************************

define other_pgdn()
{
   other_window_key(Key_PgDn);
}

% ******************************************************************************

define x_clipboard_insert()
{
   () = run_shell_cmd("xsel --output --clipboard");
}

% ******************************************************************************

define x_copy_region_to_clipboard()
{
   variable status;
   status = pipe_region("xsel --input --clipboard");

   if(status)
   {
      error(sprintf("Error %d copying region to clipboard", status));
   }
}

% ******************************************************************************

define tidy_json_region()
{
   pipe_region_through("jq .");
}

% ******************************************************************************

define x_cut_region_to_clipboard()
{
   variable status;

   narrow_to_region();
   select_buff();
   status = pipe_region("xsel --input --clipboard");

   select_buff();
   yp_kill_region();

   widen_region();
   if(status)
   {
      error(sprintf("Error %d copying region to clipboard", status));
   }
}

% ******************************************************************************
% Define buffer hook that saves the current buffer before switching
% and, if running on X, sets the title bar to the current buffer

private variable previous_buffer = "";

private define my_switch_buffer_hook (old_buffer)
{
   previous_buffer = old_buffer;

#ifdef XWINDOWS
   x_set_window_name ("Jed: "+whatbuf ());
#endif
}

append_to_hook ("_jed_switch_active_buffer_hooks", &my_switch_buffer_hook);

% ******************************************************************************
% Switch to the previously-visited buffer (if there is one)

define switch_to_previous_buffer()
{
   if (previous_buffer != "")
   {
      sw2buf(previous_buffer);
   }
   else
   {
      error("No previous buffer");
   }
}

% ******************************************************************************
% Remove control codes and ANSI sequences from a string

define clean_ansi(str)
{
   variable i;
   variable result="";
   variable adding=1;

   for(i=0; i<strlen(str); i++)
   {
      if(str[i] == 27)
      {
         adding = 0;
      }
      else if(str[i] >= ' ' and adding)
      {
         result = result + char(str[i]);
      }

      if(not adding && str[i] == 'm')
      {
         adding = 1;
      }
   }

   return result;
}

% ******************************************************************************

define undefined_key()
{
   error(sprintf("Undefined key: %s", expand_keystring (LASTKEY)));
}

% ******************************************************************************
% After-save hook - check the filetype and if it is one that we process on save
% then process it and, if necessary, reload it

define format_file_on_save(save_filename)
{
   variable tmp_filename = "";
   variable status;
   variable infile, buf;
   variable msg;

   % Run 'terraform fmt' on Terraform source files and 'got fmt' on
   % Golang source files.
   % Report the first error on any failure.

   if(path_extname(save_filename) == ".tf" or path_extname(save_filename) == ".tfvars")
   {
      tmp_filename = make_tmp_file("jed-save");
      flush("Reformatting " + save_filename);
      status = system(sprintf("terraform fmt %s >/dev/null 2>%s", save_filename, tmp_filename));

      reload_buffer();

      if(status)
      {
         setbuf(COMPILE_BUFFER);
         erase_buffer();
         insert_file(tmp_filename);

         strip_control_codes();
         bob();
         replace("\n", " ");
         replace("  ", " ");
         replace("Error:", "\nError:");

         bol_fsearch("Error:");
         push_mark();
         eol();
         msg = bufsubstr();
         pop_mark();

         error(msg);
      }
   }
   else if(path_extname(save_filename) == ".go")
   {
      tmp_filename = make_tmp_file("jed-save");
      flush("Reformatting " + save_filename);
      status = system(sprintf("go fmt %s 2>%s", save_filename, tmp_filename));

      reload_buffer();

      if(status)
      {
         setbuf(COMPILE_BUFFER);
         erase_buffer();
         insert_file(tmp_filename);

         bob();
         push_mark();
         eol();
         msg = bufsubstr();
         pop_mark();

         error(msg);
      }
   }

EXIT_BLOCK
   {
      if(tmp_filename != "")
      {
         () = remove(tmp_filename);
      }
   }
}

% ******************************************************************************

append_to_hook ("_jed_save_buffer_after_hooks",  &format_file_on_save);
