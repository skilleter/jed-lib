% -*- SLang -*-      bufed.sl
%
% Simple JED `bufed' mode by Mark Olesen <olesen@me.QueensU.CA>
%
% Bufed is a simple buffer manager -- patterned somewhat after dired.
% Provides easy, interactive switching, saving and killing of buffers.
%
% To invoke Bufed, do `M-x bufed'.
% Or re-bind to the key sequence which is normally bound to the
% `list_buffers' function `C-x C-b' (emacs)
%
% ------------------------------------------------------------------------
% TO USE THIS MODE: add the line
%
%  autoload ("bufed", "bufed");     to ~/.jedrc
%
% and optionally re-bind to the `C-x C-b' (emacs) key sequence
%
%  setkey ("bufed", "^X^B");
% ------------------------------------------------------------------------

require("util");

static variable Bufed_buf = "*BufferList*";  % as used by `list_buffers' (buf.sl)

static variable top_line = 3;
static variable bottom_line = 0;
static variable showall = 0;
static variable Line_Mark;
static variable relative_path = 1;

static variable bufed_help_msg = "[K]ill, [S]ave, [SPC]:Show buffer, [CR]:Switch to buffer, Toggle show [H]idden buffers, [Q]uit, [R]e-read buffer, Re-read [A]ll buffers";

% extract the buffer name associated with the current line
% Note: The details of this routine will depend upon how buf.sl formats
%       the line.  Currently, this looks like:
% ----------- 0000    "*scratch*"                   /aluche/h1/davis/src/jed/lib/

static define bufed_get ()
{
   variable buf;

   push_spot_bol ();
   EXIT_BLOCK { pop_spot (); }

   ifnot (ffind_char ('"'))
      return Null_String;

   go_right_1 ();
   push_mark ();

   ifnot (ffind_char ('"'))
   {
      pop_mark_1 ();
      return Null_String;
   }

   buf = bufsubstr ();
   ifnot (bufferp (buf)) buf = "";

   return buf;
}

% save the buffer

define bufed_savebuffer (buf)
{
   variable file, dir, flags, ch, this_buf;

   ch = int (buf);

   if ((ch == 32) or (ch == '*')) return; % internal buffer or special

   this_buf = whatbuf ();
   (file,dir,,flags) = getbuf_info (buf);

   setbuf (this_buf);
   ERROR_BLOCK { setbuf (this_buf); }

   if (strlen (file) and (flags & 1))  % file assciated with it
   {
      !if (write_buffer (dircat (dir, file)))
         error ("Error writing buffer " + buf);
   }
}

% ******************************************************************************
% Expanded to sort the list and do lots of other nice things as well.
% ******************************************************************************

static define list_buffers ()
{
   variable i, j, tmp, this, name, flags, flag_chars, changed, pos, len;
   variable flag_col, name_col, dir_col, chg_col, info_col, size_col;
   variable bufname, filename, dirname, fullname, lines, bytes;
   variable cwd = getcwd ();

   bottom_line = top_line-1;

   flag_col   =   2;
   size_col   =  14;
   info_col   =  24;
   name_col   =  34;
   chg_col    =  85;
   dir_col    =  88;

   tmp = "*BufferList*";
   this = whatbuf();
   pop2buf(tmp);
   set_readonly(0);
   erase_buffer();
   TAB = 8;

   flag_chars = "CBKN-UORDAM";

   goto_column(flag_col);  insert("Flags");
   goto_column(size_col);  insert("Size");
   goto_column(info_col);  insert("Lines");
   goto_column(name_col);  insert("Buffer Name");
   goto_column(chg_col);   insert("C");
   goto_column(dir_col);   insert("Dir/File");

   insert("\n");

   goto_column(flag_col);  insert("===========");
   goto_column(size_col);  insert("========");
   goto_column(info_col);  insert("========");
   goto_column(name_col);  insert("==============================");
   goto_column(chg_col);   insert("=");
   goto_column(dir_col);   insert("=======================================\n");

   loop (buffer_list())
   {
      name = ();
      if (showall==0 and ((int(name) == ' ') or int(name) == '*'))
         continue;   %% internal buffers begin with a space

      if(name == " <mini>")
         continue;

      setbuf(name);
      (filename, dirname, bufname, flags) = getbuf_info();    % more on stack

      push_spot();
      eob();
      lines = what_line();
      bytes = count_chars();
      if (string_match(bytes, ".*of \\([0-9]+\\)$", 1))
      {
         (pos, len) = string_match_nth(1);
         bytes = substr(bytes, pos + 1, len);
      }
      else
      {
         bytes = -1;
      }

      bottom_line++;

      pop_spot();
      fullname = dirname+filename;
      changed = file_changed_on_disk(fullname);

      setbuf(tmp);
      bol();
      i = 0x400;
      j = 0;

      goto_column(flag_col);
      while (i)
      {
         if (flags & i)
            flag_chars[j];
         else
            '-';

         insert_char (());

         i = i shr 1;
         j++;
      }

      goto_column(size_col);
      vinsert("%8s", bytes);

      goto_column(info_col);
      vinsert("%8d", lines);

      goto_column (name_col);
      insert("\""+bufname+"\"");

      if(changed)
      {
         goto_column(chg_col);

         insert("*");
      }

      goto_column(dir_col);
      !if (eolp())
      {
         eol();
         insert_single_space();
      }

      if(relative_path)
      {
         insert(get_relative_path(fullname, cwd));
      }
      else
      {
         insert(fullname);
      }

      newline();
   }

   goto_line(top_line);
   goto_column(dir_col);
   push_mark();
   goto_line(bottom_line);
   sort_using_function_eol(&strcmpi);

   eob();

   goto_column(flag_col);  insert("===========");
   goto_column(size_col);  insert("========");
   goto_column(info_col);  insert("========");
   goto_column(name_col);  insert("==============================");
   goto_column(chg_col);   insert("=");
   goto_column(dir_col);   insert("=======================================\n");

   insert("\n"+bufed_help_msg);
   insert("\nFlags: CBKN-UORDAM");
   insert("\n       C : CR Mode                  B : Binary file              ");
   insert("\n       K : Not backed up            N : No autosave              ");
   insert("\n       U : Undo enabled             O : Overwrite mode           ");
   insert("\n       R : Read-only                D : File changed on disk     ");
   insert("\n       A : Autosave enable          M : Modified                 ");

   set_buffer_modified_flag (0);
   set_readonly (1);
   goto_line(top_line);
}

% ******************************************************************************
% try to re-load the file from disk
% ******************************************************************************

define bufed_update ()
{
   variable file, dir, flags;

   (file,dir,,flags) = getbuf_info ();

   if (flags & 2)    % file on disk modified?
   {
      !if (find_file (dircat (dir, file)))
         error ("Error reading file '"+file+"'");
   }
}

% ******************************************************************************
% Go to a buffer
% ******************************************************************************

define bufed_pop2buf ()
{
   variable buf = bufed_get ();

   !if (int (buf)) return;

   pop2buf (buf);
   bufed_update ();
   pop2buf (Bufed_buf);
}

% ******************************************************************************
% Ensure that the cursor is restricted to the lines between the header and
% footer.
% ******************************************************************************

static define limit_cursor()
{
   if(what_line() < top_line)
   {
      goto_line(top_line);
   }
   else if(what_line > bottom_line)
   {
      goto_line(bottom_line);
   }

   emacs_recenter();
   bol();
   bufed_pop2buf();
}

% ******************************************************************************
% ******************************************************************************

define bufed_help ()
{
   message (bufed_help_msg);
}

% ******************************************************************************
% ******************************************************************************

define update_bufed_hook ()
{
   Line_Mark = create_line_mark (11);
   bufed_help();
}

% ******************************************************************************
% ******************************************************************************

define bufed_list ()
{
   Line_Mark = NULL;
   check_buffers ();
   list_buffers ();
   pop2buf (Bufed_buf);
   set_buffer_hook ("update_hook", &update_bufed_hook);
   set_readonly (0);
   bob();
   set_readonly (1);
   set_buffer_modified_flag(0);
   %goto_column (21);
}

% ******************************************************************************
% ******************************************************************************

define bufed_showall()
{
   showall = 1-showall;
   bufed_list();
}

% ******************************************************************************
% kill a buffer, if it has been modified then pop to it so it's obvious
% ******************************************************************************

define bufed_kill ()
{
   variable file, dir, flags, buf = bufed_get ();
   variable line;

   ifnot (strlen (buf)) return;

   line = what_line ();
   (file,dir,,flags) = getbuf_info (buf);

   if (flags & 1)    % modified
   {
      pop2buf (buf);
      update (1);
   }

   delbuf (buf);

   if (strcmp (buf, Bufed_buf))
     bufed_list ();

   goto_line (line);
   limit_cursor();
}

% ******************************************************************************
% reload a buffer from disk
% ******************************************************************************

define bufed_reload(buf)
{
   variable ch, flags, line;

   ch = int(buf);
   if ((ch <= 32) or (ch == '*'))
      return;

   (,,, flags) = getbuf_info(buf);

   !if (flags & 4)
      return;

   flush(sprintf("Reloading '%s'", string(buf))); usleep(250);

   pop2buf(buf);
   line = what_line ();
   reload_buffer();

   pop2buf(Bufed_buf);
   bufed_list ();
   goto_line (line);
}

% ******************************************************************************
% Reload the current buffer from disk
% ******************************************************************************

define bufed_reload_buffer()
{
   bufed_reload(bufed_get ());
}

% ******************************************************************************
% Reload all buffers
% ******************************************************************************

define bufed_reload_all()
{
   flush("Reloading all modified buffers...");

   loop (buffer_list())
   {
      variable buf = ();

      bufed_reload(buf);
   }
}

% ******************************************************************************
% Save a buffer
% ******************************************************************************

define bufed_save ()
{
   variable buf = bufed_get ();
   bufed_savebuffer (buf);
}

% ******************************************************************************

define bufed_sw2buf (one)
{
   variable buf = bufed_get ();
   ifnot (int (buf)) return;
   sw2buf (buf);
   bufed_update ();
   if (one) onewindow ();
}

% ******************************************************************************

define bufed_exit ()
{
   delbuf (whatbuf ());
}

% ******************************************************************************

define bufed_up()
{
   go_up_1();
   limit_cursor();
}

% ******************************************************************************

define bufed_down()
{
   go_down_1();
   limit_cursor();
}

% ******************************************************************************

define bufed_pgdn()
{
   () = down(window_info('r')-1);
   limit_cursor();
}

% ******************************************************************************

define bufed_pgup()
{
   () = up(window_info('r')-1);
   limit_cursor();
}

% ******************************************************************************

define bufed_top()
{
   goto_line(top_line);
   limit_cursor();
}

% ******************************************************************************

define bufed_bottom()
{
   goto_line(bottom_line);
   limit_cursor();
}

% ******************************************************************************
% Create keymap

$1 = "bufed";

ifnot (keymap_p ($1)) make_keymap ($1);

definekey ("describe_mode",         "h",           $1);
definekey ("bufed_kill",            "k",           $1);
definekey ("bufed_save",            "s",           $1);
definekey ("bufed_pop2buf",         " ",           $1);
definekey (".0 bufed_sw2buf",       "\r",          $1);
definekey (".1 bufed_sw2buf",       "\t",          $1);
definekey ("bufed_exit",            "q",           $1);
definekey ("bufed_help",            "?",           $1);
definekey ("bufed_reload_buffer",   "r",           $1);
definekey ("bufed_reload_all",      "a",           $1);
definekey ("bufed_up",              Key_Up,        $1);
definekey ("bufed_down",            Key_Down,      $1);
definekey ("bufed_pgdn",            Key_PgDn,      $1);
definekey ("bufed_pgup",            Key_PgUp,      $1);
definekey ("bufed_top",             Key_Ctrl_PgUp, $1);
definekey ("bufed_bottom",          Key_Ctrl_PgDn, $1);
definekey ("bufed_showall",         "h",           $1);

% TODO: Also possible,
%  U  toggle_undo
%  O  toggle_overwrite
%  R  toggle_readonly
%  C  toggle_crmode

% ******************************************************************************
%!%+
%\function{bufed}
%\synopsis{bufed}
%\description
% Mode designed to aid in navigating through multiple buffers
% patterned somewhat after dired.
%
% To invoke Bufed, do \var{M-x bufed} or bind to \var{C-x C-b} (emacs)
%
% \var{g}   Update the buffer listing.
%
% \var{k}   Kill the buffer described on the current line, like typing
%  \var{M-x kill_buffer} and supplying that buffer name.
%
% \var{s}   Save the buffer described on the current line.
%
% \var{f}, \var{SPC}, \var{CR}, \var{TAB}
%  Visit the buffer described on the current line.
%  \var{f} and \var{SPC} will create a new window if required.
%  \var{CR} will use the current window.
%  \var{TAB} will revert to a single window.
%
% \var{Q}   Quit bufed mode.
%!%-

define bufed ()
{
   variable mode = "bufed";
   variable this_buf;

   this_buf = sprintf ("\"%s\"", whatbuf ());
   bufed_list ();
   () = fsearch (this_buf);

   bufed_help ();
   use_keymap (mode);
   set_mode (mode, 0);
   run_mode_hooks ("bufed_hook");
   limit_cursor();
}
