%%
%%  Ispell interface
%%

%!%+
%\variable{Ispell_Program_Name}
%\synopsis{spell-check program name}
%\usage{variable Ispell_Program_Name = ""}
%\description
% The spell check command used by the \sfun{ispell} function. It must
% be ispell-compatible (one of "ispell", "aspell" or "hunspell").  If
% unset, the ispell program will be auto-detected by searching the
% path for one of the above programs.
%\seealso{ispell, search_path_for_file}
%!%-
custom_variable("Ispell_Program_Name", NULL);

% ******************************************************************************
% Search for candidates for the spell-checking command:

if ((Ispell_Program_Name == NULL) || (Ispell_Program_Name == ""))
{
   Ispell_Program_Name = "ispell";

   foreach $1 (["aspell", "hunspell", "ispell"])
     {
        if (NULL != search_path_for_file(getenv("PATH"), $1))
          {
             Ispell_Program_Name = $1;
             break;
          }
     }
   Ispell_Program_Name += " -a";
}

% ******************************************************************************
% Spell-check the word under the cursor

define ispell ()
{
   variable ibuf, buf, file, letters, num_win, old_buf;
   variable word, cmd, p, num, n, new_word;

#ifdef OS2
   file = make_tmp_file("jedt");
#else
   file = make_tmp_file("/tmp/jed_ispell");
#endif
   letters = "\a"R;

   ibuf = " *ispell*";
   buf = whatbuf();

   % Mark the current word

   skip_chars(letters); bskip_chars(letters); push_mark();

   n = _get_point ();
   skip_chars(letters);
   if (_get_point () == n)
     {
        pop_mark_0 (); %pop_mark_0 ();
        return;
     }

   % Call the spelling check to check the word

#ifdef MSDOS MSWINDOWS WIN32
   () = system(sprintf("echo %s | %s > %s",
                       bufsubstr(), Ispell_Program_Name, file));
#else
   if (pipe_region(sprintf ("%s > '%s'", Ispell_Program_Name, file)))
       error ("ispell process returned a non-zero exit status.");
#endif

   % Put the output into the ' *ispell*' buffer and delete the temp file

   setbuf(ibuf); erase_buffer();
   () = insert_file(file);
   () = delete_file(file);

   %%
   %% parse output
   %%

   % Check for, and delete the ispell header

   bob();
   if (looking_at("@(#)"))
     {
        del_through_eol ();
     }

   % Correct spelling, so do nothing

   if (looking_at_char('*') or looking_at_char('+'))
     {
        message ("Correct");   % '+' ==> is derived from
        bury_buffer (ibuf);
        return;
     }

   % Don't know what this means!

   if (looking_at_char('#'))
     {
        bury_buffer (ibuf);
        return (message("No clue."));
     }

   % Get the list of alternative spellings (in the form '& WORD
   % NUMBER_OF_ALTERNATIVES XXX: COMMA_SEPARATED_LIST_OF_ALTERNATIVE_SPELLINGS

   % Delete the prefix and remove leading/trailing space

   del(); trim(); eol_trim(); bol();

   % Delete the prefix, leaving just the word list

   if (ffind_char (':'))
     {
        skip_chars(":\t ");
        push_mark();
        bol();
        del_region();
     }

   % Convert the list of words into a menu

   insert ("(0) ");
   n = 1;
   while (ffind_char (','))
     {
        del ();
        trim(); newline();
        vinsert ("(%d) ", n);
        ++n;
     }

   % Show the buffer

   bob();
   num_win = nwindows();
   pop2buf(buf);
   old_buf = pop2buf_whatbuf(ibuf);

   % Clear the buffer modified flag

   set_buffer_modified_flag(0);
   variable ok = 0;

   % Prompt the user to enter the correct spelling and replace
   % the word

   try
     {
        num = read_mini("Enter choice. (^G to abort)", "0", "");
        if (0 == fsearch(sprintf ("(%s)", num)))
          throw RunTimeError, "$num is an invalid choice"$;

        () = ffind_char (' '); trim();
        push_mark_eol(); trim(); new_word = bufsubstr();
        set_buffer_modified_flag(0);
        sw2buf(old_buf);
        pop2buf(buf);
        ok = 1;
        bskip_chars(letters); push_mark();
        skip_chars(letters); del_region();
        insert(new_word);
     }
   finally
     {
        if (ok == 0)
          {
             sw2buf(old_buf);
             pop2buf(buf);
          }
        if (num_win == 1) onewindow();
        bury_buffer(ibuf);
     }
}

% ******************************************************************************
% If a region is defined, save it to a temporary file, run the
% spelling checker and load it back
% Otherwise select the whole buffer and do it

define ispell_region()
{
   variable tmpfile, result;
   variable end_mark, start_mark, pos;

   if(is_readonly())
   {
      error("Buffer is read-only");
   }

   % Writing the region to a file seems to unmark the region
   % so we save the start/end/current pos and recreate the
   % region when we've done.

   if (markp())
   {
      end_mark = create_user_mark();
      pos = end_mark;
      pop_mark(1);
      start_mark = create_user_mark();
      push_mark();
      goto_user_mark(end_mark);
   }
   else
   {
      pos = create_user_mark();
      bob();
      start_mark = create_user_mark();
      push_mark();
      eob();
      end_mark = create_user_mark();
   }

   tmpfile = make_tmp_file("/tmp/jed_ispell");

   () = write_region_to_file(tmpfile);
   result = run_shell_cmd("xterm -e aspell -c " + tmpfile);

   if(result == 0)
   {
      % Recreate the region, delete it, insert the corrected file

      goto_user_mark(start_mark);
      push_mark();
      goto_user_mark(end_mark);

      del_region();
      () = insert_file(tmpfile);
      () = delete_file(tmpfile);

      goto_user_mark(pos);
   }
   else
   {
      () = delete_file(tmpfile);
      error(sprintf("Spell-check aborted with error #%d", result));
   }

   pop_spot();
}
