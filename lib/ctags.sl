%==========================================================================
% CTAGS.SL = Rewritten by JMS to support ectags
%
% tags.sl   -*- SLang -*-
%
% read a tags file produced by ctags/etags programs
%
% By default, the file "tags" is used.  However, setting the variable
% `Tag_File' as in
%  global_variable Tag_File = "mytag.file";
% will override this.
%
%==========================================================================

% By default, the file "tags" is used.  However, setting the variable
% `Tag_File' as in
%  variable Tag_File = "mytag.file";
% will override this.
% Similarly, we can set the default directory for the tag file, although
% the software will search up the directory tree from this point until it finds it.

custom_variable ("Tag_File", "tags");
custom_variable ("Tag_Dir", ".\\");

static variable tagline = 0;
static variable found=0;

static variable tag_file_time = 0;
static variable tag_count = 0;

static variable tag = "";
static variable cbuf = "";

static variable case_search = 0;

static variable tag_mark = NULL;

% ******************************************************************************
% Return the datestamp of a file on disk or -1 if it does not exist
% ******************************************************************************

define file_time (file)
{
   variable st;

   st = stat_file(file);

   if (st == NULL)
      return -1;

   return st.st_mtime;
}

% ******************************************************************************
% Find the next instance of a tag in a ctags file
%
% ctags format:
%  EITHER: TAGNAME \t FILENAME \t /^ SEARCHSTRING $/ \f TYPE
%      OR  TAGNAME \t FILENAME \t LINE-NUMBER
%
% Note that, in the first form, the SEARCHSTRING can itself contain tab
% characters.
% ******************************************************************************

static define ctags_find (tag)
{
   variable n, filename, search_str, msg;
   variable tagdata, fields, tag_type;

   tagline = 0;

   % Do a regex search for line matching TAG tab FILENAME tab /SEARCH/"; tab TYPE
   % or                                  TAG tab FILENAME tab /SEARCH/

   n = re_fsearch(strcat("^", tag, "[\t ]+", "\\([^\t]+\\)", "[\t ]+", "\\(.*\\);\"", "[\t ]", "\\(.*\\)$"));
   if (n == 0)
   {
      n = re_fsearch(strcat("^", tag, "[\t ]+", "\\([^\t]+\\)", "[\t ]+", "\\(.*\\)"));
   }

   if (n==0)
   {
      if(found != 0)
      {
         msg = sprintf("no more instances of it in the tag file (%d found)!", tag_count);
      }
      else
      {
         msg = "no matches in the tag file!";
      }

      error (sprintf("Tag '%s' not found - %s", tag, msg));

      return 0;
   }

   filename   = regexp_nth_match(1);
   search_str = regexp_nth_match(2);
   tag_type   = regexp_nth_match(3);

   tag_type = tag_type[0];

   tag_count++;

   % Move down the tag file (must get the matches first - see
   % regexp_nth_match definition

   go_down_1();

   % Ensure that the filename is an absolute path

   if(filename[[0]] == ".")
   {
      filename = Tag_Dir + filename[[2:]];
   }
   else if(filename[[0]] != "/")
   {
      filename = Tag_Dir + filename;
   }

   % Decide how to locate the tag based on the tag type field in the
   % tag data

   if(tag_type == 'F')
   {
      % F = file, so just load the file in question

      !if(read_file(filename))
      {
         error ("File "+filename+" not found.");
      }

      message(sprintf("%d: Found file '%s'", tag_count, filename));
   }
   else if(tag_type == 'd')
   {
      % d = line number, so load the file and go to the line

      variable line_no;

      !if(read_file(filename))
      {
         error (sprintf("%d: File %s not found.", filename));
      }

      line_no = integer(search_str);
      goto_line(line_no);

      message(sprintf("%d: Found definition of %s at line %d in file '%s'", tag_count, tag, line_no, filename));
   }
   else
   {
      variable len;

      % Default case - for any other value in the tag type field we assume
      % we're given a string to search for in a file and try and
      % determine the identifier type from the tag_type field

      switch(tag_type)
      { case 'e' : tag_type = "enumeration field"; }
      { case 'f' : tag_type = "function"; }
      { case 'g' : tag_type = "global definition"; }
      { case 'm' : tag_type = "struct member"; }
      { case 'p' : tag_type = "public function"; }
      { case 's' : tag_type = "struct"; }
      { case 't' : tag_type = "typedef"; }
      { case 'u' : tag_type = "union"; }
      { case 'v' : tag_type = "variable"; }
      { case 'x' : tag_type = "extern"; }
      { tag_type = ""; }

      !if (read_file (filename))
      {
         error (sprintf("%d: File %s not found.", tag_count, filename));
      }

      % The search string may have ^ and $ to mark the start and end
      % of the line, but isn't a regexp, so we need to unconvert regexp
      % characters. We also need to unescape '/' characters

      search_str = strreplace(search_str, "*", "[*]");
      search_str = strreplace(search_str, ".", "[.]");
      search_str = strreplace(search_str, "?", "[?]");
      search_str = strreplace(search_str, "\\/", "/");

      len = strlen(search_str);
      if(search_str[[0]] == "/" and search_str[[len-1]] == "/")
         search_str = search_str[[1:len-2]];

      % Search from the top of the buffer - this will be a problem if the
      % same tag is present more than once in the same file in the same format
      % but if we don't go to the top of the file then we won't necesarily find
      % anything

      bob();
      n = re_fsearch (search_str);

      if(n == 0)
      {
         % If we can't find the search string defined in the tag file,
         % try searching just for the tag string itself. Hopefully, the
         % first occurence of the string is the definition we want.

         n = fsearch (tag);
         if(n == 0)
         {
            error(sprintf("%d: Tag '%s' (%s) no longer exists in %s", tag_count, tag, tag_type, filename));
         }
         else
         {
            message (sprintf("%d: Approximate match for tag '%s' (%s) found at line %d in %s", tag_count, tag, tag_type, what_line(), filename));
         }
      }
      else
      {
         message (sprintf("%d: Tag '%s' (%s) found at line %d in %s", tag_count, tag, tag_type, what_line(), filename));
      }
   }

   emacs_recenter();

   tagline = create_line_mark(3);

   found = 1;

   return 1;
}

% ******************************************************************************
% Find the next instance of a tag in an etags file
%
% etags format:
%  ^L
%  filename,some-number
%  [function-type] function-name ^?line-name,some-number
% ******************************************************************************

static define etags_find (tag)
{
   variable file, line, msg = "Tag file needs updating?";

   error("etags_find");
   tagline = 0;

   !if (re_fsearch (strcat (tag, "[\t ]+\x7F\\(\\d+\\),")))
   {
    error (msg);
      return 0;
   }

   line = integer (regexp_nth_match (1));

   () = bol_bsearch (char (014));   % previous ^L
   go_down_1 ();
   push_mark (); skip_chars ("^,");
   file = bufsubstr ();

   if(file[[0]] == ".")
   {
      file = Tag_Dir + file[[2:]];
   }

   !if (read_file (file))
   {
      error ("File "+file+" not found.");
   }

   tag_count++;
   goto_line (line);
   tagline = create_line_mark(3);
   emacs_recenter();

   message(sprintf("%d: Tag %s found at line %d in %s", tag_count, tag, line, file));

   return 1;
}

% ******************************************************************************
% Find the next instance of the current tag, using either the etags or ctags
% find function.
% ******************************************************************************

define find_next_tag()
{
   variable etags, result;

   if(whatbuf() != TAGS_BUFFER)
   {
      setbuf(TAGS_BUFFER);
   }

   push_spot();
   bob ();
   etags = looking_at_char (014);
   pop_spot();

   if (etags) % if first char is ^L (etags)
     result = etags_find (tag);
   else
     result = ctags_find (tag);

   pop2buf (whatbuf ());
   pop2buf (cbuf);

   return result;
}

% ******************************************************************************
% Find the tag file, or update it if it has changed on disk
% ******************************************************************************

static define find_current_tagfile()
{
   variable load_tags;

   % If the tag buffer exists, and is out-of-date, we need to reload it
   % If the buffer doesn't exist then we definitely need to load it.

   if(bufferp(TAGS_BUFFER))
   {
      if(file_time(Tag_Dir+Tag_File) != tag_file_time)
      {
         setbuf (TAGS_BUFFER);
         set_readonly (0);
         erase_buffer ();
         load_tags = 1;
      }
      else
      {
         load_tags = 0;
      }
   }
   else
   {
      load_tags = 1;
   }

   if (load_tags)
   {
      setbuf (TAGS_BUFFER);

      flush("Searching for tag file...");

      % Look for tag file in parent directories first then, if not found, look in
      % current directory

      Tag_Dir = find_up_dir(Tag_File);

      flush("Loading tag file from " + Tag_Dir + Tag_File);

      if (insert_file (Tag_Dir+Tag_File) < 0)
      {
         delbuf(TAGS_BUFFER);
         tag_file_time = 0;
      }
      else
      {
         tag_file_time = file_time(Tag_Dir+Tag_File);
         set_buffer_modified_flag(0);
         set_readonly (1);
      }
   }

   if(tag_file_time)
   {
      flush("Tag file loaded from " + Tag_Dir + Tag_File);
      return 1;
   }
   else
   {
      return 0;
   }
}

% ******************************************************************************

static define _find_tag()
{
   if(find_current_tagfile() == 0)
   {
      % Problem with this as it stands is that you could end up
      % tagging an entire filesystem, so we try to exclude big default
      % directories.

      flush("Cannot locate tag file - running ctags");

      system("ctags --recurse=yes " +
             "--exclude=bin --exclude=opt --exclude=usr --exclude=dev --exclude=etc --exclude=lib " +
             "--exclude=lib64 --exclude=mnt --exclude=proc --exclude=snap --exclude=sbin --exclude=root " +
             "--exclude=home --exclude=sys --exclude=usr --exclude=var 2>/dev/null");

      if(find_current_tagfile() == 0)
      {
         error("Cannot locate or create tag file");
      }
   }

   setbuf(TAGS_BUFFER);
   bob ();

   return find_next_tag();
}

% ******************************************************************************
% Find the first instance of the tag under the cursor, optionally asking the
% user if that is what they want to do
% ******************************************************************************

define find_tag_string(askflag)
{
   variable tagchars, cursortag, dot_flag = 0;;

   cbuf = whatbuf ();
   tagchars = "0-9A-Z_a-z";

#ifdef VMS
   tagchars = strcat (tagchars, "$");
#endif

   found = 0;

   % Save our current position
   push_spot ();

   % Skip whitespace then skip tag characters to the left,
   % set a mark and skip them to the right.
   skip_white ();
   bskip_chars (tagchars);
   push_mark ();
   skip_chars (tagchars);

   % Include '.xxx' in the tag string, if present

   if(looking_at("."))
   {
      if(right(1))
      {
         skip_chars(tagchars);
         dot_flag = 1;
      }
   }

   cursortag = bufsubstr();

   % Restore the original cursor position

   pop_spot ();

   if(askflag)
   {
      if (MINIBUFFER_ACTIVE)
      {
         error("Minibuffer active!");
      }

      tag = read_mini ("Find tag:", cursortag, Null_String);
      tag = strtrim (tag);

      if(cursortag == tag)
      {
         % Tag search is case-sensitive if matching against a strng
         % in the buffer, except where the string contains a '.'
         % as that is probably a header file name, which isn't
         % case-sensitive on Windows.

         case_search = 1;

         if(dot_flag)
         {
            case_search = 0;
         }
      }
   else
      {
         case_search = 0;
      }
   }

   !if (strlen (tag))
      error("No tag specified!");

   _find_tag();
}

% ******************************************************************************
% Find the first instance of the tag under the cursor, prompting the user
% to confirm that's what they want.
% ******************************************************************************

define find_tag ()
{
   tag_count = 0;

   tag_mark = create_user_mark();

   find_tag_string(1);
}

% ******************************************************************************
% Find the first instance of the specified tag
% ******************************************************************************

define find_tag_text(tagname)
{
   tag_count = 0;
   cbuf      = whatbuf ();
   tag       = tagname;

   return _find_tag();
}

% ******************************************************************************
% Return to where we went to the tag from
% ******************************************************************************

define return_after_find_tag()
{
   goto_user_mark(tag_mark);
}
