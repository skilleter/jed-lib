%==========================================================================
% occurmode.sl
%
% Create and initialize the syntax table for occur searching
%
%==========================================================================

create_syntax_table ("OCCUR");
set_syntax_flags("OCCUR", 1);

%define_syntax ("/*", "*/", '%', "OCCUR");
define_syntax ("([{", ")]}", '(', "OCCUR");
%define_syntax ('"', '"', "OCCUR");
%define_syntax ('\'', '\'', "OCCUR");
%define_syntax ('\\', '\\', "OCCUR");
define_syntax ("0-9a-zA-Z_", 'w', "OCCUR");     % words

%define_syntax (",;.?:", ',', "OCCUR");
%define_syntax ('#', '#', "OCCUR");
define_syntax ("$@%-+/&*=<>|!~^\\`?()[]{}", '+', "OCCUR");

define occur_mode ()
{
        set_mode ("OCCUR", 0);
        use_syntax_table ("OCCUR");
   set_comment_info("OCCUR", NULL, NULL, NULL, 0);

        run_mode_hooks ("occur_mode_hook");
}
