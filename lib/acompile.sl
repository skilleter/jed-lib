% Changes: Callback function that can be called when compilation
%          finished Compile() prompts user for compile command using
%          default Switches to invocation directory prior to
%          compilation

% Asynchronous compilation

% ******************************************************************************
% ******************************************************************************

variable Compile_Callback = &NULL;

static variable old_horizontal_pan = 0;
public variable Compile_Process_Id = -1;

static variable exec_time;

% ******************************************************************************
% Added callback function to be execution on compilation finished
% ******************************************************************************

define compile_set_callback(callback_function)
{
   Compile_Callback = callback_function;
}

% ******************************************************************************
% ******************************************************************************

private define compile_set_status_line (state)
{
   set_mode ("compile" + state, 0);
}

% ******************************************************************************
% ******************************************************************************

private define compile_signal_handler (pid, flags, status)
{
   variable min, sec;
   variable str = aprocess_stringify_status (pid, flags, status);

   exec_time = _time() - exec_time;
   min = exec_time/60;
   sec = exec_time-min*60;

   HORIZONTAL_PAN = old_horizontal_pan;
   eob ();
   vinsert ("\n\nProcess finished at %s, run time=%d:%02d : %s\n", time(), min, sec, str);

   % Get rid of any cabbage reruns that weren't interpreted as line
   % ends along with junk and excessively blank lions.

   bob();
   replace("\r", "");
   strip_control_codes();
   trim_buffer();

   % Move back to the top for the error parsing.

   bob();

   compile_set_status_line (str);

   if (flags != 2) Compile_Process_Id = -1;

   % Call callback function, if defined, then reset it

   if(Compile_Callback != NULL)
   {
      @Compile_Callback();
      Compile_Callback = NULL;
   }

   complilation_reset_bufpos_hack=1;
}

% ******************************************************************************

private define compile_start_process (cmd)
{
   variable dir, name, file, flags;
   variable shell, shopt;

   if (cmd == NULL)
     cmd = read_mini ("Compile command (using "+current_compiler_name+"):", "", Compile_Last_Compile_Cmd);
   ifnot (strlen (cmd))
     return;

   if(cmd != "")
      Compile_Last_Compile_Cmd = cmd;

   % Originally, the code below switched to the directory where the
   % current buffer was loaded from before building Instead, we'll
   % switch back to the directory that was current when the editor was
   % invoked.

   % (,dir,,) = getbuf_info ();

   (,dir,,) = getbuf_info ("*scratch*");

   if (change_default_dir (dir))
     error ("Unable to chdir.");

   pop2buf (Compile_Output_Buffer);

   set_readonly (0);
   erase_buffer ();
   log_mode();
   (file,,name,flags) = getbuf_info ();
   setbuf_info (file, dir, name, flags);
   Compile_Line_Mark = 0;

   compile_set_status_line ("");
   insert (cmd); newline ();

#ifdef WIN32
   shopt = "";
   shell = "";
#else
   shell = getenv ("SHELL");
   if (shell == NULL) shell = "sh";
   shopt = "-c";
#endif

   exec_time = _time();

   old_horizontal_pan = HORIZONTAL_PAN;
   HORIZONTAL_PAN = 1;

   Compile_Process_Id = open_process (shell,shopt, cmd, 2);

   if (Compile_Process_Id == -1)
     error ("Unable to start subprocess.");

   compile_set_status_line (" run");

   set_process (Compile_Process_Id, "signal", &compile_signal_handler);
   set_process (Compile_Process_Id, "output", "");
}

% ******************************************************************************

public define compile ()
{
   variable b, n;
   variable cmd = NULL;

   debugset("COMPILE", 1);

   if (_NARGS == 1)
     cmd = ();

   % Read compile command from user with default as passed in cmd
   % parameter

   cmd = read_mini ("Compile command (using "+current_compiler_name+") :", cmd, Compile_Last_Compile_Cmd);

   if(cmd != "")
   {
      Compile_Last_Compile_Cmd = cmd;
   }

   Compile_Output_Buffer = COMPILE_BUFFER;

   if (Compile_Process_Id != -1)
   {
      if (bufferp (Compile_Output_Buffer))
      error ("A compile process is already running.");
        try kill_process (Compile_Process_Id);
        catch RunTimeError;
      Compile_Process_Id = -1;
   }

   b = whatbuf();
   call ("save_some_buffers");

   debugmsg("COMPILE", "--------------------------------------------------------------------------------");
   debugmsg("COMPILE", sprintf("Compiling '%s' into buffer '%s'", cmd, COMPILE_BUFFER));

   compile_start_process (cmd);

   pop2buf(b);

   %compile_parse_errors ();
}
