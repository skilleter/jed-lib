% ******************************************************************************
% Source Control functions
% Adapted from (much) earlier MKS source control functions
% ******************************************************************************

static variable currentfile;
static variable currentbuf;
static variable currentline, currentcol;
static variable currentflags;

%******************************************************************************
% reread_file()
%
% Reload the specified buffer from disk, discarding any unsaved modifications
%******************************************************************************

static define reread_file()
{
        flush("Reloading '"+currentfile+"'");

        sw2buf(currentbuf);
   set_buffer_modified_flag(0);
   delbuf(currentbuf);
   () = find_file(currentfile);

   goto_line(currentline);
   goto_column(currentcol);
}

%******************************************************************************
% scs_shell_cmd()
%
% Save any modified buffers and run the specified shell command
%******************************************************************************

static define scs_shell_cmd(cmdline)
{
        variable msg;

        flush("Saving modified buffers & running '"+cmdline+"'");
        usleep(100);

        save_buffers();

        % We put the output into the scratch buffer

   sw2buf("*scratch*");
        erase_buffer ();

        insert(cmdline);
        newline();
        newline();

        % Run the Source Control command

        () = run_shell_cmd(cmdline);

        % Extract the response from Source Control

        bob();
        push_mark();
        eol();
        msg = bufsubstr();
        pop_mark(0);

        % Re-read the file

        reread_file();

        % Display the result message from mks

        flush(msg);
}

% ******************************************************************************
% get_comment()
%
% Prompt for a comment, returning the last comment if nothing better is
% entered.
% ******************************************************************************

static variable last_comment = "";

static define get_comment(prompt)
{
        variable comment;

        comment = read_mini(prompt, last_comment, "");

        if(comment!="")
        {
                last_comment = comment;
        }

        return last_comment;
}

%******************************************************************************
% checkout()
%
% Check-out the current file from the source control system
%******************************************************************************

static define checkout()
{
        variable comment;

        comment = get_comment("Checkout comment for '"+currentfile+"'");

        scs_shell_cmd("ct co -c \"" + comment + "\" " + currentfile);

        reread_file();
}

%******************************************************************************
% checkin()
%
% Check the current file back into the source control system
%******************************************************************************

static define checkin()
{
        variable comment;

        comment = get_comment("Checkin comment for '"+currentfile+"'");

        scs_shell_cmd("ct ci -c \"" + comment + "\" " + currentfile);

        reread_file();
}

% ******************************************************************************
% uncheckout()
%
% Cancel the current file's checkouted status
% ******************************************************************************

static define uncheckout()
{
        scs_shell_cmd("ct unco " + currentfile);

        reread_file();
}

% ******************************************************************************
% scsdiff()
%
% Diff between the current file and the version of the file in the SCS
% ******************************************************************************

static define scsdiff()
{
        error("Sorry, I'm still working on this!");
}

%******************************************************************************
% scs_menu()
%
% Entry point for the module - displays a menu of source control options
% and waits for the user to decide what they want to do!
%******************************************************************************

define scs_menu()
{
        variable c;
        variable file, dir;
        variable menu;

        % The default menu format

        menu = "Source control: O: Check-out  I: Check-in  U: Un-check-out D:Diff >";

        % Prompt the user

        !if (input_pending(1)) flush (menu);
   c = toupper (getkey());

        % Whaddya wannado?

   switch (c)
        { case 'O' : checkout();        }
        { case 'I' : checkin();         }
        { case 'U' : uncheckout();      }
        { case 'D' : scsdiff();         }
        { case 'Q' : return;                    }
   {                     beep();
                                         clear_message ();      }
}

