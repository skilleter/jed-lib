#d linuxdoc <!doctype linuxdoc system>
#d begin#1 <$1>
#d end#1 </$1>
#d tt#1 <tt>$1</tt>
#d bf#1 <bf>$1</bf>
#d em#1 <em>$1</em>
#d title <title>
#d author <author>
#d date <date>
#d toc <toc>
#d section#1 <sect>$1<p>
#d subsection#1 <sect1>$1<p>
#d subsubsection#1 <sect2>$1<p>
#d % &percnt
#d label#1 <label id="$1">
#d ref#2 <ref id="$1" name="$2">
#d item <item>
