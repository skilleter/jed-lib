% This is a simple shell mode.  It does not defined any form of indentation
% style.  Rather, it simply implements a highlighting scheme.

% ******************************************************************************
% Move to the top of a function
% ******************************************************************************

define sh_top_of_thing()
{
   variable topline;

   % Search for a '{' at the start of the line

   push_spot();

   if(bol_bsearch("{") != 0)
   {
      if(what_line() > topline or topline == -1)
      {
         topline = what_line();
      }
   }

   pop_spot();

   % If we have found something suitable, then go there

   if(topline > 0)
   {
      goto_line(topline);
      emacs_recenter();
   }
   else
   {
      error("Can't find the top of anything!");
   }
}

% ******************************************************************************
% Move to the end of a function
% ******************************************************************************

define sh_end_of_thing()
{
   variable botline=-1;

   % Search downwards for a '} at the start of a line

   push_spot();
   eol();

   if(bol_fsearch("}") != 0)
   {
      if(what_line() < botline or botline==-1)
      {
         botline = what_line();
      }
   }

   pop_spot();

   % If we have found something suitable, then go there

   if(botline > 0)
   {
      goto_line(botline);
      emacs_recenter();
   }
   else
   {
      error("Can't find the bottom of anything!");
   }
}

$1 = "SH";

create_syntax_table ($1);

define_syntax ("#", "", '%', $1);
define_syntax ("([{", ")]}", '(', $1);

% Unfortunately, the editor cannot currently correctly deal with multiple
% string characters.  So, inorder to handle something like:
%    echo "I'd rather be home"
% make the '"' character the actual string character but also give '\''
% a string syntax.  However, this will cause '"' to give problems but
% usually, '"' characters will be paired.
define_syntax ('\'', '"', $1);
define_syntax ('"', '"', $1);

define_syntax ('\\', '\\', $1);
define_syntax ("-0-9a-zA-Z_", 'w', $1);        % words
define_syntax ("-+0-9", '0', $1);   % Numbers
define_syntax (",;:", ',', $1);
define_syntax ("%-+/&*=<>|!~^$", '+', $1);

#ifdef HAS_DFA_SYNTAX

%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache ("shmode.dfa", name);

   dfa_define_highlight_rule ("\\\\.", "normal", name);
   dfa_define_highlight_rule ("#.*$", "comment", name);
   dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*\"", "string", name);
   dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*$", "string", name);
   dfa_define_highlight_rule ("'[^']*'", "string", name);
   dfa_define_highlight_rule ("'[^']*$", "string", name);
   dfa_define_highlight_rule ("[\\|&;\\(\\)<>]", "Qdelimiter", name);
   dfa_define_highlight_rule ("[\\[\\]\\*\\?]", "Qoperator", name);
   dfa_define_highlight_rule ("[^ \t\"'\\\\\\|&;\\(\\)<>\\[\\]\\*\\?]+",
                          "Knormal", name);
   dfa_define_highlight_rule (".", "normal", name);
   dfa_build_highlight_table (name);
}

dfa_set_init_callback (&setup_dfa_callback, "SH");

%%% DFA_CACHE_END %%%
#endif

% Misc. keywordy-type things

append_keywords($1, 0,
                "match", "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$9", "$0", "$#");

% Internal commands and control structures

append_keywords ($1, 1,
                 "do", "fi", "if", "in", "for", "let", "nohup",
                 "case", "done", "elif", "else", "esac", "ifeq","test", "then", "type",
                 "break", "endsw", "ifdef", "ifneq", "local", "shift", "unset", "until", "while",
                 "ifndef", "return", "setenv", "source", "switch", "breaksw", "continue", "function",
                 "readonly", "select", "alias", "shopt", "ulimit", "declare");

% Bash built-ins

append_keywords($1, 2,
                "pwd", "set", "cd", "pushd", "popd", "export", "printf", "echo", "expr",
                "lockfile", "rm", "cat", "mv", "cp", "find", "touch", "complete", "read",
                "umask", "printenv", "builtin", "unalias", "su", "mkdir", "dirname",
                "basename", "rsync", "trap", "sudo", "disown", "sleep", "readarray",
                "cut", "exec", "compgen", "stty", "command", "ls", "getopts", "ifconfig",
                "nice", "time", "rmmod", "insmod", "mknod", "chmod", "chown", "chgrp", "ln", "man",
                "which", "true", "false", "eval", "exit");

% Popular extern commands

append_keywords($1, 3,
                "tput", "dcop", "rsh", "rcp", "telnet", "rlogin", "ftp", "ping", "disk",
                "mail", "finger", "help", "zip", "unzip", "compress", "uncompress", "date",
                "gzip", "gunzip", "bzip2", "bunzip2", "perl", "python", "python3", "less", "getopt",
                "cleartool","make", "ct", "uniq", "tail", "sort", "hexdump", "clear",
                "xclearcase", "mount", "swapon", "syslogd", "dhcpcd", "portmap", "utelnetd",
                "umount", "losetup", "killall", "mdev", "repo", "ssh-agent", "ssh-add",
                "readlink", "m4", "konqueror", "grep", "egrep", "sed", "xargs", "tar", "nroff",
                "endview", "setview", "lsvob", "ci", "co", "lshistory", "findmerge", "unco",
                "lslock", "mkview", "setcs", "rmview", "lsprivate", "mkelem", "rmname",
                "protect", "stat", "unregister", "rmtag", "awk", "readelf", "tee", "dd", "ddd",
                "md5sum", "rdesktop", "objdump", "xxd", "tr", "git", "uname", "timeout",
                "wget", "java", "rgrep", "fgrep", "docker", "ps", "strace", "diff", "aws",
                "envsubst", "useradd", "groupadd", "yum", "apt", "curl", "pip", "cmp",
                "apt-get", "puppet", "service", "more", "less", "nala", "usermod");

% Bash variables

append_keywords($1, 4,
                "BASH", "BASH_ARGC", "BASH_ARGV", "BASH_COMMAND", "BASH_EXECUTION_STRING", "BASH_REMATCH",
                "BASH_LINENO", "BASH_SOURCE", "BASH_SUBSHELL", "BASH_VERSINFO", "BASH_VERSION",
                "COMP_CWORD", "COMP_LINE", "COMP_POINT", "COMP_WORDBREAKS", "COMP_WORDS", "DIRSTACK",
                "EUID", "FUNCNAME", "GROUPS",
                "HISTCMD", "HOSTNAME", "HOSTYPE", "LINENO", "MACHTYPE", "OLDPWD", "OPTARG", "OPTIND",
                "OSTYPE", "PIPESTATUS", "PPID", "PWD", "RANDOM", "REPLY", "SECONDS", "SHELLOPTS",
                "SHLVL", "UID", "BASH_ENV", "CDPATH", "COLUMNS", "COMPREPLY", "EMACS", "FCEDIT",
                "FIGNORE", "GLOBIGNORE", "HISTCONTROL", "HISTFILE", "HISTFILESIZE", "HISTIGNORE",
                "HISTSIZE", "HISTTIMEFORMAT", "HOME", "HOSTFILE", "IFS", "IGNOREEOF", "INPUTRC",
                "LANG", "LC_ALL", "LC_COLLATE", "LC_CTYPE", "LC_MESSAGES", "LC_NUMERIC", "LINES",
                "MAIL", "MAILCHECK", "MAILPATH", "OPTERR", "PATH", "POSXILY_CORRECT", "PROMPT_COMMAND",
                "PS1", "PS2", "PS3", "PS4", "SHELL", "TIMEFORMAT", "TMOUT", "auto_resume", "histchars",
                "LD_LIBRARY_PATH", "KONSOLE_DCOP_SESSION", "CLEARCASE_ROOT");

% Unused

append_keywords($1, 5, "");

% External Thingy functions

append_keywords($1, 5,
                "docker-purge", "trimpath", "xchmod", "yaml");

% Unused

append_keywords($1, 6, "");

% Unused

append_keywords($1, 7, "");

% WiSki help text nroffy stuff

append_keywords($1, 6,
                "TH", "SH", "TP", "fB", "fP", "NAME", "SYNOPSIS", "DESCRIPTION", "EOF", "OPTIONS");

!if (keymap_p ($1)) make_keymap ($1);

definekey ("sh_top_of_thing",     MetaKey+Key_Up,   $1);
definekey ("sh_end_of_thing",     MetaKey+Key_Down, $1);

define sh_mode ()
{
   set_mode("SH", 0);
   use_syntax_table ("SH");
   use_keymap("SH");
   use_dfa_syntax(0);
%   mode_set_mode_info ("SH", "fold_info", "#{{{\r#}}}\r\r");
   mode_set_mode_info ("SH", "fold_info", "{\r}\r");
   set_comment_info("SH", "#", NULL, NULL, 0);
   run_mode_hooks("sh_mode_hook");
}
