variable filename = "/tmp/jed-fifo";

define fifo_test_function()
{
   % Attempt to read data from the FIFO, convert it into a list of
   % filenames then attempt to load the files specified.

   variable filestat;

   % See if the FIFO exists

   filestat = stat_file(filename);

   if(filestat == NULL)
   {
      error("FIFO does not exist");
   }
   else
   {
      if(not stat_is("fifo", filestat.st_mode))
      {
         error("File exists, but is not a FIFO");
      }
   }

   % Read the list of files from the FIFO

   variable fifo;

   fifo = open(filename, O_RDONLY|O_TEXT|O_NONBLOCK);

   ERROR_BLOCK
   {
      close(fifo);
   }

   if( fifo == NULL)
   {
      error("Unable to open FIFO");
   }

   while(1)
   {
      variable size, buf="";

      size = read(fifo, &buf, 4096);

      if(size < 0)
      {
         flush(sprintf("Error (%d) %d reading data from FIFO", size, errno));
         continue;
      }

      if(size == 0)
      {
         flush("No data read from the FIFO");
         continue;
      }

      flush(sprintf("Read %d characters = %s", size, buf));

      % Split the file list into an array of lines

      variable fifo_lines;

      fifo_lines = strchop(buf, '\n', 0);

      % Read each line (ignore null filenames caused by
      % superfluous/trailing \n characters

      foreach(fifo_lines)
      {
         variable filelist;

         % Split each line into a list of files (optionally double-quoted)

         filelist = strchop((), ' ', '"');

         foreach(filelist)
         {
            variable file=();

            if(file != "")
            {
               filestat = stat_file(file);

               if(filestat != NULL && stat_is("dir", filestat.st_mode))
               {
                  flush(sprintf("'%s' is a directory!", filename));
               }
               else
               {
                  find_file(file);
               }
            }
         }
      }
      break;
   }
}
