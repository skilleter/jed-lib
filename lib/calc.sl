%------------------calc.sl-------------------------------------
%% Simple SLang calculator
%% Version: 1.2
%% Author:  Marko Mahnie <marko.mahnic@usa.net>
%%
%% Usage:  autoload ("calc", "calc", "calc2");
%%         M-x calc
%%      or
%%         M-x calc2
%%      for two-window mode.
%%
%% A simple calculator that takes a SLang expression, evaluates
%% it and prints the result in the form of a comment after the
%% expression.
%%
%% If you use it with two windows, one window (Calculator) is used
%% to edit expressions, in the other one (*calcres*) the results
%% are displayed.
%%
%% The result of an expression is everything that is left on the
%% SLang stack after the expression is evaluated.
%% The result of evaluation (^A) of
%%    1+1; 2+2;
%% would be
%%    2
%%    4,
%% but only 4 is written into the expression buffer. The other
%% results can be found in the result buffer (^C^W).
%%
%%
%% An expression can be any valid SLang code. Multiple expressions
%% can be divided by a tag (^C^N).
%%
%% There are 25 global variables defined (a-z) that can be displayed
%% with (^C^V).
%%
%% Use ^H for help on keys.

static variable buf_expr = "Calculator";
static variable buf_result = "*calcres*";
static variable history_tag = "%-------------- :-)";
static variable history_file = "";
static variable format = "%.6f";
static variable exprid = 0;
static variable use_result_win = 0;
static variable use_result_comment = 1; % 0 never, 1 not for arrays, 2 allways

variable
  a = 0, b = 0, c = 0, d = 0, e = 0,
  f = 0, g = 0, h = 0, i = 0, j = 0,
  k = 0, l = 0, m = 0, n = 0, o = 0,
  p = 0, q = 0, r = 0, s = 0, t = 0,
  u = 0, v = 0, w = 0, x = 0, y = 0,
  z = 0;

static define calc_select_expression_buf ()
{
   pop2buf (buf_expr);
}

static define history_next ()
{
   re_fsearch ("^" + history_tag);
}

static define history_prev ()
{
   re_bsearch ("^" + history_tag);
}

define calc_next_expression ()
{
   eob();
   !if (bobp())
   {
      variable empty = 0;

      while (not empty)
      {
         go_up(1);
         bol();
         empty = (re_looking_at("^[ \t]*$"));
         eob();
         !if (empty) insert ("\n");
      }
   }

   exprid++;
   vinsert ("%s   E%d\n", history_tag, exprid);
}

static define calc_display_value(val, linepref);
static define calc_display_value(val, linepref)
{
   if (typeof (val) == String_Type)
   {
      vinsert ("%s\"%s\"", linepref, string (val));
      return;
   }
   else if (typeof (val) != Array_Type)
   {
      vinsert ("%s%s", linepref, string (val));
      return;
   }

   variable i, j;
   variable dims, num_dims, data_type;
   (dims, num_dims, data_type) = array_info (val);

   vinsert ("%sArray: %s\n", linepref, string (val));
   if (num_dims == 1)
   {
      calc_display_value (val[0], linepref);
      for (i = 1; i < dims[0]; i++)
      {
         calc_display_value (val[i], ", ");
      }
   }
   else if (num_dims == 2)
   {
      for (i = 0; i < dims[0]; i++)
      {
         calc_display_value (val[i, 0], linepref + "> ");
         for (j = 1; j < dims[1]; j++)
         {
            calc_display_value (val[i, j], ", ");
         }
         if (i < dims[0]-1) insert("\n");
      }
   }
   else if (num_dims > 2)
   {
      calc_display_value (sprintf (" :( %d-D array ", num_dims), linepref);
   }
}

static define calc_display_stack ()
{
   variable res;

   if (_stkdepth () < 1)
   {
      insert ("\t---\n");
      res = "---";
   }
   else
   {
      _stk_reverse (_stkdepth());
      while (_stkdepth ())
      {
         % res = string (());
         % insert ("\t" + res + "\n");
         res = ();
         calc_display_value (res, "\t");
         insert ("\n");
      }
   }

   insert("\n");
   recenter (window_info ('r'));

   return (res);
}

define calc_display_variables ()
{
   pop2buf (buf_result);
   eob ();
   variable iii, sss, ccc, www = SCREEN_WIDTH / 2 + 2;
   insert ("Variables:\n");
   for (iii = 'a'; iii <= 'm'; iii++)
   {
      insert (sprintf ("    %c: %s", iii, string (eval (sprintf ("%c",iii)))));
      ccc = what_column();
      if (ccc < www) insert_spaces (www - ccc);
      else insert (" | ");

      insert (sprintf ("%c: %s\n", iii+13, string (eval (sprintf ("%c",iii+13)))));
   }

   insert("\n");
   recenter (window_info ('r'));

   calc_select_expression_buf();
}

define calc_result_window ()
{
   pop2buf (buf_result);
   eob();
   recenter (window_info ('r'));
   calc_select_expression_buf();
}

define calc_make_calculation ()
{
   variable expr, id = "";

   _pop_n (_stkdepth());

   sw2buf (buf_expr);

   push_spot();
   eol ();
   !if (history_prev()) bob();
   else
   {
      go_right (strlen(history_tag));
      skip_white();
      !if (eolp())
      {
         push_mark();
         eol();
         id = strtrim (bufsubstr());
      }
      bol();
   }
   push_mark();
   eol ();
   !if (history_next()) eob();
   expr = bufsubstr();
   pop_spot();

   eval (expr);

   if (use_result_win) pop2buf (buf_result);
   else setbuf (buf_result);

   eob ();

   if (id == "") insert ("R:\n");
   else vinsert ("R(%s):\n" ,id);

   variable lastres = calc_display_stack();

   calc_select_expression_buf();

   %% Display the last result in expression buffer
   if (not use_result_win or use_result_comment)
   {
      push_spot();
      !if (history_next())
      {
         pop_spot();
         calc_next_expression();
         push_spot();
         history_prev();
      }

      go_up(1); bol();
      while (not bobp() and (re_looking_at("^[ \t]*$")))
      {
         go_up(1); bol();
      }
      eol();

      % vinsert ("\n\t%%R:  %s", lastres);
      insert ("\n");

      if (use_result_comment < 2 and typeof(lastres) == Array_Type)
         calc_display_value ("Array...", "\t%R:  ");
      else
         calc_display_value (lastres, "\t%R:  ");

      pop_spot();
   }
}


static define calc_find_max_id ()
{
   variable id = 0;

   setbuf (buf_expr);
   push_spot();
   bob();
   exprid = 0;

   while (history_next())
   {
      go_right (strlen(history_tag));
      skip_white();
      !if (eolp())
      {
         push_mark();
         eol();
         if (1 == sscanf (strtrim (bufsubstr()), "E%d", &id))
            if (id > exprid) exprid = id;
      }
   }

   pop_spot();
}


define calc_read_file ()
{
   setbuf (buf_expr);
   erase_buffer();
   history_file = read_with_completion("Read file:", "", "", 'f');

   () = insert_file (history_file);
   set_buffer_modified_flag (0);
   bob();
   calc_find_max_id();
}

define calc_write_file ()
{
   setbuf (buf_expr);
   history_file = read_with_completion("Write to file:", "", "", 'f');

   push_spot();
   mark_buffer();
   () = write_region_to_file (history_file);
   pop_spot();
}

define calc_float_format ()
{
   format = read_mini ("Float format:", format, "");
   set_float_format (format);
}

define calc_help ()
{
   variable shlp = "^A Evaluate  ^C^F Format  ^C^V Variables  ^C^N New  ^C^S Save  ^C^R Read";

   !if (use_result_win) shlp = shlp + "  ^C^W Results";
   message (shlp);
}

static define calc_prepare_keymap ()
{
   $1 = "SLangCalc";
   !if (keymap_p ($1))
   {
      copy_keymap ($1, "C");
      definekey ("calc_make_calculation", "^A", $1);
      definekey ("calc_display_variables", "^C^V", $1);
      definekey ("calc_next_expression", "^C^N", $1);
      definekey ("calc_read_file", "^C^R", $1);
      definekey ("calc_write_file", "^C^S", $1);
      definekey ("calc_result_window", "^C^W", $1);
      definekey ("calc_float_format", "^C^F", $1);
      definekey ("calc_help", "^H", $1);

      % definekey ("calc_make_calculation", "^M", $1);   % Return
      % definekey ("newline", "^[^M", $1);               % Alt-Return
   }

   use_keymap ($1);
}

static define init_menu (menu)
{
   menu_append_item (menu, "&Evalute", "calc_make_calculation");
   menu_append_item (menu, "&Variables", "calc_display_variables");
   menu_append_item (menu, "Result &window", "calc_result_window");
   menu_append_item (menu, "&New expression", "calc_next_expression");
   menu_append_item (menu, "&Float format", "calc_float_format");
   menu_append_item (menu, "&Read expressions", "calc_read_file");
   menu_append_item (menu, "&Save expressions", "calc_write_file");
}

static define calc_start ()
{
   if (_NARGS > 0) use_result_win = ();

   if (use_result_win)
   {
      onewindow();
      splitwindow();

      % Top window: results
      if (window_info('t') != 1) otherwindow();
      sw2buf (buf_result);

      % Bottom window: expressions
      % otherwindow();
   }

   pop2buf (buf_expr);
   slang_mode();

   set_mode ("SLangCalc", 2 |8);
   mode_set_mode_info ("SLangCalc", "init_mode_menu", &init_menu);
   calc_prepare_keymap();

   run_mode_hooks ("calc_mode_hook");

   set_float_format (format);
   if (bobp() and eobp())
   {
      calc_next_expression();
   }
}


define calc ()
{
   calc_start (0);
}

define calc2 ()
{
   calc_start (1);
}
