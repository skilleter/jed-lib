% This is a simple m4 mode.  It does not defined any form of indentation
% style.  Rather, it simply implements a highlighting scheme.
% This is all something of a kludge as m4 is a preprocessor that is run
% on files in some other language/format so I've incorporarted some aspects
% of the types of file I usually run m4 on.
%
% Also, I've assumed use of the 'm4_' prefix on m4 macros which I normally
% use to avoid namespace conflicts with the language of the file I'm running
% m4 on.

$1 = "m4";

create_syntax_table ($1);

define_syntax ("#", "", '%', $1);
define_syntax ("([{", ")]}", '(', $1);

% Can't express backtick/single-quoted strings in M4 since the it uses different
% start/end delimiters and can't express single-quoted strings due to
% m4 syntax.

%define_syntax ('\'', '"', $1);
define_syntax ('"', '"', $1);
%define_syntax ('`', '"', $1);

define_syntax ('\\', '\\', $1);
define_syntax ("-0-9a-zA-Z_", 'w', $1);        % words
define_syntax ("-+0-9x", '0', $1);   % Numbers
define_syntax (",;:", ',', $1);
define_syntax ("${}%-+/&*=<>|!~^`@?.", '+', $1);

#ifdef HAS_DFA_SYNTAX

%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache ("m4mode.dfa", name);

   dfa_define_highlight_rule ("\\\\.", "normal", name);
   dfa_define_highlight_rule ("^#.*$", "comment", name);
   dfa_define_highlight_rule (" #.*$", "comment", name);
%   dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*\"", "string", name);
%   dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*$", "string", name);
   dfa_define_highlight_rule ("`[^']*'", "string", name);
%   dfa_define_highlight_rule ("'[^']*$", "string", name);
   dfa_define_highlight_rule ("[\\|&;\\(\\)<>]", "Qdelimiter", name);
   dfa_define_highlight_rule ("[\\[\\]\\*\\?]", "Qoperator", name);
   dfa_define_highlight_rule ("[^ \t\"'\\\\\\|&;\\(\\)<>\\[\\]\\*\\?]+", "Knormal", name);
   dfa_define_highlight_rule (".", "normal", name);
   dfa_build_highlight_table (name);
}

dfa_set_init_callback (&setup_dfa_callback, "m4");

%%% DFA_CACHE_END %%%
#endif

% Internal commands and control structures

append_keywords ($1, 1,
                 "m4_define", "m4_ifelse", "m4_format", "m4_undefine", "m4_defn",
                 "m4_pushdef", "m4_popdef", "m4_indir", "m4_builtin", "m4_ifdef",
                 "m4_shift", "m4_dumpdef", "m4_traceon", "m4_traceoff", "m4_debugmode",
                 "m4_debugfile", "m4_dnl", "m4_changequote", "m4_changecom", "m4_m4wrap",
                 "m4_include", "m4_sinclude", "m4_divert", "m4_undivert", "m4_divnum",
                 "m4_len", "m4_index", "m4_regexp", "m4_substr", "m4_translit", "m4_patsubst",
                 "m4_incr", "m4_decr", "m4_eval", "m4_syscmd", "m4_esyscmd", "m4_sysval",
                 "m4_mkstemp", "m4_maketemp", "m4_errprint", "m4_exit");

append_keywords ($1, 3,
                 "define", "ifelse", "format", "undefine", "defn",
                 "pushdef", "popdef", "indir", "builtin", "ifdef",
                 "shift", "dumpdef", "traceon", "traceoff", "debugmode",
                 "debugfile", "dnl", "changequote", "changecom", "m4wrap",
                 "include", "sinclude", "divert", "undivert", "divnum",
                 "len", "index", "regexp", "substr", "translit", "patsubst",
                 "incr", "decr", "eval", "syscmd", "esyscmd", "sysval",
                 "mkstemp", "maketemp", "errprint", "exit");

%

append_keywords($1, 2, "__file__", "__gnu__", "__line__", "__os2__", "__program__", "__unix__", "__windows__");

append_keywords($1, 3, "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$9", "$#", "$*", "$@");

!if (keymap_p ($1)) make_keymap ($1);

define m4_mode ()
{
   set_mode("m4", 0);
   use_syntax_table ("m4");
   use_keymap("m4");
   set_comment_info("m4", "#", NULL, NULL, 0);
   mode_set_mode_info ("m4", "fold_info", "#{{{\r#}}}\r\r");
   run_mode_hooks("m4_mode_hook");
}

