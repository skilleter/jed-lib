% Create and initialize the syntax tables.

create_syntax_table ("S");

define_syntax ("# ", "", '%', "S");
define_syntax ("//", "", '%', "S");
define_syntax ("/*", "*/", '%', "S");
define_syntax ("([{", ")]}", '(', "S");
define_syntax ('"', '"', "S");
define_syntax ('\'', '\'', "S");
define_syntax ('\\', '\\', "S");
define_syntax (".0-9a-zA-Z_", 'w', "S");
define_syntax ("-+0-9a-fA-FxXUL", '0', "S");
define_syntax (",;.?:", ',', "S");
define_syntax ('#', '#', "S");
define_syntax ("%-+/&*=<>|!~^", '+', "S");

set_syntax_flags ("S", 0x4|0x40);

append_keywords("S", 0, ".macro", ".endm", ".set", ".fill", ".comm");
append_keywords("S", 1, "t0", "t1", "t2", "a0", "a1", "a2", "a3", "sp", "zero");
append_keywords("S", 2, "jr", "dli", "dmtc0", "dsrl", "or", "mtc0", "li", "dsll", "tlbwi", "zero",
                        "_ehb", "mfc0", "xori", "xor", "ori", "j", "jal", "andi", "beqz");
append_keywords("S", 3, "defined");
define s_mode ()
{
        set_abbrev_mode (1);

        set_mode ("S", 2);
        %use_keymap ("S");
        use_syntax_table ("S");
   set_comment_info("S", "#", "/*", "*/", 0);

        run_mode_hooks ("s_mode_hook");
}

