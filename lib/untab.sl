% untabify region function

define entab()
{
   variable c, count=0;

   check_region (0);
   narrow ();
   bob ();

   USE_TABS;
   USE_TABS=1;

   while (fsearch_char (' '))
   {
      c = what_column ();
      push_mark ();
      skip_white ();
      c = what_column () - c;

      if (c > 1)
      {
         del_region ();
         whitespace (c);
         count+=1;
      }
      else
         pop_mark_0 ();
   }
   USE_TABS=();
   widen ();
}

%!%+
%\function{untab}
%\synopsis{untab}
%\usage{Void untab ();}
%\description
% This function may be used either to convert tabs to spaces or, if called
% with a prefix argument, it will perform the opposite conversion from
% spaces to tabs.  This function operates on a region.
%!%-
define untab ()
{
   if (-1 != prefix_argument (-1))
   {
      entab();
   }
   else
   {
      check_region (0);
      narrow ();
      bob ();
      USE_TABS;
      USE_TABS = 0;

      while (fsearch ("\t"))
      {
         skip_white ();
         what_column ();   % on stack
         bskip_chars ("\t ");
         () - what_column ();   % on stack
         trim ();
         whitespace (());
      }

      USE_TABS = ();
      widen ();
   }
}
