% This is a simple Fish shell mode.  It does not defined any form of indentation
% style.  Rather, it simply implements a highlighting scheme.

$1 = "fish";

create_syntax_table ($1);

define_syntax ("#", "", '%', $1);
define_syntax ("([{", ")]}", '(', $1);

% Unfortunately, the editor cannot currently correctly deal with multiple
% string characters.  So, inorder to handle something like:
%    echo "I'd rather be home"
% make the '"' character the actual string character but also give '\''
% a string syntax.  However, this will cause '"' to give problems but
% usually, '"' characters will be paired.
define_syntax ('\'', '"', $1);
define_syntax ('"', '"', $1);

define_syntax ('\\', '\\', $1);
define_syntax ("-0-9a-zA-Z_", 'w', $1);        % words
define_syntax ("-+0-9", '0', $1);   % Numbers
define_syntax (",;:", ',', $1);
define_syntax ("%-+/&*=<>|!~^$", '+', $1);

#ifdef HAS_DFA_SYNTAX

%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache ("fishmode.dfa", name);

   dfa_define_highlight_rule ("\\\\.", "normal", name);
   dfa_define_highlight_rule ("#.*$", "comment", name);
   dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*\"", "string", name);
   dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*$", "string", name);
   dfa_define_highlight_rule ("'[^']*'", "string", name);
   dfa_define_highlight_rule ("'[^']*$", "string", name);
   dfa_define_highlight_rule ("[\\|&;\\(\\)<>]", "Qdelimiter", name);
   dfa_define_highlight_rule ("[\\[\\]\\*\\?]", "Qoperator", name);
   dfa_define_highlight_rule ("[^ \t\"'\\\\\\|&;\\(\\)<>\\[\\]\\*\\?]+",
                          "Knormal", name);
   dfa_define_highlight_rule (".", "normal", name);
   dfa_build_highlight_table (name);
}

dfa_set_init_callback (&setup_dfa_callback, "fish");

%%% DFA_CACHE_END %%%
#endif

% Misc. keywordy-type things

append_keywords($1, 0,
                "argv");

% Internal commands and control structures

append_keywords ($1, 1,
                 "and", "begin", "break", "case", "else", "end", "for", "function", "if", "not", "or",
                 "return", "switch", "while");

% built-ins

append_keywords($1, 2,
                "abbr", "alias", "argparse", "bg", "bind", "block", "breakpoint", "builtin", "cd", "cdh",
                "command", "commandline", "complete", "contains", "continue", "count", "dirh", "dirs", "disown",
                "echo", "emit", "eval", "exec", "exit", "false", "fg", "fish", "fish_add_path", "fish_breakpoint_prompt", "fish_config",
                "fish_git_prompt", "fish_hg_prompt", "fish_indent", "fish_job_summary", "fish_key_reader", "fish_mode_prompt", "fish_opt",
                "fish_prompt", "fish_right_prompt", "fish_status_to_signal", "fish_svn_prompt", "fish_update_completions", "fish_vcs_prompt",
                "funced", "funcsave", "functions", "help", "history", "isatty", "jobs", "math", "nextd",
                "open", "popd", "prevd", "printf", "prompt_pwd", "psub", "pushd", "pwd", "random", "read", "realpath",
                "set", "set_color", "source", "status", "string", "string-collect", "string-escape", "string-join",
                "string-join0", "string-length", "string-lower", "string-match", "string-repeat", "string-replace",
                "string-split", "string-split0", "string-sub", "string-trim", "string-unescape", "string-upper",
                "suspend", "test", "time", "trap", "true", "type", "ulimit", "umask", "vared", "wait");

% Popular extern commands

append_keywords($1, 3,
                "cut", "tput", "dcop", "rsh", "rcp", "telnet", "rlogin", "ftp", "ping", "disk",
                "mail", "finger", "help", "zip", "unzip", "compress", "uncompress", "date",
                "gzip", "gunzip", "bzip2", "bunzip2", "perl", "python", "python3", "less", "getopt",
                "cleartool", "make", "ct", "uniq", "tail", "sort", "hexdump", "clear",
                "xclearcase", "mount", "swapon", "syslogd", "dhcpcd", "portmap", "utelnetd",
                "umount", "losetup", "killall", "mdev", "repo", "ssh-agent", "ssh-add",
                "readlink", "m4", "konqueror", "grep", "egrep", "sed", "xargs", "tar", "nroff",
                "endview", "setview", "lsvob", "ci", "co", "lshistory", "findmerge", "unco",
                "lslock", "mkview", "setcs", "rmview", "lsprivate", "mkelem", "rmname",
                "protect", "stat", "unregister", "rmtag", "awk", "readelf", "tee", "dd", "ddd",
                "md5sum", "rdesktop", "objdump", "xxd", "tr", "git", "uname", "timeout",
                "wget", "java", "rgrep", "fgrep", "docker", "ps", "strace", "diff", "aws",
                "envsubst", "useradd", "groupadd", "yum", "apt", "curl", "pip", "cmp",
                "apt-get", "puppet", "service", "more", "less", "ls", "ifconfig", "mkdir",
                "touch", "calendar", "env", "which", "rm");

% Bash variables

append_keywords($1, 4,
                "BASH", "BASH_ARGC", "BASH_ARGV", "BASH_COMMAND", "BASH_EXECUTION_STRING", "BASH_REMATCH",
                "BASH_LINENO", "BASH_SOURCE", "BASH_SUBSHELL", "BASH_VERSINFO", "BASH_VERSION",
                "COMP_CWORD", "COMP_LINE", "COMP_POINT", "COMP_WORDBREAKS", "COMP_WORDS", "DIRSTACK",
                "EUID", "FUNCNAME", "GROUPS",
                "HISTCMD", "HOSTNAME", "HOSTYPE", "LINENO", "MACHTYPE", "OLDPWD", "OPTARG", "OPTIND",
                "OSTYPE", "PIPESTATUS", "PPID", "PWD", "RANDOM", "REPLY", "SECONDS", "SHELLOPTS",
                "SHLVL", "UID", "BASH_ENV", "CDPATH", "COLUMNS", "COMPREPLY", "EMACS", "FCEDIT",
                "FIGNORE", "GLOBIGNORE", "HISTCONTROL", "HISTFILE", "HISTFILESIZE", "HISTIGNORE",
                "HISTSIZE", "HISTTIMEFORMAT", "HOME", "HOSTFILE", "IFS", "IGNOREEOF", "INPUTRC",
                "LANG", "LC_ALL", "LC_COLLATE", "LC_CTYPE", "LC_MESSAGES", "LC_NUMERIC", "LINES",
                "MAIL", "MAILCHECK", "MAILPATH", "OPTERR", "PATH", "POSXILY_CORRECT", "PROMPT_COMMAND",
                "PS1", "PS2", "PS3", "PS4", "SHELL", "TIMEFORMAT", "TMOUT", "auto_resume", "histchars",
                "LD_LIBRARY_PATH", "KONSOLE_DCOP_SESSION", "CLEARCASE_ROOT");

% Fish variables

append_keywords($1, 5,
                "fish_color_normal", "fish_color_command", "fish_color_quote", "fish_color_redirection",
                "fish_color_end", "fish_color_error", "fish_color_param", "fish_color_comment", "fish_color_match",
                "fish_color_selection", "fish_color_search_match", "fish_color_operator", "fish_color_escape",
                "fish_color_cwd", "fish_color_autosuggestion", "fish_color_user", "fish_color_host",
                "fish_color_host_remote", "fish_color_cancel", "fish_kill_signal", "fish_pager_color_progress", "fish_pager_color_background",
                "fish_pager_color_prefix", "fish_pager_color_completion", "fish_pager_color_description",
                "fish_pager_color_secondary_background", "fish_pager_color_secondary_",
                "fish_pager_color_secondary_completion", "fish_pager_color_secondary_description",
                "fish_pager_color_selected_background", "fish_pager_color_selected_prefix",
                "fish_pager_color_selected_completion", "fish_pager_color_selected_description",
                "fish_color_cwd_root", "fish_color_history_current", "fish_color_status", "fish_color_valid_path",
                "fish_greeting", "fish_key_bindings", "fish_user_paths",
                "__fish_bin_dir", "__fish_config_dir", "__fish_data_dir", "__fish_help_dir", "__fish_initialized",
                "__fish_sysconf_dir", "__fish_preview_current_file", "__fish_user_data_dir",
                "fish_complete_path", "fish_function_path", "fish_key_bindings", "fish_pid", "fish_vi_force_cursor", "FISH_VERSION");

% External Thingy functions

append_keywords($1, 5,
                "addpath", "aws-lambda-upload", "awslist", "awsssh", "borger", "clip", "console-colours",
                "docker-gitlab", "docker-jenkins", "docker-purge", "dropstart", "ffind", "ftrace", "ggit",
                "ggrep", "gitprompt", "jsonview", "jumpapp", "linecount", "pep", "portainer", "pup-dep",
                "readable", "remdir", "remotex", "rpylint", "run_jed", "run_jed_f", "set-app-icon",
                "set-icons", "splitpics", "strreplace", "tag", "tags", "thingy-config", "thingy-lint",
                "thingy-update", "trimpath", "usb-backup", "vb-time-sync", "vpn", "weather", "webmon",
                "xchmod", "xseticon", "yamlcheck", "gcd", "gcm", "gcd", "gits", "grh", "jed", "gg",
                "keys");

% Fish keyboard commands

append_keywords($1, 6,
                "and", "accept-autosuggestion", "backward-char", "backward-bigword", "backward-delete-char",
                "backward-kill-bigword", "backward-kill-line", "backward-kill-path-component", "backward-kill-word",
                "backward-word", "beginning-of-buffer", "beginning-of-history", "beginning-of-line", "begin-selection",
                "cancel", "capitalize-word", "complete", "complete-and-search", "delete-char", "delete-or-exit",
                "down-line", "downcase-word", "end-of-buffer", "end-of-history", "end-of-line", "end-selection",
                "expand-abbr", "execute", "force-repaint", "forward-bigword", "forward-char", "forward-single-char", "forward-word",
                "history-search-backward", "history-search-forward", "history-prefix-search-backward",
                "history-prefix-search-forward", "history-token-search-backward", "history-token-search-forward",
                "forward-jump and backward-jump", "forward-jump-till and backward-jump-till",
                "repeat-jump and repeat-jump-reverse", "kill-bigword", "kill-line", "kill-selection", "kill-whole-line",
                "kill-word", "pager-toggle-search", "repaint", "repaint-mode", "self-insert", "self-insert-notfirst",
                "suppress-autosuggestion", "swap-selection-start-stop", "transpose-chars", "transpose-words",
                "up-line", "upcase-word", "yank", "yank-pop");

% Conditionals

append_keywords($1, 7, "eq", "ne", "gt", "ge", "lt", "le");

% WiSki help text nroffy stuff

append_keywords($1, 6,
                "TH", "SH", "TP", "fB", "fP", "NAME", "SYNOPSIS", "DESCRIPTION", "EOF", "OPTIONS");

!if (keymap_p ($1)) make_keymap ($1);

define fish_mode ()
{
   set_mode("fish", 0);
   use_syntax_table ("fish");
   use_keymap("fish");
   use_dfa_syntax(0);
%   mode_set_mode_info ("fish", "fold_info", "#{{{\r#}}}\r\r");
   mode_set_mode_info ("fish", "fold_info", "{\r}\r");
   set_comment_info("fish", "#", NULL, NULL, 0);
   run_mode_hooks("fish_mode_hook");
}
