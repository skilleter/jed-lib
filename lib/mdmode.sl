create_syntax_table ("md");

define_syntax ("#", "", '%', "md");             % comments
define_syntax ("([{", ")]}", '(', "md");                % delimiters
define_syntax ("0-9a-zA-Z", 'w', "md");         % words
define_syntax ("-+0-9.eE", '0', "md");          % Numbers
define_syntax ("_,;:`.", ',', "md");                    % punctuation
define_syntax ("$@%-+/*=<>!^", '+', "md");              % operators

set_syntax_flags ("md", 0);                     % keywords ARE case-sensitive

define md_mode ()
{
   TAB = 8;
   set_mode ("md", 0x4); % flag value of 4 is generic language mode
   use_syntax_table ("md");
   run_mode_hooks ("md_mode_hook");
   set_visual_wrap (1);  % Enable visual wrapping
}
