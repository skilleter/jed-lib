% ******************************************************************************
% Jed mode for editing SGS toolkit configuration files (e.g. hw.cfg)
% ******************************************************************************

% -------------------------------------------------------------------------
% Word characters
% -------------------------------------------------------------------------

define_word ("0-9A-Za-z_");

define cfg_top_of_thing()
{
        forever
        {
                bol();
                skip_white();

                !if(looking_at("##"))
                {
                        break;
                }

                () = up(1);
        }
}

define cfg_end_of_thing()
{
        forever
        {
                bol();
                skip_white();

                !if(looking_at("##"))
                {
                        break;
                }

                () = down(1);
        }
}

!if (keymap_p ("CFG"))
        make_keymap ("CFG");

definekey ("cfg_top_of_thing", "\e\xE0H", "CFG");
definekey ("cfg_end_of_thing", "\e\xE0P", "CFG");

% Now create and initialize the syntax tables.

create_syntax_table ("CFG");

%define_syntax ("##",                                   "",             '%',    "CFG");
define_syntax ("//",                                    "",             '%',    "CFG");
define_syntax ("([{",                                   ")]}",  '(',    "CFG");
define_syntax ('"',                                                                     '"',    "CFG");
define_syntax ('\'',                                                            '\'', "CFG");
define_syntax ('\\',                                                            '\\',   "CFG");
define_syntax ("0-9a-zA-Z_",                                            'w',    "CFG");        % words
define_syntax ("-+0-9a-fA-F.xXUL",                              '0',    "CFG");   % Numbers
define_syntax (",;.?:",                                                         ',',    "CFG");
define_syntax ("$%-+/&*=<>|!~^",                                        '+',    "CFG");

set_syntax_flags ("CFG", 0x4|0x40);

% Type 0 keywords

() = define_keywords_n ("CFG", "cdifmvrm", 2, 0);
() = define_keywords_n ("CFG", "forlogpwdrunsys", 3, 0);
() = define_keywords_n ("CFG", "copycoredumpevalfilefillheaphelploadpeekpokeprocquitstepstoptaskvarswaitwhen", 4, 0);
() = define_keywords_n ("CFG", "alterbreakentryparseplaceprintprocsresetstackspacestoretracewatchwherewhilewrite", 5, 0);
() = define_keywords_n ("CFG", "actionclinfodefinedeleteenableerrorseventsfwritememorymemsetmodifynoinitremovetargetwindow", 6, 0);
() = define_keywords_n ("CFG", "addhelpcompareconnectcontextdisabledisplayfappendforwardincludemodulesprogram"+
                                                                                 "profileprogramrestartruntimesessionstepoutsymbols", 7, 0);
() = define_keywords_n ("CFG", "bootdatacontinueregisterromslice", 8, 0);
() = define_keywords_n ("CFG", "addressofdirectoryinitstackprocessorromoriginstatementtracedumptraceload", 9, 0);
() = define_keywords_n ("CFG", "disconnectentrypointstartstatesearchpathstartstate", 10, 0);
() = define_keywords_n ("CFG", "romfilenametracebuffer", 11, 0);
() = define_keywords_n ("CFG", "st20ccoptions", 13, 0);

% Type 1 keywords

() = define_keywords_n("CFG",   "c2", 2, 1);
() = define_keywords_n("CFG",   "RAMROMtap", 3, 1);
() = define_keywords_n("CFG",   "aregbregcregfulliptrwptr", 4, 1);
() = define_keywords_n("CFG",   "DEBUGioregtdesc", 5, 1);
() = define_keywords_n("CFG",   "DEVICEsharegshbregshcregstatus", 6, 1);
() = define_keywords_n("CFG",   "RESERVEDpriority", 8, 1);
() = define_keywords_n("CFG",   "PERIPHERAL", 10, 1);
define cfg_mode ()
{
   set_mode("CFG", 2);
   use_keymap("CFG");
   use_syntax_table ("CFG");
   set_comment_info("CFG", "//", NULL, NULL, 0);

   run_mode_hooks("cfg_mode_hook");
}

