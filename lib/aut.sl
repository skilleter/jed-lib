% ******************************************************************************
% AUTOLT scripting language mode
% ******************************************************************************

define_word ("0-9A-Za-z_");

!if (keymap_p ("aut"))
        make_keymap ("aut");

create_syntax_table ("aut");


define_syntax (";", "", '%', "aut");
define_syntax ("(", ")", '(', "aut");
define_syntax ('"', '"', "aut");
define_syntax ('\'', '\'', "aut");
% define_syntax ('\\', '\\', "aut");
define_syntax ("0-9@a-zA-Z_{}", 'w', "aut");        % words
define_syntax ("-+0-9a-fA-F.xXUL", '0', "aut");   % Numbers
define_syntax (",;.?:$", ',', "aut");
define_syntax ('#', '#', "aut");
define_syntax ("%-+/&*=<>|!~^", '+', "aut");


set_syntax_flags ("aut", 0x1|0x4|0x40);

#ifdef HAS_DFA_SYNTAX

%%% DFA_CACHE_BEGIN %%%

private define setup_dfa_callback (name)
{
        dfa_enable_highlight_cache("autmode.dfa", name);

        dfa_define_highlight_rule("^[ \t]*#", "PQpreprocess", name);
        dfa_define_highlight_rule(";.*", "comment", name);
        dfa_define_highlight_rule("[@A-Za-z_\\$][@A-Za-z_0-9\\$]*", "Knormal", name);
        dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?", "number", name);
        dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*[LlUu]*", "number", name);
        dfa_define_highlight_rule("[0-9]+[LlUu]*", "number", name);
        dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"", "string", name);
        dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*'", "string", name);
        dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule("[ \t]+", "normal", name);
        dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
        dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);

        dfa_build_highlight_table(name);
}

dfa_set_init_callback (&setup_dfa_callback, "aut");

%%% DFA_CACHE_END %%%

#endif

append_keywords("aut", 0,
                                         "and",                 "byref",                "case",                 "const",                "continueloop",
                                         "dim",                 "do",                   "else",                 "elseif",               "endfunc",
                                         "endif",       "endselect",    "exit",                 "exitloop",     "for",
                                         "func",        "global",               "if",           "local",                "next",
                                         "not",                 "or",                   "return",       "redim",                "select",
                                         "step",        "then",                         "to",           "until",                "wend",
                                         "while",       "exit",                 "default",      "switch",               "endswitch");

append_keywords("aut", 1,
                                         "@appdatacommondir",   "@appdatadir",                  "@autoitexe",                           "@autoitversion",               "@commonfilesdir",
                                         "@compiled",                           "@computername",                        "@comspec",                             "@cr",                                          "@crlf",
                                         "@desktopcommondir",   "@desktopdir",                  "@desktopheight",               "@desktopwidth",                        "@desktopdepth",
                                         "@desktoprefresh",             "@documentscommondir",  "@error",                                       "@extended",                            "@favoritescommondir",
                                         "@favoritesdir",               "@gui_ctrlid",                  "@gui_ctrlhandle",              "@gui_winhandle",               "@homedrive",
                                         "@homepath",                           "@homeshare",                           "@hour",                                        "@inetgetactive",               "@inetgetbytesread",
                                         "@ipaddress1",                         "@ipaddress2",                  "@ipaddress3",                  "@ipaddress4",                  "@lf",
                                         "@logondnsdomain",             "@logondomain",                         "@logonserver",                         "@mday",                                        "@min",
                                         "@mon",                                        "@mydocumentsdir",              "@numparams",                           "@osbuild",                             "@oslang",
                                         "@osservicepack",              "@ostype",                                      "@osversion",                           "@programfilesdir",             "@programscommondir",
                                         "@programsdir",                        "@scriptdir",                           "@scriptfullpath",              "@scriptname",                  "@sec",
                                         "@startmenucommondir", "@startmenudir",                        "@startupcommondir",    "@startupdir",                  "@sw_disable",
                                         "@sw_enable",                  "@sw_hide",                             "@sw_maximize",                         "@sw_minimize",                         "@sw_restore",
                                         "@sw_show",                            "@sw_showdefault",              "@sw_showmaximized",    "@sw_showminimized",    "@sw_showminnoactive",
                                         "@sw_showna",                  "@sw_shownoactivate",   "@sw_shownormal",               "@systemdir",                           "@tab",
                                         "@tempdir",                            "@userprofiledir",              "@username",                            "@wday",                                        "@windowsdir",
                                         "@workingdir",                         "@yday",                                        "@year",
                                         "@hotkeypressed",              "@scriptlinenumber",            "@processorarch",                       "@autoitpid");

append_keywords("aut", 2,
                                         "abort",                               "cancel",                               "ignore",                               "no",                                           "ok",
                                         "retry",                               "yes",                                  "false",                                        "true");

append_keywords("aut", 3,
                                         "abs",         "acos",         "adlibdisable",         "adlibenable",  "asc",  "asin",         "assign",       "atan",         "autoitsetoption",
                                         "autoitwingettitle",   "autoitwinsettitle",    "bitand",       "bitnot",       "bitor",        "bitshift",     "bitxor",
                                         "blockinput",  "break",        "call",         "cdtray",       "chr",  "clipget",      "clipput",      "consolewrite",         "controlclick",
                                         "controlcommand",      "controldisable",       "controlenable",        "controlfocus",         "controlgetfocus",      "controlgethandle",
                                         "controlgetpos",       "controlgettext",       "controlhide",  "controllistview",      "controlmove",  "controlsend",
                                         "controlsettext",      "controlshow",  "cos",  "dec",  "dircopy",      "dircreate",    "dirgetsize",   "dirmove",      "dirremove",
                                         "dllcall",     "dllclose",     "dllopen",      "drivegetdrive",        "drivegetfilesystem",   "drivegetlabel",        "drivegetserial",
                                         "drivegettype",        "drivemapadd",  "drivemapdel",  "drivemapget",  "drivesetlabel",        "drivespacefree",       "drivespacetotal",
                                         "drivestatus",         "envget",       "envset",       "envupdate",    "eval",         "exp",  "filechangedir",        "fileclose",    "filecopy",
                                         "filecreateshortcut",  "filedelete",   "fileexists",   "filefindfirstfile",    "filefindnextfile",     "filegetattrib",        "filegetlongname",
                                         "filegetshortcut",     "filegetshortname",     "filegetsize",  "filegettime",  "filegetversion",       "fileinstall",  "filemove",
                                         "fileopen",    "fileopendialog",       "fileread",     "filereadline",         "filerecycle",  "filerecycleempty",     "filesavedialog",
                                         "fileselectfolder",    "filesetattrib",        "filesettime",  "filewrite",    "filewriteline",        "ftpsetproxy",  "guicreate",
                                         "guictrlcreateavi",    "guictrlcreatebutton",  "guictrlcreatecheckbox",        "guictrlcreatecombo",   "guictrlcreatecontextmenu",
                                         "guictrlcreatedate",   "guictrlcreatedummy",   "guictrlcreateedit",    "guictrlcreategroup",   "guictrlcreateicon",
                                         "guictrlcreateinput",  "guictrlcreatelabel",   "guictrlcreatelist",    "guictrlcreatelistview",        "guictrlcreatelistviewitem",
                                         "guictrlcreatemenu",   "guictrlcreatemenuitem",        "guictrlcreatepic",     "guictrlcreateprogress",        "guictrlcreateradio",
                                         "guictrlcreateslider",         "guictrlcreatetab",     "guictrlcreatetabitem",         "guictrlcreatetreeview",        "guictrlcreatetreeviewitem",
                                         "guictrlcreateupdown",         "guictrldelete",        "guictrlgetstate",      "guictrlread",  "guictrlrecvmsg",       "guictrlsendmsg",
                                         "guictrlsendtodummy",  "guictrlsetbkcolor",    "guictrlsetcolor",      "guictrlsetcursor",     "guictrlsetdata",       "guictrlsetfont",
                                         "guictrlsetimage",     "guictrlsetlimit",      "guictrlsetonevent",    "guictrlsetpos",        "guictrlsetresizing",   "guictrlsetstate",
                                         "guictrlsetstyle",     "guictrlsettip",        "guidelete",    "guigetcursorinfo",     "guigetmsg",    "guisetbkcolor",        "guisetcoord",
                                         "guisetcursor",        "guisetfont",   "guisethelp",   "guiseticon",   "guisetonevent",        "guisetstate",  "guistartgroup",        "guiswitch",
                                         "hex",         "hotkeyset",    "httpsetproxy",         "inetget",      "inetgetsize",  "inidelete",    "iniread",      "inireadsection",
                                         "inireadsectionnames",         "iniwrite",     "inputbox",     "int",  "isadmin",      "isarray",      "isdeclared",   "isfloat",      "isint",
                                         "isnumber",    "isstring",     "log",  "memgetstats",  "mod",  "mouseclick",   "mouseclickdrag",       "mousedown",    "mousegetcursor",
                                         "mousegetpos",         "mousemove",    "mouseup",      "mousewheel",   "msgbox",       "number",       "ping",         "pixelchecksum",        "pixelgetcolor",
                                         "pixelsearch",         "processclose",         "processexists",        "processlist",  "processsetpriority",   "processwait",  "processwaitclose",
                                         "progressoff",         "progresson",   "progressset",  "random",       "regdelete",    "regenumkey",   "regenumval",   "regread",      "regwrite",
                                         "round",       "run",  "runasset",     "runwait",      "send",         "seterror",     "setextended",  "shutdown",     "sin",  "sleep",        "soundplay",
                                         "soundsetwavevolume",  "splashimageon",        "splashoff",    "splashtexton",         "sqrt",         "statusbargettext",     "string",       "stringaddcr",
                                         "stringformat",        "stringinstr",  "stringisalnum",        "stringisalpha",        "stringisascii",        "stringisdigit",        "stringisfloat",
                                         "stringisint",         "stringislower",        "stringisspace",        "stringisupper",        "stringisxdigit",       "stringleft",   "stringlen",
                                         "stringlower",         "stringmid",    "stringreplace",        "stringright",  "stringsplit",  "stringstripcr",        "stringstripws",        "stringtrimleft",
                                         "stringtrimright",     "stringupper",  "tan",  "timerdiff",    "timerinit",    "tooltip",      "traytip",      "ubound",       "winactivate",
                                         "winactive",   "winclose",     "winexists",    "wingetcaretpos",       "wingetclasslist",      "wingetclientsize",     "wingethandle",         "wingetpos",
                                         "wingetprocess",       "wingetstate",  "wingettext",   "wingettitle",  "winkill",      "winlist",      "winmenuselectitem",    "winminimizeall",
                                         "winminimizeallundo",  "winmove",      "winsetontop",  "winsetstate",  "winsettitle",  "winsettrans",  "winwait",      "winwaitactive",
                                         "winwaitclose",        "winwaitnotactive",
                                         "iskeyword", "isbool", "consolewriteerror", "consoleread", "hwnd", "ishwnd", "binarystring", "isbinarystring", "filecreatentfslink",
                                         "execute","beep", "winflash", "floor", "ceiling", "bitrotate");

append_keywords("aut", 4,
                                         "{!}",                                         "{#}",                                  "{+}",                                  "{^}",                                  "{{}",
                                         "{}}",                                         "{space}",                              "{ENTER}",                              "{ALT}",                                "{BACKSPACE}",
                                         "{BS}",                                "{DELETE}",                     "{DEL}",                                "{UP}",                                         "{DOWN}",
                                         "{LEFT}",                              "{RIGHT}",                              "{HOME}",                               "{END}",                                "{ESCAPE}",
                                         "{ESC}",                               "{INSERT}",                     "{INS}",                                "{PGUP}",                               "{PGDN}",
                                         "{F1}",                                "{F2}",                                         "{F3}",                                         "{F4}",                                         "{F5}",
                                         "{F6}",                                "{F7}",                                         "{F8}",                                         "{F9}",                                         "{F10}",
                                         "{F11}",                               "{F12}",                                "{TAB}",                                "{PRINTSCREEN}",                "{LWIN}",
                                         "{RWIN}",                              "{NUMLOCK}",                    "{CTRLBREAK}",          "{PAUSE}",                              "{CAPSLOCK}",
                                         "{NUMPAD0}",                   "{NUMPAD1}",                    "{NUMPAD2}",                    "{NUMPAD3}",                    "{NUMPAD4}",
                                         "{NUMPAD5}",                   "{NUMPAD6}",                    "{NUMPAD7}",                    "{NUMPAD8}",                    "{NUMPAD9}",
                                         "{NUMPADMULT}",                "{NUMPADADD}",          "{NUMPADSUB}",          "{NUMPADDIV}",          "{NUMPADDOT}",
                                         "{APPSKEY}",                   "{ALTDOWN}",                    "{ALTUP}",                              "{SHIFTDOWN}",          "{SHIFTUP}",
                                         "{CTRLDOWN}",          "{CTRLUP}",                     "{ASC}");

define aut_mode ()
{
   set_mode("aut", 2);
        use_keymap("aut");
   use_syntax_table ("aut");

   run_mode_hooks("aut_mode_hook");
}

provide ("aut_mode");
