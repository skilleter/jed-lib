% [JMS] Console colour scheme

set_color("normal",      			"black",    	"white");        		% default fg/bg
set_color("status",      			"white",    	"blue");  		% status or mode line
set_color("region",      			"black",       "grey");   		% for marking regions

set_color("operator",    			"red",  			"white");     			% +, -, etc..
set_color("number",      			"red",    		"white");        		% 10, 2.71,... TeX formulas
set_color("comment",     			"black",   		"brightgreen"); 		% /* comment */
set_color("string",      			"red", 	 		"yellow");  	% "string" or 'char'

set_color("keyword",     			"green",   		"white");        		% if, while, unsigned, ...
set_color("keyword1",    			"brown",  		"white");        		
set_color("keyword2",    			"blue",  		"white");        		
set_color("keyword3",    			"green",  		"white");        		
set_color("keyword4",            "black",    	"cyan");
set_color("keyword5",            "blue",    		"cyan");
set_color("keyword6",            "green",    	"cyan");
set_color("keyword7",            "magenta",    	"cyan");
set_color("keyword8",            "green",    	"yellow");
set_color("keyword9",            "blue",    		"yellow");

set_color("delimiter",   			"magenta", 		"white");   % {}[](),.;...
set_color("preprocess",  			"black",   		"yellow"); 		% #ifdef ....

set_color("message",     			"black",       "yellow");    		% color for messages
set_color("error",       			"yellow",     	"red");			% color for errors
set_color("dollar",      			"black",    	"red");  		% color dollar sign continuation
set_color("cursorovr",	 			"cyan",   		"white");
set_color("cursor",	 	 			"red",   		"white");
set_color("html",                "green", "grey");
set_color("...",         			"cyan",   		"yellow"); 			% folding indicator

set_color("menu",        			"black",      	"grey");   		% menu bar
set_color("menu_selection", 		"white",			"grey");
set_color("menu_popup",     		"black",			"grey");
set_color("menu_char", 				"white",			"grey");

set_color("occur_highlight", 		"black", 		"yellow");

set_color("linenum", 				"yellow", 		"blue");
set_color("trailing_whitespace", "black", 		"brightcyan");
% set_color("tab", 						"black", 		"brightcyan");

