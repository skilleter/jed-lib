% [JMS] JMS default color scheme for Windows ONLY

% 0 : black on white
% 1 : green on red
% 2 : white on blue
% 3 : black on grey
% 4 : black on grey
% 5 : brown on white
% 6 : red on white
% 7 : brown on yellow
% 8 : black on green
% 9 : magenta on white
% 10: black on pale yellow
% 11: black on yellow
% 12: black on pink
% 13: yellow on red
% 14: cyan on blue
% 15: black on white
% 16: black on grey
% 17: grey on black
% 18: black on pale grey
% 19: magenta on dark blue

% Define new colours

w32_define_color("normal_f",       0,   0,   0);
w32_define_color("normal_b",		255, 255, 255);
w32_define_color("number_f",     255,  64,  64);
w32_define_color("operator_f",   182,  64,  64);
w32_define_color("string_f",  	128,   0,   0);
w32_define_color("string_b",  	255, 255, 192);
w32_define_color("comment_b",   	 32, 255, 128);
w32_define_color("comment_f",      0,   0,   0);
w32_define_color("keyword_f",   	  0, 160,   0);
w32_define_color("keyword1_f",     0,   0, 255);
w32_define_color("keyword2_f",   192, 128,   0);
w32_define_color("keyword3_f",   255,   0,  96);
w32_define_color("keyword4_f",   160,  32, 160);
w32_define_color("keyword5_f",   255, 128,  96);
w32_define_color("keyword6_f",    96, 128,  96);
w32_define_color("keyword7_f",    64,  96, 128);
w32_define_color("keyword8_f",     0,  32, 192);
w32_define_color("keyword9_f",   255, 128,  32);
w32_define_color("delimiter_f",  192,  64, 192);
w32_define_color("delimiter_b",	255, 255, 255);
w32_define_color("folding_f",      0, 255, 255);
w32_define_color("error_b",		255, 192, 192);
w32_define_color("error_f",		  0,   0,   0);
w32_define_color("msg_f",			  0,   0, 192);
w32_define_color("msg_b",			255, 255,   0);

w32_define_color("dollar_f",		255, 255,   0);
w32_define_color("dollar_b",		255,   0,   0);

w32_define_color("preproc_f",		  0,   0,   0);
w32_define_color("preproc_b",		255, 255, 192);

w32_define_color("cursor_of",  	  0, 192, 192);
w32_define_color("cursor_ob",  	255, 255, 255);

w32_define_color("cursor_fof",  	  0,  96,  96);
w32_define_color("cursor_fob",  	255, 255, 255);

w32_define_color("cursor_if",  	255,   0,   0);
w32_define_color("cursor_ib",  	255, 255, 255);

w32_define_color("cursor_fif",  	128,   0,   0);
w32_define_color("cursor_fib",  	255, 255, 255);

w32_define_color("status_f",     255, 255, 255);
w32_define_color("status_b",      32,  64, 192);

w32_define_color("menu_f",		 	  0,   0,   0);
w32_define_color("menu_b",		 	192, 192, 192);
w32_define_color("menu_h",		 	255, 255,   0);
w32_define_color("menu_cf",		  0,   0,   0);
w32_define_color("menu_cb",		255, 255, 255);

% Define the highlighting

set_color("normal",      			"normal_f",    "normal_b");        		% default fg/bg
set_color("status",      			"status_f",    "status_b");  		% status or mode line
set_color("region",      			"menu_f",      "menu_b");   		% for marking regions

set_color("operator",    			"operator_f",  "normal_b");     			% +, -, etc..
set_color("number",      			"number_f",    "normal_b");        		% 10, 2.71,... TeX formulas
set_color("comment",     			"comment_f",   "comment_b"); 		% /* comment */
set_color("string",      			"string_f", 	 "string_b");  	% "string" or 'char'

set_color("keyword",     			"keyword_f",   "normal_b");        		% if, while, unsigned, ...
set_color("keyword1",    			"keyword1_f",  "normal_b");
set_color("keyword2",    			"keyword2_f",  "normal_b");
set_color("keyword3",    			"keyword3_f",  "normal_b");
set_color("keyword4",    			"keyword4_f",  "normal_b");
set_color("keyword5",    			"keyword5_f",  "normal_b");
set_color("keyword6",    			"keyword6_f",  "normal_b");
set_color("keyword7",    			"keyword7_f",  "normal_b");
set_color("keyword8",    			"keyword8_f",  "normal_b");
set_color("keyword9",    			"keyword9_f",  "normal_b");

set_color("delimiter",   			"delimiter_f", "delimiter_b");   % {}[](),.;...
set_color("preprocess",  			"preproc_f",   "preproc_b"); 		% #ifdef ....

set_color("message",     			"msg_f",       "msg_b");    		% color for messages
set_color("error",       			"error_f",     "error_b");			% color for errors
set_color("dollar",      			"dollar_f",    "dollar_b");  		% color dollar sign continuation
set_color("html",                               "white",       "normal_b");
set_color("cursorovr",	 			"cursor_ob",   "cursor_of");
set_color("cursorovrflash",		"cursor_fob",   "cursor_fof");

set_color("cursor",	 	 			"cursor_ib",   "cursor_if");
set_color("cursorflash",			"cursor_fib",  "cursor_fif");

set_color("...",         			"folding_f",   "blue"); 			% folding indicator

set_color("menu",        			"menu_f",      "menu_b");   		% menu bar
set_color("menu_selection", 		"menu_f",		"menu_h");
set_color("menu_popup",     		"menu_f",		"menu_b");
set_color("menu_char", 				"menu_cf",		"menu_cb");

set_color("occur_highlight", 		"black", 		"yellow");

set_color("linenum", 				"yellow", 		"blue");
set_color("trailing_whitespace", "black", 		"brightcyan");
% set_color("tab", 						"black", 		"brightcyan");

