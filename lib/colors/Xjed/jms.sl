% [JMS] Colour scheme for Jed

#ifdef XWINDOWS

set_color("normal",      			"Black",    "White");        % default fg/bg
set_color("status",      			"White",    "#2040C0");  		% status or mode line
set_color("region",      			"Black",    "#C0C0C0");   		% for marking regions

set_color("operator",    			"#B64040",    "White");     	% +, -, etc..
set_color("number",      			"#4040FF",    "White");        % 10, 2.71,... TeX formulas
set_color("comment",     			"Black",    "#C0FFC0"); 		   % /* comment */
set_color("string",      			"#000000",    "#FFFFC0");  	   % "string" or 'char'

% 10 Keyword colour combinations

set_color("keyword",     			"#005000",     "White");       % mid-green
set_color("keyword1",    			"#0000FF",     "White");       % bright blue
set_color("keyword2",    			"#C08000",     "White");       % brown
set_color("keyword3",    			"#FF0060",     "White");       % magenta
set_color("keyword4",    			"#A020A0",     "White");
set_color("keyword5",    			"#FF8060",     "White");
set_color("keyword6",    			"#008060",     "White");
set_color("keyword7",    			"#888800",     "White");
set_color("keyword8",    			"#0020C0",     "White");
set_color("keyword9",    			"#FF8020",     "White");

set_color("delimiter",   			"#C040C0",     "White");       % {}[](),.;...
set_color("preprocess",  			"Black",     "#FFCFC0"); 		% #ifdef ....

set_color("message",     			"#0000C0",     "#FFFF00");    	% color for messages
set_color("error",       			"Black",     "#FFC0C0");			% color for errors
set_color("dollar",      			"#FFFF00",     "#FF0000");  		% color dollar sign continuation

%set_color("cursorovr",	 			"#00C0C0",     "Black");
% set_color("cursorovrflash",		        "#006060",     "White");

%set_color("cursor",	 	 		"#606060",     "Black");
% set_color("cursorflash",			"#800000",     "White");

set_color("...",         			"#00FFFF",     "#2020FF"); 		% folding indicator

set_color("menu",        			"Black",      "#C0C0C0");   	% menu bar
set_color("menu_selection", 		"#C0C0C0",      "Black");
set_color("menu_selection_char", "#FF5555",      "#0000AA");
set_color("menu_popup",     		"Black",      "#C0C0C0");
set_color("menu_char", 				"Black",      "White");
set_color("menu_shadow",         "Black",      "#A0A0A0");

set_color("occur_highlight", 		"Black",      "#FFFF20");

set_color("linenum", 				"#FFFF20",      "#2020FF");
set_color("trailing_whitespace", "Black",      "#FF00FF");
set_color("tab", 				      "Black",      "#FF00FF");

set_color("mouse",               "blue",         "blue");

set_color("url",                 "Black",      "#E8DCE0");

set_color("italic", 				   "#005599", 	"#E0E0E0");
set_color("underline", 				"#F00000", 	"#E0E0E0");
set_color("bold", 				   "#0000FF", 	"#E0E0E0");

set_color("wibble",              "#00FFFF",  "#808080");
#else

% These colours don't work very well at all, because I can't persuade Jed to set the background
% to be white, as opposed to medium gray (or grey).

set_color("normal",      			"black",          "white");      % default fg/bg
set_color("status",      			"white",  	      "blue");  		% status or mode line
set_color("region",      			"black",          "gray");   		% for marking regions

set_color("operator",    			"red",  	         "white");     	% +, -, etc..
set_color("number",      			"brightmagenta",  "white");      % 10, 2.71,... TeX formulas
set_color("comment",     			"black",    	   "yellow"); 		% /* comment */
set_color("string",      			"red",    	      "yellow");  	% "string" or 'char'

set_color("keyword",     			"brightgreen",    "white");      % if, while, unsigned, ...
set_color("keyword1",    			"blue",     	   "white");
set_color("keyword2",    			"brown",     	   "white");
set_color("keyword3",    			"magenta",        "white");
set_color("keyword4",    			"brightmagenta",  "white");
set_color("keyword5",    			"brightred",  	   "white");
set_color("keyword6",    			"brightgreen",    "white");
set_color("keyword7",    			"cyan",     	   "white");
set_color("keyword8",    			"red",     	      "white");
set_color("keyword9",    			"brown",     	   "white");

set_color("delimiter",   			"magenta",     	"white");      % {}[](),.;...
set_color("preprocess",  			"black",     	   "yellow"); 		% #ifdef ....

set_color("message",     			"blue",     	   "white");    	% color for messages
set_color("error",       			"black",     	   "red");			% color for errors
set_color("dollar",      			"red",     	      "black");  		% color dollar sign continuation

set_color("cursorovr",	 			"green",     	   "white");
% set_color("cursorovrflash",		"#006060",     	"white");

set_color("cursor",	 	 		   "red",     	      "black");
% set_color("cursorflash",			"#800000",     	"white");

set_color("...",         			"white",     	   "blue"); 		% folding indicator

set_color("menu",        			"white",      	   "blue");   		% menu bar
set_color("menu_selection", 		"blue",      	   "white");
set_color("menu_selection_char", "green",  	      "black");
set_color("menu_popup",     		"white",      	   "blue");
set_color("menu_char", 				"black",      	   "white");
set_color("menu_shadow",         "black",      	   "gray");

set_color("occur_highlight", 		"white",      	   "black");

set_color("linenum", 				"green",      	   "blue");
set_color("trailing_whitespace", "black",      	   "cyan");
set_color("tab", 				      "black",      	   "cyan");

set_color("mouse",               "blue",           "blue");

set_color("url",                 "black",      	   "blue");

set_color("italic", 				   "cyan", 	         "gray");
set_color("underline", 				"red", 		      "gray");
set_color("bold", 				   "green", 	      "gray");

#endif
