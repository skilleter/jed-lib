% ASN.1 mode is just a superset of C mode so make sure it is loaded.

require ("cmode");

$1 = "asn";

create_syntax_table ($1);

define_syntax ("--", "--", '%', $1);
define_syntax ("--", "", '%', $1);
define_syntax ("([{", ")]}", '(', $1);
%define_syntax ('"', '"', $1);
%define_syntax ('\'', '\'', $1);
%define_syntax ('\\', '\\', $1);
define_syntax ("0-9a-zA-Z_\-", 'w', $1);        % words
define_syntax ("-+0-9a-fA-F.xXL", '0', $1);   % Numbers
define_syntax (",;.?:", ',', $1);
%define_syntax ('#', '#', $1);
define_syntax ("%-+/&*=<>|!~^", '+', $1);

set_syntax_flags ($1, 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache("asnmode.dfa", name);

   dfa_define_highlight_rule("^[ \t]*#",                                                                "PQpreprocess", name);
   dfa_define_highlight_rule("//.*",                                                                    "comment", name);
   dfa_define_highlight_rule("/\\*.*\\*/",                                                              "Qcomment", name);
   dfa_define_highlight_rule("^([^/]|/[^\\*])*\\*/",                                                    "Qcomment", name);
   dfa_define_highlight_rule("/\\*.*",                                                                  "comment", name);
   dfa_define_highlight_rule("^[ \t]*\\*+([ \t].*)?$",                                                  "comment", name);
   dfa_define_highlight_rule("[A-Za-z_\\$][A-Za-z_0-9\\$]*",                                    "Knormal", name);
   dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?",        "number", name);
   dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*[LU]*",                                                  "number", name);
   dfa_define_highlight_rule("[0-9]+[LU]*",                                                             "number", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"",                                                  "string", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$",                                              "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*'",                                                     "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*\\\\?$",                                                "string", name);
   dfa_define_highlight_rule("[ \t]+",                                                                  "normal", name);
   dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]",                                               "delimiter", name);
   dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^]",                                               "operator", name);

   dfa_build_highlight_table(name);
}

dfa_set_init_callback (&setup_dfa_callback, "asn");

%%% DFA_CACHE_END %%%
#endif

append_keywords("asn", 0,
                                         "ABSENT",                              "ABSTRACT-SYNTAX",      "ALL",                                  "ANY",
                                         "APPLICATION",         "AUTOMATIC",                    "BEGIN",                                        "BIT",
                                         "BY",                                          "CHARACTER",                    "CHOICE",
                                         "CLASS",                               "COMPONENT",                    "COMPONENTS",                   "CONSTRAINED",
                                         "DEFAULT",                             "DEFINED",                              "DEFINITIONS",                  "EMBEDDED",
                                         "END",                                 "ENUMERATED",                   "EXCEPT",                               "EXPLICIT",
                                         "EXPORTS",                             "EXTENSIBILITY",                "EXTERNAL",                             "FALSE",
                                         "FROM",                                        "HAS",                                  "IDENTIFIED",
                                         "IMPLICIT",                    "IMPLIED",                              "IMPORTS",                              "INCLUDES",
                                         "INSTANCE",                    "INTERSECTION",         "MAX",
                                         "MIN",                                 "MINUS-INFINITY",               "NULL",
                                         "OCTET",                               "OF",                                           "OPTIONAL",                             "PDV",
                                         "PLUS-INFINITY",               "PRESENT",                              "PRIVATE",                              "PROPERTY",
                                         "REAL",                                        "RELATIVE-OID",         "SEQUENCE",                             "SET",
                                         "SIZE",                                        "STRING",                               "SYNTAX",                               "TAGS",
                                         "TAGS",                                        "TRUE",                                 "TYPE-IDENTIFIER",      "UNION",
                                         "UNIQUE",                              "UNIVERSAL",                    "WITH");

append_keywords("asn", 1,
                                         "administration",                                                      "ccitt",
                                         "identified-organization",                             "identified-organization",
                                         "international-organisation",
                                         "iso",                                                                                 "itu-t",
                                         "joint-iso-ccitt",                                                     "joint-iso-itu-t",
                                         "member-body",                                                         "network-operator",
                                         "question",                                                                    "recommendation",
                                         "standard");

append_keywords("asn", 2,
                                         "BOOLEAN",                             "INTEGER",                              "OBJECT",                          "IDENTIFIER",
                                         "BMPString",                   "GeneralString",                "GeneralizedTime",      "GraphicString",
                                         "IA5String",                   "ISO646String",         "NumericString",                "ObjectDescriptor",
                                         "PrintableString",     "T61String",                    "TeletexString",                "UTCTime",
                                         "UTF8String",                  "UniversalString",      "VideotexString",               "VisibleString");

define asn_mode ()
{
   c_mode ();
   set_mode ("asn", 2);
   use_syntax_table ("asn");
   set_comment_info("asn", "--", NULL, NULL, 0);
   run_mode_hooks("asn_mode_hook");
}
