% ******************************************************************************
% -*- SLang -*-      dired.sl
%
%  Simple dired mode for JED.
% ******************************************************************************

% ******************************************************************************

variable Dired_Buffer = "*dired*";
variable Dired_Current_Directory;

static variable Line_Mark;
static variable Hide_Dotfiles = 1;

% ******************************************************************************
% Constants defining the columns where to put stuff on the display
% ******************************************************************************

static variable FILE_COL=3, SIZE_COL=48, ATTR_COL=62;

#ifndef MSDOS OS2 WIN32 IBMPC_SYSTEM
static variable DATE_COL=76;
#else
static variable DATE_COL=68;
#endif

% ******************************************************************************

variable Dired_Quick_Help;

Dired_Quick_Help = "[T]ag/Untag, [.] Hide/Show dotfiles, [V]iew, [M]ove, re[N]ame, [D]elete, [Q]uit, CR:open/chdir, Refresh, BKSP=.., [~]-Tag backup files";

% ******************************************************************************

static variable Tag_Char = '*';

% ******************************************************************************

!if (keymap_p (Dired_Buffer)) make_keymap (Dired_Buffer);

% ******************************************************************************

definekey ("dired_tag_toggle",         "t",        Dired_Buffer);
definekey ("dired_view",               "v",        Dired_Buffer);
definekey ("dired_move",               "m",        Dired_Buffer);
definekey ("dired_rename",             "n",        Dired_Buffer);
definekey ("dired_delete",             "d",        Dired_Buffer);
definekey ("dired_quit",               "q",        Dired_Buffer);
definekey ("dired_find",               "\r",       Dired_Buffer);
definekey ("dired_updir",              Key_BS,     Dired_Buffer);
definekey ("dired_reread_dir",         "r",        Dired_Buffer);
definekey ("dired_open",               "o",        Dired_Buffer);
definekey ("dired_flag_backup",        "~",        Dired_Buffer);

definekey ("dired_up",                 Key_Up,        Dired_Buffer);
definekey ("dired_down",               Key_Down,      Dired_Buffer);
definekey ("dired_pgdn",               Key_PgDn,      Dired_Buffer);
definekey ("dired_pgup",               Key_PgUp,      Dired_Buffer);
definekey ("dired_top",                Key_Ctrl_PgUp, Dired_Buffer);
definekey ("dired_bottom",             Key_Ctrl_PgDn, Dired_Buffer);
definekey ("dired_top",                Key_Home,      Dired_Buffer);
definekey ("dired_bottom",             Key_End,       Dired_Buffer);
definekey ("dired_dot",                ".",           Dired_Buffer);

% ******************************************************************************
% Recentre the screen and move the cursor to the beginning of the line
% ******************************************************************************

static define limit_cursor()
{
   emacs_recenter();
   bol();
}

% ******************************************************************************

define dired_up()
{
   go_up_1();
   limit_cursor();
}

% ******************************************************************************

define dired_down()
{
   go_down_1();
   limit_cursor();
}

% ******************************************************************************

define dired_pgup()
{
   () = up(window_info('r')-1);
   limit_cursor();
}

% ******************************************************************************

define dired_pgdn()
{
   () = down(window_info('r')-1);
   limit_cursor();
}

% ******************************************************************************

define dired_top()
{
   bob();
   limit_cursor();
}

% ******************************************************************************

define dired_bottom()
{
   eob();
   limit_cursor();
}

% ******************************************************************************

define dired_quit()
{
   delbuf(Dired_Buffer);
}

% ******************************************************************************
% takes somthing like dev:[a.b.c] and converts it to dev:[a.b]c.dir
% ******************************************************************************

#ifdef VMS
define dired_vms_dir (dir)
{
   variable cdir = Null_String, last = Null_String,  this;
   variable n, file, ch;

   n = strlen (dir);
      if (int (substr (dir, n, 1)) != ']')
         return dir;

   forever
     {
   n--;
         !if (n)
            break;

   ch = int (substr (dir, n, 1));

         if ((ch == '.') or (ch == '['))
            break;
      }

   if (ch == '[')
     substr (dir, 1, n) +  "000000]";
   else
     substr (dir, 1, n - 1) + "]";

   cdir = ();

   file = substr (dir, n + 1, strlen (dir));
   file = substr (file, 1, strlen (file) - 1) + ".dir";
   cdir + file;
}
#endif

%============================================================================
% Populate the buffer

static define dired_directory()
{
   variable filelist, size, i, dir, line;

   (,dir,,) = getbuf_info ();

   size = directory(dir+"*");

   if(dir != "/")
   {
      "..";
      size += 1;
   }

   if(size==0)
   {
      insert("No files found!\n");
      return;
   }

   for(i=0; i<size; i++)
   {
      variable filename, err, status;

      filename = ();

      if(Hide_Dotfiles and filename[0] == '.' and filename != "..")
      {
         continue;
      }

      status = stat_file(dir+filename);

      goto_column(FILE_COL);

      insert(sprintf("%s", filename));

      if(status != NULL)
      {
         if(stat_is("dir", status.st_mode))
         {
            insert("/");
         }
      }

      goto_column(SIZE_COL);
      del_eol();

      if(status == NULL)
      {
         insert("ERROR");
      }
      else
      {
         !if(stat_is("dir", status.st_mode))
         {
            insert(sprintf("%12d  ", status.st_size));
         }

         goto_column(ATTR_COL);

#ifndef MSDOS OS2 WIN32 IBMPC_SYSTEM
         if(stat_is("sock", status.st_mode))
            insert("s");
         else if(stat_is("fifo", status.st_mode))
            insert("f");
         else if(stat_is("blk", status.st_mode))
            insert("b");
         else if(stat_is("chr", status.st_mode))
            insert("c");
         else
#endif
         if(stat_is("dir", status.st_mode))
            insert("d");
         else if(stat_is("lnk", status.st_mode))
            insert("l");
         else
            insert("-");

         if(status.st_mode & S_IRUSR) insert("r"); else insert("_");
         if(status.st_mode & S_IWUSR) insert("w"); else insert("_");

#ifndef MSDOS OS2 WIN32 IBMPC_SYSTEM
         if(status.st_mode & S_IXUSR) insert("x"); else insert("_");

         insert("-");

         if(status.st_mode & S_IRGRP) insert("r"); else insert("_");
         if(status.st_mode & S_IWGRP) insert("w"); else insert("_");
         if(status.st_mode & S_IXUSR) insert("x"); else insert("_");

         insert("-");

         if(status.st_mode & S_IXOTH) insert("r"); else insert("_");
         if(status.st_mode & S_IXOTH) insert("w"); else insert("_");
         if(status.st_mode & S_IXOTH) insert("x"); else insert("_");
#endif
         goto_column(DATE_COL);

         insert(sprintf("%s", ctime(status.st_mtime)));
      }

      if(i < size-1)
      {
         newline();
      }
   }

   % Sort the listing to put directories first

   bob();
   () = goto_column_best_try(ATTR_COL);
   set_mark_cmd();
   eob();
   eol();
   rev_sort_eol();

   eob(); eol();
   if(bolp())
      go_up_1();

   % Find the last directory in the list

   bob();
   set_mark_cmd();
   () = goto_column_best_try(ATTR_COL);

   while(looking_at_char('d'))
   {
      !if(down(1))
      {
         break;
      }
      () = goto_column_best_try(ATTR_COL);
   }

   % Add a blank line after the directories then sort them

   go_up_1();
   eol();
   newline();
   line = what_line();
   go_up_1();
   sort_eol();

   % Stick a spot on the first file in the file

   goto_line(line);
   go_down_1();
   bol();

   % Now sort the file list

   set_mark_cmd();
   eob();
   go_up_1();
   sort_eol();
   trim_buffer();

   % Move to the first file in the list

   goto_line(line);
   () = goto_column_best_try(FILE_COL);
}

% ******************************************************************************

define dired_read_dir (dir)
{
   variable file, flags, spaces = "  ";

#ifdef VMS
   dir = dired_vms_dir (dir);
#elifdef IBMPC_SYSTEM
   dir = expand_filename (dir);
# ifdef MSDOS MSWINDOWS
   dir = msdos_fixup_dirspec (dir);
# endif
#endif

   if ( file_status (dir) == 2 )
   {
      (Dired_Current_Directory,) = parse_filename (dircat (dir, Dired_Buffer));
#ifdef VMS
      dir = Dired_Current_Directory;
#endif
   }
   else
   {
      (Dired_Current_Directory,dir) = parse_filename (dir);
   }

   if ( change_default_dir (Dired_Current_Directory) )
      error ("Failed to chdir.");

   sw2buf (Dired_Buffer);
   (file,,,flags) = getbuf_info ();
   setbuf_info (file, Dired_Current_Directory, Dired_Buffer, flags);
   set_status_line (" DIRED: "+Dired_Current_Directory+" | %b   (%m%n)  (%p)  |  press '?' for help.", 0);
   set_readonly (0);
   erase_buffer ();
   use_keymap (Dired_Buffer);
   set_mode ("dired", 0);

#ifdef VMS
   shell_cmd ("directory/size/date/prot/notrail " + dir);
#else
   dired_directory();
#endif

#ifdef VMS
   push_mark ();
   go_down (3);
   del_region ();

   do
   {
      insert (spaces);
   }
   while (down_1 ());

   bob ();
#endif

   set_buffer_modified_flag (0);
   set_readonly(1);
   flush ("");
}

% ******************************************************************************

define dired_reread_dir ()
{
   dired_read_dir (Dired_Current_Directory);
}

% ******************************************************************************

define dired_dot()
{
   Hide_Dotfiles = 1-Hide_Dotfiles;
   dired_reread_dir();
}

% ******************************************************************************
% set the point at the start of the file name
% ******************************************************************************

define dired_point (dirn)
{
   if (dirn > 0)
      go_down_1 ();
   else if (dirn < 0)
      go_up_1 ();

#ifdef VMS
   bol ();
   go_right (2);
#else
   () = goto_column_best_try(FILE_COL);
#endif
}

% ******************************************************************************

#ifndef VMS
define dired_kill_line ()
{
   bol ();
   if ( bobp () )  return;    % do not remove top line
   set_readonly (0);
   push_mark (); go_down_1 ();
   del_region ();
   set_buffer_modified_flag (0);
   set_readonly (1);
   dired_point (0);
}
#endif

% ******************************************************************************
% (name, type) = dired_getfile ()

% name = name of file or directory

% type = 0 : nothing
% type = 1 : file
% type = 2 : directory
% type = 3 : link
% ******************************************************************************

define dired_getfile ()
{
   variable name, type, ext, stat_buf;

   bol ();
   type = not (bobp ());      % assume it will be a file

#ifdef VMS
   if (type)
     {
   go_right (2);
   if ( ffind ( ".DIR" ) )
     {
        type = 2;
     }
   else
     {
        skip_white ();
        if ( what_column () > 3 )
          type = 0;
     }
     }

   !if ( type )
     {
   bol ();
   return (Null_String, type);
     }

   dired_point (0);
   push_mark ();
   () = ffind_char (';');
   skip_chars (";0-9");
   name = bufsubstr ();       % leave on the stack
#else

   if(goto_column_best_try(ATTR_COL) == ATTR_COL)
   {
      if(looking_at_char('d'))
      {
        type = 2;
     }
   else
     {
         type = 1;
      }
   }
   else
   {
          type = 0;
   }
# endif

   !if ( type )
     {
   bol ();
   return (Null_String, type);
     }

#ifdef VMS
   dired_point (0);
   push_mark ();        % extract the name
   skip_chars ("^ ");         % skip past name
   name = bufsubstr ();       % the name
   skip_white ();

   if ( what_column () <= 12 )
     {
   push_mark ();
      skip_chars ("^ ");      % skip past ext
   ext = bufsubstr ();
   if (strlen (ext) or (type != 2))
     name = sprintf ("%s.%s", name, ext);
     }
#else
   () = goto_column_best_try(FILE_COL);
   push_mark();
   () = goto_column_best_try(SIZE_COL-1);
   name = strtrim(bufsubstr());
# endif

   dired_point (0);
   return (name, type);
}

% ******************************************************************************
% tag/untag the current file in the listing
% ******************************************************************************

define dired_tag_toggle ()
{
   variable type;

   (, type) = dired_getfile ();

   if ( type != 1 )
      return;     % only files!

   bol();

   set_readonly (0);
   if(looking_at_char(Tag_Char))
   {
      insert_char (32);
   }
   else
   {
      insert_char (Tag_Char);
   }

   del ();
   set_buffer_modified_flag (0);
   set_readonly (1);

   dired_point(1);
}

% ******************************************************************************
% perform operation on tagged files
% ******************************************************************************

define dired_xop_tagged_files (prompt, msg, op_function)
{
   variable lbuf = " *Deletions*";
   variable stack, n, fails = Null_String;
   variable file;

   setbuf (Dired_Buffer);
   push_spot_bob ();

   stack = _stkdepth;         % save stack depth
   ERROR_BLOCK
   {
      _pop_n ( _stkdepth - stack );
      sw2buf (Dired_Buffer);
      set_readonly (0);
      bob ();
      while ( bol_fsearch_char ('%') )
      {
         insert_char (Tag_Char);
         del ();
      }
      pop_spot ();
      set_buffer_modified_flag (0);
      set_readonly (1);
   }

   set_readonly (0);

   while ( bol_fsearch_char (Tag_Char) )
   {
      insert_char ('%'); del ();
      dired_getfile ();
      pop ();        % pop type, leave name on stack
   }

   n = _stkdepth - stack;
   !if (n)
      error ("No tags!");

   sw2buf (lbuf);
   erase_buffer ();

   loop (n)
   {
      insert ();           % tagged files on stack
      newline ();
   }

   bob ();
   buffer_format_in_columns ();
   if ( get_yes_no (prompt) == 1)
   {
      sw2buf (Dired_Buffer);
      bob ();
      while ( bol_fsearch_char ('%') )
      {
         (file,) = dired_getfile ();
         bol ();
         push_spot ();
         file = dircat (Dired_Current_Directory, file);
         if ((@op_function)(file) )
         {
            pop_spot ();
            flush (msg + file);
            push_mark ();
            go_down_1 ();
            del_region ();
            go_left_1 ();
         }
         else
         {
            pop_spot ();
            fails += " " + file;
            insert_char (32);
            del ();
         }
      }
   }

   EXECUTE_ERROR_BLOCK;

   if ( strlen (fails) )
     message ("Operation Failed:" + fails);
}

% ******************************************************************************

define dired_delete ()
{
   dired_xop_tagged_files ("Delete these files", "Deleted ", &delete_file);
}

% ******************************************************************************

define dired_open ()
{
   dired_xop_tagged_files ("Open these files", "Opened ", &read_file);
}

% ******************************************************************************

variable Dired_Move_Target_Dir;

define dired_do_move (file)
{
   variable name;

   (name,) = dired_getfile ();
   name = dircat (Dired_Move_Target_Dir, name);

   not (rename_file (file, name));
}

% ******************************************************************************

define dired_move ()
{
   Dired_Move_Target_Dir = read_file_from_mini ("Move to dir");

   if ( file_status (Dired_Move_Target_Dir) != 2 )
     error ("Expecting directory name");

   dired_xop_tagged_files ("Move these to " + Dired_Move_Target_Dir, "Moved ", &dired_do_move);
}

% ******************************************************************************

#ifndef VMS
define dired_flag_backup ()
{
   variable name, type;

   push_spot_bob (); set_readonly (0);
   while ( fsearch_char ( '~' ) )
     {
   (name, type) = dired_getfile ();
   if ( (type == 1) and (string_match (name, "~", strlen(name))) )
     {
        bol ();
         insert_char (Tag_Char);
         del ();  % is a backup file
      }
   eol ();
     }

   pop_spot ();
   set_buffer_modified_flag (0);
   set_readonly (1);
}
#endif

% ******************************************************************************

define dired_rename ()
{
   variable oldf, type, len, f, n, nf, nd, od, status;

   error("dired_rename!");

   (oldf, type)  = dired_getfile ();
   !if ( type ) return;
   sprintf ("Rename %s to", oldf);
   n = read_file_from_mini (());

   %  If new name is a dir, move it to the dir with oldname.
   %  If file is not a directory and exists, signal error.

   status = file_status (n);
   if ( status == 1 )
      error ("File exists. Not renamed.");
   else if ( status == 2 )
      n = dircat (n, oldf);

   %  Check to see if rename to new directory

   (nd,nf) = parse_filename (n);
   f = dircat (Dired_Current_Directory, oldf);

   (od,) = parse_filename (f);

   if ( rename_file (f, n) )
      error ("Operation Failed!");

   set_readonly (0);
#ifdef UNIX
   if ( strcmp (od, nd) )
#elifdef VMS IBMPC_SYSTEM
   if ( strcmp (strup (od), strup (nd)) )
#endif
     {
   delete_line ();
     }
   else
     {
        dired_point (0);
   push_mark ();
#ifdef MSDOS MSWINDOWS
      go_right (12);
      del_region ();

   variable nf1 = extract_element (nf, 1, '.');

      if (nf1 == NULL)
         nf1 = "";

   vinsert ("%-8s %-3s",
      extract_element (nf, 0, '.'), nf1);
#else
   go_right (strlen (oldf)); del_region ();
   insert (nf);
#endif
     }
   dired_point (1);
   set_buffer_modified_flag (0);
   set_readonly (1);
}

% ******************************************************************************

static define update_dired_hook ()
{
   Line_Mark = create_line_mark (11);
   message(Dired_Quick_Help);
   dired_point(0);
}

% ******************************************************************************

define dired_find ()
{
   variable name, type;

   (name, type) = dired_getfile ();

   name = dircat (Dired_Current_Directory, name);

   if ( type == 1 )
   {
      !if ( read_file (name) )
         error ("Unable to read file.");
      pop2buf (whatbuf ());
   }
   else if ( type == 2 )
   {
      dired_read_dir (name);
   }
}

% ******************************************************************************

define dired_updir()
{
   dired_read_dir("..");
}

% ******************************************************************************

define dired_view ()
{
   variable name, type;

   (name, type) = dired_getfile ();

   name = dircat (Dired_Current_Directory, name);
   if ( type == 1 )
   {
      !if ( read_file (name) )
         error ("Unable to read file.");

      pop2buf (whatbuf ());
      most_mode ();
   }
}

% ******************************************************************************

#ifndef VMS
define dired_search_files ()
{
   go_right_1 ();    % start after this one
   if ( fsearch (LAST_SEARCH) )
     return (1);     % found - stop search

   if ( buffer_modified () ) error ("buffer has been modified");
   delbuf (whatbuf ());
   pop2buf (Dired_Buffer);
   return 0;
}
#endif

% ******************************************************************************

#ifndef VMS

define dired_search ()
{
   variable str, name, type;

   !if ( bufferp (Dired_Buffer) )
      error ( "*dired* not available.");

   if ( strcmp (Dired_Buffer, whatbuf () ) ) % continue last search
   {
      !if ( strlen (LAST_SEARCH) )
         error ("No specified search string");

      if ( dired_search_files () )
         return;

      go_down_1 ();        % do the next file!
   }
   else
   {
      str = read_mini ("dired_search:", Null_String, LAST_SEARCH);
      !if ( strlen (str) )
         error ("Specify search string");

      save_search_string (str);
   }

   do
   {
      (name, type) = dired_getfile ();
      if ( type == 1 )        % only search files
      {
         name = dircat (Dired_Current_Directory, name);
         !if ( read_file (name) )
            error ("Unable to read file.");

         if ( dired_search_files () )
         {
            pop2buf (whatbuf ());
            return;
         }
      }
   }
   while ( down_1 () );
}
#endif

% ******************************************************************************
%!%+
%\function{dired}
%\synopsis{dired}
%\description
% Mode designed for maintaining and editing a directory.
%
% To invoke Dired, do \var{M-x dired} or \var{C-x d} (emacs)
%
% Dired will get a listing of files in the directory associated with the current
% buffer.
%
% The primary use of Dired is to "flag" files for deletion and then delete
% the previously flagged files.
%
% \var{t}   Flag this file for deletion.
% \var{u}   Remove deletion flag on this line.
% \var{~}   Flag all backup files for deletion.
%
% \var{d}   Delete all flagged files.  Dired will show a list of the
%  files tagged for deletion and ask for confirmation before actually
%  deleting the files.
%
% \var{n}   Rename file on the current line; prompts for a newname
% \var{m}   Move tagged files to a new dir; prompts for dir name
%
% \var{r}   Update the entire contents of the Dired buffer
%
% \var{CR}  Visit the file described on the current line, like typing
%  \var{M-x find_file} and supplying that file name.  If current line is a
%  directory, runs dired on the directory and the old buffer is killed.
%
% \var{v}   View the file described on the current line in MOST mode.
%
% \var{q}   Quit dired mode.
%
% \var{M-x dired_search}
%  use fsearch to perform a search through the files listed in the
%  dired buffer from the current point forward.  \var{M-x dired_search}
%  from the visited file will revert to the dired buffer and continue
%  the search from the next file in the list.
%
%!%-
% ******************************************************************************

define dired ()
{
   variable dir;

   (,dir,,) = getbuf_info ();

   dired_read_dir(dir);

   set_buffer_hook ("update_hook", &update_dired_hook);
   run_mode_hooks ("dired_hook");
}
