ifnot(keymap_p("RUST")) make_keymap("RUST");
definekey("c_newline_and_indent", "\r", "RUST");

% Now create and initialize the syntax tables.
create_syntax_table("RUST");
define_syntax("/*", "*/", '%', "RUST");
define_syntax("//", "", '%', "RUST");
define_syntax("([{", ")]}", '(', "RUST");
define_syntax('"', '"', "RUST");
define_syntax('\'', '\'', "RUST");
define_syntax('\\', '\\', "RUST");
define_syntax("0-9a-zA-Z_", 'w', "RUST");  % words
define_syntax("-+0-9a-fA-F.xXUL", '0', "RUST");
define_syntax(",;.?:", ',', "RUST");
define_syntax('#', '#', "RUST");
define_syntax("%-+/&*=<>|!~^", '+', "RUST");
set_syntax_flags("RUST", 0x4 | 0x40);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback(name)
{
   dfa_enable_highlight_cache("rustmode.dfa", name);
   dfa_define_highlight_rule("^[ \t]*#", "PQpreprocess", name);
   dfa_define_highlight_rule("//.*", "comment", name);
   dfa_define_highlight_rule("/\\*.*\\*/", "Qcomment", name);
   dfa_define_highlight_rule("^([^/]|/[^\\*])*\\*/", "Qcomment", name);
   dfa_define_highlight_rule("/\\*.*", "comment", name);
   dfa_define_highlight_rule("^[ \t]*\\*+([ \t].*)?$", "comment", name);
   dfa_define_highlight_rule("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
   dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?",
                             "number", name);
   dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*[LlUu]*", "number", name);
   dfa_define_highlight_rule("[0-9]+[LlUu]*", "number", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"", "string", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*'", "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule("[ \t]+", "normal", name);
   dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
   dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);
   dfa_build_highlight_table(name);
}
dfa_set_init_callback(&setup_dfa_callback, "RUST");
%%% DFA_CACHE_END %%%
#endif

% Keywords

append_keywords("RUST", 0,
                "as", "break", "const", "continue", "crate", "else", "enum", "extern",
                "false", "fn", "for", "if", "impl", "in", "let", "loop", "match", "mod",
                "move", "mut", "pub", "ref", "return", "Self", "self", "static", "struct",
                "super", "trait", "true", "type", "unsafe", "use", "where", "while",
                "usize");

% Reserved keywords

append_keywords("RUST", 1,
                "abstract", "alignof", "become", "box", "do", "final", "macro", "offsetof",
                "override", "priv", "proc", "pure", "sizeof", "typeof", "unsized", "virtual",
                "yield", "with_capacity");

append_keywords("RUST", 2,
                "String");

append_keywords("RUST", 3,
                "from", "println", "writeln", "push_str", "clone", "push");

append_keywords("RUST", 4,
                "i32", "u64", "Vec");


define rust_mode()
{
   set_mode("RUST", 2);

   mode_set_mode_info("RUST", "fold_info", "/*{{{\r/*}}}\r*/\r*/");
   mode_set_mode_info("RUST", "dabbrev_case_search", 0);
   use_syntax_table("RUST");
   set_comment_info("RUST", "//", "/*", "*/", 0);
   USE_TABS = 0;
   TAB = 4;

   run_mode_hooks("rust_mode_hook");
}

provide("rust_mode");
