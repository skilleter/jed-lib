#!perl -w
use strict;	# yes even for such a small program!

@ARGV = 'perldoc -u perlfunc|';
while (<>) { /^=head2\s+Alphabetical/ and last }	# cue up

my %kw = map { $_ => length } map { /^=item\s+([a-z\d]+)/ } <>;  # keywords

## standard keywords + carp/croak (which everyone always uses)
for (qw(if else elsif for foreach unless until while carp croak)) {
    $kw{$_} = length
}
delete @kw{ grep { /^dbm/ } keys %kw };	# obsolete

my @list;		# store sorted keywords by length
$list[$kw{$_}] .= $_  for ( sort keys %kw );

splice @list, 0, (my $n = 2);	# keywords with < 2 letters are useless

for (@list) {
  defined and length
    and print "() = define_keywords(\$1,\n  \"$_\",\n  $n);\n";
  $n++
}
__END__
#endif	% done processing perl
% --------------------------------------------------------------- [end of Perl]
