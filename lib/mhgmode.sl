%==========================================================================
% MHGMODE.SL
%
% MHEG mode - based on C mode
%
%==========================================================================

!if(keymap_p ("MHEG"))
        make_keymap ("MHEG");

% -------------------------------------------------------------------------
% Word characters
% -------------------------------------------------------------------------

define_word ("0-9A-Za-z_");

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------

% Now create and initialize the syntax tables.
create_syntax_table ("MHEG");

% define_syntax ("/*", "*/",    '%',            "MHEG");
define_syntax ("([{", ")]}",    '(',            "MHEG");
define_syntax ('"',                     '"',            "MHEG");
define_syntax ('\'',                    '\'',           "MHEG");
% define_syntax ('\\',                  '\\',           "MHEG");
define_syntax ("0-9a-zA-Z_",    'w',            "MHEG");                % words
define_syntax ("-+0-9",                 '0',            "MHEG");        % Numbers
define_syntax (",;.?:",                 ',',            "MHEG");
% define_syntax ('#',                   '#',            "MHEG");
define_syntax ("%-+/&*=<>|!~^", '+',            "MHEG");

set_syntax_flags ("MHEG", 1);

#ifdef HAS_DFA_SYNTAX
private define setup_dfa_callback (name)
{
        dfa_enable_highlight_cache ("mhgmode.dfa", name);
        % dfa_define_highlight_rule ("^[ \t]*#", "PQpreprocess", name);
        % dfa_define_highlight_rule ("//.*", "comment", name);
        % dfa_define_highlight_rule ("/\\*.*\\*/", "Qcomment", name);
        % dfa_define_highlight_rule ("^([^/]|/[^\\*])*\\*/", "Qcomment", name);
        % dfa_define_highlight_rule ("/\\*.*", "comment", name);
        % dfa_define_highlight_rule ("^[ \t]*\\*+([ \t].*)?$", "comment", name);
        dfa_define_highlight_rule ("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
        dfa_define_highlight_rule ("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?",
                                                "number", name);
        % dfa_define_highlight_rule ("0[xX][0-9A-Fa-f]*[LlUu]*", "number", name);
        dfa_define_highlight_rule ("[0-9]+[LlUu]*", "number", name);
        dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\"", "string", name);
        dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*'", "string", name);
        dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule ("[ \t]+", "normal", name);
        dfa_define_highlight_rule ("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
        dfa_define_highlight_rule ("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);
        dfa_build_highlight_table (name);
}
dfa_set_init_callback (&setup_dfa_callback, "MHEG");
#endif

% Type 0 keywords

append_keywords ("MHEG", 1,
                                          "run",                                "link",                                         "quit",                                         "stop",                                 "items",
                                          "scene",                              "bitmap",                               "tiling",                               "scenecs",                              "setdata",
                                          "settimer",                   "eventdata",                    "eventtype",                    "rectangle",                    "contentref",
                                          "linkeffect",                 "sendtoback",                   "setpoition",                   "application",          "eventsource",
                                          "group-items",                "origboxsize",          "origcontent",          "setposition",                  "bringtofront",
                                          "content-data",       "content-hook",                 "movingcursor",                 "origposition",                 "setlinestyle",
                                          "setlinewidth",       "transitionto",                 "inputeventreg",                "newrefcontent",                "origlinewidth",
                                          "setfillcolour",      "setlinecolour",                "origfillcolour",       "origlinecolour",    "newabsolutecolour",
                                          "original-box-size",
                                          "original-position",
                                          "origreffillcolour",
                                          "origreflinecolour");

% Type 0 keywords

append_keywords("MHEG", 0, "true", "false", "userinput", "timerfired");

define mheg_mode ()
{
        variable kmap = "MHEG";

        set_abbrev_mode (1);

        set_mode (kmap, 2);
        use_keymap (kmap);
        use_syntax_table (kmap);
%       set_buffer_hook ("par_sep", "c_paragraph_sep");
%       set_buffer_hook ("indent_hook", "c_indent_line");
%       set_buffer_hook ("newline_indent_hook", "c_newline_and_indent");
   set_comment_info(kmap, NULL, NULL, NULL, 0);
        run_mode_hooks ("mhg_mode_hook");
}
