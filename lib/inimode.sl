create_syntax_table ("ini");

define_syntax ("#", "", '%', "ini");            % comments
define_syntax ("([{", ")]}", '(', "ini");               % delimiters
define_syntax ('"', '"', "ini");                        % quoted strings
define_syntax ('\'', '\'', "ini");                      % quoted characters
define_syntax ("0-9a-zA-Z_", 'w', "ini");               % words
define_syntax ("-+0-9.eE", '0', "ini");         % Numbers
define_syntax (",;.", ',', "ini");                      % punctuation
define_syntax ("$@%-+/*=<>!^", '+', "ini");             % operators

set_syntax_flags ("ini", 0);                    % keywords ARE case-sensitive

define ini_mode ()
{
   TAB = 4;
   set_mode ("ini", 0x4); % flag value of 4 is generic language mode
   use_syntax_table ("ini");
   set_comment_info("ini", "#", NULL, NULL, 0);
   run_mode_hooks ("ini_mode_hook");
}
