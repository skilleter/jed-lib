%==========================================================================
% dosmode.sl
%
% Syntax highlighting for config.sys and batch files
%
%==========================================================================

!if(keymap_p ("DOS"))
   make_keymap ("DOS");

%definekey ("indent_line", "\t", "DOS");
%definekey ("c_newline_and_indent", "\r", "DOS");

% -------------------------------------------------------------------------
% Word characters
% -------------------------------------------------------------------------

define_word ("0-9A-Za-z_");

% -------------------------------------------------------------------------
% Now create and initialize the syntax tables.
% -------------------------------------------------------------------------

create_syntax_table ("DOS");

define_syntax ("rem",                   "\n",   '%',    "DOS");
define_syntax ("REM",                   "\n",   '%',    "DOS");
define_syntax ("::",                    "\n",   '%',    "DOS");
define_syntax ("[(",                    "])", '(',      "DOS");
define_syntax ('"',                                             '"',    "DOS");
define_syntax ("0-9a-zA-Z_",                    'w',    "DOS"); % words
define_syntax ('`',                                             '"',    "DOS");

define_syntax ("-+0-9.",                                '0',    "DOS");  % Numbers

define_syntax (",;.?_",                                 ',',    "DOS");
define_syntax ("@%-+/&*=<>|!~^\\",      '+',    "DOS");

set_syntax_flags ("DOS", 1);

#ifdef HAS_DFA_SYNTAX
private define setup_dfa_callback (name)
{
        dfa_enable_highlight_cache ("dosmode.dfa", name);

        %dfa_define_highlight_rule("^[ \t]*#",                                     "PQpreprocess", name);
        %dfa_define_highlight_rule("^rem",                                         "comment", name);
        %dfa_define_highlight_rule("/\\*.*\\*/",                               "Qcomment", name);
        %dfa_define_highlight_rule("^([^/]|/[^\\*])*\\*/",                         "Qcomment", name);
        %dfa_define_highlight_rule("/\\*.*",                                   "comment", name);
        %dfa_define_highlight_rule("^[ \t]*\\*+([ \t].*)?$",                   "comment", name);
        dfa_define_highlight_rule ("^:.*$",                                      "preprocess", name);
        dfa_define_highlight_rule ("^[rR][eE][mM].*$",                           "comment", name);
        dfa_define_highlight_rule ("[A-Za-z_\\$][A-Za-z_0-9\\$]*",               "Knormal", name);
        dfa_define_highlight_rule ("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?",   "number", name);
        dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\"",                     "string", name);
        dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\\\\?$",                 "string", name);
        dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*'",                        "string", name);
        dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*\\\\?$",                   "string", name);
        dfa_define_highlight_rule ("[ \t]+",                                     "normal", name);
        dfa_define_highlight_rule ("[\\(\\[{}\\]\\),;\\.\\?:]",                  "delimiter", name);
        dfa_define_highlight_rule ("[%\\-\\+/&\\*=<>\\|!~\\^@\\\\]",             "operator", name);
        dfa_build_highlight_table (name);
}
dfa_set_init_callback (&setup_dfa_callback, "DOS");
#endif

% Type 1 keywords

() = define_keywords_n ("DOS", "?y", 1, 1);
() = define_keywords_n ("DOS", "cddoifmdonrd", 2, 1);
() = define_keywords_n ("DOS", "andcddclsdeldirdoseraforifflognotoffpopremrensetteeumbvervol", 3, 1);
() = define_keywords_n ("DOS", "beepcallcasechcpcopycttydatedirsechoelseesetexitfcbsfreegotohelphighlistlockmovepathpopdpushquittextthentimetreetype.or.", 4, 1);
() = define_keywords_n ("DOS", "aliasbreakcolordelayechosechosenddoeraseexistffindfilesgosubinkeyinputisdirkeybdleavemkdirpausepushdshellshiftstarttimertouchunsetuntilwhile", 5, 1);
() = define_keywords_n ("DOS", "attribcanceldeletedeviceendiffexceptglobalmemoryoptionpromptrebootrenamereturnscreenscrputselectsetdosstacksswitchunlockverifywindow", 6, 1);
() = define_keywords_n ("DOS", "bufferscountrydefaultdrawboxechoerrelseiffendtexthistoryiterateloadbtmnumlockunaliasvscrput", 7, 1);
() = define_keywords_n ("DOS", "describeechoserrendlocalkeystackloadhighmenuitemsetlocalswappingswitchestruename", 8, 1);
() = define_keywords_n ("DOS", "drawhlinedrawvlineendswitchlastdrive", 9, 1);
() = define_keywords_n ("DOS", "devicehighdirhistory", 10, 1);
() = define_keywords_n ("DOS", "menudefault", 11, 1);
() = define_keywords_n ("DOS", "", 12, 1);
() = define_keywords_n ("DOS", "", 13, 1);
() = define_keywords_n ("DOS", "", 14, 1);

% Type 0 keywords

() = define_keywords_n ("DOS", "?", 1, 0);
() = define_keywords_n ("DOS", "%#??eqgegtinlelhltneor_?", 2, 0);
() = define_keywords_n ("DOS", "@ifdaydecdowdoyemsextincintlenlfnlptmaxsfnxms_bg_ci_co_dv_fg", 3, 0);
() = define_keywords_n ("DOS", "charclipdowievalexecfullleftlinenameresttrimwildwordyear_cpu_cwd_cwp_day_dos_dow_doy_env_ndp_row_win", 4, 0);
() = define_keywords_n ("DOS", "@date@timeasciicdromcommaerrorfilesindexinstrlabellineslowermonthreadyrightstripupperwords_4ver_ansi_boot_cwds_cwps_date_disk_dowi_dpmi_hour_rows_time_year", 5, 0);
() = define_keywords_n ("DOS", "@aliasattribdevicedosmemexpandformatinsertmasterrandomremoterepeatsearchselectsubstrunique_alias_apmac_batch_dname_kbhit_month_mouse_shell_video", 6, 0);
() = define_keywords_n ("DOS", "convertexecstrfileageforeverislabelmakeagenumericreadscrreplace_column_dosver_kstack_minute_second_syserr", 7, 0);
() = define_keywords_n ("DOS", "descriptdiskfreediskusedextendedfiledatefilenamefileopenfilereadfileseekfilesizefiletimefindnextmaketimetruename_apmbatt_apmlife_columns_country_logfile_monitor", 8, 0);
() = define_keywords_n ("DOS", "disktotalfileclosefileseeklfilewritefindclosefindfirstremovable_codepage_hlogfile_lastdisk_swapping_wintitle", 9, 0);
() = define_keywords_n ("DOS", "filewriteb_batchline_batchname_transient", 10, 0);
() = define_keywords_n ("DOS", "", 11, 0);
() = define_keywords_n ("DOS", "", 12, 0);
() = define_keywords_n ("DOS", "", 13, 0);
() = define_keywords_n ("DOS", "", 14, 0);
() = define_keywords_n ("DOS", "", 15, 0);
() = define_keywords_n ("DOS", "", 16, 0);

define dos_mode ()
{
   variable kmap = "DOS";

   set_abbrev_mode (1);

   set_mode (kmap, 2);
   use_keymap (kmap);
   use_syntax_table (kmap);
   set_comment_info(kmap, "REM ", NULL, NULL, 0);
   run_mode_hooks ("dos_mode_hook");
}

