% ******************************************************************************
% My editor mode for Jed - split from defaults.sl as of v0.99.13 as defaults.sl
% is executed before the editor mode file.
% ******************************************************************************

_debug_info = 1;
_traceback = 1;

% ******************************************************************************
% Add the underscore and digits to the list of characters that form a word
% ******************************************************************************

define_word("0-9A-Za-z_");

_Reserved_Key_Prefix = "^D";

!if(abbrev_table_p("Global"))
{
   create_abbrev_table("Global", "");
}

%==========================================================================
% Lots of autoloads
%==========================================================================

require("keydefs");
require("faketab");
require("ctags");

autoload("x_cut_region_to_clipboard",        "defaults");
autoload("x_clipboard_insert",               "defaults");
autoload("x_copy_region_to_clipboard",       "defaults");
autoload("update_statusline",                "defaults");
autoload("undefined_key",                    "defaults");

#ifdef MSWINDOWS
   autoload("win_font_size",              "defaults");
   autoload("win_font_select",            "defaults");
#endif

autoload("backspace",                  "defaults");
autoload("x_cut_region_to_cutbuffer",  "defaults");
autoload("load_file_under_cursor",     "defaults");
autoload("source_control_menu",        "scs");
autoload("bas_mode",                   "basmode");
autoload("sql_mode",                   "sqlmode");
autoload("s_mode",                     "smode");
autoload("notesmode",                  "notesmode");
autoload("cscope",                     "cscope");
autoload("rexx_mode",                  "rexxmode");
autoload("dos_mode",                   "dosmode");
autoload("cfg_mode",                   "cfgmode");
autoload("cmm_mode",                   "cmmmode");
autoload("ruby_mode",                  "rubymode");
autoload("make_mode",                  "makemode");
autoload("pipeline_mode",              "pipelinemode");
autoload("kconfig_mode",               "kconfigmode");
autoload("gitmsg_mode",                "gitmsgmode");
autoload("mheg_mode",                  "mhgmode");
autoload("bufed",                      "bufed");
autoload("filename_extension",         "textmac");
autoload("find_file_other_window",     "emacsmsc");
autoload("find_buffer_other_window",   "emacsmsc");
autoload("find_alternate_file",        "emacsmsc");
autoload("delete_blank_lines",         "emacsmsc");
autoload("forward_sexp",               "emacsmsc");
autoload("backward_sexp",              "emacsmsc");
autoload("kill_sexp",                  "emacsmsc");
autoload("scroll_up_in_place",         "emacsmsc");
autoload("scroll_down_in_place",       "emacsmsc");
autoload("trim_buffer",                "util");
autoload("clean_buffer",               "util");
autoload("strip_control_codes",        "util");
autoload("insert_wildcard",            "util");
autoload("save_buffers",               "buf");
autoload("macro_execute_center",       "macro");
autoload("macro_save_macro",           "macro");
autoload("macro_to_function",          "macro");
autoload("macro_assign_macro_to_key",  "macro");
autoload("save_abbrevs",               "abbrmisc");
autoload("define_abbreviation",        "abbrmisc");
autoload("occur",                      "occur");
autoload("hpcalc",                     "hpcalc");
autoload("charset",                    "charset");
autoload("keycode",                    "keycode");
autoload("ispell",                     "ispell");
autoload("ispell_region",              "ispell");
autoload("unset_keyrange",             "defaults");
autoload("pipe_region_through",        "pipe");
autoload("select_grep_tool",           "grep");
autoload("switch_to_compile_buffer",   "compile");
autoload("load_file",                  "loadfile");

%==========================================================================
% The default keybindings for Jed use ^W, ^F, and ^K keymaps.
% We want them for ourselves, so turn them off.
%
% In addition the default keybinding uses \eN, but QANSI terminal uses
% it as a leader for ALT keys, so we must undefine it to use them
%==========================================================================

unsetkey("^K");
unsetkey("^W");
unsetkey("^F");
unsetkey("\eN");

% Undefine the whole range of control, alt and meta-key keypresses so that
% we start with a clean sheet!

unset_keyrange(MetaKey,        " ", "~");
unset_keyrange("^",            "a", "z");
unset_keyrange(AltKey,         "a", "z");
unset_keyrange(AltKey,         "A", "Z");
unset_keyrange("^X",           "A", "Z");
unset_keyrange("^X",           "a", "z");
unset_keyrange(MetaKey+"^",    "A", "Z");
unset_keyrange(MetaKey+AltKey, "A", "Z");

%==========================================================================
% Define shifted cursor keys to set region
%==========================================================================

() = evalfile("wmark.sl");

%==========================================================================
% Abbreviations
%==========================================================================

!if(runhookfile("abbrev.sl"))
{
   flush("Warning: Couldn't execute abbrev.sl");
   usleep(250);
}

% ******************************************************************************
% ******************************************************************************

update_statusline();

%============================================================================
%============================================================================
%                             KEY DEFINITIONS
%============================================================================
%============================================================================

%============================================================================
% CONTROL Keys
%============================================================================

setkey("select_buff",            "^A");
setkey("format_paragraph_down",  "^B");
% ^C is used as a prefix for folding functions
setkey("isearch_forward",        "^D");
setkey("isearch_backward",       "^E");
setkey("search_command",         "^F");
set_abort_char (7);                       % ^G = abort
% ^H is used as a mode-specific prefix
setkey("ft_tab",                 "^I");
setkey("newline",                "^J");
setkey("yp_kill_line",           "^K");
setkey("search_again",           "^L");
setkey("newline_and_indent",     "^M");
setkey("other_next",             "^N");
setkey("other_window",           "^O");
setkey("other_prev",             "^P");
setkey("quoted_insert",          "^Q");
setkey("search_previous",        "^R"); % Search in opposite direction
setkey("save_buffer",            "^S");
setkey("del_word",               "^T");
setkey("yp_kill_bol",            "^U");
setkey("dabbrev",                "^V");
setkey("yp_kill_region",         "^W");
% ^X is the emacs-style prefix key
setkey("yp_yank",                "^Y");
setkey("sys_spawn_cmd",          "^Z");

setkey("goto_match",                     "^\\");

%============================================================================
% Cursor keys
%============================================================================

setkey ("move_cursor_up_1",               Key_Up);
setkey ("move_cursor_down_1",             Key_Down);

setkey ("other_prev",                     Key_Alt_Up);
setkey ("other_next",                     Key_Alt_Down);

setkey ("ft_left",                        Key_Left);
setkey ("ft_right",                       Key_Right);

setkey ("search_char_backwards",          Key_Alt_Left);
setkey ("search_char_forwards",           Key_Alt_Right);

setkey ("new_jms_skip_word(0)",           Key_Ctrl_Left);   % ^Left
setkey ("new_jms_skip_word(1)",           Key_Ctrl_Right);  % ^Right

setkey ("jms_bskip_white",                MetaKey+Key_Left);   % M-Left
setkey ("jms_skip_white",                 MetaKey+Key_Right);  % M-Right

setkey ("other_pgup",                     Key_Alt_PgUp);
setkey ("other_pgdn",                     Key_Alt_PgDn);

setkey ("bol",                            Key_Home);        % Home
setkey ("eol",                            Key_End);         % End

setkey ("jms_next_buffer",                Key_Ctrl_Home);   % ^Home
setkey ("jms_prev_buffer",                Key_Ctrl_End);    % ^End

setkey ("move_cursor_page_up",            Key_PgUp);        % PgUp
setkey ("move_cursor_page_down",          Key_PgDn);        % PgDn

setkey ("bob",                            Key_Ctrl_PgUp);   % ^PgUp
setkey ("eob",                            Key_Ctrl_PgDn);   % ^PgDn

setkey ("ft_del",                         Key_Del);         % Del
setkey ("x_cut_region_to_clipboard",      Key_Shift_Del);   % $Del = Cut to clipboard

setkey ("toggle_overwrite",               Key_Ins);         % Ins
setkey ("x_clipboard_insert",             Key_Shift_Ins);   % $Ins = Paste clipboard
setkey ("x_copy_region_to_clipboard",     Key_Ctrl_Ins);    % ^Ins = Copy to selection

setkey ("scroll_up",                      Key_Ctrl_Up);     % ^UP = scroll up
setkey ("scroll_down",                    Key_Ctrl_Down);   % ^DOWN = scroll down

setkey ("dabbrev",                        "`");             % `=expand abbreviation

%============================================================================
% ALT Keys
%============================================================================

setkey("one_window",                AltKey+"1");    % ALT+1 = single window
setkey("split_window",              AltKey+"2");    % ALT+2 = split window
setkey("comment_region_or_line",    AltKey+"3");    % ALT+3 = comment out current region
setkey("split_window_horizontal",   AltKey+"4");    % ALT+4 = horizontal window split

% Mac keyboards don't have an INS key, so we use ALT+4,5,6 for the INS
% key functions

% setkey ("toggle_overwrite",               AltKey+"4");    % ALT+4 = Ins
% setkey ("x_clipboard_insert",             AltKey+"5");    % ALT+5 = $Ins = Paste clipboard
% setkey ("x_copy_region_to_clipboard",     AltKey+"6");    % ALT+6 = ^Ins = Copy to selection

#ifdef MSWINDOWS
   setkey("win_font_select(-1)",    AltKey+"7");    % ALT+7 = select previous font
   setkey("win_font_select(1)",     AltKey+"8");    % ALT+8 = select next font
   setkey("win_font_size(-1)",      AltKey+"9");    % ALT+9 = decrease font size
   setkey("win_font_size(1)",       AltKey+"0");    % ALT+0 = increase font size
#else
   setkey("undefined_key",          AltKey+"7");    % ALT+7 =
   setkey("undefined_key",          AltKey+"8");    % ALT+8 =
   setkey("undefined_key",          AltKey+"9");    % ALT+9 =
   setkey("undefined_key",          AltKey+"0");    % ALT+0 =
#endif

setkey("abbrev_mode",               AltKey+"a");    % ALT+A = toggle abbrev mode
setkey("bufed",                     AltKey+"b");    % ALT+B
setkey("emacs_recenter",            AltKey+"c");    % ALT+C = Centre line on display
setkey("dired",                     AltKey+"d");    % ALT+D
setkey("evaluate_buffer",           AltKey+"e");    % ALT+E = Execute buffer as S-Lang
setkey("grep(0)",                   AltKey+"f");    % ALT+F = run grep
setkey("bkmrk_goto_mark",           AltKey+"g");    % ALT+G = goto mark
setkey("occur",                     AltKey+"h");    % ALT+H = occur
setkey("centre_top",                AltKey+"i");    % ALT+I = Redraw with current line at the top of the window
setkey("centre_bottom",             AltKey+"j");    % ALT+J = Redraw with current line at the bottom of the window
setkey("describe_bindings",         AltKey+"k");    % ALT+K = list keys
setkey("load_file_under_cursor",    AltKey+"l");    % ALT+L = load the file who's name is under the cursor
setkey("bkmrk_set_mark",            AltKey+"m");    % ALT+M = set mark
setkey("keycode",                   AltKey+"n");    % ALT+N = display keycode for key press
setkey("one_window",                AltKey+"o");    % ALT+O = Other window
setkey("untab_buffer",              AltKey+"p");    % ALT+P = Remove tabs from buffer
setkey("exit_jed",                  AltKey+"q");    % ALT+Q = Close buffer
setkey("reload_buffer",             AltKey+"r");    % ALT+R = Reload buffer from disk, losing changes
setkey("save_buffers",              AltKey+"s");    % ALT+S = Save all buffers
setkey("trim_buffer",               AltKey+"t");    % ALT+T = Trim buffer
setkey("show_traceback",            AltKey+"u");    % ALT+U = show traceback buffer
setkey("switch_to_output_buffer",   AltKey+"v");    % ALT+V = show shell buffer
setkey("switch_to_compile_buffer",  AltKey+"w");    % ALT+W = redraw with current line at bottom of the window
setkey("save_and_exit",             AltKey+"x");    % ALT+X
setkey("cut_line",                  AltKey+"y");    % ALT+Y
setkey("jms_test_function",         AltKey+"z");    % ALT+Z = misc. test function

setkey("enlargewin",                AltKey+",");
setkey("otherwindow(); enlargewin(); otherwindow()",  AltKey+".");

%============================================================================
% Meta/^X +/- number keys
%============================================================================

setkey("save_region_contents(0,0)",  AltKey+"=1");         % ALT = 1 - Copy to copy buffer 1
setkey("save_region_contents(1,0)",  AltKey+"=2");         % ALT = 2 - Copy to copy buffer 2
setkey("save_region_contents(2,0)",  AltKey+"=3");         % ALT = 3 - Copy to copy buffer 3
setkey("save_region_contents(3,0)",  AltKey+"=4");         % ALT = 4 - Copy to copy buffer 4
setkey("save_region_contents(4,0)",  AltKey+"=5");         % ALT = 5 - Copy to copy buffer 5
setkey("save_region_contents(5,0)",  AltKey+"=6");         % ALT = 6 - Copy to copy buffer 6
setkey("save_region_contents(6,0)",  AltKey+"=7");         % ALT = 7 - Copy to copy buffer 7
setkey("save_region_contents(7,0)",  AltKey+"=8");         % ALT = 8 - Copy to copy buffer 8
setkey("save_region_contents(8,0)",  AltKey+"=9");         % ALT = 9 - Copy to copy buffer 9
setkey("save_region_contents(9,0)",  AltKey+"=0");         % ALT = 0 - Copy to copy buffer 0

setkey("save_region_contents(0,1)",  AltKey+"+1");       % ALT + 1 - Append to copy buffer 1
setkey("save_region_contents(1,1)",  AltKey+"+2");       % ALT + 2 - Append to copy buffer 2
setkey("save_region_contents(2,1)",  AltKey+"+3");       % ALT + 3 - Append to copy buffer 3
setkey("save_region_contents(3,1)",  AltKey+"+4");       % ALT + 4 - Append to copy buffer 4
setkey("save_region_contents(4,1)",  AltKey+"+5");       % ALT + 5 - Append to copy buffer 5
setkey("save_region_contents(5,1)",  AltKey+"+6");       % ALT + 6 - Append to copy buffer 6
setkey("save_region_contents(6,1)",  AltKey+"+7");       % ALT + 7 - Append to copy buffer 7
setkey("save_region_contents(7,1)",  AltKey+"+8");       % ALT + 8 - Append to copy buffer 8
setkey("save_region_contents(8,1)",  AltKey+"+9");       % ALT + 9 - Append to copy buffer 9
setkey("save_region_contents(9,1)",  AltKey+"+0");       % ALT + 0 - Append to copy buffer 0

setkey("insert_saved_region(0)",  AltKey+"-1");        % ALT - 1 - Insert from copy buffer 1
setkey("insert_saved_region(1)",  AltKey+"-2");        % ALT - 2 - Insert from copy buffer 2
setkey("insert_saved_region(2)",  AltKey+"-3");        % ALT - 3 - Insert from copy buffer 3
setkey("insert_saved_region(3)",  AltKey+"-4");        % ALT - 4 - Insert from copy buffer 4
setkey("insert_saved_region(4)",  AltKey+"-5");        % ALT - 5 - Insert from copy buffer 5
setkey("insert_saved_region(5)",  AltKey+"-6");        % ALT - 6 - Insert from copy buffer 6
setkey("insert_saved_region(6)",  AltKey+"-7");        % ALT - 7 - Insert from copy buffer 7
setkey("insert_saved_region(7)",  AltKey+"-8");        % ALT - 8 - Insert from copy buffer 8
setkey("insert_saved_region(8)",  AltKey+"-9");        % ALT - 9 - Insert from copy buffer 9
setkey("insert_saved_region(9)",  AltKey+"-0");        % ALT - 0 - Insert from copy buffer 0

%============================================================================
% ^C Prefix Keys
%============================================================================

% Only define folding if we have line attributes so that we can hide
% lines to fold them.

#ifdef HAS_LINE_ATTR
   folding_mode();
#endif

%============================================================================
% ^X Prefix Keys
%============================================================================

setkey("undefined_key",             "^XA");
setkey("switch_to_existing_buffer", "^XB");
setkey("comment_region_or_line",    "^XC");
setkey("uncomment_region_or_line",  "^XD");
setkey("execute_macro",             "^XE");
setkey("set_fill_column",           "^XF");
setkey("undefined_key",             "^XG");
setkey("emacs_mark_buffer",         "^XH");
setkey("insert_file",               "^XI");
setkey("pop_spot",                  "^XJ");
setkey("kill_buffer",               "^XK");
setkey("undefined_key",             "^XL");
setkey("calendar",                  "^XM");
setkey("undefined_key",             "^XN");
setkey("other_window",              "^XO");
setkey("jms_switch_to_last_buffer", "^XP");
setkey("macro_query",               "^XQ");

setkey("kill_rect",                 "^XRK");
setkey("open_rect",                 "^XRO");
setkey("insert_rect",               "^XRY");
setkey("copy_rect",                 "^XRR");
setkey("string_rectangle",               "^XRT");
setkey("blank_rect",                     "^XRB");

setkey("sort_eol",                  "^XS");
setkey("undefined_key",             "^XT");
setkey("undefined_key",             "^XU");
setkey("jms_new_buffer",            "^XV");

if (is_defined ("KILL_ARRAY_SIZE"))
{
   setkey("reg_copy_to_register",   "^XW");
   setkey("undefined_key",          "^XX");
   setkey("reg_insert_register",    "^XY");
   setkey("undefined_key",          "^XZ");
}
else
{
   setkey("undefined_key",          "^XW");
   setkey("undefined_key",          "^XX");
   setkey("undefined_key",          "^XY");
   setkey("undefined_key",          "^XZ");
}

setkey("insert_wildcard",           "^X*");
setkey("set_selective_display",     "^X$");
setkey("narrow_to_region",          "^X<");
setkey("widen_region",              "^X>");
setkey("c_comment_region",          "^X;");
setkey("showkey",                   "^X?");
setkey("begin_macro",               "^X(");
setkey("end_macro",                 "^X)");
setkey("mark_spot",                 "^X/");
setkey("forward_sexp",              "^X[");
setkey("backward_sexp",             "^X]");
setkey("delete_window",             "^X0");
setkey("one_window",                "^X1");
setkey("split_window",              "^X2");
setkey("split_window_horizontal",   "^X3");
setkey("digraph_cmd",                    "^X8");
setkey("pipe_region_through",       "^X@");

setkey("whatpos",                   "^X=");

setkey("changelog_add_change",     "^X4A");
setkey("find_buffer_other_window",  "^X4B");
setkey("find_file_other_window",    "^X4F");

%============================================================================
% ^X Prefix Control Keys
%============================================================================

setkey("macro_assign_macro_to_key", "^X^A"); % ^X^A
setkey("undefined_key",             "^X^B");
setkey("jms_kill_buffer",           "^X^C");
setkey("undefined_key",             "^X^D");
setkey("undefined_key",             "^X^E");
setkey("find_file",                 "^X^F");
% ^X^G (can't use, ^G is abort)
setkey("undefined_key",             "^X^H");
setkey("undefined_key",             "^X^I");
setkey("undefined_key",             "^X^J");
setkey("undefined_key",             "^X^K");
setkey("undefined_key",             "^X^L");
setkey("macro_to_function",         "^X^M");  % ^X^M
setkey("toggle_line_number_mode",   "^X^N");
setkey("delete_blank_lines",        "^X^O");
setkey("toggle_visual_wrap",        "^X^P");
setkey("coredump",                  "^X^Q");
setkey("toggle_file_readwrite",     "^X^R");  % ^X^R
setkey("rev_sort_eol",              "^X^S");
setkey("transpose_lines",           "^X^T");
setkey("undefined_key",             "^X^U");
setkey("find_alternate_file",       "^X^V");
#ifdef MSWINDOWS
   setkey("win_write_buffer",       "^X^W");
#else
   setkey("write_buffer",           "^X^W");
#endif

setkey("undefined_key",             "^X^X");
setkey("undefined_key",             "^X^Y");
setkey("macro_save_macro",          "^X^Z");  % ^X^Z

%============================================================================
% Metakeys
%============================================================================

setkey("smart_set_mark_cmd",    MetaKey+" ");
setkey("ispell_region",         MetaKey+"!");
setkey("undefined_key",         MetaKey+"\\");
setkey("ispell",                MetaKey+"$");
setkey("undefined_key",         MetaKey+"^");
setkey("align_function_call",   MetaKey+"&");
setkey("switch_to_previous_buffer", MetaKey+"-");
setkey("undefined_key",         MetaKey+"<");
setkey("undefined_key",         MetaKey+">");
setkey("indent_region",         MetaKey+")");
setkey("unindent_region",       MetaKey+"(");
setkey("jms_narrow_fn",         MetaKey+".");
setkey("jms_copy_word",         MetaKey+",");
setkey("jms_copy_line",         MetaKey+"/");
setkey("c_make_comment",        MetaKey+";");
setkey("align_assign",          MetaKey+"=");     % M-= - Align assignements in region
setkey("align_comments",        MetaKey+"*");     % M-* - Align comments in region

setkey("digit_arg",             MetaKey+"0");
setkey("digit_arg",             MetaKey+"1");
setkey("digit_arg",             MetaKey+"2");
setkey("digit_arg",             MetaKey+"3");
setkey("digit_arg",             MetaKey+"4");
setkey("digit_arg",             MetaKey+"5");
setkey("digit_arg",             MetaKey+"6");
setkey("digit_arg",             MetaKey+"7");
setkey("digit_arg",             MetaKey+"8");
setkey("digit_arg",             MetaKey+"9");

setkey("define_abbreviation",   MetaKey+"a"); % M-A
setkey("undefined_key",         MetaKey+"b");
setkey("capitalize_word",       MetaKey+"c");
setkey("do_shell_cmd",          MetaKey+"d");
setkey("undefined_key",         MetaKey+"e");
setkey("undefined_key",         MetaKey+"f");
setkey("goto_line_cmd",         MetaKey+"g");
setkey("undefined_key",         MetaKey+"h");
% M-I - Reserved for mode-specific commands
setkey("tidy_json_region",      MetaKey+"j");     % M-J = tidy JSON code in current region/buffer
setkey("pc_showkey",            MetaKey+"k");     % M-K = list keys
setkey("downcase_word",         MetaKey+"l");
setkey("c_mark_function",       MetaKey+"m");
setkey("cursor_num_conv",       MetaKey+"n");     % M-N = base convert the number under the cursor
% M-O - Mode menu
setkey("scroll_down_in_place",  MetaKey+"p");
setkey("format_paragraph",      MetaKey+"q");
setkey("replace_command",       MetaKey+"r");
% M-S - Search menu
setkey("clean_buffer",          MetaKey+"t");     % M-T = Clean buffer
setkey("upcase_word",           MetaKey+"u");
setkey("strip_control_codes",   MetaKey+"v");     % M-V = strip control codes from the current buffer
setkey("yp_copy_region_as_kill",MetaKey+"w");
setkey("emacs_escape_x",        MetaKey+"x");
setkey("yp_yank_pop",           MetaKey+"y");
setkey("undefined_key",         MetaKey+"z");

setkey("bkmrk_swap_mark",       MetaKey+"#");    % M-# = switch cursor between 2 most recent bookmark locations

%============================================================================
% Numberical keypadulation codes are different in X & Win :-(
%
% However, they use the same prefix (MetaKey+"O").
%============================================================================

setkey("ins_ovr_text(\"/\")",         NumKey_Div);
setkey("ins_ovr_text(\"*\")",         NumKey_Star);
setkey("ins_ovr_text(\"-\")",         NumKey_Minus);
setkey("ins_ovr_text(\"+\")",         NumKey_Plus);
setkey("newline",                     NumKey_Enter);
setkey("ins_ovr_text(\".\")",         NumKey_Dot);
setkey("ins_ovr_text(\"0\")",         NumKey_0);
setkey("ins_ovr_text(\"1\")",         NumKey_1);
setkey("ins_ovr_text(\"2\")",         NumKey_2);
setkey("ins_ovr_text(\"3\")",         NumKey_3);
setkey("ins_ovr_text(\"4\")",         NumKey_4);
setkey("ins_ovr_text(\"5\")",         NumKey_5);
setkey("ins_ovr_text(\"6\")",         NumKey_6);
setkey("ins_ovr_text(\"7\")",         NumKey_7);
setkey("ins_ovr_text(\"8\")",         NumKey_8);
setkey("ins_ovr_text(\"9\")",         NumKey_9);

%============================================================================
% Meta + Control-keys
%============================================================================

setkey("undefined_key",          MetaKey+"^A");
setkey("undefined_key",          MetaKey+"^B");
setkey("pad_to_column",          MetaKey+"^C"); % M-^C - Insert spaces to move the cursor to a specific column to the right
setkey("delete_to_column",       MetaKey+"^D"); % M-^D - Delete characters to move the cursor to a specific column to the left
setkey("goto_column_cmd",        MetaKey+"^E"); % M-^E - Go to column
setkey("undefined_key",          MetaKey+"^F");
setkey("undefined_key",          MetaKey+"^G");
setkey("undefined_key",          MetaKey+"^H");
setkey("align_to_column",        MetaKey+"^I"); % M-^I - Insert or delete characters to move the cursor to the specific column
setkey("undefined_key",          MetaKey+"^J");
setkey("kill_sexp",              MetaKey+"^K");
setkey("search_last_match",      MetaKey+"^L"); % M-^L - Return to last search match
setkey("undefined_key",          MetaKey+"^M");
setkey("undefined_key",          MetaKey+"^N");
setkey("undefined_key",          MetaKey+"^O");
setkey("undefined_key",          MetaKey+"^P");
setkey("undefined_key",          MetaKey+"^Q");
setkey("undefined_key",          MetaKey+"^R");
setkey("undefined_key",          MetaKey+"^S");
setkey("insert_date",            MetaKey+"^T");
setkey("undefined_key",          MetaKey+"^U");
setkey("undefined_key",          MetaKey+"^V");
setkey("undefined_key",          MetaKey+"^W");
setkey("undefined_key",          MetaKey+"^X");
setkey("undefined_key",          MetaKey+"^Y");
setkey("undefined_key",          MetaKey+"^Z");

%******************************************************************************
% Meta-alt-keys
%******************************************************************************

setkey("undefined_key",          MetaKey+AltKey+"a");
setkey("undefined_key",          MetaKey+AltKey+"b");
setkey("roughly_recenter",       MetaKey+AltKey+"c");     % M-ALT+C = Roughly re-center the current line
setkey("switch_to_debug_buffer", MetaKey+AltKey+"d");     % M-ALT+D = switch to debug buffer
setkey("undefined_key",          MetaKey+AltKey+"e");
setkey("grep(1)",                MetaKey+AltKey+"f");      % M-ALT+F = run grep
setkey("goto_column_cmd",        MetaKey+AltKey+"g");      % M-ALT+G = goto column
setkey("undefined_key",          MetaKey+AltKey+"h");
setkey("undefined_key",          MetaKey+AltKey+"i");
setkey("undefined_key",          MetaKey+AltKey+"j");
setkey("undefined_key",          MetaKey+AltKey+"k");
setkey("undefined_key",          MetaKey+AltKey+"l");
setkey("undefined_key",          MetaKey+AltKey+"m");
setkey("undefined_key",          MetaKey+AltKey+"n");
setkey("undefined_key",          MetaKey+AltKey+"o");
setkey("undefined_key",          MetaKey+AltKey+"p");
setkey("undefined_key",          MetaKey+AltKey+"q");
setkey("reload_buffers",         MetaKey+AltKey+"r");     % M-ALT+R = Reload all modified buffers
setkey("undefined_key",          MetaKey+AltKey+"s");
setkey("show_traceback",         MetaKey+AltKey+"t");     % M-ALT+D = switch to trace buffer
setkey("undefined_key",          MetaKey+AltKey+"t");
setkey("undefined_key",          MetaKey+AltKey+"u");
setkey("undefined_key",          MetaKey+AltKey+"v");
setkey("undefined_key",          MetaKey+AltKey+"w");
setkey("undefined_key",          MetaKey+AltKey+"x");
setkey("undefined_key",          MetaKey+AltKey+"y");
setkey("undefined_key",          MetaKey+AltKey+"z");

%============================================================================
% Function keys
%============================================================================

setkey("show_help_popup",      Key_F1);      % F1 - Help
setkey("search_menu",          Key_F2);      % F2 - Select search type
setkey("search_again",         Key_F3);      % F3 - Repeat last search
setkey("search_command",       Key_F4);      % F4 - Search
setkey("source_control_menu",  Key_F5);      % F5 - Source control system
setkey("tab_menu",             Key_F6);      % F6 - Tab menu
setkey("insert_comment_block", Key_F7);      % F7 - fn comment
setkey("insert_comment_line",  Key_F8);      % F8 - comment line
setkey("jms_compile()",        Key_F9);      % F9 - compile/lint
setkey("find_tag",             Key_F10);     % F10 - find tag
setkey("macro_execute_center", Key_F11);     % F11 - execute macro and recenter the display
setkey("select_menubar",       Key_F12);     % F12 - Jed Menu

%============================================================================
% Shifted Function keys
%============================================================================

setkey("jms_c_help",           Key_Shift_F1);    % SH+F1 - C keyword help
setkey("select_compiler_menu", Key_Shift_F2);    % SH+F2 - Select compile parser
setkey("search_previous",      Key_Shift_F3);    % SH+F3 - Reverse search direction
setkey("select_scs",           Key_Shift_F4);    % SH+F4 - Select the source control system
setkey("hide_help_popup",      Key_Shift_F5);    % SH+F5 - Remove help
setkey("select_grep_tool",     Key_Shift_F6);    % SH+F6 - Select the tool for greppage
setkey("insert_comment_header",Key_Shift_F7);    % SH+F7 - Insert comment header
setkey("reformat_c_comment",   Key_Shift_F8);    % SH+F8 - reformat a C comment
setkey("list_routines",        Key_Shift_F9);    % SH+F9 - Function browser
setkey("cscope",               Key_Shift_F10);   % SH+F10 - Invoke cscope
setkey("hpcalc",               Key_Shift_F11);   % SH+F11 - Calculator
setkey("py_toggle_breakpoint", Key_Shift_F12); % SH+F12 - Add or remove a call to pdb.set_trace()

%============================================================================
% Control+Function keys
%============================================================================

setkey("jms_jed_help",        Key_Ctrl_F1);     % ^F1 - Load Jed html help
setkey("undefined_key",       Key_Ctrl_F2);
setkey("undefined_key",       Key_Ctrl_F3);     % ^F3 -
setkey("jms_kill_buffer",     Key_Ctrl_F4);     % ^F4 - Kill buffer
setkey("jms_run",             Key_Ctrl_F5);     % ^F5 - Run a program
setkey("jms_next_buffer",     Key_Ctrl_F6);     % ^F6 - Next buffer
setkey("parse_previous_error",Key_Ctrl_F7);     % ^F7 - previous error
setkey("parse_next_error",    Key_Ctrl_F8);     % ^F8 - next compile error
setkey("undefined_key",       Key_Ctrl_F9);     % ^F9    -
setkey("find_next_tag",       Key_Ctrl_F10);    % ^F10   - find next tag
setkey("end_macro",           Key_Ctrl_F11);    % ^F11   - end macro
setkey("parse_output_menu",   Key_Ctrl_F12);     % ^F12 - Parser selection menu

%============================================================================
% ALT+Function keys
%============================================================================

setkey("help_prefix",                  Key_Alt_F1);      % ALT+F1 - Jed help
setkey("save_buffers",                 Key_Alt_F2);      % ALT+F2 - Save buffers
setkey("undefined_key",                Key_Alt_F3);      % ALT+F3 -
setkey("save_and_exit",                Key_Alt_F4);      % ALT+F4 - Save & exit
setkey("undefined_key",                Key_Alt_F5);      % ALT+F5
setkey("parse_current_output",         Key_Alt_F6);      % ALT+F6 - return to current warning/grep match
setkey("parse_previous_output",        Key_Alt_F7);      % ALT+F7 - previous warning/grep match
setkey("parse_next_output",            Key_Alt_F8);      % ALT+F8 - next compile warning/grep
setkey("jms_load_build_log",           Key_Alt_F9);      % ALT+F9 - load & parse a build log
setkey("return_after_find_tag",        Key_Alt_F10);     % ALT+F10 - return after jumping to tag
setkey("begin_macro",                  Key_Alt_F11);     % ALT+F11- record macro
setkey("undefined_key",                Key_Alt_F12);     % ALT+F12

%============================================================================
% Meta+Function keys
%============================================================================

setkey("undefined_key",                MetaKey+Key_F1);
setkey("undefined_key",                MetaKey+Key_F2);
setkey("undefined_key",                MetaKey+Key_F3);
setkey("undefined_key",                MetaKey+Key_F4);
setkey("undefined_key",                MetaKey+Key_F5);
setkey("undefined_key",                MetaKey+Key_F6);
setkey("undefined_key",                MetaKey+Key_F7);
setkey("undefined_key",                MetaKey+Key_F8);
setkey("undefined_key",                MetaKey+Key_F9);
setkey("undefined_key",                MetaKey+Key_F10);
setkey("undefined_key",                MetaKey+Key_F11);
setkey("undefined_key",                MetaKey+Key_F12);

%============================================================================
% Miscellaneous key definitions
%
% The default binding for the quote keys (", ') is 'text_smart_quote'.
% Most users do not seem to like this so it is unset here.
%============================================================================

setkey("self_insert_cmd",     "\"");
setkey("self_insert_cmd",     "'");

% ******************************************************************************
% Backspace key
% ******************************************************************************

setkey("ft_backspace",                    Key_BS);          % BKSP
setkey("ft_backspace",                    Key_Shift_BS);    % SHIFT+BKSP  - BUGFIX - SHIFT+BKSP should return ^_^?

setkey("bkill_word",                      Key_Ctrl_BS);        % CNTRL+BKSP
setkey("bkill_word",                      MetaKey + Key_BS);   % M-BKSP (as CTRL+BKSP)
setkey("undo",                            AltKey+Key_BS);   % ALT+BKSP = undo
