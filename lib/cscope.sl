% ******************************************************************************
% cscope.sl
%
% Interface with the cscope utility to avoid time-wasting greps through the
% entire source tree.
% ******************************************************************************

% Default (current) search pattern

static variable cscope_pattern = "";

% Directory where the cscope database is stored

static variable cscope_file_dir = "";

% Number of results returned

static variable cscope_results = 0;

% ******************************************************************************
% Perform a cscope search
% ******************************************************************************

define cscope()
{
        variable pattern,
                                cscope_file,
                                buf;

        % Use the word under the cursor as the default search term

        pattern = cursor_word();

        if(pattern != "")
        {
                cscope_pattern = pattern;
        }

        cscope_pattern = read_mini("cscope:", cscope_pattern, cscope_pattern);

        % Search for the cscope database

   setbuf ("*scratch*");

        cscope_file_dir = find_up_dir("cscope.out");

        if(cscope_file_dir=="")
        {
                error("Cannot locate cscope.out!");
        }

        cscope_file = cscope_file_dir+"cscope.out";

        % Remember where we started from

        buf = whatbuf();

        % Run cscope to find the specified string and store the results in the *grep* buffer

        flush("Running cscope "+cscope_pattern);
        setbuf(GREP_BUFFER);
        erase_buffer();
        (, ) = run_shell_cmd_buf("cscope -d -f "+cscope_file+" -C -L -0"+cscope_pattern, GREP_BUFFER);

        % Determine the number of results (-1 to allow for the blank line at the end)

        eob();
        cscope_results = what_line() - 1;

        % Start at the top of the results

        bob();

        % Go back to the buffer we were editing

   setbuf(buf);

        % Set the error/grep/whatever parse for cscope mode (yuk - nasty global variable
        % it would be nicer to use a function pointer for this!)

        parse_type = PARSE_CSCOPE;
   last_parse_dir = 0;

        % Do the first parse to locate ourselves on the first result (if any)

        cscope_parse(1);
}

% ******************************************************************************
% Parse backwards or forewards through the cscope results
% ******************************************************************************

define cscope_parse(direction)
{
   variable filename,
                                lineno,
                                context,
                                current_line;

        % Sanity check

        if(parse_type != PARSE_CSCOPE)
        {
                error("Internal error: cscope_parse() called when not parsing cscope results!");
        }

   setbuf(GREP_BUFFER);

        do
        {
           % If we are searching backwards, try to move to the previous line

           if(direction < 0)
           {
              if(up(1) == 0)
              {
                 error("Already at the start of the cscope results!");
              }
           }
                else if(direction == 0)
                {
                        goto_line(1);
                }
                else
                {
                        if(eobp())
                        {
                                error("Already at the end of the cscope results!");
                        }
                }

                % Get the current line number

                current_line = what_line();

           % The filename should be at the start of the line, so drop the mark there

           bol();

           % Extract the filename & add the root directory name

           push_mark();
                ffind(" ");
           filename = cscope_file_dir + bufsubstr();
           pop_mark(1);

                % Now get the context

                skip_white();

                push_mark();
                ffind(" ");
                context = bufsubstr();
                pop_mark(1);

           % Now get the line number

                skip_white();

                push_mark();
                ffind(" ");
                lineno = bufsubstr();
                pop_mark(1);

           % Finish off by moving down if we are parsing downwards

           if(direction > 0)
           {
              () = down(1);
           }
        }
        while(string_match_list(filename, parse_ignore_file_list, '|') > 0);

   % Find the file referenced, and go to the correct line and column

   () = find_file(filename);
   goto_line(integer(lineno));

        message(sprintf("Found %s in "+context+" in file %s (%d/%d)",
                                                 cscope_pattern, filename,
                                                 current_line, cscope_results));
}

