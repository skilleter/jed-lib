%==========================================================================
% ninjamode.sl
%
% Create and initialize the syntax table for Ninja build files
%
%==========================================================================

create_syntax_table ("ninja");

define_syntax ("#", "", '%', "ninja");
define_syntax ("([{",            "}])", '(', "ninja");
define_syntax ('"',                     '"', "ninja");
define_syntax ("0-9a-zA-Z_[]",          'w', "ninja");  % words
define_syntax ("$:=",                   ',',                    "ninja");

append_keywords("ninja", 1, "rule", "build", "pool");

append_keywords("ninja", 3, "default");

append_keywords("ninja", 2,
                "deps", "command", "depth", "console", "depfile", "msvc_deps_prefix",
                "description", "generator", "in", "in_newline", "out", "restat",
                "rspfile", "rspfile_content");

append_keywords("ninja", 4, "gcc", "msvc");

append_keywords("ninja", 5, "ninja_required_version", "builddir");

define ninja_mode ()
{
        set_mode ("ninja", 0);
        use_syntax_table ("ninja");
   set_comment_info("ninja", "#", NULL, NULL, 0);
        run_mode_hooks ("ninja_mode_hook");
}
