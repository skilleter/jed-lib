#!/bin/bash

################################################################################
# S-Lang code reformatter using GNU indent
#
# Known issues:
#  * Doesn't handle ifnot properly
#  * Should, but doesn't, but braces after a struct on the following line
#  * Doesn't recognize wordy booleans (or, and, etc.)
#  * Adds 'define' to the end of the file (?)
################################################################################

slang=${1:-comments.sl}
tmpfile="/tmp/indent.$$"

indent_options="
-i3
--brace-indent0
--braces-after-struct-decl-line
--continue-at-parentheses
--dont-cuddle-do-while
--dont-cuddle-else
--line-length160
--no-space-after-for
--no-space-after-function-call-names
--no-space-after-if
--no-space-after-parentheses
--no-space-after-while
--no-tabs
--standard-output
"

# Convert S-Lang '%' comments to C '//' comments and '//' already there to '##/##/'
# Use GNU Indent to format the code now that it looks like C then
# revert the comment conversion and remove the newlines that indent adds after 'define'

sed -r -e 's!//!##/##/!g' -e 's!%!//!g' "$slang" | \
   indent $indent_options | \
   sed -r -e 's!//!%!g' -e 's!##/##/!//!g' | \
   awk 'BEGIN{RS="define\n" ; ORS="define ";}{ print }'  > "new-$slang"
