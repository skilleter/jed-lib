% ******************************************************************************
% Attempt to load a file into a buffer as find_file() does, but only issue
% warnings when (if) it goes wrong, rather than errors - used when processing
% command line parameters to allow reckless use of wildcards to load files but
% only whinge about directories, rather than failing.
% If the filename is in the format FILENAME:LINE_NUMBER it opens the
% file and moves the edit point to the line number (TODO: this functionality
% should probably be moved somewhere more generical).
% If the file doesn't exist and the last character of the filename is
% a colon then remove it and try again
%
% Accepts the following qualifiers;
%   new (default=1) - create a empty new buffer if the file does not exist
%   hardfail (default=0) - fail with any errors, rather than issuing warnings

% TODO: Handle cases like site:sl:35:wibble where we ignore ':wibble' and go to line 35 of site.sl

define load_file(filename);

define load_file(filename)
{
   variable newbuffer = qualifier("new", 1);
   variable hardfail  = qualifier("hardfail", 0);

   variable chopped;
   variable status, filestat, msg="";
   variable line_no = -1, col_no = -1;
   variable msg_delay = 500;
   variable new_filename;

   % If the file exists, make sure it is a file and not a directory, socket, etc.

   filestat = stat_file(filename);

   % If the file does not exist and filename has trailing colon remove it
   % and try again.

   if(filestat == NULL and filename[-1] == ':')
   {
      filename = filename[[:-2]];
      filestat = stat_file(filename);
   }

   % That didn't work, check for FILENAME:LINE_NUMBER:COLUMN

   if(filestat == NULL and string_match(filename, "^.*:[0-9]+:[0-9]+$"))
   {
      chopped = strchop(filename, ':', 0);

      col_no = atoi(chopped[-1]);
      line_no = atoi(chopped[-2]);

      new_filename = strjoin(chopped[[:-2]]);

      filestat = stat_file(new_filename);

      if(filestat != NULL)
        filename = new_filename;
   }

   % If that didn't work, try for a match with FILENAME:LINE_NUMBER

   if(filestat == NULL and string_match(filename, "^.*:[0-9]+$"))
   {
      chopped = strchop(filename, ':', 0);

      line_no = atoi(chopped[-1]);
      chopped[-1] = "";
      new_filename = strjoin(chopped);

      filestat = stat_file(new_filename);

      if(filestat != NULL)
        filename = new_filename;
   }

   % We've tried all the possibilities - either it does or doesn't exist
   % and we may, or may not have line and column numbers to go to

   if(filestat == NULL)
   {
      % It doesn't exist

      if(newbuffer == 0)
      {
         msg = sprintf("File '%s' does not exist", filename);
      }
   }
   else
   {
      % It DOES exist!

      % If it is a directory, recursively call load_file() to load
      % files in the directory (we don't recurse down the directory
      % tree as that is the way to madness - 'jed /' would amuse greatly).

      if(stat_is("dir", filestat.st_mode))
      {
         foreach(glob(path_concat(filename, "*")))
         {
            variable entry = ();

            if(entry != "." && entry != "..")
            {
               if(file_status(entry) == 1)
               {
                  load_file(entry);
               }
            }
         }
         return;
      }
      else if(stat_is("sock", filestat.st_mode))
      {
         msg = sprintf("'%s' is a socket!", filename);
      }
      else if(stat_is("fifo", filestat.st_mode))
      {
         msg = sprintf("'%s' is a FIFO!", filename);
      }
      else if(stat_is("blk", filestat.st_mode))
      {
         msg = sprintf("'%s' is a block device", filename);
      }
      else if(stat_is("chr", filestat.st_mode))
      {
         msg = sprintf("'%s' is a character device", filename);
      }
      else
      {
         status = file_status(filename);

         if(status == -1)
         {
            msg = sprintf("Access to '%s' denied!", filename);
         }
         else if(status == -2)
         {
            msg = sprintf("Invalid path: '%s'", filename);
         }
         else if(status == -3)
         {
           msg = sprintf("Unknown error attempting to access '%s'", filename);
         }
      }
   }

   % If we've got this far without an error message, actually
   % try to load or create the file

   if(msg == "")
   {
      if(find_file(filename))
      {
         if(line_no >= 0)
         {
            goto_line(line_no);
         }

         if(col_no >= 0)
         {
            goto_column(col_no);
         }
      }
   }

   % Report any of the numerous possible errors

   if(msg != "")
   {
      if(hardfail)
      {
         error(msg);
      }
      else
      {
         flush(msg);
         usleep(msg_delay);
      }
   }
}
