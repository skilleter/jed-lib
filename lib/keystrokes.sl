% ******************************************************************************
% Use the before/after key hooks to record the most recent key strokes
% Intended for the cursor up/down functions to work out which column
% to go to based on whether the user is moving up/down repeatedly or typing stuff
% ******************************************************************************

% Buffer containing last 16 keypresses

private variable key_buffer = String_Type[16];

% Current index into the buffer

private variable key_index = -1;

% Count of number of keypresses recorded

private variable key_count = 0;

% Hook called when a key is pressed, before it is handled
% We just store the key function and increment the count

define before_key_hook(fun)
{
   key_index++;
   if(key_index == 16)
      key_index = 0;

   key_buffer[key_index] = fun;

   key_count++;
}

% Initialisation

define init_keystrokes()
{
   add_to_hook ("_jed_before_key_hooks", &before_key_hook);
}

% Get the nth-most-recent keystroke where 0 is the most recent

define get_keystroke(n)
{
   variable offset = key_index-n;
   if(offset < 0)
      offset += 16;

   return key_buffer[offset];
}

% Get the count of keystrokes

define get_keykey_count()
{
   return key_count;
}

% Initialise automatically

init_keystrokes();
