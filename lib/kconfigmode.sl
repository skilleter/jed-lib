% ******************************************************************************
% kconfig editing mode
% ******************************************************************************

define_word ("0-9A-Za-z_");

define kconfig_end_of_comment()
{
        forever
        {
                bol();
                skip_white();

                !if(looking_at("#"))
                {
                        break;
                }

                if(down(1) != 1)
                {
                        break;
                }
        }
}

define kconfig_top_of_comment()
{
        forever
        {
                bol();
                skip_white();

                !if(looking_at("#"))
                {
                        break;
                }

                if(up(1) != 1)
                {
                        break;
                }
        }
}

define kconfig_newline_and_indent()
{
   variable indentlevel=0;

   ERROR_BLOCK
   {
      _clear_error ();
      pop_spot();
   }

   push_spot();

   !if(bolp())
   {
      () = left(1);
      if(looking_at(":"))
      {
         indentlevel=1;
      }
   }

   bol();
   if(looking_at("\t"))
   {
      indentlevel=1;
   }

   ERROR_BLOCK
   {
   }

   pop_spot();
   newline();
   if(indentlevel==1)
   {
      insert("\t");
   }

}

define kconfig_indent_line()
{
   message("INDENT");
   bol();
   insert("\t");
}

create_syntax_table ("KCONFIG");

define_syntax ("#",                                             "",    '%',     "KCONFIG");
define_syntax ("([{",                                   ")]}", '(', "KCONFIG");
define_syntax ('"',                                             '"',                    "KCONFIG");
define_syntax ('\'',                                    '\'',           "KCONFIG");
define_syntax ('\\',                                    '\\',           "KCONFIG");
define_syntax ("0-9a-zA-Z_-",       'w',                        "KCONFIG");        % words
define_syntax ("-+0-9a-fA-F.xXL",       '0',                    "KCONFIG");   % Numbers
define_syntax (",;.?:$@\\",                     ',',                    "KCONFIG");
define_syntax ('.',                                             '#',                    "KCONFIG");
define_syntax ('!',                                             '#',                    "KCONFIG");
define_syntax ("%-+/&*=<>|~^",      '+',                        "KCONFIG");
set_syntax_flags ("KCONFIG", 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%

private define setup_dfa_callback (name)
{
    dfa_enable_highlight_cache ("kconfigmode.dfa", name);

    dfa_define_highlight_rule ("\\\\.", "normal", name);
    dfa_define_highlight_rule ("#.*$", "comment", name);
    % dfa_define_highlight_rule ("^.*:", "keyword", name);
    dfa_define_highlight_rule ("\\$\\(.*\\)", "keyword", name);
    dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*\"", "string", name);
    dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*$", "string", name);
    dfa_define_highlight_rule ("'[^']*'", "string", name);
    dfa_define_highlight_rule ("'[^']*$", "string", name);
    dfa_define_highlight_rule ("\\$\\(.*\\)", "string", name);
    dfa_define_highlight_rule ("[\\|&;\\(\\)<>\\:]", "Qdelimiter", name);
    dfa_define_highlight_rule ("[\\[\\]\\*\\?=]", "Qoperator", name);
    dfa_define_highlight_rule ("[A-Za-z_]+",
                               "Knormal", name);
    dfa_define_highlight_rule ("^\t[ \t]*", "region", name);
    % dfa_define_highlight_rule ("^\t.*", "preprocess", name);
    dfa_define_highlight_rule (".", "normal", name);
    dfa_build_highlight_table (name);

%       dfa_define_highlight_rule("^\\.",                                    "PQpreprocess",    name);
%       dfa_define_highlight_rule("#.*" ,                                    "comment",                         name);
%       dfa_define_highlight_rule("[A-Za-z_][A-Za-z_0-9]*",                     "Knormal",                      name);
%       dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?","number",                  name);
%       dfa_define_highlight_rule("$\([A-Z0-9_].\)",                            "string",                       name);
%       dfa_define_highlight_rule("[ \t]+",                                  "normal",                  name);
%       dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]",               "delimiter",               name);
%       dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^\\$\\\\@]",       "operator",                name);
%       dfa_define_highlight_rule("\\$",                                                                                                        "dollar",                       name);
%       dfa_define_highlight_rule("^\t[ \t]*",                                                                          "region",                       name);
%       dfa_build_highlight_table(name);
}
dfa_set_init_callback (&setup_dfa_callback, "KCONFIG");
%%% DFA_CACHE_END %%%
#endif

!if (keymap_p ("KCONFIG")) make_keymap ("KCONFIG");
definekey ("kconfig_top_of_comment", "\e\xE0H", "KCONFIG");
definekey ("kconfig_end_of_comment", "\e\xE0P", "KCONFIG");

% Kconfig keywords

append_keywords("KCONFIG", 0, "if", "endif", "choice", "endchoice");

append_keywords("KCONFIG", 1, "menu", "config", "endmenu");

append_keywords("KCONFIG", 2, "select", "help", "depends", "prompt", "default", "visible");

append_keywords("KCONFIG", 3, "bool", "tristate", "int", "hex", "range");

append_keywords("KCONFIG", 4);

append_keywords("KCONFIG", 5);

append_keywords("KCONFIG", 6);

append_keywords("KCONFIG", 7);

define kconfig_mode ()
{
   TAB = 8;
   USE_TABS=1;
   CASE_SEARCH=1;
   use_keymap("KCONFIG");
   set_mode("KCONFIG", 4);
   set_buffer_hook ("indent_hook", "kconfig_indent_line");
   set_buffer_hook ("newline_indent_hook", "kconfig_newline_and_indent");
   set_comment_info("KCONFIG", "#", NULL, NULL, 0);
   use_syntax_table ("KCONFIG");
   run_mode_hooks("kconfig_mode_hook");
}

