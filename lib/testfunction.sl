% ******************************************************************************
% General-purpose temporary test source code
% ******************************************************************************

require("dired-new");

% ******************************************************************************

variable use_onig;
try
{
   require ("onig");
   use_onig = 1;
}
catch AnyError:
{
   use_onig = 0;
}

define test_onig_re()
{
   variable o, str;
   str = read_mini("Onig search for: ", "", "");
   o = onig_new(str, ONIG_OPTION_NONE, "utf8", "posix_extended");

   while(1)
   {
      variable text = line_as_string();

      if(onig_search(o, text))
         break;

      !if(down (1))
      {
         error("Not found");
         break;
      }
   }
}

% ******************************************************************************

define generic_test_function()
{
   %variable x, y;
   %flush("Normal {{1}}Hello {{2}}World {{3}}! {{-}} Normal");
   %x = create_user_mark();
   %go_right_1();
   %y = create_user_mark();
   %error(sprintf("%s %d:%d:%d:%d", user_mark_buffer(x), user_mark_line(x), user_mark_column(x), what_column(), x==y));
   flush("{{0}}ZERO {{1}}ONE {{2}}TWO {{3}}THREE {{4}}FOUR {{5}}FIVE {{6}}SIX {{7}}SEVEN {{8}}EIGHT {{9}}NINE {{10}TEN {{11}ELEVEN {{12}TWELVE");

   %variable x;

   %x = cursor_text(0);
   %flush("Word: '"+x+"'");
}

% ******************************************************************************

define tidy_sds_log()
{
   bob();
   replace("\n", "\n\n");
   replace("\\n", "\n");
   replace("\\t", "\t");
   replace(" | ", "\n\t| ");
}

% ******************************************************************************

define jms_test_function()
{
   variable c;
   flush("Select test function 1) New dired, 2) Test regexp search 3) Generic test function 4) Tidy SDS log");

   c = getkey();
   clear_message();

   switch(c)
   { case '1': dired_new(); }
   { case '2': test_onig_re(); }
   { case '3': generic_test_function(); }
   { case '4': tidy_sds_log(); }
}
