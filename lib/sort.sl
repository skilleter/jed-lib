% Modified to add end_col_flag - if specified and set to 1
% then sorts entire lines from start column. If unspecified or 0
% then default is to sort from start column to current column


static define _sort_using_function (sort_fun, eol_flag)
{
   variable end_line, n, index, i, beg, begc, endc, keys;

   check_region(0);
   endc = what_column();
   end_line = what_line();
   pop_mark_1 ();
   beg = what_line();

   n = end_line - beg; ++n;          %/* number of lines */
   begc = what_column();

   if(eol_flag == 0)
   {
      if (endc < begc)
      {
         endc; endc = begc; begc = ();
      }
   }

   keys = String_Type [n];

  %
  % extract keys and fill array
  %

   goto_line(beg);
   for (i = 0; i < n; ++i)
   {
      goto_column(begc);
      push_mark_eol();

      if(eol_flag == 0)
      {
         if (what_column() > endc) goto_column(endc);
      }

      keys[i] = bufsubstr();
      go_down_1 ();
   }

   index = array_sort(keys, sort_fun;; __qualifiers());

   %
   %  arrange region
   %
   goto_line (end_line);
   ifnot (down_1 ())
   {
      eol();
      newline();
   }

   push_spot();
   for(i = 0; i < n; ++i)
   {
      goto_line(index[i] + beg);
      line_as_string ();          %  on stack-- also we are at eol now
      pop_spot();
      bol(); insert(()); newline();
      push_spot();
   }

   pop_spot();
   goto_line(beg);
   bol(); push_mark();
   goto_line(end_line + 1);
   bol(); del_region();
}

define sort_using_function(sort_fun)
{
   _sort_using_function(sort_fun, 0);
}

define sort_using_function_eol(sort_fun)
{
   _sort_using_function(sort_fun, 1);
}

define sort ()
{
   variable dir = (prefix_argument() == NULL) ? +1 : -1;
   sort_using_function (&strcmp; dir=dir);
   %flush("Done.");
}

define sort_eol ()
{
   _sort_using_function (&strcmp, 1);
}

static define neg_strcmp(a, b)
{
   return strcmp(b, a);
}

define rev_sort ()
{
   _sort_using_function (&neg_strcmp, 0);
}

define rev_sort_eol ()
{
   _sort_using_function (&neg_strcmp, 1);
}

