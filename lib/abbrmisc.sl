% Simplified to use the word under the cursor as the default
% thing to be abbreviated and to save the abbreviations without
% prompting for the filename

% Miscellaneous function for the abbrev tables

require ("abbrev");

define define_abbrev_for_table (table, word)
{
   variable n = what_line ();
   variable use_bskip = 1;
   variable abbrev, expans;

   abbrev = read_mini ("Abbreviation:", "", cursor_word());
   ifnot (strlen (abbrev)) return;

        expans = read_mini("Expansion for '"+abbrev+"'? ", "", abbrev);
   ifnot (strlen (expans)) return;

   define_abbrev (table,  abbrev, expans);
}

define define_abbreviation ()
{
   variable tbl, word;

   (tbl, word) = what_abbrev_table ();
   ifnot (strlen (tbl))
     {
        tbl = "Global";
        create_abbrev_table (tbl, Null_String);
        (tbl, word) = what_abbrev_table ();
     }

   define_abbrev_for_table (tbl, word);
}

private define quote_this_line ()
{
   push_spot ();
   while (ffind_char ('\\'))
     {
        insert_char ('\\');
        go_right_1 ();
     }
   pop_spot ();
   push_spot ();
   while (ffind_char ('"'))
     {
        insert_char ('\\');
        go_right_1 ();
     }
   pop_spot ();
}

define save_abbrevs ()
{
   variable file=Abbrev_File;
   variable n, table, word;

#iffalse
  ifnot (strlen (extract_filename (file)))
     {
        file = dircat (file, Abbrev_File);
     }

  ifnot (strlen (extract_filename (file))) error ("Invalid file.");
#endif

   n = list_abbrev_tables ();          %  tables on stack
   ifnot (n) return;

   () = read_file (file);
   erase_buffer ();

   loop (n)
     {
        table = ();
        push_spot ();
        word = dump_abbrev_table (table);   %  buffer now contain abbrevs
        pop_spot ();

        vinsert("create_abbrev_table (\"%s\", \"%s\");\n", table, word);
        go_up_1 ();

        while (down_1 () and not(eobp()))
          {
             insert ("define_abbrev (\""); insert(table);
             insert ("\",\t\"");
             quote_this_line ();
             () = ffind_char ('\t');
             trim ();
             insert ("\",\t\"");
             eol ();
             insert ("\");");
          }
     }
   save_buffer ();
   delbuf (whatbuf);
}

