%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (c)1997,98 Mark Olesen
%
% Do as you wish with this code under the following conditions:
% 1) leave this notice intact
% 2) don't try to sell it
% 3) don't try to pretend it is your own code
% 4) it's nice when improvements make their way back to the 'net'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%!% Void charset (Void)
%!% display a popup buffer with the character set
define charset ()
{
   variable i, buf = "*character set*";

   pop2buf (buf);
   erase_buffer ();
   insert (buf);
   i = 32;
   loop (7)
     {
        vinsert ("\n%3d [0x%X]\t", i, i);
        loop (32)
          {
             insert_char (i);
             i++;
          }
     }

   bob ();
   set_buffer_modified_flag (0);        % pretend no modifications
}
%%%%%%%%%%%%%%%%%%%%%%%%%%% end-of-file (SLang) %%%%%%%%%%%%%%%%%%%%%%%%%%
