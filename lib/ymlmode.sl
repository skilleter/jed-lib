$1 = "yml";

create_syntax_table ($1);

define_syntax ("#", "", '%', $1);
define_syntax ("[]{};%-+/&*=<>|!~^$", '+', $1);
%define_syntax ('\'', '"', $1);
define_syntax('\\', '\\', $1);
define_syntax ('"', '"', $1);
define_syntax ("-0-9a-zA-Z_", 'w', $1);        % words
define_syntax ("-+0-9ax.", '0', $1);    % Numbers
define_syntax(":,;.?", ',', $1);
%define_syntax("%-+/&*=<>|!~^", '+', $1);

set_syntax_flags ($1, 4);

% Keywords are specific to GitLab-CI which is currently the only place
% I use YAML. If I use YAML for other purposes I'll probably add a
% function to turn the GitLab-CI keywords on and off.

append_keywords($1, 0, "script", "image", "services", "stage", "type", "before_script",
                       "after_script", "variables", "only", "except", "tags",
                       "allow_failure", "when", "dependencies", "artifacts", "environment",
                       "coverage", "stages");

append_keywords($1, 1, "untracked", "paths", "on_success", "on_failure", "always", "manual");

append_keywords($1, 2, "CI", "CI_COMMIT_REF_NAME", "CI_COMMIT_REF_SLUG", "CI_COMMIT_SHA",
                       "CI_COMMIT_TAG", "CI_DEBUG_TRACE", "CI_ENVIRONMENT_NAME",
                       "CI_ENVIRONMENT_SLUG", "CI_JOB_ID", "CI_JOB_MANUAL", "CI_JOB_NAME",
                       "CI_JOB_STAGE", "CI_JOB_TOKEN", "CI_REPOSITORY_URL",
                       "CI_RUNNER_DESCRIPTION", "CI_RUNNER_ID", "CI_RUNNER_TAGS",
                       "CI_PIPELINE_ID", "CI_PIPELINE_TRIGGERED", "CI_PROJECT_DIR",
                       "CI_PROJECT_ID", "CI_PROJECT_NAME", "CI_PROJECT_NAMESPACE",
                       "CI_PROJECT_PATH", "CI_PROJECT_URL", "CI_REGISTRY", "CI_REGISTRY_IMAGE",
                       "CI_REGISTRY_PASSWORD", "CI_REGISTRY_USER", "CI_SERVER", "CI_SERVER_NAME",
                       "CI_SERVER_REVISION", "CI_SERVER_VERSION", "ARTIFACT_DOWNLOAD_ATTEMPTS",
                       "GET_SOURCES_ATTEMPTS", "GITLAB_CI", "GITLAB_USER_ID", "GITLAB_USER_EMAIL",
                       "RESTORE_CACHE_ATTEMPTS");

append_keywords($1, 3, "true", "on", "yes", "false", "off", "no", ".inf", ".NAN", "null");

!if (keymap_p ($1)) make_keymap ($1);

define yml_mode ()
{
   variable mode = "yml";

   set_mode (mode, 0);
   use_syntax_table (mode);
   use_keymap(mode);
   use_dfa_syntax(0);
   set_comment_info(mode, "#", NULL, NULL, 0);

   USE_TABS=0;
   TAB=2;

   run_mode_hooks("yml_mode_hook");
}
