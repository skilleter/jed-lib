%******************************************************************************
% Module dealing with calling and parsing grep & its results
%
% TODO: If using grep, need to handle multiple matches on same line as it doesn't return column number
% TODO: If grep returns matches in filenames which contain colons then the parser will break
% TODO: Grep can't return column numbers but can return multiple matches per line.
% TODO: Rg CAN report multiple matches per line
% TODO: Sigh.... write grep that does what I want it to :-(
%******************************************************************************

static variable USE_GREP = 0;
static variable USE_GIT_GREP = 1;
static variable USE_RG = 2;

static variable grep_matches = 0;

static variable grep_dir = NULL;

static variable last_filename_match = "";
static variable last_lineno_match = 0;

static variable search_tool = USE_RG;

static variable vcs_ignore = 0; % Set to 1 to search only files NOT matching .gitignore etc.

static variable grep_tool_name = ["Grep", "Git Grep", "ripgrep"];

%******************************************************************************
% Find the next (direction>0) or previous (direction<0) search result

define grep_parse (direction)
{
   variable filename, lineno, colno, gline, str, ignored=0;
   variable warnmsg="", match, chopped, suffix="";

   setbuf(GREP_BUFFER);

   % Loop until we find a valid thing and move there, or report an error

   do
   {
      % If we're going backwards, start by going up 1 line

      if(direction == -1)
      {
         if(up(1) == 0)
         {
            error("Already at the start of the grep results!");
         }
      }

      % Check for going past the end

      if(what_line > grep_matches)
      {
         error("Grep - no more matches for '" + grep_pattern + "'");
      }

      % Get the current line of the grep output

      bol();
      push_mark();
      eol();
      match = bufsubstr();
      pop_mark(1);

      % Check for error messages

      if(ffind("No files matching") or looking_at("Binary file"))
      {
         eol();
         str = bufsubstr();
         pop_mark(1);

         if(direction == 1)
         {
            () = down(1);
         }

         error(sprintf("Match %d of %d with '%s': %s", what_line(), grep_matches, grep_pattern, str));
      }

      % Check for file:line:col:match or file:line:match

      if(string_match(match, "^.*:[0-9]+:[0-9]+:.*") == 1)
      {
         chopped = strchop(match, ':', 0);

         filename = grep_dir + chopped[0];
         lineno = integer(chopped[1]);
         colno = integer(chopped[2]);
         match = strjoin(chopped[[3:]], ":");
      }
      else if(string_match(match, "^.*:[0-9]+:.*") == 1)
      {
         chopped = strchop(match, ':', 0);

         filename = grep_dir + chopped[0];
         lineno = integer(chopped[1]);
         colno = -1;
         match = strjoin(chopped[[2:]], ":");
      }
      else
      {
         error("Unable to parse search output: '"+match+"'");
      }

      gline = what_line();

      pop_mark(1);

      % If we're going downwards, finish by going down

      if(direction == 1)
      {
         () = down(1);
      }

      % If the file is in the ignore list then iterate around again,
      % otherwise escape from the loop

      if(string_match_list(filename, parse_ignore_file_list, '|') > 0)
      {
         ignored++;
      }
      else
      {
         break;
      }
   }
   while(1);

   % Move to the match

   () = find_file(filename);

   goto_line(lineno);

   % Can't use goto_column as it doesn't take account of tabs

   bol();
   if(colno > 0)
   {
      () = right(colno-1);
   }
   else
   {
      () = ffind(match);
   }

   if(not looking_at(match))
   {
      warnmsg = " - {{3}}Warning: File has changed{{0}}";
   }

   last_filename_match = filename;
   last_lineno_match = lineno;

   if(ignored > 0)
   {
      suffix = sprintf("(ignored %d matches) ", ignored);
   }

   message(sprintf("Match %d of %d %swith '%s' at line %d, column %d in %s%s", gline, grep_matches, suffix, grep_pattern, lineno, colno, filename, warnmsg));
}

%==========================================================================
% Run rgrep and capture the results
%==========================================================================

define grep_search(recurse)
{
   variable buf,
            pattern,
            grep_case_sensitive,
            grep_words,
            grep_opt,
            grep_cmd,
            grep_desc,
            status;

   (, grep_case_sensitive, , , grep_words) = search_get_defaults();
   pattern = cursor_word();

   grep_opt = "";
   grep_cmd = "";
   grep_desc = "";

   if(pattern != "")
   {
      grep_pattern = pattern;
   }

   last_parse_dir = 0;

   if(grep_words)
   {
      grep_desc = grep_desc + "word ";
   }

   if(recurse)
   {
      grep_desc = grep_desc + "recursive ";
   }

   % Do a case-sensitive search if case-sensitive searches are enabled, or
   % the search pattern is mixed case

   if (grep_pattern != strlow(grep_pattern) and grep_pattern != strup(grep_pattern))
   {
      grep_case_sensitive = 1;
   }

   if(grep_case_sensitive == 1)
   {
      grep_desc = grep_desc + "case-sensitive ";
   }
   else
   {
      grep_desc = grep_desc + "case-insensitive ";
   }

   grep_pattern   = read_mini(grep_desc + "grep :", grep_pattern, grep_pattern);
   grep_wildcard  = read_mini("Files to grep:", grep_wildcard, grep_wildcard);

   parse_type = PARSE_GREP;

   buf = whatbuf();

   save_buffers();

   setbuf(GREP_BUFFER);
   erase_buffer ();
   setbuf(buf);

#ifdef UNIX

   % Default set of options - suppress warnings, report every match,

   grep_opt += " --no-messages --only-matching --exclude='*.keep'";

   % Exclude a set of files that we generally don't care about

   grep_opt += " --exclude='*.contrib' --exclude='*.contrib.[0-9]*' --exclude='*.bak' --exclude=tags";

   % Disable word matching if the search string contains whitespace

   if(is_substr(grep_pattern, " ") or is_substr(grep_pattern, "\t") or is_substr(grep_pattern, "\n"))
   {
      grep_words = 0;
   }

   % Grepping for whole words only?

   if(grep_words)
   {
      grep_opt += " --word-regexp";
      grep_desc = grep_desc + "word ";
   }

   % Recursing?

   if(recurse)
   {
      (grep_wildcard, ) = strreplace(grep_wildcard, " ", "' --include='", 9999);

      % Recurse, ignoring .git, .terraform directories

      grep_opt += " . --include='" + grep_wildcard + "' --exclude-dir=.git --exclude-dir=.terraform --recursive";
      grep_wildcard = "";
   }
   else
   {
      grep_opt += " --directories=skip";
   }

   % Case-insensitive

   if(grep_case_sensitive == 0)
   {
      grep_opt += " --ignore-case";
   }

   grep_opt += "  --line-number";

   % Build the grep command

   grep_cmd = "egrep '" + grep_pattern + "' " + grep_opt + " " + grep_wildcard;

#else

   % On non-Unix platforms, we only have a limited choice using
   % standard grep

   if(grep_case_sensitive == 0)
   {
      grep_opt = "-i ";
   }

   if(recurse)
   {
      grep_opt += "-R ";
   }

   grep_cmd = "rgrep -n " + grep_opt + "\"" + strreplace(grep_pattern,"\"", "\\\"") + "\" " + grep_wildcard;

#endif

   % Actually run the grep command, outputting to the grep buffer

   flush("Running " + grep_desc + " for '" + grep_pattern + "' in " + grep_wildcard);

   (grep_dir, status) = run_shell_cmd_buf(grep_cmd, GREP_BUFFER);

   flush("Done");
   eob();
   grep_matches = what_line() - 1;

   bob();

   % If we have any matches then move automagically to the first one

   if(grep_matches > 0)
   {
      grep_parse(1);
   }
   else
   {
      error("No matches for '" + grep_pattern + "'");
   }
}

% ******************************************************************************

define git_grep(recurse)
{
   variable buf,
            gg_case_sensitive,
            gg_words,
            gg_opt,
            gg_cmd,
            gg_desc,
            status;

   gg_opt = "--only-matching";
   gg_cmd = "";
   gg_desc = "";

   (, gg_case_sensitive, , , gg_words) = search_get_defaults();

   grep_pattern = cursor_word();

   % Do a case-sensitive search if case-sensitive searches are enabled, or
   % the search pattern is mixed case

   if (grep_pattern != strlow(grep_pattern) and grep_pattern != strup(grep_pattern))
   {
      gg_case_sensitive = 1;
   }

   if(gg_case_sensitive == 1)
   {
      gg_desc = gg_desc + "case-sensitive ";
   }
   else
   {
      gg_desc = gg_desc + "case-insensitive ";
   }

   if(gg_words)
   {
      gg_desc = gg_desc + "word ";
   }

   grep_pattern   = read_mini(gg_desc + "git grep :", grep_pattern, grep_pattern);

   buf = whatbuf();

   save_buffers();

   parse_type = PARSE_GREP;

   setbuf(GREP_BUFFER);
   erase_buffer ();
   setbuf(buf);

   % Default set of options - include column number

   gg_opt += " --column --line-number";

   % Exclude a set of files that we generally don't care about

   %gg_opt += "";

   % Disable word matching if the search string contains whitespace

   if(is_substr(grep_pattern, " ") or is_substr(grep_pattern, "\t") or is_substr(grep_pattern, "\n"))
   {
      gg_words = 0;
   }

   % Grepping for whole words only?

   if(gg_words)
   {
      gg_opt += " --word-regexp";
      gg_desc = gg_desc + "word ";
   }

   % Case-insensitive

   if(gg_case_sensitive == 0)
   {
      gg_opt += " --ignore-case";
   }

   % Non-recursive

   if(not recurse)
   {
      gg_opt += " --no-recursive";
   }

   % Build the command

   gg_cmd = "git grep " + gg_opt + " \"" + strreplace(grep_pattern,"\"", "\\\"") + "\"";

   % Actually run the grep command, outputting to the grep buffer

   flush("Running " + gg_desc + "search for '" + grep_pattern + "'");
   (grep_dir, status) = run_shell_cmd_buf(gg_cmd, GREP_BUFFER);

   % Remove any blank lines from the output

   trim_blank_lines();

   % Count the number of matches

   eob();
   grep_matches = what_line() - 1;

   bob();

   % If we have any matches then move automagically to the first one

   if(grep_matches > 0)
   {
      grep_parse(1);
   }
   else
   {
      error("No matches for '" + grep_pattern + "'");
   }
}

%==========================================================================
% Run ripgrep and capture the results

define ripgrep_search(recurse)
{
   variable buf,
            pattern, wildcard,
            rg_case_sensitive,
            rg_words,
            rg_opt,
            rg_cmd,
            rg_exclude,
            rg_desc,
            status;

   (, rg_case_sensitive, , , rg_words) = search_get_defaults();

   % What and where to search

   pattern = read_mini((recurse ? "Recursive search using rg:" : "Search current directory using rg:") , "", cursor_word());

   if(pattern == "")
       error("No search pattern specified");

   wildcard  = read_mini("Files to search:", "", grep_wildcard);

   % Default options for all searches

   rg_opt = "--color=never --no-messages --only-matching --line-number --column --no-heading --no-config --no-ignore --hidden";
   rg_exclude = "";
   rg_desc = "";

   last_parse_dir = 0;

   % Current directory, or recursive

   if(recurse)
   {
      rg_desc += "recursive ";
      rg_exclude += " --glob=!.git --glob=!.terraform";
   }
   else
   {
      rg_opt += " --max-depth=1";
   }

   % Exclude a set of files that we generally don't care about

   rg_exclude += " --glob='!*.bak' --glob='!tags'";

   % Disable word matching if the search string contains whitespace

   if(is_substr(pattern, " ") or is_substr(pattern, "\t") or is_substr(pattern, "\n"))
   {
      rg_words = 0;
   }

   % Normal or just word search

   if(rg_words)
   {
      rg_opt += " --word-regexp";
      rg_desc += "word ";
   }

   % Fixed-string search

   if(grep_fixed_string)
   {
      rg_opt += " --fixed-strings";
      rg_desc += "fixed string ";
   }

   % Force case-sensitivity if the pattern is mixed-case

   if(pattern != strlow(pattern) and pattern != strup(pattern))
   {
      rg_case_sensitive = 1;
   }

   if(rg_case_sensitive == 1)
   {
      rg_desc += "case-sensitive ";
      rg_opt += " --case-sensitive";
   }
   else
   {
      rg_desc += "case-insensitive ";
      rg_opt += " --ignore-case";
   }

   grep_pattern = pattern;
   grep_wildcard = wildcard;

   parse_type = PARSE_GREP;

   buf = whatbuf();

   save_buffers();

   setbuf(GREP_BUFFER);
   erase_buffer ();
   setbuf(buf);

   % Build the grep command

   rg_cmd = "rg '" + grep_pattern + "' " + rg_exclude + " " + rg_opt;
   rg_desc += " for '" + grep_pattern + "'";

   if(grep_wildcard != "")
   {
      rg_cmd += " --glob '" + grep_wildcard + "'";
      rg_desc += " in " + grep_wildcard;
   }

   % Actually run the grep command, outputting to the grep buffer

   flush("Running " + rg_desc);

   (grep_dir, status) = run_shell_cmd_buf(rg_cmd, GREP_BUFFER);

   flush("Done");

   eob();
   grep_matches = what_line() - 1;
   bob();

   % If we have any matches then move automagically to the first one

   if(grep_matches > 0)
   {
      grep_parse(1);
   }
   else
   {
      error("No matches for '" + grep_pattern + "'");
   }
}

% ******************************************************************************

define select_grep_tool()
{
   variable done = 0;

   while(not done)
   {
      variable c;
      variable search_type;

      if(grep_fixed_string)
         search_type = "Fixed";
      else
         search_type = "Regex";

      flush("Current tool: {{3}}"+grep_tool_name[search_tool] + "{{-}}: Select {{2}}G{{-}}rep, A{{2}}c{{-}}k, G{{2}}i{{-}}t Grep, {{2}}R{{-}}ipgrep - Search type ("+search_type+"): {{2}}F{{-}}ixed, {{2}}R{{-}}egex");

      c = toupper(getkey());

      switch(c)
      { case 'G': search_tool = USE_GREP;     }
      { case 'I': search_tool = USE_GIT_GREP; }
      { case 'R': search_tool = USE_RG;       }
      { case 'F': grep_fixed_string = 1;      }
      { case 'E': grep_fixed_string = 0;      }
      { case '\r': done = 1;                  }
      { beep();                               }
   }

   message("Now using {{2}}" + grep_tool_name[search_tool] + "{{-}} as current grep tool with {{2}}" + (grep_fixed_string ? "fixed strings" : "regexes"));
}

% ******************************************************************************
% Run grep or rg something

define grep(recurse)
{
   switch(search_tool)
   { case USE_GREP:     grep_search(recurse); }
   { case USE_GIT_GREP: git_grep(recurse);    }
   { case USE_RG:       ripgrep_search(recurse); }
   {
      variable err = sprintf("Invalid value for search_tool: %d", search_tool);
      error(err);
   }
}
