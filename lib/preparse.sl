variable Preprocess_Only = 1;
() = evalfile ("bytecomp.sl");

#ifdef HAS_DFA_SYNTAX
define preparse_enable_highlight_cache (file, name)
{
   create_syntax_table (name);

   % Make sure existing cached files are removed
   forever
     {
   variable dirfile = search_path_for_file (Jed_Highlight_Cache_Path, file, ',');
   if (dirfile == NULL)
     break;

   if (-1 == remove (dirfile))
     break;
     }
   dirfile = dircat (Jed_Highlight_Cache_Dir, file);
   () = remove (dirfile);

   _dfa_enable_highlight_cache (dirfile, name);
}

private define create_dfa_cache (file)
{
   file = expand_jedlib_file (file);
   ifnot (strlen (file))
     return;
   setbuf ("*dfa-cache*");
   erase_buffer ();
   if (-1 == insert_file_region (file,
             "%%% DFA_CACHE_BEGIN %%%",
             "%%% DFA_CACHE_END %%%"))
     return;

   bob ();
   ifnot (fsearch ("dfa_enable_highlight_cache"))
     return;

   replace ("dfa_enable_highlight_cache", "preparse_enable_highlight_cache");
   eob ();
   ifnot (re_bsearch ("[ \t]*dfa_set_init_callback[ \t]*([ \t]*&[ \t]*\\([^,]+\\),[ \t]*\"\\([^\"]+\\)\""))
     return;

   variable fun = regexp_nth_match (1);
   variable mode = regexp_nth_match (2);
   delete_line ();
   % Note that $1 has been unitialized because the use of $1 here is not
   % supported.
   vinsert ("()=__tmp($1); %s(\"%s\");", fun, mode);

   evalbuffer ();
}

flush ("creating DFA syntax tables...");

% List of modes for which dfa cache tables should be constructed

foreach ([
     "asnmode.sl",
     "basmode.sl",
     "sqlmode.sl",
     "dockermode.sl",
     "notesmode.sl",
     "rexxmode.sl",
     "cfgmode.sl",
     "cmmmode.sl",
     "cmdmode.sl",
     "cmode.sl",
     "cssmode.sl",
     "makemode.sl",
     "m4mode.sl",
     "pipelinemode.sl",
     "kconfigmode.sl",
     "gitmsgmode.sl",
     "dosmode.sl",
     "hlpmode.sl",
     "html.sl",
     "javamode.sl",
     "logmode.sl",
     "mhgmode.sl",
     "perl.sl",
     "rubymode.sl",
     "php.sl",
     "preparse.sl",
     "pscript.sl",
     "pymode.sl",
     "shmode.sl",
     "fishmode.sl",
     "slmode.sl",
     "tclmode.sl",
     "texcom.sl",
     "textmode.sl",
     "tpascal.sl",
     "gomode.sl",
     "rustmode.sl"
     ])
{
   create_dfa_cache ();
}
#endif

exit_jed();
