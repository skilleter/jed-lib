% ******************************************************************************
% Makefile editing mode
% ******************************************************************************

define_word ("0-9A-Za-z_");

define make_end_of_comment()
{
        forever
        {
                bol();
                skip_white();

                !if(looking_at("#"))
                {
                        break;
                }

                if(down(1) != 1)
                {
                        break;
                }
        }
}

define make_top_of_comment()
{
        forever
        {
                bol();
                skip_white();

                !if(looking_at("#"))
                {
                        break;
                }

                if(up(1) != 1)
                {
                        break;
                }
        }
}

define make_newline_and_indent()
{
   variable indentlevel=0;

   ERROR_BLOCK
   {
      _clear_error ();
      pop_spot();
   }

   push_spot();

   !if(bolp())
   {
      () = left(1);
      if(looking_at(":"))
      {
         indentlevel=1;
      }
   }

   bol();
   if(looking_at("\t"))
   {
      indentlevel=1;
   }

   ERROR_BLOCK
   {
   }

   pop_spot();
   newline();
   if(indentlevel==1)
   {
      insert("\t");
   }

}

define make_indent_line()
{
   message("INDENT");
   bol();
   insert("\t");
}

create_syntax_table ("MAKE");

define_syntax ("#",                                             "",    '%',     "MAKE");
define_syntax ("([{",                                   ")]}", '(', "MAKE");
define_syntax ('"',                                             '"',                    "MAKE");
define_syntax ('\'',                                    '\'',           "MAKE");
define_syntax ('\\',                                    '\\',           "MAKE");
define_syntax ("0-9a-zA-Z_-",       'w',                        "MAKE");        % words
define_syntax ("-+0-9a-fA-F.xXL",       '0',                    "MAKE");   % Numbers
define_syntax (",;.?:$@\\",                     ',',                    "MAKE");
define_syntax ('.',                                             '#',                    "MAKE");
define_syntax ('!',                                             '#',                    "MAKE");
define_syntax ("%-+/&*=<>|~^",      '+',                        "MAKE");
set_syntax_flags ("MAKE", 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%

private define setup_dfa_callback (name)
{
    dfa_enable_highlight_cache ("makemode.dfa", name);

    dfa_define_highlight_rule ("\\\\.", "normal", name);
    dfa_define_highlight_rule ("#.*$", "comment", name);
    % dfa_define_highlight_rule ("^.*:", "keyword", name);
    dfa_define_highlight_rule ("\\$\\(.*\\)", "keyword", name);
    dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*\"", "string", name);
    dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*$", "string", name);
    dfa_define_highlight_rule ("'[^']*'", "string", name);
    dfa_define_highlight_rule ("'[^']*$", "string", name);
    dfa_define_highlight_rule ("\\$\\(.*\\)", "string", name);
    dfa_define_highlight_rule ("[\\|&;\\(\\)<>\\:]", "Qdelimiter", name);
    dfa_define_highlight_rule ("[\\[\\]\\*\\?=]", "Qoperator", name);
    dfa_define_highlight_rule ("[A-Za-z_]+",
                               "Knormal", name);
    dfa_define_highlight_rule ("^\t[ \t]*", "region", name);
    % dfa_define_highlight_rule ("^\t.*", "preprocess", name);
    dfa_define_highlight_rule (".", "normal", name);
    dfa_build_highlight_table (name);

%       dfa_define_highlight_rule("^\\.",                                    "PQpreprocess",    name);
%       dfa_define_highlight_rule("#.*" ,                                    "comment",                         name);
%       dfa_define_highlight_rule("[A-Za-z_][A-Za-z_0-9]*",                     "Knormal",                      name);
%       dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?","number",                  name);
%       dfa_define_highlight_rule("$\([A-Z0-9_].\)",                            "string",                       name);
%       dfa_define_highlight_rule("[ \t]+",                                  "normal",                  name);
%       dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]",               "delimiter",               name);
%       dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^\\$\\\\@]",       "operator",                name);
%       dfa_define_highlight_rule("\\$",                                                                                                        "dollar",                       name);
%       dfa_define_highlight_rule("^\t[ \t]*",                                                                          "region",                       name);
%       dfa_build_highlight_table(name);
}
dfa_set_init_callback (&setup_dfa_callback, "MAKE");
%%% DFA_CACHE_END %%%
#endif

!if (keymap_p ("MAKE")) make_keymap ("MAKE");
definekey ("make_top_of_comment", "\e\xE0H", "MAKE");
definekey ("make_end_of_comment", "\e\xE0P", "MAKE");

% Make keywords (a mixture of GNU & Borland makes)

append_keywords("MAKE", 3,
                                         % GNU MAKE
                                         ".if",                                         ".end",                                         "define",                               "endef",                                "export",
                                         "ifdef",                               "ifndef",                               "ifeq",                                         "ifneq",                                "else",
                                         "endif",                               "include",                              "-include",                     "sinclude",                     "override",
                                         "unexport",                    "vpath",             "else",
                                         % Borland MAKE
                                         "!if",                                 "!endif",                               "!else",                                        "!elif",                                        "!ifdef",
                                         "!ifndef",                     "!error",                               "!undef",                               "!cmdswitches",         "!message",
                                         "!undef",
                                         ".autodepend",                 ".cacheautodepend",     ".ignore",                              ".keep",
                                         ".noautodepend",       ".nocacheautodepend",".noignore",                       ".nokeep",
                                         ".nosilent",                   ".noswap",                              ".path",                                        ".precious",
                                         ".silent",                             ".suffixes",                    ".swap",             ".quiet");

% Make functions

append_keywords("MAKE", 1,
                                         "subst", "patsubst", "strip", "findstring", "filter", "filter-out", "sort", "dir",
                                         "notdir", "suffix", "basename", "addsuffix", "addprefix", "join", "word", "words",
                                         "wordlist", "firstword", "wildcard", "error", "warning", "info", "shell", "origin", "foreach",
                "call", "realpath", "abspath", "eval");

% Make variables

append_keywords("MAKE", 2,
                                         "$@", "$%", "$<", "$?", "$^", "$+", "$+", "$*", "$(@D)", "$(@F)", "$@.", "$(*D)", "$(*F)", "$*.", "$(%D)",
                                         "$(%F)", "$%.", "$(<D)", "$(<F)", "$<.", "$(^D)", "$(^F)", "$^.", "$(+D)", "$(+F)", "$+.", "$(?D)");

% More Make variables

append_keywords("MAKE", 4,
                                         "MAKEFILES", "VPATH", "SHELL", "MAKESHELL", "MAKE", "MAKELEVEL", "MAKECMDGOALS",
                "CURDIR", "MAKEFLAGS", "MAKEOVERRIDES", "NOTPARALLEL",
                                         "SUFFIXES", ".LIBPATTERNS", "PHONY", "DEFAULT");

% Shell commands

append_keywords("MAKE", 5,
                "echo", "sudo", "mv", "cd", "cp", "while", "read", "do", "done", "rm",
                "chmod", "mkdir", "for", "in", "touch", "printf", "ln", "dirname", "exit", "file", "xargs",
                "time", "env", "bash", "envsubst", "test", "ls");

% External commands

append_keywords("MAKE", 0,
                "sed", "cat", "cut", "bunzip2", "zip", "md5sum", "date", "gcc", "find", "grep", "patch", "install", "tar", "xargs",
                "git", "pushd", "popd", "awk", "gzip", "tail", "head", "stat", "tee", "tar", "ctags", "repo", "cscope", "which",
                "ddd", "aws", "docker", "cmp", "bundle", "wget");

% Shell keywords

append_keywords("MAKE", 6,
                "true", "false", "if", "then", "elif", "fi");

% make command

append_keywords("MAKE", 7,
                "make");

define make_mode ()
{
   TAB = 8;
   USE_TABS=1;
   CASE_SEARCH=1;
   use_keymap("MAKE");
   set_mode("MAKE", 4);
   set_buffer_hook ("indent_hook", "make_indent_line");
   set_buffer_hook ("newline_indent_hook", "make_newline_and_indent");
   set_comment_info("MAKE", "#", NULL, NULL, 0x08);
   use_syntax_table ("MAKE");
   run_mode_hooks("make_mode_hook");
}

