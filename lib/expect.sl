$1="expect";

!if (keymap_p ($1)) make_keymap ($1);

create_syntax_table ($1);
define_syntax ("#", "", '%', $1);
define_syntax ("--", "", '%', $1);              % comments
define_syntax ("([{", ")]}", '(', $1);          % delimiters
define_syntax ('"', '"', $1);                   % quoted strings
define_syntax ('\'', '\'', $1);                 % quoted characters
define_syntax ("0-9a-zA-Z_", 'w', $1);          % words
%define_syntax ("-+0-9.eE", '0', $1);           % Numbers
define_syntax (",;.", ',', $1);                 % punctuation
define_syntax ("$%-+/*=<>!^", '+', $1);         % operators
set_syntax_flags ($1, 0);                       % keywords ARE case-sensitive

append_keywords($1, 0,
                "close", "debug", "disconnect", "exit",
                "exp_continue", "exp_internal", "exp_open", "exp_pid", "exp_send",
                "exp_send_error", "exp_send_log", "exp_send_tty", "exp_send_user",
                "exp_version", "expect", "expect_after", "expect_background",
                "expect_before", "expect_tty", "fork", "interact", "interpreter",
                "log_file", "log_user", "match_max", "overlay", "parity", "remove_nulls",
                "send", "send_error", "send_log", "send_tty", "send_user", "sleep", "spawn",
                "strace", "stty", "system", "timestamp", "trap", "wait");

append_keywords($1, 1,
                "if", "set");

append_keywords($1, 2,
                "timeout", "eof", "llength", "lindex", "lrange", "lassign");

append_keywords($1, 3,
                "argv");

define expect_mode()
{
   variable modename = "expect";

   TAB = 4;
   set_mode (modename, 0x4); % flag value of 4 is generic language mode
   use_keymap(modename);
   use_syntax_table (modename);
   run_mode_hooks ("expect_mode_hook");
}
