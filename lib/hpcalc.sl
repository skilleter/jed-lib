% hpcalc.sl             -*- mode: SLang; mode: Fold -*-
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (c)1997,98 Mark Olesen
%
% Do as you wish with this code under the following conditions:
% 1) leave this notice intact
% 2) don't try to sell it
% 3) don't try to pretend it is your own code
% 4) it's nice when improvements make their way back to the 'net'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HP-style RPN calculator (v0.9) by Mark Olesen <no fixed email address>
% for the Jed editor by John E. Davis <davis@space.mit.edu>
%
% minimum requirements:
%    JED (0.98.7)
%
% to use this mode:
% place it somewhere in the JED_LIBRARY path and add
%
%       autoload ("hpcalc",     "hpcalc");
%
% to ~/.jedrc.sl or $JED_ROOT/lib/defaults.sl
%
% you may also want to preparse it
% ------------------------------------------------------------------------
%{{{ Notes:
%
% The displayed calculator looks like this:
%
%       <-deg   base 10 ->              <-- status line
%       ...     0.000000                \
%       <-3->   0.000000                 |
%       <-2->   0.000000                 |- stack
%       <-1->   0.000000                 |
%       <-0->   0.000000                /
%       ->      ________                <-- input region
%
%  Numbers or commands may be entered in the input region after the '->'
%  prompt.  Input is terminated by [ENTER]
%
%  After each operation, a new calculator is appended to the buffer --
%  like an adding machine.  After many calculations you may wish to
%  `clear' the stack and buffer.
%
%  There's no support for memory storage/recall -- use cut/paste from
%  previously displayed stacks.
%
%  Features not found in most calculators:
%
%       - character values      eg, 'J'
%       - octal numbers         eg, 0112
%       - hex numbers           eg, 0x4A
%
%       - "stack" multiple values/operations separated by whitespace
%         and/or ',' and/or ';'
%         eg.
%             calculate sin^2 (3*15) + cos^2 (9*5) = ...
%
%         -> 3 15 * sin sq 9 5 * cos sq +
% ------------------------------------------------------------------------
% supported commands:
%               + - * /
%   `M-|'       abs     abs(x)
%   `M--'       chs     -x
%   `M-/'       inv     1/x
%               int     integer part of x
%               frac    fractional part of x
%
% circular functions
%   `M-P'       PI      3.141592654
%   `M-s'       sin
%   `M-c'       cos
%   `M-t'       tan
%   `M-as'      asin
%   `M-ac'      acos
%   `M-at'      atan
%
%               deg     degrees
%               rad     radians
%               todeg   convert x from radians to degrees
%               torad   convert x from degrees to radians
%
% math
%   `M-E'       E       2.718281828
%               ^       y^x
%   `C-Q'       sq      x^2
%   `M-e'       exp     exp(x)
%   `M-n'       ln      log(x)
%   `M-l'       log     log10(x)
%   `M-L'       alog    10^x
%   `M-q'       sqrt    sqrt(x)
%
% Stack
%               help    insert help file
%               base    displayed base (0,2, 8,10,16) = char, bin, oct, dec, hex
%               prec    adjust decimal point (base 10)
%               clear   clear stack and buffer
%  `M-Enter'    last    retrieve last x
%  `C-V'        roll    rolldown stack
%               ret     push x onto stack
%               x       exchange (x,y)
%
% integer/bitwise operations
%
%               &       y and x (bitwise)
%               |       y or x  (bitwise)
%               %       y mod x
%               !       not (x)
%               xor     y xor x (bitwise)
%               <<      y shl x = y * 2^x
%               >>      y shr x = y / 2^x
% ------------------------------------------------------------------------
%}}}
%{{{ Revisions:
% 31 Oct 98     updated to Jed 0.98.7, with all of its new SLang changes
%
% 17 Sept 96    combined help file with SLang code
%               updated to reflect some new JED shortcuts
%
% 10 Jan 96     moved ^L key-binding "last" to M-Enter
%
% 11 Sept 95    the preprocessor define `FLOAT_TYPE', which is new in
%               SLang 0.99-20, makes it possible to use this file with a
%               non-floating-point JED binary for integer only calculations.
%               Added ability to handle negative char/octal/hex numbers
%
% 23 June 95    Can no longer directly edit the stack -- nice idea but
%               subject to parse errors.
%               Simplified several routines and implemented USER_BLOCKS
%               for a net size savings of about 15 per cent for the
%               preparsed code.
%               HPexit() kills the buffer and removes definitions to
%               save memory.
% ------------------------------------------------------------------------
%}}}
%{{{ variables:
variable hpsz = 6;              % stack size; 4 <= sz <= 10
variable hpbuf = "*hpcalc*";    % the buffer name

variable hplastx = 0.0;         % last value entered
variable hpbase = 10;           % base (0,2, 8,10,16)

#ifdef SLANG_DOUBLE_TYPE
variable hpprec = 5;            % number of decimal points
variable hpfmt = "%.*f";
variable hpdeg = PI / 180;      % deg -> radians conversion
% variable hpstk = create_array ('f', hpsz + 1, 1);
variable hpstk = Double_Type [hpsz+1];
#else
variable hpfmt = "%d";
% variable hpstk = create_array ['i', hpsz + 1];
variable hpstk = Integer_Type [hpsz+1];
#endif
%}}}

define HPclear()
{
   variable i;
    hplastx = 0;
   _for (0, hpsz, 1) { i = (); hpstk[i] = 0; }  % clear the stack
}

% recenter the buffer
define HPrecenter () { recenter (hpsz+2); }

define binprint(value_f)
{
   variable i, j, result="", value=int(value_f);

   j=4;

   for(i=0; i<32; i++)
   {
      if(value & 1)
        result="1" + result;
      else
        result="0" + result;
      value = value >> 1;

      j=j-1;
      if(j==0)
      {
         result=" " + result;
         j=4;
      }

   }
   result;
}

%!% print the calculator stack
define HPprint (help)%{{{
{
   variable i, x;

   sw2buf (hpbuf);
   eob ();
   newline ();

   if (help)
     {
        EXIT_BLOCK
          {
             () = getkey ();
             flush ("");
             eob ();
          }
        i = insert_file_region (expand_jedlib_file ("hpcalc.sl"),
                                "<--",  "#endif");
        if (i < 0)
          {
             flush ("hpcalc.sl not found in JED_LIBRARY path: Strike any key");
          }
        else
          {
             go_up (i / 2); bol ();
             recenter (0);
             flush ("Strike any key");
          }
     }
   else
     {
        insert ("<-");
#ifdef SLANG_DOUBLE_TYPE
        if (hpdeg == 1) "rad"; else "deg";
#else
        "int";                  % integer only
#endif
        insert (());
        insert (sprintf ("\tbase %d ->\n", hpbase));
        _for (hpsz - 1, 0, -1)
          {
             i = ();
             vinsert ("<-%d->\t", i);
             x = hpstk[i];
             i = int (x);

             switch (hpbase)
#ifdef SLANG_DOUBLE_TYPE
               {case 10: sprintf (hpfmt, hpprec, x);}
#endif
               {case 0 and (x == 10) : "'^J'"; }        % linefeed
          {case 2: binprint(x); }
               {sprintf (hpfmt, i); }
             insert (());
             newline ();
          }
        insert ("->\t");                % the prompt
        HPrecenter ();
     }
   set_buffer_modified_flag (0);        % pretend no modifications
}
%}}}

%!% command dispatch routine
define HPdispatch (cmd)%{{{
{
   variable i, x = hpstk[0], y = hpstk[1];
   variable ix = int (x), iy = int (y);

   % roll-down the stack
   USER_BLOCK0
     {
        _for (1, hpsz, 1) { i = (); hpstk[i - 1] = hpstk[i]; }
     }

   % push a value onto the stack
   USER_BLOCK1
     {
        x = ();
        _for (hpsz - 1, 1, -1) { i = (); hpstk[i] = hpstk[i - 1]; }
        hpstk[0] = x;
     }

   cmd = strlow (cmd);
   switch (cmd)
     {case "?":         HPprint (1); }                          % help file
   % binary functions
     {case "*":         X_USER_BLOCK0; hpstk[0] = (y * x); }    % y * x
     {case "+":         X_USER_BLOCK0; hpstk[0] = (y + x); }    % y + x
     {case "-":         X_USER_BLOCK0; hpstk[0] = (y - x); }    % y - x
     {case "/":         X_USER_BLOCK0; hpstk[0] = (y / x); }    % y / x
     {case "^":         X_USER_BLOCK0; hpstk[0] = (y^x);}       % y^x
     {case "roll":      X_USER_BLOCK0; hpstk[hpsz - 1] = x; }   % rotating stack
   % integer/bitwise operations
     {case "<<":        X_USER_BLOCK0; hpstk[0] = (iy shl ix); }        % (y << x)
     {case ">>":        X_USER_BLOCK0; hpstk[0] = (iy shr ix); }        % (y >> x)
     {case "%":         X_USER_BLOCK0; hpstk[0] = (iy mod ix); }
     {case "&":         X_USER_BLOCK0; hpstk[0] = (iy & ix); }
     {case "|":         X_USER_BLOCK0; hpstk[0] = (iy | ix); }
     {case "xor":       X_USER_BLOCK0; hpstk[0] = (iy xor ix); }
     {case "x":         hpstk[1] = x; hpstk[0] = y; }           % exch (x,y)
     {case "last":      X_USER_BLOCK1 (hplastx); }
     {case "\r":        X_USER_BLOCK1 (x); }                    % re-enter x
     {case "sq" or case "^2":   hpstk[0] = (x^2); }             % x^2
#ifdef SLANG_DOUBLE_TYPE
     {case "e":         X_USER_BLOCK1 (E); }                    % natural base
     {case "pi":        X_USER_BLOCK1 (PI); }                   % PI
     {case "alog":      hpstk[0] = (10^x); }                    % 10^x
     {case "sin":       hpstk[0] = (sin (x * hpdeg)); }
     {case "cos":       hpstk[0] = (cos (x * hpdeg)); }
     {case "tan":       hpstk[0] = (tan (x * hpdeg)); }
     {case "asin":      hpstk[0] = (asin (x) / hpdeg); }
     {case "acos":      hpstk[0] = (acos (x) / hpdeg); }
     {case "atan":      hpstk[0] = (atan (x) / hpdeg); }
     {case "exp":       hpstk[0] = (exp (x)); }
     {case "ln":        hpstk[0] = (log (x)); }
     {case "log":       hpstk[0] = (log10 (x)); }
     {case "sqrt":      hpstk[0] = (sqrt (x)); }
#endif
   % home-rolled functions
     {case "print":     HPprint (0); }
      {case "base":             % set base
         X_USER_BLOCK0;
%       i = integer (read_mini ("Base 0,2,8,10,16:", "", ""));
        switch (x)
          {case 0:  0; "'%c'"; }
          {case 8:  8; "%#o"; }
          {case 16: 16; "0x%X"; }
        {case 2: 2; ""; }
          {10;          % default to base 10
#ifdef SLANG_DOUBLE_TYPE
             "%.*f";
#else
             "%d";
#endif
          }
        hpfmt = (); hpbase = ();
      }

#ifdef SLANG_DOUBLE_TYPE
      {case "prec":             % set precision
        %i = integer (read_mini ("Precision:", string (hpprec), ""));
         X_USER_BLOCK0;
        if ((x >= 0) and (x <= 16)) hpprec = x;
      }
      {case "sci":      hpbase = 10; hpfmt = "%.*e"; }
      {case "fix":      hpbase = 10; hpfmt = "%.*f"; }
#endif
      {case "clear":    % clear the stack & buffer
         hplastx = 0;
         _for (0, hpsz, 1) { i = (); hpstk[i] = 0; }    % clear the stack
         erase_buffer ();
      }
#ifdef SLANG_DOUBLE_TYPE
     {case "inv":       hpstk[0] = (1.0/x); }   % 1/x
     {case "deg":       hpdeg = PI/180; }
     {case "rad":       hpdeg = 1; }
     {case "torad":     hpstk[0] = (x * PI/180); }
     {case "todeg":     hpstk[0] = (x * 180/PI); }
     {case "frac":      hpstk[0] = (x-ix); }    % frac (x)
     {case "int":       hpstk[0] = (ix); }      % int (x)
#endif
     {case "abs":       if (x < 0) hpstk[0] = -x; }     % abs (x)
     {case "chs":       hpstk[0] = -x; }                % -x
     {case "!":         hpstk[0] = not (ix); }
     {verror ("no calc function `%s'", cmd); }
}
%}}}

%!% parse the input string & send to dispatch routine
define HPenter ()%{{{
{
   variable str, delim = "\t ,:;";     % various delimiters to skip

   EXIT_BLOCK { HPprint (0); }

   % dispatch to the stack
   USER_BLOCK0
     {
        hplastx = ();                  % new last-x
        HPdispatch ("last");
     }

   bol ();
   !if (looking_at ("->")) return;     % invalid entry

   go_right (2); skip_white ();        % skip prompt
   if (eolp ())
     {
        HPdispatch ("\r");             % blank command-line
        return;
     }

   % dispatch command line args/values
   while (skip_chars (delim), not (eolp ()))
     {
        push_mark ();           % mark region
        skip_chars ("^" + delim);
        str = bufsubstr ();

        switch (_slang_guess_type (str))
          { case Double_Type:
             X_USER_BLOCK0 (atof (str)); }
          { case Integer_Type:
             X_USER_BLOCK0 (integer (str)); }
          { case String_Type:
             if (str[0] == '\'')        % character values
               {
                  X_USER_BLOCK0 (int (extract_element (str, 1, '\'')));
               }
             else
               {
                  HPdispatch (str);     % single character or string: a command
               }
          }
     }
}
%}}}

%!% direct command dispatch routine
define HPcmd (cmd)
{
   insert (cmd);
   HPenter ();
}

define HPins (str)
{
   eob ();
   insert (" " + str + " ");
}

%{{{ Keymap, Syntax highlighting
$1 = "hpcalc";
!if (keymap_p ($1)) make_keymap ($1);
definekey ("HPenter",           "\r",   $1);
definekey ("HPenter",           "\eOM", $1);
definekey (".eob HPrecenter",   "\f",   $1);
definekey (".\"?\"HPcmd",       "?",    $1);
#ifdef SLANG_DOUBLE_TYPE
definekey (".\"E\"HPins",       "\eE",  $1);
definekey (".\"PI\"HPins",      "\eP",  $1);
definekey (".\"cos\"HPins",     "\ec",  $1);
definekey (".\"sin\"HPins",     "\es",  $1);
definekey (".\"tan\"HPins",     "\et",  $1);
definekey (".\"acos\"HPins",    "\eC",  $1);
definekey (".\"asin\"HPins",    "\eS",  $1);
definekey (".\"atan\"HPins",    "\eT",  $1);
definekey (".\"acos\"HPins",    "\e^c", $1);
definekey (".\"asin\"HPins",    "\e^s", $1);
definekey (".\"atan\"HPins",    "\e^t", $1);
definekey (".\"exp\"HPins",     "\ee",  $1);
definekey (".\"log\"HPins",     "\el",  $1);
definekey (".\"alog\"HPins",    "\eL",  $1);
definekey (".\"alog\"HPins",    "\e^l", $1);
definekey (".\"ln\"HPins",      "\en",  $1);
definekey (".\"sqrt\"HPins",    "\eq",  $1);
definekey (".\"inv\"HPins",     "\e/",  $1);
#endif
definekey (".\"abs\"HPins",     "\e|",  $1);
definekey (".\"chs\"HPins",     "\e-",  $1);

definekey (".\"last\"HPins",    "\e\r", $1);
definekey (".\"sq\"HPins",      "^Q",   $1);
definekey (".\"roll\"HPcmd",    "^V",   $1);
definekey ("HPexit",            "^XK",  $1);

$1 = "hpcalc";
% Now create and initialize the syntax tables.
create_syntax_table ($1);
define_syntax ("<-", "->", '%', $1);
define_syntax ('\'', '\'',      $1);
define_syntax ("a-zA-Z", 'w',   $1);            % words
define_syntax ("-+.0-9a-fA-FxX", '0',   $1);    % Numbers
define_syntax (",;.",           ',',    $1);
define_syntax ("%-+/&*<>|!~^",  '+',    $1);
set_syntax_flags ($1, 5);

() = define_keywords_n ($1, "Ex", 1, 0);
() = define_keywords_n ($1, "lnPIsq", 2, 0);
() = define_keywords_n ($1, "abschscosdegexpfixintinvlogradretscisintanxor", 3, 0);
() = define_keywords_n ($1, "acosasinatanbasefraclastprecrollsqrt", 4, 0);
() = define_keywords_n ($1, "clearprinttodegtorad", 5, 0); % log10
%}}}

define hpcalc ()
{
   variable i = bufferp (hpbuf), mode = "hpcalc";
   pop2buf (hpbuf);
   !if (i)
     {
        use_keymap (mode);
        use_syntax_table (mode);
        set_buffer_no_autosave ();
        set_buffer_undo (0);
        set_status_line ("(%v)   HP-rpn: (%b)   Help: '?'  (%p)   %t", 0);
        HPdispatch ("clear");
     }

   % resize window (migrate to site.sl?)
   if (nwindows () == 2)
     {
        i = hpsz + 2 - window_info ('r');
        if (i >= 0)
          {
             loop (i) enlargewin ();
          }
        else
          {
             otherwindow ();
             loop (-i) enlargewin ();
             otherwindow ();
          }
     }

   HPprint (0);
}

%!% kill the buffer and unload as many definitions as possible
define HPexit ()
{
   if (hpbuf == whatbuf ())
     {
        set_buffer_modified_flag (0);
        delbuf (hpbuf);

        eval (".()HPcmd");
        eval (".()HPrecenter");
        eval (".()HPdispatch");
        eval (".()HPenter");
        eval (".()HPprint");
        % quasi-autoloading, redefine 'hpcalc'
        eval (".(\"hpcalc.sl\"expand_jedlib_file evalfile pop)hpcalc");
     }
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{{{HELP file - do not delete or resize
#iffalse
<------------- HP-style RPN calculator (deg rad base fix sci prec) ---------->
<-                                                                          ->
<-             M-/: inv  M-e: exp   M-q: sqrt         M-s: sin  M-^s: asin  ->
<-  M-L: last  M-|: abs  M-n: ln    C-Q: sq   [x^2]   M-c: cos  M-^c: acos  ->
<-  C-V: roll  M--: chs  M-l: log   M-L: alog [10^x]  M-t: tan  M-^t: atan  ->
<-  M-Enter: last                           ^ [y^x]   M-p: PI   M-e:  E     ->
<-                                                                          ->
<-  + - * / ! & | xor % << >> clear int frac todeg torad   x = exch (x,y)   ->
#endif
%}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%% end-of-file (SLang) %%%%%%%%%%%%%%%%%%%%%%%%%%
