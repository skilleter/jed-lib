% Ruby mode

define_word ("0-9A-Za-z_");

!if (keymap_p ("ruby"))
        make_keymap ("ruby");

% Create and initialize the syntax tables.

create_syntax_table ("ruby");

define_syntax ("#", "", '%', "ruby");
define_syntax ("([{", ")]}", '(', "ruby");
define_syntax ('"', '"', "ruby");
define_syntax ('\'', '\'', "ruby");
define_syntax ('\\', '\\', "ruby");
define_syntax ("0-9a-zA-Z_", 'w', "ruby");        % words
define_syntax ("-+0-9a-fA-F.xXUL", '0', "ruby");   % Numbers
define_syntax (",;.?:", ',', "ruby");
% define_syntax ('#', '#', "ruby");
define_syntax ("@%-+/&*=<>|!~^", '+', "ruby");

set_syntax_flags ("ruby", 0x4|0x40);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%

private define setup_dfa_callback (name)
{
        dfa_enable_highlight_cache("rubymode.dfa", name);

%       dfa_define_highlight_rule("^[ \t]*#", "PQpreprocess", name);
        dfa_define_highlight_rule("#.*", "comment", name);
%       dfa_define_highlight_rule("/\\*.*\\*/", "Qcomment", name);
%       dfa_define_highlight_rule("^([^/]|/[^\\*])*\\*/", "Qcomment", name);
%       dfa_define_highlight_rule("/\\*.*", "comment", name);
%       dfa_define_highlight_rule("^[ \t]*\\*+([ \t].*)?$", "comment", name);
%       dfa_define_highlight_rule("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
%       dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?", "number", name);
%       dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*[LlUu]*", "number", name);
%       dfa_define_highlight_rule("[0-9]+[LlUu]*", "number", name);
%       dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"", "string", name);
%       dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
%       dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*'", "string", name);
%       dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
%       dfa_define_highlight_rule("[ \t]+", "normal", name);
%       dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
%       dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);

        dfa_build_highlight_table(name);
}

dfa_set_init_callback (&setup_dfa_callback, "ruby");
%%% DFA_CACHE_END %%%
#endif

append_keywords("ruby", 0,
                "alias", "and", "begin", "break", "case", "class", "def",
                "defined?", "do", "else", "elsif", "end", "ensure",
                "for", "if", "in", "module", "next", "not", "or",
                "redo", "rescue", "retry", "return", "self", "super",
                "then", "undef", "unless", "until", "when", "while",
                "yield");

append_keywords("ruby", 1,
                "__FILE__", "__LINE__", "__ENCODING__",
                "BEGIN", "END",
                "false", "true",
                "nil");

define ruby_mode ()
{
   set_mode ("ruby", 2);
   use_keymap("ruby");
   use_syntax_table ("ruby");
   set_comment_info("ruby", "#", NULL, NULL, 0);

   USE_TABS=0;
   TAB=2;
   run_mode_hooks ("ruby_mode_hook");
}

provide ("ruby_mode");
