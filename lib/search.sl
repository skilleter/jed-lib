% ******************************************************************************
% New search functionality
%
% TODO:
% * Case-specific search and replace doesn't seem to work
% * handle folded files - see folding.sl
% * fix isearch.sl
% * fix multi-line search strings
% * Handle zero-length search strings (?)
% * leave cursor after searching or s/r in a buffer
% * Need option in global s/r to skip buffer - quit now stops
% * Word search does not work with regexp
% * Global operations ignore buffers created after the search started
% * sort out global search and s/r and where to finish and where to
% * Terminating global operations is a bit awkward - it may miss a
%   match to the left of the start position on the current line
% * Global search doesn't always work - e.g. stops after match in
%   start buffer
%   Should set a mark on the first match then if subsequent search
%   finds itself back at the mark, it should stop, leaving the cursor
%   unmoved from previous position.
% * Should build a list of all matches found and stop if next search already
%   in the list (which means that if I do a search, then move 1 one then
%   search again after the last match, it wouldn't find it again).
% * Maybe it should just keep going and only stop when it runs out of matches?
% * Must be a sensible way of knowing when to stop!
%
% BUGS:
% * Case-sensitive s/r does insensitive!
% * Sometime search does not work at all if you switch from global back to
%   local search.
%     Global search, press F3 until no more matches.
%     Switch back to local search and F3 won't work, but a new search will
%     search_command() -> search_again() -> search_next_match()
% * Need to save buffer position on global search so if no matches in
%   buffer we go back to orginal position before moving to next buffer
% * In global search, if you hit the end of the search results, then
%   move elsewhere - (e.g. to the top of the file) and search again,
%   it finds nothing. Feels like it should backtrack to the file/pos
%   where you searched again.
% ******************************************************************************

debugset("DEBUG_SEARCH", 0);

% Default search configuration

private variable default_configuration = struct
{
   forwards = 1,             % Search direction
   global = 0,               % Set to 1 for global search & search and replace
   regexp = 0,               % Set to 1 for all search & search/replace commands to use regular expressions
   words = 0,                % Set to 1 to search for whole words only (Not implemented fully **##**)
   case_sensitive = 0,       % Set to 1 to be case-sensitive
};

% Default search options - all the data related to the current search
% or search and replace operation

private variable default_search = struct
{
   search_active = 0,               % 1 if a search has been made, 0 otherwise
   search_fn = NULL,                % Current search function

   search_text = "",                % String currently being searched for
   replace_text = "",               % Replacement text in search & replace operations

   type = "(no search)",            % Current search type (e.g. 'Global, forward search')

   last_match_pos = NULL,           % User mark saving location of last match

   found = 0,                       % Zero if last search failed, otherwise the length of the most recent match
   replacements = 0,                % Count of replacements made so far in current search & replace operation
   search_line = 0,

   buffers = NULL,                  % Used when doing global search and replace - the list of buffers to visit
   visited = NULL,                  % and the list of buffers visited, so that we can go backwards

   start_buffer = "",               % Buffer where the search started
   back_to_start = 0,               % Set to true when a global search loops back to the buffer it started from
                                    % so we don't overlap the end of the search with the start
   initial_buffer_pos = NULL,       % User mark for location in current buffer before global search
   found_in_current_buffer = 0,     % Set to 0 when switching buffers and 1 when string found - used to work out if we reset buffer location
};

% Reset the current search configuration & options

private variable config = @default_configuration;
private variable search = @default_search;

% Forward definitions

private define wrap_re_fsearch(str);
private define wrap_re_bsearch(str);

define mark_next_nchars(n, dir);

% ******************************************************************************
% Work out which search function to use

private define set_search_function()
{
   if(config.regexp)
   {
      if(config.forwards)
      {
         search.search_fn = &wrap_re_fsearch;
         search.type = "forward regex";
      }
      else
      {
         search.search_fn = &wrap_re_bsearch;
         search.type = "feverse regex";
      }
   }
   else
   {
      if(config.forwards)
      {
         search.search_fn = &fsearch;
         search.type = "forward";
      }
      else
      {
         search.search_fn = &bsearch;
         search.type = "reverse";
      }
   }

   if(config.global)
   {
      search.type = "global " + search.type;
   }

   if(config.words)
   {
      search.type = search.type + " word";
   }

   search.type = search.type + " search";
}

% ******************************************************************************
% Reset search parameters in preparation for a new search operation

private define reset_search(str, rep)
{
   search = @default_search;

   LAST_SEARCH = str;

   search.search_text = str;
   if(rep != NULL)
      search.replace_text = rep;

   search.search_active = 1;
   search.search_line = what_line();
   search.buffers = editable_buffers(0);
   search.visited = {};
   search.start_buffer = whatbuf();
   search.initial_buffer_pos = create_user_mark();
   search.found_in_current_buffer = 0;

   set_search_function();

   debugmsg("DEBUG_SEARCH", "--------------------------------------------------------------------------------");

   if(rep == NULL)
      debugmsg("DEBUG_SEARCH", sprintf("New search: str='%s', %d buffers", str, length(search.buffers)));
   else
      debugmsg("DEBUG_SEARCH", sprintf("New search & replace: str='%s', rep='%s', %d buffers", str, rep, length(search.buffers)));

   debugmsg("DEBUG_SEARCH",
            sprintf("Config: forwards: %d, global: %d, regex: %d, words: %d, case: %d",
                    config.forwards, config.global, config.regexp, config.words, config.case_sensitive));
}

% ******************************************************************************

private define search_multiline(linebreak)
{
   variable s, s1, n;

   % Split the search string at the first linebreak

   s = substr (search.search_text, 1, linebreak);
   s1 = substr (search.search_text, linebreak + 1, strlen (search.search_text));

   n = strlen (s);

   debugmsg("DEBUG_SEARCH", sprintf("search_multiline: s='%s', s1='%s'", s, s1));

   push_mark ();

   % Search for the string followed by the bit after the linebreak
   % (which can, itself, contain more linebreaks).

   while((@search.search_fn)(s))
   {
      % we are matched at end of the line.
      go_right (n);

      if(looking_at (s1))
      {
         go_left (n);
         pop_mark(0);

         return strlen(search.search_text);
      }

      if(not config.forwards)
         go_left (n);
   }

   pop_mark(1);

   return 0;
}

% ******************************************************************************

private define search_singleline(multi_word)
{
   variable len = strlen(search.search_text);

   if(config.words)
   {
      while((@search.search_fn)(search.search_text))
      {
         if(search.search_text == cursor_word())
         {
            return len+1;
         }
         go_right(len);
      }

      return 0;
   }
   else
   {
      return (@search.search_fn)(search.search_text);
   }
}

% -------------------------------------------------------------------------
% Wrapper for re_fsearch() that matches the behaviour of fsearch()
% by return the match length or zero

private define wrap_re_fsearch(str)
{
   variable result = re_fsearch(str);

   if(result == 0)
   {
      return 0;
   }

   return result-1;
}

% -------------------------------------------------------------------------
% Wrapper for re_bsearch() that matches the behaviour of bsearch()
% by return the match length or zero

private define wrap_re_bsearch(str)
{
   variable result = re_bsearch(str);

   if(result == 0)
   {
      return 0;
   }

   return result-1;
}

% -------------------------------------------------------------------------
% buffer_search_function()
%
% Generic function for searching for a specified string using either
% fsearch or bsearch where the search string may contain a '\n'.
% Returns 1 + the length of the match, or 0 if no match
%
% **##** Could be improved to fit in the new paradigm better

private define buffer_search_function ()
{
   variable linebreak, n, s, s1, result;
   variable match_word = config.words;

   % Only match words if the search string is a word and, for the moment
   % if we are not doing a regex search

   if(config.words)
      if(not is_word(search.search_text) or config.regexp)
         match_word = 0;

   % If the search string doesn't have a line break then just
   % do a simple search for the term

   linebreak = is_substr (search.search_text, "\n");

   if(linebreak)
   {
      return search_multiline(linebreak);
   }
   else
   {
      return search_singleline(match_word);
   }
}

% ******************************************************************************
% Perform the current search
% If config.global is set, it will search through all
% buffers for the next match, otherwise it only searches the current
% buffer.

private define search_function()
{
   % We only loop if doing a global search (we break out otherwise)

   while(1)
   {
      variable found = buffer_search_function();

      debugmsg("DEBUG_SEARCH", sprintf("Searching: str='%s', buf='%s', global=%d, found=%d", search.search_text, whatbuf(), config.global, found));

      if(found == 0)
      {
         % Not found, look for the next buffer if we are searching
         % globally

         if(config.global)
         {
            variable buf = NULL;

            debugmsg("DEBUG_SEARCH", "Global: looking for next buffer to search");

            % Don't try and switch to a buffer that no longer exists

            while(length(search.buffers))
            {
               buf = list_pop(search.buffers);

               if(bufferp(buf))
               {
                  debugmsg("DEBUG_SEARCH", sprintf("Next buffer: '%s'", buf));
                  break;
               }

               debugmsg("DEBUG_SEARCH", sprintf("Buffer '%s' no longer exists, skipping it", buf));
            }

            % Found another buffer we haven't visited yet, switch to it,
            % move to the top of the buffer, add it to the list of
            % visited buffers and try again.

            if(buf == NULL)
            {
               debugmsg("DEBUG_SEARCH", "No more buffers");
            }
            else
            {
               if(buf == search.start_buffer)
               {
                  debugmsg("DEBUG_SEARCH", sprintf("Returning to original buffer: '%s'", search.start_buffer));
                  search.back_to_start = 1;
               }
               else
               {
                  debugmsg("DEBUG_SEARCH", sprintf("Going to '%s'", buf));
                  search.back_to_start = 0;
               }

               % Before switching to the next buffer, we need
               % to return to the original location in the buffer if
               % we didn't find any matches then we switch and save
               % the new current position before moving to the top

               !if(search.found_in_current_buffer)
               {
                  sw2buf(user_mark_buffer(search.initial_buffer_pos));
                  goto_user_mark(search.initial_buffer_pos);
               }

               flush("Switching to next buffer ("+buf+")");
               sw2buf(buf);

               search.initial_buffer_pos = create_user_mark();
               search.found_in_current_buffer = 0;

               bob();

               list_insert(search.visited, buf);
               continue;
            }
         }
      }
      else
      {
         % Found a match, but are we doing a global search, have looped
         % back to the first buffer and have gone past where we started?

         debugmsg("DEBUG_SEARCH", sprintf("Found match for search string '%s' (%d) in %s at line %d", search.search_text, strlen(search.search_text), whatbuf(), what_line()));

         if(search.back_to_start)
         {
            if(what_line() >= search.search_line)
            {
               debugmsg("DEBUG_SEARCH", sprintf("Match is in the start buffer, but is past the starting point (line %d) so we've looped and it doesn't count", search.search_line));
               found = 0;
            }
         }

         search.found_in_current_buffer = 1;
      }

      % Exit the loop if we have a match or aren't searching globally
      % or are, but can't find a match

      break;
   }

   return found;
}

% ******************************************************************************
% Return the text of the search or replace prompt including the
% current options

private define search_prompt(search_type)
{
   variable prompt;

   prompt = config.forwards ? "Forward " : "Reverse ";

   if(config.global)
      prompt += "global ";

   if(config.regexp)
      prompt += "regex ";

   if(config.case_sensitive == 0)
      prompt += "case-independent ";

   if(config.words)
      prompt += "word ";

   prompt += search_type + ":";

   return prompt;
}

% ******************************************************************************
% Search for the next match of the current search

private define search_next_match()
{
   variable current_location, current_buffer;

   !if(strlen (search.search_text))
      error("No search string specified");

   % Save the current position

   current_buffer = whatbuf();
   current_location = create_user_mark();

   % If we are at the last match position then move past it in the
   % search direction to avoid finding the thing we're currently
   % sitting on!

   if (search.last_match_pos == create_user_mark())
   {
      config.forwards ? go_right_1 () : go_left_1 ();
   }

   % Call the appropriate search function - it handles details like
   % local/global, normal/regex

   CASE_SEARCH = config.case_sensitive;
   search.found = search_function();

   % Not found, return to the previous location

   !if(search.found)
   {
      sw2buf(current_buffer);
      goto_user_mark(current_location);
   }

   return search.found;
}

% ******************************************************************************
% Perform the search and replace
% **##** Doesn't handle \N references in regexp replacements

private define replace_with_query()
{
   variable n, prompt, doit, err, ch, pat_len;
   variable undo_stack_type = struct
   {
      rep_len,
      prev_string,
      user_mark,
      next
   };
   variable undo_stack = NULL;
   variable tmp;
   variable query = 1;

   roughly_recenter();
   prompt = sprintf("Replace '%s' with '%s'? (y/n/u/U/!/q/h)", search.search_text, search.replace_text);

   debugmsg("DEBUG_SEARCH", sprintf("replace_with_query: search: '%s' (%d) with '%s' (%d)",
                                    search.search_text, strlen(search.search_text),
                                    search.replace_text, strlen(search.replace_text)));

   % Loop around all the matches

   while(pat_len = search_function(), pat_len > 0)
   {
      debugmsg("DEBUG_SEARCH", sprintf("match: pat_len=%d", pat_len));

      % If not querying each replacement, just replace everything

      ifnot(query)
      {
         if(config.regexp)
            () = replace_match(search.replace_text, 0);
         else
            () = replace_chars(pat_len, search.replace_text);

         if(pat_len == 0)
            go_right_1();

         search.replacements++;
         continue;
      }

      % Prompt the user and wait for a keypress

      message(prompt);
      mark_next_nchars(pat_len, -1);
      roughly_recenter();

      ch = getkey();

      switch (ch)
      {
      case 'u' and (undo_stack != NULL):
         goto_user_mark(undo_stack.user_mark);
         push_spot();
         () = replace_chars(undo_stack.rep_len, undo_stack.prev_string);
         pop_spot();
         undo_stack = undo_stack.next;
         search.replacements--;
      }
      {
      case 'U':
         while(undo_stack != NULL)
         {
            goto_user_mark(undo_stack.user_mark);
            push_spot();
            () = replace_chars(undo_stack.rep_len, undo_stack.prev_string);
            pop_spot();
            undo_stack = undo_stack.next;
            search.replacements--;
         }
      }
      {
      case 'y':
         tmp = @undo_stack_type;
         tmp.next = undo_stack;
         undo_stack = tmp;

         push_spot();
         push_mark();
         go_right(pat_len);
         undo_stack.prev_string = bufsubstr();
         pop_spot();
         undo_stack.user_mark = create_user_mark();
         undo_stack.rep_len = strlen(search.replace_text);

         if(config.regexp)
            () = replace_match(search.replace_text, 0);
         else
            () = replace_chars(pat_len, search.replace_text);

         if(pat_len == 0)
            go_right_1();

         search.replacements++;
      }
      {
      case 'n':
         go_right_1();
      }
      {
      case '!':
         query = 0;
      }
      {
      case 'q':
         break;
      }
      {
         flush("y:replace, n:skip, !:replace all, u: undo last, U:undo all, q:quit");
         () = input_pending(30);
      }
   }
}

% -------------------------------------------------------------------------
% Get the word under the cursor, but use the default if the
% word is too short or long

define search_cursor_word(srchdefault)
{
   variable str;

   str = cursor_word ();

   if((strlen(str) > 64) or (strlen(str) == 0)
      or looking_at(" ") or looking_at("\t"))
   {
      str = LAST_SEARCH;
   }

   return str;
}

% ******************************************************************************
% Report the current search configuration

private define search_report_defaults()
{
   variable msg = "Search operations are: ";

   msg += config.global                ? "global, " : "local, ";
   msg += config.forwards              ? "forwards" : "backwards";
   msg += " and ";
   msg += config.case_sensitive == 0   ? "case-independent" : "case-sensitive";
   msg += " using ";
   msg += config.regexp                ? "regexp" : "literal";
   msg += " matching ";

   if(config.words)
      msg += "on whole words only";

   return msg;
}

% ******************************************************************************
% Perform the current search

define search_again()
{
   variable result;

   !if(search.search_active)
   {
      error("No search active");
   }

   result = search_next_match();

   % Found, highlight the match

   if(result)
   {
      debugmsg("DEBUG_SEARCH", "search_again() - highlighting match");

      search.last_match_pos = create_user_mark();

      roughly_recenter();

      go_right(result);
      push_visible_mark();
      go_left(result);
      update(1);
      ungetkey(getkey());
      pop_mark_0();
   }
   else
   {
      debugmsg("DEBUG_SEARCH", "search_again() - no match found");

      error(sprintf("NOT FOUND: %s: '%s'", search.type, search.search_text));
   }
}

% ******************************************************************************
% Repeat the last search in the opposite direction

define search_previous()
{
   config.forwards = 1 - config.forwards;
   set_search_function();

   search_again();

   config.forwards = 1 - config.forwards;
   set_search_function();
}

% ******************************************************************************
% Prompt for something to search for and seach for it using current
% search configuration. Designed to be bound to keyboard or menu.

define search_command()
{
   variable cword, str;
   variable prompt;

   % Prompt the user for the search string

   prompt = search_prompt("search");

   cword = search_cursor_word(LAST_SEARCH);
   str = read_mini (prompt, cword, "");

   % If they've entered something to search for, save it and find the
   % next match

   if(strlen(str) > 0)
   {
      reset_search(str, NULL);
      search_again ();
   }
}

% ******************************************************************************
% **##**

define mark_next_nchars(n, dir)
{
   variable h;
   ERROR_BLOCK
   {
      set_line_hidden(h);
      pop_mark_0();
   }

   h = is_line_hidden();
   set_line_hidden(0);
   push_visible_mark();
   go_right(n);
   if(dir < 0)
      exchange_point_and_mark();
   update(1);
   ungetkey(getkey());
   EXECUTE_ERROR_BLOCK;
}

% ******************************************************************************
% Prompt the user and perform a search & replace

define replace_command()
{
   variable cword, str, rep;
   variable prompt;
   variable replaced=1;

   % Prompt the user for the search string

   prompt = search_prompt("replace");

   cword = search_cursor_word(LAST_SEARCH);
   str = read_mini (prompt, cword, "");

   % If they've entered something to search for, save it and find the
   % next match

   if(strlen(str) > 0)
   {
      prompt = sprintf("%s '%s' with:", prompt, str);

      rep = read_mini (prompt, "", search.replace_text);

      reset_search(str, rep);
      replace_with_query();

      flush(sprintf("{{4}}%d{{-}} instance(s) of '{{4}}%s{{-}}' replaced with '{{4}}%s{{-}}'", search.replacements, search.search_text, search.replace_text));
   }
}

% ******************************************************************************
% Return the current search configuration

define search_get_defaults()
{
   return (config.forwards,
           config.case_sensitive,
           config.regexp,
           config.global,
           config.words);
}

% ******************************************************************************
% Set search direction, etc. - Similar to search_set_defaults but sets the values
% explicitly, rather than having options to toggle or leave unchanged

define search_set_parameters(srch_dir, srch_case, srch_reg, srch_global, srch_words)
{
   config.forwards          = srch_dir;
   config.regexp            = srch_reg;
   config.global            = srch_global;
   config.words             = srch_words;
   config.case_sensitive    = srch_case;

   set_search_function();
}

% ******************************************************************************
% Return the current search and replace text strings

define search_get_text()
{
   return search.search_text, search.replace_text;
}

% ******************************************************************************
% Restore saved search and replace text strings

define search_set_text(srch_text, rpl_text)
{
   search.search_text = srch_text;
   search.replace_text = rpl_text;
}

% ******************************************************************************
% search_menu()
%
% Prompt the user to set the global search options (case-sensitivity,
% local/global, regexp/string match, forward/reverse)

define search_menu ()
{
   variable c, done=0, prompt;
   variable new_config = config;

   while(done==0)
   {
      prompt = new_config.forwards         ? "{{2}}FORWARD{{-}}/{{3}}b{{-}}ackwards, "               : "{{3}}f{{-}}orward/{{2}}BACKWARDS{{-}}, ";
      prompt += new_config.global          ? "{{2}}GLOBAL{{-}}/{{3}}l{{-}}ocal, "                    : "{{3}}g{{-}}lobal/{{2}}LOCAL{{-}}, ";
      prompt += new_config.case_sensitive  ? "{{2}}CASE-SENSITIVE{{-}}/case-{{3}}i{{-}}ndependent, " : "{{3}}c{{-}}ase-sensitive/{{2}}CASE-INDEPENDENT{{-}}, ";
      prompt += new_config.regexp          ? "{{2}}REGEXP{{-}}/{{3}}n{{-}}ormal, "                   : "{{3}}r{{-}}egexp/{{2}}NORMAL{{-}}, ";
      prompt += new_config.words           ? "{{2}}WORD{{-}}/{{3}}a{{-}}ll matches"                  : "{{3}}w{{-}}ord/{{2}}ALL MATCHES{{-}}";

      flush ("Search type ({{4}}Q{{-}} to leave unchanged): " + prompt + " -->");

      c = toupper (getkey());

      % Whaddya wannado?

      switch (c)
      { case 'F' : new_config.forwards = 1; }
      { case 'B' : new_config.forwards = 0; }
      { case 'G' : new_config.global = 1; }
      { case 'L' : new_config.global = 0;  }
      { case 'C' : new_config.case_sensitive = 1;  }
      { case 'I' : new_config.case_sensitive = 0;  }
      { case 'R' : new_config.regexp = 1;  }
      { case 'N' : new_config.regexp = 0;  }
      { case 'W' : new_config.words = 1;  }
      { case 'A' : new_config.words = 0;  }
      { case 'Q' : done       = -1; }
      { case '\r': done       = 1;  }
   }

   clear_message();

   if(done == -1)
   {
      message("Settings not changed");
      return;
   }

   config = new_config;
   message(search_report_defaults());
   update_statusline();
   set_search_function();
}

% ******************************************************************************
% Prompt for a character and search backwards for the next instance of it.
% If a visible region is not active, it creates one starting at the
% initial cursor position.

define search_char_backwards()
{
   variable ch, result;

   flush("Press character to move backwards to");
   ch = getkey();

   if(is_visible_mark() == 0)
      push_visible_mark();

   go_left_1();
   result = bsearch_char(ch);

   if(result == 0)
   {
      error("Not found");
   }
   else
   {
      flush("");
   }
}

% ******************************************************************************
% Prompt for a character and search forward for the next instance of it.
% If a visible region is not active, it creates one starting at the
% initial cursor position.

define search_char_forwards()
{
   variable ch, result;

   flush("Press character to move forwards to");
   ch = getkey();

   if(is_visible_mark() == 0)
      push_visible_mark();

   go_right_1();
   result = fsearch_char(ch);

   if(result == 0)
   {
      error("Not found");
   }
   else
   {
      flush("");
   }
}
