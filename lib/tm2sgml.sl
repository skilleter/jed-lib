if (__argc != 4)
{
   message ("Usage: jed -script tm2sgml.sl <tm-file-without-extension>");
   quit_jed ();
}

!if (is_defined ("textmac_to_linuxdoc"))
  autoload ("textmac_to_linuxdoc", "textmac");

variable file = __argv[3];

() = read_file (strcat (file, ".tm"));

if (1)
{

   ERROR_BLOCK
     {
        % Write out what we have to provide some additional debugging clues
        () = write_buffer (strcat (file, ".sgml"));
     }
   textmac_to_linuxdoc ();
}

() = write_buffer (strcat (file, ".sgml"));
quit_jed ();
