% S-Lang mode is just a superset of C mode so make sure it is loaded.
require ("cmode");

$1 = "SLANG";

create_syntax_table ($1);
define_syntax ("%", "", '%', $1);
define_syntax ("([{", ")]}", '(', $1);
define_syntax ('"', '"', $1);
define_syntax ('`', '"', $1);
define_syntax ('\'', '\'', $1);
define_syntax ('\\', '\\', $1);
define_syntax ("0-9a-zA-Z_$", 'w', $1);        % words
define_syntax ("-+0-9a-fA-F.xX", '0', $1);   % Numbers
define_syntax (",;:.?", ',', $1);
define_syntax ('#', '#', $1);
define_syntax ("@%-+/&*=<>|!~^", '+', $1);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache ("slmode.dfa", name);
   dfa_define_highlight_rule("^[ \t]*#", "PQpreprocess", name);
   dfa_define_highlight_rule("%.*$", "comment", name);
   dfa_define_highlight_rule("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
   dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?",
                         "number", name);
   dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*", "number", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"", "string", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*'", "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
   dfa_define_highlight_rule("[ \t]+", "normal", name);
   dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
   dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);
   dfa_define_highlight_rule("!if", "keyword0", name);
   dfa_build_highlight_table(name);
}
dfa_set_init_callback (&setup_dfa_callback, "SLANG");
%%% DFA_CACHE_END %%%
#endif

% 0: keywords

append_keywords($1, 0,
          "and",           "andelse",       "break",         "case",          "catch",
          "continue",      "define",        "do",            "else",          "ERROR_BLOCK",
          "exch",          "EXIT_BLOCK",    "finally",       "_for",          "for",
          "foreach",       "forever",       "!if",           "if",            "loop",
          "mod",           "not",           "or",            "orelse",        "pop",
          "private",       "public",        "return",        "shl",           "shr",
          "static",        "struct",        "switch",        "__tmp",         "throw",
          "try",           "typedef",       "USER_BLOCK1",   "USER_BLOCK2",   "USER_BLOCK0",
          "USER_BLOCK4",   "USER_BLOCK3",   "using",         "variable",
          "while",         "xor",           "ifnot",         "then");

% 1: built-in constants

append_keywords($1, 1,
                "ADD_NEWLINE", "ALT_CHAR", "BACKUP_BY_COPYING", "BATCH", "BLINK", "CASE_SEARCH", "CASE_SEARCH_DEFAULT",
                                        "CHEAP_VIDEO", "CURRENT_KBD_COMMAND", "DEC_8BIT_HACK", "DEFINING_MACRO", "DISPLAY_EIGHT_BIT",
                                        "DISPLAY_TIME", "DOLLAR_CHARACTER", "EXECUTING_MACRO", "FN_CHAR", "HIGHLIGHT", "HORIZONTAL_PAN",
                                        "IGNORE_BEEP", "IGNORE_USER_ABORT", "IsHPFSFileSystem", "JED_ROOT", "KILL_ARRAY_SIZE", "KILL_LINE_FEATURE",
                                        "LASTKEY", "LAST_CHAR", "LINENUMBERS", "MAX_HITS", "MESSAGE_BUFFER", "META_CHAR", "MINIBUFFER_ACTIVE",
                                        "Replace", "NULL",
                                        "SCREEN_HEIGHT", "SCREEN_WIDTH", "Simulate_Graphic_Chars", "Skip", "Status_Line_String", "TAB", "TAB_DEFAULT",
                                        "TERM_BLINK_MODE", "TERM_CANNOT_INSERT", "TERM_CANNOT_SCROLL", "TOP_WINDOW_ROW", "USE_ANSI_COLORS",
               "USE_TABS_DEFAULT", "USE_TABS", "WANT_EOB",
                                        "WANT_SYNTAX_HIGHLIGHT", "WRAP", "WRAP_INDENTS", "X_LAST_KEYSYM");

append_keywords($1, 2,
                "OSError", "MallocError", "RunTimeError", "InvalidParmError", "InternalError", "UnknownError", "ImportError",
                "TypeMismatchError", "UserBreakError", "StackError", "StackUnderflowError", "StackOverflowError", "ReadOnlyError",
                "VariableUninitializedError", "NumArgsError", "IndexError", "UsageError", "ApplicationError", "NotImplementedError",
                "LimitExceededError", "ForbiddenError", "MathError", "DivideByZeroError", "ArithOverflowError", "ArithUnderflowError",
                "DomainError", "IOError", "WriteError", "ReadError", "OpenError", "DataError", "UnicodeError", "UTF8Error",
                "NamespaceError", "ParseError", "SyntaxError", "DuplicateDefinitionError", "UndefinedNameError");

append_keywords($1, 1,
                "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$9");

% 3: S-Lang functions

append_keywords($1, 3,
                                        "all", "any", "array_info", "array_map", "array_reverse", "array_shape", "array_sort", "array_swap", "cumsum", "init_char_array",
                                        "length", "max", "min", "reshape", "sum", "transpose", "where", "wherefirst", "wherelast", "assoc_delete_key", "assoc_get_keys",
                                        "assoc_get_values", "assoc_key_exists", "array_to_bstring", "bstring_to_array", "bstrlen", "pack", "pad_pack_format", "sizeof_pack",
                                        "unpack", "Assoc_Type", "List_Type", "String_Type", "Struct_Type", "Ref_Type", "chdir", "chmod", "chown", "getcwd", "hardlink", "listdir",
                                        "lstat_file", "mkdir", "readlink", "remove", "rename", "rmdir", "stat_file", "stat_is", "symlink", "autoload", "byte_compile_file", "eval",
                                        "evalfile", "get_slang_load_path", "set_slang_load_path", "get_import_module_path", "import", "set_import_module_path", "add_doc_file",
                                        "get_doc_files", "get_doc_string_from_file", "is_defined", "set_doc_files", "list_append", "list_delete", "list_insert",
                                        "list_new", "list_pop", "list_reverse", "abs", "acos", "acosh", "asin", "asinh", "atan", "atan2", "atanh", "ceil", "Conj",
                                        "cos", "cosh", "exp", "floor", "hypot", "Imag", "isinf", "isnan", "log", "log10", "mul2", "nint", "polynom", "Real", "round",
                                        "set_float_format", "sign", "sin", "sinh", "sqr", "sqrt", "tan", "tanh", "errno", "errno_string", "error", "message", "new_exception",
                                        "usage", "verror", "vmessage", "current_namespace", "getenv", "implements", "putenv", "use_namespace", "path_basename", "path_basename_sans_extname",
                                        "path_concat", "path_dirname", "path_extname", "path_get_delimiter", "path_is_absolute", "path_sans_extname", "close",
                                        "dup_fd", "fileno", "isatty", "lseek", "open", "read", "write", "getegid", "geteuid", "getgid", "getpid", "getppid", "getuid",
                                        "kill", "mkfifo", "setgid", "setpgid", "setuid", "sleep", "system", "umask", "uname", "alarm", "signal", "sigprocmask", "sigsuspend",
                                        "sigsuspend", "dup", "exch", "pop", "clearerr", "fclose", "fdopen", "feof", "ferror", "fflush", "fgets", "fgetslines", "fopen",
                                        "fprintf", "fputs", "fputslines", "fread", "fread_bytes", "fseek", "ftell", "fwrite", "pclose", "popen", "printf", "create_delimited_string",
                                        "extract_element", "glob_to_regexp", "is_list_element", "is_substr", "make_printable_string", "Sprintf", "sprintf", "sscanf", "strbytelen",
                                        "strbytesub", "strcat", "strcharlen", "strchop", "strchopr", "strcmp", "strcompress", "string_match", "string_match_nth", "strjoin",
                                        "strlen", "strlow", "strnbytecmp", "strncharcmp", "strncmp", "strreplace", "strsub", "strtok", "strtrans", "strtrim", "strtrim_beg",
                                        "strtrim_end", "strup", "str_delete_chars", "str_quote_string", "str_replace", "str_uncomment_string", "substr", "substrbytes",
                                        "get_struct_field", "get_struct_field_names", "is_struct_type", "set_struct_field", "set_struct_fields", "ctime", "gmtime", "localtime",
                                        "mktime", "strftime", "tic", "time", "times", "toc", "atof", "atoi", "atol", "atoll", "char", "define_case", "double",
                                        "int", "integer", "isdigit", "string", "tolower", "toupper", "typecast", "typeof");

% 4: Lib functions

append_keywords($1, 4,
               "abbrev_table_p", "add_color_object", "add_completion",
                                        "add_to_hook", "append_region_to_file", "append_region_to_kill_array", "append_to_hook", "autosave", "autosaveall",
                                        "backward_paragraph", "beep", "bfind", "bfind_char", "blink_match", "blocal_var_exists", "bob", "bobp", "bol",
                                        "bol_bsearch", "bol_bsearch_char", "bol_fsearch", "bol_fsearch_char", "bolp", "bsearch", "bsearch_char",
                                        "bskip_chars", "bskip_non_word_chars", "buffer_keystring", "buffer_list", "buffer_visible", "bufferp",
                                        "bufsubstr", "bury_buffer", "call", "change_default_dir", "check_buffers", "check_region", "clear_message",
                                        "color_number", "copy_file", "copy_keymap", "copy_rect", "copy_region", "copy_region_to_kill_array", "core_dump",
                                        "count_chars", "count_narrows", "create_abbrev_table", "create_blocal_var", "create_line_mark", "create_syntax_table",
                                        "create_user_mark", "define_abbrev", "define_keywords_n", "define_syntax", "define_word", "definekey", "del",
                                        "del_region", "delbuf", "delete_abbrev_table", "delete_file", "dfa_build_highlight_table", "dfa_define_highlight_rule",
                                        "dfa_enable_highlight_cache", "dfa_set_init_callback", "directory", "down", "dump_abbrev_table", "dump_bindings",
                                        "dupmark", "enable_flow_control", "enable_top_status_line", "enlargewin", "eob", "eobp", "eol", "append_keywords",
                                        "eolp", "erase_buffer", "evalbuffer", "exit", "exit_jed", "expand_filename", "expand_symlink", "extract_filename",
                                        "ffind", "ffind_char", "file_changed_on_disk", "file_status", "file_time_compare", "find_file", "find_matching_delimiter",
                                        "flush", "flush_input", "forward_paragraph", "fsearch", "fsearch_char", "get_blocal_var", "get_color", "get_hostname",
                                        "get_jed_library_path", "get_key_binding", "get_last_macro", "get_mini_response", "get_passwd_info", "get_process_input",
                                        "get_realname", "get_scroll_column", "get_termcap_string", "get_username", "get_word_chars", "get_y_or_n", "get_yes_no",
                                        "getbuf_info", "getkey", "getpid", "goto_column", "goto_column_best_try", "goto_line", "goto_user_mark", "gpm_disable_mouse",
                                        "indent_line", "input_pending", "insbuf", "insert", "insert_byte", "insert_char", "insert_file", "insert_file_region", "insert_from_kill_array",
                                        "insert_rect", "is_internal", "is_line_hidden", "is_user_mark_in_narrow", "is_visible_mark", "keymap_p", "kill_process",
                                        "kill_rect", "left", "list_abbrev_tables", "looking_at", "make_keymap", "map_input", "markp", "menu_append_item",
                                        "menu_append_popup", "menu_append_separator", "menu_copy_menu", "menu_create_menu_bar", "menu_delete_item", "menu_delete_items",
                                        "menu_insert_item", "menu_insert_popup", "menu_insert_separator", "menu_select_menu", "menu_set_init_menubar_callback",
                                        "menu_set_menu_bar_prefix", "menu_set_object_available", "menu_set_select_menubar_callback", "menu_set_select_popup_callback",
                                        "menu_use_menu_bar", "mouse_get_event_info", "mouse_map_buttons", "mouse_set_current_window", "mouse_set_default_hook",
                                        "move_user_mark", "msdos_fixup_dirspec", "narrow", "narrow_to_region", "nwindows", "onewindow", "open_process",
                                        "open_rect", "otherwindow", "parse_to_point", "pipe_region", "pop2buf", "pop2buf_whatbuf", "pop_mark", "pop_narrow", "pop_spot",
                                        "prefix_argument", "process_mark", "process_query_at_exit", "push_mark", "push_narrow", "push_spot", "push_spot_bob", "push_spot_bol", "quit_jed",
                                        "random", "re_bsearch", "re_fsearch", "read_file", "read_mini", "read_with_completion", "recenter", "regexp_nth_match",
                                        "remove_from_hook", "rename_file", "replace", "replace_chars", "replace_match", "right", "run_program", "run_shell_cmd",
                                        "search_file", "send_process", "send_process_eof", "set_abort_char", "set_blocal_var", "set_buffer_hook", "set_buffer_umask",
                                        "set_color", "set_color_esc", "set_color_object", "set_column_colors", "set_current_kbd_command", "set_expansion_hook",
                                        "set_file_translation", "set_fortran_comment_chars", "set_highlight_cache_dir", "set_hostname", "set_jed_library_path",
                                        "set_line_hidden", "set_line_readonly", "set_mode", "set_prefix_argument", "set_process", "set_realname", "set_region_hidden",
                                        "set_scroll_column", "set_status_line", "set_syntax_flags", "set_term_vtxxx", "set_top_status_line", "set_username", "setbuf",
                                        "setbuf_info", "setkey", "signal_process", "skip_chars", "skip_hidden_lines_backward", "skip_hidden_lines_forward", "skip_non_word_chars",
                                        "skip_white", "skip_word_chars", "splitwindow", "suspend", "sw2buf", "translate_region", "trim", "tt_send", "undefinekey",
                                        "ungetkey", "unset_buffer_hook", "unsetkey", "up", "update", "update_sans_update_hook", "use_abbrev_table", "use_dfa_syntax",
                                        "use_keymap", "use_syntax_table", "user_mark_buffer", "usleep", "vms_get_help", "vms_send_mail", "w132", "w80", "what_abbrev_table",
                                        "what_char", "what_column", "what_keymap", "what_line", "what_mode", "what_syntax_table", "whatbuf", "which_key", "whitespace",
                                        "widen", "widen_buffer", "widen_region", "window_info", "window_line", "write_buffer", "write_region_to_file",
                                        "x_insert_cutbuffer", "x_insert_selection", "x_server_vendor", "x_set_icon_name", "x_set_keysym",
                                        "x_set_meta_keys", "x_set_window_name", "x_warp_pointer", "xform_region", "run_mode_hooks", "_get_frame_info");

% 5: Jed functions

append_keywords($1, 5,
                                        "C_Preprocess_Indent", "Compile_Default_Compiler", "Dabbrev_Case_Search", "Dabbrev_Default_Buflist",
                                        "Dabbrev_Look_in_Folds", "Dabbrev_delete_tail", "Enable_Mode_Hook_Eval", "Fold_Bob_Eob_Error_Action",
                                        "Help_File", "History_File", "Info_Directory", "Jed_Highlight_Cache_Dir", "Jed_Highlight_Cache_Path",
                                        "Jed_Home_Directory", "Jed_Tmp_Directory", "Minued_Lines", "Mode_Hook_Pointer", "Mouse_Save_Point_Mode",
                                        "Perl_Expert_Flags", "Perl_Indent", "Startup_With_File", "Tab_Always_Inserts_Tab",
                                        "add_keyword", "add_keyword_n", "add_keywords", "add_mode_for_extension", "add_file_for_extension", "append_string_to_file",
                                        "auto_compression_mode", "bol_skip_white", "bol_trim", "bskip_white", "bufed", "buffer_filename",
                                        "buffer_format_in_columns", "buffer_modified", "bufsubstr_delete", "c_mode", "c_set_style", "call_function",
                                        "close_filter_process", "cua_delete_word", "cua_escape_cmd", "cua_escape_cmd", "cua_indent_region_or_line",
                                        "cua_one_press_escape", "cua_save_buffer", "custom_color", "custom_variable", "dabbrev", "del_eol", "del_through_eol",
                                        "deln", "dircat", "dired", "disable_dfa_syntax_for_mode", "docbook_mode", "down_1", "edt_advance", "edt_append",
                                        "edt_backup", "edt_find", "edt_findnxt", "edt_ldel", "edt_page", "edt_replace", "edt_sect", "edt_uldel", "edt_uwdel",
                                        "edt_wdel", "eol_trim", "expand_jedlib_file", "expand_keystring", "f90_mode", "file_type", "find_jedlib_file",
                                        "fortran_mode", "get_comment_info", "go_down", "go_down_1", "go_left", "go_left_1", "go_right", "go_right_1", "go_up",
                                        "go_up_1", "goto_spot", "help", "help_for_help_string", "history_load", "history_local_save", "history_save", "html_bskip_tag",
                                        "html_mark_next_tag", "html_mark_prev_tag", "html_mode", "html_skip_tag", "insert_single_space", "is_overwrite_mode",
                                        "is_readonly", "jed_startup_hook", "latex_mode", "line_as_string", "local_setkey", "local_unsetkey", "looking_at_char",
                                        "lua_mode", "make_tmp_buffer_name", "make_tmp_file", "man_clean_manpage", "mark_buffer", "matlab_mode", "minued_mode",
                                        "mode_hook", "modeline_hook", "most_mode", "newline", "next_buffer", "no_mode", "nroff_mode", "occur", "open_filter_process",
                                        "parse_filename", "perl_mode", "perltidy", "php_mode", "pop_mark_0", "pop_mark_1", "provide", "push_mark_eob",
                                        "push_mark_eol", "push_visible_mark", "python_mode", "read_file_from_mini", "set_visual_wrap",
                                        "read_string_with_completion", "redo", "remove_keywords", "rename_buffer", "repeat_search", "require", "runhooks",
                                        "save_buffer", "save_buffer_as", "save_buffers", "search_path_for_file", "set_buffer_modified_flag", "set_buffer_no_autosave",
                                        "set_buffer_no_backup", "set_buffer_undo", "set_comment_info", "set_mark_cmd", "set_overwrite", "set_readonly", "shell_builtin",
                                        "smart_set_mark_cmd", "str_replace_all", "strncat", "tcl_mode", "tex_mode", "text_indent_relative", "text_mode", "toggle_case_search",
                                        "toggle_crmode", "toggle_line_number_mode", "toggle_overwrite", "toggle_readonly", "toggle_undo", "unix_man", "untab", "up_1",
                                        "vhdl_mode", "vinsert", "whatpos", "write_string_to_file");

% Format paragraph hook
private define skip_comment_whitespace ()
{
   skip_white ();
   skip_chars ("%");
   if (looking_at ("//!") || looking_at ("///"))   %  doxygen
     go_right (3);
   skip_white ();
}

private define is_empty_comment_line ()
{
   bol ();
   skip_comment_whitespace ();
   return eolp ();
}

private define mark_comment_whitespace ()
{
   bol ();
   push_mark ();
   skip_comment_whitespace ();
}

private define is_para_sep ()
{
   bol_skip_white ();
   ifnot (looking_at ("%"))
     return 1;

   if (is_empty_comment_line ())
     return 1;

   % Now look for special documentation marks.  The embedded tm docs begin
   % with %!%+ and end with %!%-
   bol ();
   if (looking_at ("%!%"))
     return 1;

   % Something like %\function{foo}
   if (looking_at ("%\\"))
     return 1;

   return 0;
}

private define wrapok_hook ()
{
   push_spot ();
   EXIT_BLOCK
     {
        pop_spot ();
     }

   bol_skip_white ();
   return (what_char () == '%');
}

private define wrap_hook ()
{
   push_spot ();
   go_up_1 ();
   mark_comment_whitespace ();
   variable prefix = bufsubstr ();
   go_down_1 ();
   insert (prefix);
   pop_spot ();
}

private define format_paragraph_hook ();
private define format_paragraph_hook ()
{
   push_spot ();
   EXIT_BLOCK
     {
        pop_spot ();
     }
   eol ();

   if (parse_to_point () != -2)
     return;
   bol_skip_white ();
   ifnot (looking_at ("%"))
     return;

   bol();
   push_mark ();
   skip_white ();
   skip_chars ("%");
   variable prefix = bufsubstr ();

   skip_white ();
   if (eolp ())
     return;
   variable indent_column = what_column ();

   % Find start
   while (up_1 ())
     {
        if (is_para_sep ())
          {
             go_down_1 ();
             break;
          }
     }
   bol ();
   push_mark ();

   % Find comment end
   while (down_1 ())
     {
        if (is_para_sep ())
          {
             up_1 ();
             break;
          }
     }
   narrow ();

   bob ();
   do
     {
        mark_comment_whitespace ();
        del_region ();
        whitespace (indent_column-1);
     }
   while (down_1 ());

   bob ();

   unset_buffer_hook ("format_paragraph_hook");
   call ("format_paragraph");
   set_buffer_hook ("format_paragraph_hook", &format_paragraph_hook);

   bob ();
   variable prefix_len = strlen (prefix);
   do
     {
        insert (prefix);
        deln (prefix_len);
     }
   while (down_1 ());
   widen ();
   return;
}

% This function attempts to implement some form of wrapping mode in the
% presence of comments.  However, the embedded documentation comment style
% (using text-macro) means that this will need to be modified to be more
% sophisticated.  Basically the modifications would require analysing the
% text-macro context.  For example, wrapping is not appropriate in verbatim
% sections.
#ifntrue
define slmode_insert_space ()
{
   variable cstr;

   if (is_overwrite_mode ())
     {
        call ("self_insert_cmd");
        return;
     }

   EXIT_BLOCK
     {
        insert_single_space ();
     }

   % The following code attempts a wrapping mode in the presence of comments
   ifnot (cmode_is_slang_mode ()) return;
   if (not (eolp ()) or (what_column () <= WRAP)) return;

   % we are at the end of line.
   cstr = "%!% ";
   bol ();
   ifnot (looking_at (cstr), eol ()) return;
   ifnot (bfind_char (' ')) return;
   trim ();
   newline ();
   insert (cstr);
   eol ();
}
#endif

define slang_mode ()
{
   set_mode("SLang", 2 | 8);
   c_mode_common ();
   use_syntax_table ("SLANG");
   %local_setkey ("slmode_insert_space", " ");
   mode_set_mode_info ("SLang", "fold_info", "%{{{\r%}}}\r\r");
   mode_set_mode_info ("SLang", "dabbrev_case_search", 1);
   set_buffer_hook ("format_paragraph_hook", &format_paragraph_hook);
   set_buffer_hook ("wrap_hook", &wrap_hook);
   set_buffer_hook ("wrapok_hook", &wrapok_hook);
   unset_buffer_hook ("par_sep");
   set_blocal_var (1, "cmode_has_no_label_statement");
   set_comment_info("SLang", "%", NULL, NULL, 0);
   run_mode_hooks("slang_mode_hook");
}

provide("slang")
