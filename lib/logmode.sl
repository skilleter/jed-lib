% ******************************************************************************
% Logfile mode
% ******************************************************************************

$1 = "log";

create_syntax_table ($1);

define_syntax("([{",       ")]}", '(', $1);
%define_syntax("`",         "'",   '%', $1);
%define_syntax("'",         "'",   '%', $1);
%define_syntax('\'',               '"', $1);
define_syntax('"',                '"', $1);
define_syntax("~-0-9a-zA-Z_\." ,   'w', $1);        % words
define_syntax(",;?:",             ',', $1);
define_syntax("\\-+\\'\\^\\\"\\%+/&*=<>|!^",  '+', $1);
define_syntax("-+0-9a-fA-F.xXUL", '0', $1);
define_syntax ("#", "", '%', $1);               % comments

set_syntax_flags ($1, 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache("logmode.dfa", name);

   dfa_define_highlight_rule("^[ \t]*#",                                                                "PQpreprocess", name);
   dfa_define_highlight_rule("//.*",                                                                    "comment", name);
   dfa_define_highlight_rule("/\\*.*\\*/",                                                              "Qcomment", name);
   dfa_define_highlight_rule("^([^/]|/[^\\*])*\\*/",                                                    "Qcomment", name);
   dfa_define_highlight_rule("/\\*.*",                                                                  "comment", name);
   dfa_define_highlight_rule("^[ \t]*\\*+([ \t].*)?$",                                                  "comment", name);
   dfa_define_highlight_rule("[A-Za-z_\\$][A-Za-z_0-9\\$]*",                                    "Knormal", name);
   dfa_define_highlight_rule("[0-9A-Fa-F]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?",  "number", name);
   dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*[LU]*",                                                  "number", name);
   dfa_define_highlight_rule("[0-9]+[LU]*",                                                             "number", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"",                                                  "string", name);
   dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$",                                              "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*'",                                                     "string", name);
   dfa_define_highlight_rule("`([^'\\\\]|\\\\.)*'",                                             "string", name);
   dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*\\\\?$",                                                "string", name);
   dfa_define_highlight_rule("[ \t]+",                                                                  "normal", name);
   dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]",                                               "delimiter", name);
   dfa_define_highlight_rule("[%\\+/&\\*=<>\\|!~\\^]",                                                  "operator", name);

   dfa_build_highlight_table(name);
}

dfa_set_init_callback (&setup_dfa_callback, "log");

append_keywords("log", 0,
                "Notice", "Running");

append_keywords("log", 2,
                "warning", "Warning" );

append_keywords("log", 3,
                "error", "Error");

define log_mode ()
{
   set_mode ("log", 2);
   use_syntax_table ("log");
   set_comment_info("log", "%", NULL, NULL, 0);
   run_mode_hooks("log_mode_hook");
}
