% ******************************************************************************
% ft_tab()
% If using tabs in the current buffer then insert a tab character
% Otherwise, insert spaces up to the next tab stop (fake-tab function)
% ******************************************************************************

define ft_tab()
{
   if(USE_TABS)
   {
      insert_char('\t');
   }
   else
   {
      variable col = what_column ();
      variable next_tabstop;
      variable tab_size = TAB == 0 ? 1 : TAB;

      next_tabstop = ((col-1) / tab_size ) * tab_size + tab_size + 1;

      while(what_column () < next_tabstop)
         insert_char (' ');
   }
}

% ******************************************************************************
% ft_backspace() -
% If using tab character, just backspace
% Otherwise, if we are on a tab stop column and the preceding characters back to
% the last tab stop are spaces then delete all of them
% **##** Actually this deletes up to the tab stop OR the first non-ws character
%        it should only delete >1 character if all characters back to the last
%        tab stop are spaces.
%        Does odd thing if deleting whitespace at the start of a buffer.
% ******************************************************************************

define ft_backspace()
{
   if(USE_TABS || TAB == 0)
   {
      backspace();
   }
   else
   {
      variable col, prev_tabstop, spc;

      col = what_column();
      prev_tabstop = ((col-1) / TAB - 1) * TAB + 1;

      if(col > 1)
      {
         push_spot();
         go_left_1();
         spc = looking_at_char(' ');
         pop_spot();
      }
      else
      {
         spc = 0;
      }

      if(spc)
      {
         while(col > 1 and col > prev_tabstop and spc)
         {
            backspace();
            push_spot();
            go_left_1();
            spc = looking_at_char(' ');
            pop_spot();
            col--;
         }
      }
      else
      {
         backspace();
      }
   }
}

% ******************************************************************************
% ft_del - faketab function - delete with special cases:
%
%  1) at the end of a line it merges lines, removing excess whitespace at the join
%  2) it treats sequences of spaces as tabs
%
% **##** As above, this should only delete >1 char if it is spaces all the way
%        to the next tab stop.
% ******************************************************************************

define ft_del()
{
   push_spot();

   skip_white();

   if(eolp())
   {
      % Merge lines with whitespace removal

      pop_spot();
      push_mark();

      () = down(1);
      bol();
      skip_white();
      del_region();
   }
   else
   {
      pop_spot();

      if(looking_at_char('\t'))
      {
         del();
      }
      else if(looking_at_char(' '))
      {
         variable i=TAB;

         del();
         i--;

         while((looking_at_char(' ') or looking_at_char('\t')) and i > 0 and eolp()==0)
         {
            del();
            i--;
         }
      }
      else
      {
         del();
      }
   }
}

% ******************************************************************************
% ft_left - faketab function - Move left, pretending that consecutive spaces on
%           tab stop boundaries are really tab characters.
% ******************************************************************************

define ft_left()
{
   if(TAB != 0 and not USE_TABS)
   {
      variable col, tabstop;

      col = what_column();
      tabstop = ((col-1) / TAB)*TAB+1;

      if(col > 0 and col == tabstop)
      {
         variable i = TAB-1;

         if(left(1) == 1)
         {
            while(i > 0 and looking_at_char(' '))
            {
               i--;
               () = left(1);
            }
         }

         return;
      }
   }

   () = left(1);
}

% ******************************************************************************
% ft_right - faketab function - Move left, pretending that consecutive spaces on
%            tab stop boundaries are really tab characters.
% ******************************************************************************

define ft_right()
{
   if(TAB != 0 and not USE_TABS and looking_at_char(' '))
   {
      variable col, tabstop;

      tabstop = ((what_column()-1) / TAB + 1)*TAB + 1;

      () = right(1);

      while(looking_at_char(' ') and what_column() < tabstop and eolp()==0)
      {
         () = right(1);
      }
   }
   else
   {
      () = right(1);
   }
}
