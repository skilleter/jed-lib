%==========================================================================
% occur.sl
%==========================================================================

require("syntax");

%******************************************************************************
% Keymap for occur buffer
%******************************************************************************

$1 = "Occur";
!if (keymap_p ($1))
{
   make_keymap ($1);
}

definekey ("occur_goto_buffer", "^m", $1);
definekey ("occur_up",        "\xE0H", $1);
definekey ("occur_down",      "\xE0P", $1);

%******************************************************************************
% Globals
%******************************************************************************

variable Occur_Buffer = Null_String;
variable mark;

%******************************************************************************
% Move the cursor in the other window to the selected line in the occur window
% and highlight it.
%******************************************************************************

define occur_goto_line ()
{
   variable line, buffer, occur_color;

   !if (bufferp (Occur_Buffer))
     return;

   bol ();
   skip_white();
   push_mark ();
   !if (ffind (":"))
   {
      pop_mark_0 ();
      return;
   }

   buffer = bufsubstr();

   () = right(1);
   skip_white();
   push_mark();
   !if(ffind(":"))
   {
      pop_mark_0();
      return;
   }

   line = integer (bufsubstr ());

   otherwindow();
   sw2buf(buffer);
   goto_line (line);
   emacs_recenter();
        set_color_object (102, "black", "yellow");
   mark = create_line_mark(102);

   otherwindow();
   sw2buf("*occur*");
}

%******************************************************************************
% Move to the previous line in the occur window
%******************************************************************************

define occur_up()
{
   () = up(1);
   occur_goto_line();
}

%******************************************************************************
% Move to the next line in the occur window
%******************************************************************************

define occur_down()
{
   () = down(1);
   occur_goto_line();
}

%******************************************************************************
% Move to the current line in the occur buffer in the other window, clear the
% mark and make it a single window
%******************************************************************************

define occur_goto_buffer()
{
   occur_goto_line();
   mark = 0;
   otherwindow();
   onewindow();
}

%!%+
%\function{occur}
%\synopsis{occur}
%\usage{Void occur ();}
%\description
% This function may be used to search for all occurances of a string in the
% current buffer.  It creates a separate buffer called \var{*occur*} and
% associates a keymap called \var{Occur} with the new buffer.  In this
% buffer, the \var{g} key may be used to go to the line described by the
% match.
%!%-

static variable last_occur_str = "";

define occur()
{
   variable str, tmp, b, buf, n, count, r;

   str = search_cursor_word(LAST_SEARCH);
   str = read_mini("Find all occurances (Regexp):", str, Null_String);
   tmp = "*occur*";
   Occur_Buffer = whatbuf();
   pop2buf(tmp);
   set_readonly(0);
   erase_buffer();
   setbuf(tmp);

        occur_mode();

   if(last_occur_str != "")
        {
                remove_keywords("OCCUR", last_occur_str, strlen(last_occur_str), 0);
        }

        add_keywords("OCCUR", str, strlen(str), 0);

        last_occur_str = str;

   push_spot();

   count = 0;
   b = buffer_list ();             % list of buffers on to stack

   push_spot();
   loop (b)
   {
      buf = ();                   % buffer name
      b--;                        % decrement the count

      if( (buf[0] == ' ')         % skip unnamed, or internal buffers
       or (buf[0] == '*') )
         continue;

      pop_spot();
      setbuf(buf);
      push_spot();
      bob();

      while (re_fsearch(str))
      {
         line_as_string ();  % stack-- at eol too
         n = what_line ();

         setbuf(tmp);
         vinsert ("%16s:%4d:", buf, n);
         count++;
         insert(());
         newline();
         setbuf(buf);
         eol();             %% so we do not find another occurance on same line
      }
   }

   pop_spot();
   setbuf(Occur_Buffer);
   pop_spot();

   setbuf(tmp);
   bob();
   set_buffer_modified_flag(0);

   use_keymap ("Occur");
   run_mode_hooks ("occur_mode_hook");
   set_readonly(1);

   onewindow();
   splitwindow();
   sw2buf(Occur_Buffer);

   otherwindow();
   r = window_info('r');
   otherwindow();
   loop(r-5)
   {
      enlargewin();
   }

   otherwindow();
   setbuf(tmp);

   bob();
   occur_goto_line();
   flush(Sprintf("%d occurances found, press return to show context", count, 1));
}

