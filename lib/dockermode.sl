% Editing mode for dockerfiles
$1 = "docker";

% Syntax table stolen from shmode

create_syntax_table ($1);

define_syntax ("#", "", '%', $1);
define_syntax ("([{", ")]}", '(', $1);

define_syntax ('\'', '"', $1);
define_syntax ('"', '"', $1);

define_syntax ('\\', '\\', $1);
define_syntax ("-0-9a-zA-Z_", 'w', $1);        % words
define_syntax ("-+0-9", '0', $1);   % Numbers
define_syntax (",;:", ',', $1);
define_syntax ("%-+/&*=<>|!~^$", '+', $1);

#ifdef HAS_DFA_SYNTAX

%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
   dfa_enable_highlight_cache ("dockermode.dfa", name);

   dfa_define_highlight_rule ("\\\\.", "normal", name);
   dfa_define_highlight_rule ("#.*$", "comment", name);
   dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*\"", "string", name);
   dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*$", "string", name);
   dfa_define_highlight_rule ("'[^']*'", "string", name);
   dfa_define_highlight_rule ("'[^']*$", "string", name);
   dfa_define_highlight_rule ("[\\|@&;\\(\\)<>]", "Qdelimiter", name);
   dfa_define_highlight_rule ("[\\[\\]\\*\\?]", "Qoperator", name);
   dfa_define_highlight_rule ("[^ \t\"'\\\\\\|&;\\(\\)<>\\[\\]\\*\\?]+", "Knormal", name);
   dfa_define_highlight_rule (".", "normal", name);
   dfa_build_highlight_table (name);
}

dfa_set_init_callback (&setup_dfa_callback, "docker");

%%% DFA_CACHE_END %%%
#endif

append_keywords($1, 0,
                "RUN", "ADD", "COPY", "ENV", "EXPOSE", "FROM", "LABEL",
                "STOPSIGNAL", "USER", "VOLUME", "WORKDIR", "ONBUILD",
                "MAINTAINER", "ENTRYPOINT", "ARG", "LABEL", "CMD");

append_keywords($1, 1,
                "AS");

% Bash built-ins

append_keywords($1, 2,
                "pwd", "set", "cd", "pushd", "popd", "export", "printf", "echo", "expr",
                "lockfile", "rm", "cat", "mv", "cp", "find", "touch", "complete", "read",
                "umask", "printenv", "builtin", "unalias", "su", "mkdir", "dirname",
                "basename", "rsync", "trap", "sudo", "disown", "sleep", "readarray",
                "cut", "exec", "compgen", "stty", "command", "ls", "getopts", "ifconfig",
                "nice", "time", "rmmod", "insmod", "mknod", "chmod", "chown", "chgrp", "ln", "man",
                "which", "true", "false", "eval", "exit", "addgroup", "adduser");

% Popular extern commands

append_keywords($1, 3,
                "tput", "dcop", "rsh", "rcp", "telnet", "rlogin", "ftp", "ping", "disk",
                "mail", "finger", "help", "zip", "unzip", "compress", "uncompress", "date",
                "gzip", "gunzip", "bzip2", "bunzip2", "perl", "python", "python3", "pip", "pip3",
                "less", "getopt", "cleartool","make", "ct", "uniq", "tail", "sort", "hexdump",
                "clear", "xclearcase", "mount", "swapon", "syslogd", "dhcpcd", "portmap", "utelnetd",
                "umount", "losetup", "killall", "mdev", "repo", "ssh-agent", "ssh-add",
                "readlink", "m4", "konqueror", "grep", "egrep", "sed", "xargs", "tar", "nroff",
                "endview", "setview", "lsvob", "ci", "co", "lshistory", "findmerge", "unco",
                "lslock", "mkview", "setcs", "rmview", "lsprivate", "mkelem", "rmname",
                "protect", "stat", "unregister", "rmtag", "awk", "readelf", "tee", "dd", "ddd",
                "md5sum", "rdesktop", "objdump", "xxd", "tr", "git", "uname", "timeout",
                "wget", "java", "rgrep", "fgrep", "docker", "ps", "strace", "diff", "aws",
                "envsubst", "useradd", "groupadd", "yum", "apt", "rpm", "curl", "pip", "apt-get",
                "gem");

% Bash variables

append_keywords($1, 4,
                "BASH", "BASH_ARGC", "BASH_ARGV", "BASH_COMMAND", "BASH_EXECUTION_STRING", "BASH_REMATCH",
                "BASH_LINENO", "BASH_SOURCE", "BASH_SUBSHELL", "BASH_VERSINFO", "BASH_VERSION",
                "COMP_CWORD", "COMP_LINE", "COMP_POINT", "COMP_WORDBREAKS", "COMP_WORDS", "DIRSTACK",
                "EUID", "FUNCNAME", "GROUPS",
                "HISTCMD", "HOSTNAME", "HOSTYPE", "LINENO", "MACHTYPE", "OLDPWD", "OPTARG", "OPTIND",
                "OSTYPE", "PIPESTATUS", "PPID", "PWD", "RANDOM", "REPLY", "SECONDS", "SHELLOPTS",
                "SHLVL", "UID", "BASH_ENV", "CDPATH", "COLUMNS", "COMPREPLY", "EMACS", "FCEDIT",
                "FIGNORE", "GLOBIGNORE", "HISTCONTROL", "HISTFILE", "HISTFILESIZE", "HISTIGNORE",
                "HISTSIZE", "HISTTIMEFORMAT", "HOME", "HOSTFILE", "IFS", "IGNOREEOF", "INPUTRC",
                "LANG", "LC_ALL", "LC_COLLATE", "LC_CTYPE", "LC_MESSAGES", "LC_NUMERIC", "LINES",
                "MAIL", "MAILCHECK", "MAILPATH", "OPTERR", "PATH", "POSXILY_CORRECT", "PROMPT_COMMAND",
                "PS1", "PS2", "PS3", "PS4", "SHELL", "TIMEFORMAT", "TMOUT", "auto_resume", "histchars",
                "LD_LIBRARY_PATH", "KONSOLE_DCOP_SESSION", "CLEARCASE_ROOT");

define docker_mode ()
{
   set_mode("docker", 0);
   use_syntax_table ("docker");
   set_comment_info("docker", "#", NULL, NULL, 0);
   use_dfa_syntax(0);
}

