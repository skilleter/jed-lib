% *************************************************************
% TI linker command mode
% *************************************************************

$1 = "cmd";

create_syntax_table ($1);
define_syntax ("/*",                                    "*/",   '%',    $1);
define_syntax ("//",                                    "",             '%',    $1);
define_syntax ("([{",                                   ")]}",  '(',    $1);
define_syntax ('"',                                                                     '"',    $1);
define_syntax ('\'',                                                            '\'', $1);
define_syntax ('\\',                                                            '\\', $1);
define_syntax ("0-9a-zA-Z_",                                            'w',    $1);        % words
define_syntax ("-+0-9a-fA-F.xXULhHQq",                  '0',    $1);   % Numbers
define_syntax (",;.?:",                                                         ',',    $1);
define_syntax ('#',                                                                     '#',  $1);
define_syntax ("%-+/&*=<>|!~^",                                         '+',    $1);

set_syntax_flags ($1, 0x4|0x40);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
        dfa_enable_highlight_cache("cmdmode.dfa", name);

        dfa_define_highlight_rule("^[ \t]*#", "PQpreprocess", name);
        dfa_define_highlight_rule("//.*", "comment", name);
        dfa_define_highlight_rule("/\\*.*\\*/", "Qcomment", name);
        dfa_define_highlight_rule("^([^/]|/[^\\*])*\\*/", "Qcomment", name);
        dfa_define_highlight_rule("/\\*.*", "comment", name);
        dfa_define_highlight_rule("^[ \t]*\\*+([ \t].*)?$", "comment", name);
        dfa_define_highlight_rule("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
        dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?", "number", name);
        dfa_define_highlight_rule("0[xX][0-9A-Fa-f]*[LlUu]*", "number", name);
        dfa_define_highlight_rule("[0-9]+[LlUu]*", "number", name);
        dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\"", "string", name);
        dfa_define_highlight_rule("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*'", "string", name);
        dfa_define_highlight_rule("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule("[ \t]+", "normal", name);
        dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
        dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);

        dfa_build_highlight_table(name);
}

dfa_set_init_callback (&setup_dfa_callback, "cmd");

%%% DFA_CACHE_END %%%
#endif

append_keywords("cmd", 0,
                                         "align",       "GROUP",                "origin",
                                         "ALIGN",       "l",                    "ORIGIN",
                                         "attr",                "len",          "page",
                                         "ATTR",        "length",       "PAGE",
                                         "block",       "LENGTH",       "range",
                                         "BLOCK",       "load",         "run",
                                         "COPY",                "LOAD",         "RUN",
                                         "DSECT",       "MEMORY",       "SECTIONS",
                                         "f",                   "NOLOAD",       "spare",
                                         "fill",                "o",                    "type",
                                         "FILL",                "org",          "TYPE",
                                         "group",       "UNION");

append_keywords("cmd", 1,
                                         "text",                "cinit",                "pinit",
                                         "switch",      "bss",          "const",
                                         "sysmem",      "stack");

define cmd_mode ()
{
   c_mode ();
   set_mode ("cmd", 2);
   use_syntax_table ("cmd");
   set_comment_info("cmd", "//", "/*", "*/", 0);
   run_mode_hooks("cmd_mode_hook");
}
