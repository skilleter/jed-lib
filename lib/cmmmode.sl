% ******************************************************************************
% Jed mode for editing Lauterbach configuration files
% ******************************************************************************

% -------------------------------------------------------------------------
% Word characters
% -------------------------------------------------------------------------

define_word ("0-9A-Za-z_");

!if (keymap_p ("CMM"))
        make_keymap ("CMM");

% Now create and initialize the syntax tables.

create_syntax_table ("CMM");

define_syntax (";",                                        "",          '%',    "CMM");
define_syntax ("([{",                                   ")]}",  '(',    "CMM");
define_syntax ('"',                                                                     '"',    "CMM");
define_syntax ('\'',                                                            '\'', "CMM");
define_syntax ('\\',                                                            '\\',   "CMM");
define_syntax ("0-9a-zA-Z_",                                            'w',    "CMM");   % words
define_syntax ("-+0-9a-fA-F.xXUL",                              '0',    "CMM");   % Numbers
define_syntax (",;.?:",                                                         ',',    "CMM");
define_syntax ("$%-+/&*=<>|!~^",                                '+',    "CMM");

set_syntax_flags ("CMM", 1|0x4);

append_keywords("CMM", 0, "global", "local");
append_keywords("CMM", 1, "if");
append_keywords("CMM", 3, "print", "cd", "pos", "text");
append_keywords("CMM", 4, "string", "find");
append_keywords("CMM", 7, "os", "env", "menu", "version", "dialog", "file", "path");

define cmm_mode ()
{
   set_mode("CMM", 2);
   use_keymap("CMM");
   use_syntax_table ("CMM");
   set_comment_info("CMM", ";", NULL, NULL, 0);

   run_mode_hooks("cmm_mode_hook");
}

