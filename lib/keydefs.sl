% -*- mode: slang; mode: fold; -*-
% This file defines symbolic constants for many function and arrow keys.
% It may need some modifications on VMS as well as other systems.
% originally part of ide.sl by Guido Gonzato
% modified by GM <g.milde@physik.tu-dresden.de>
% modified by JED
% Major modifications to make it more reader-friendly added the
% MetaKey, AltKey, FnKey, ExtKey definitions

variable MetaKey = "\e";
variable AltKey  = "^_";
variable FnKey   = "^^";
variable NumKey  = "\eO";

#ifdef MSWINDOWS
variable ExtKey  = "\d224";
#else
variable ExtKey  = "^[[";
#endif

#ifdef IBMPC_SYSTEM %{{{
variable Key_F1      = FnKey + ";";
variable Key_F2      = FnKey + "<";
variable Key_F3      = FnKey + "=";
variable Key_F4      = FnKey + ">";
variable Key_F5      = FnKey + "?";
variable Key_F6      = FnKey + "@";
variable Key_F7      = FnKey + "A";
variable Key_F8      = FnKey + "B";
variable Key_F9      = FnKey + "C";
variable Key_F10  = FnKey + "D";
variable Key_F11  = FnKey + "\d133";
variable Key_F12  = FnKey + "\d134";

variable Key_Up      = ExtKey+"H";
variable Key_Down = ExtKey+"P";
variable Key_Right   = ExtKey+"M";
variable Key_Left = ExtKey+"K";

variable Key_Ins  = ExtKey+"R";
variable Key_Del  = ExtKey+"S";
variable Key_Home = ExtKey+"G";
variable Key_End  = ExtKey+"O";
variable Key_PgUp = ExtKey+"I";
variable Key_PgDn = ExtKey+"Q";

variable Key_BS         = _Backspace_Key;

% ALT keys

variable Key_Alt_F1  = FnKey + "h";
variable Key_Alt_F2  = FnKey + "i";
variable Key_Alt_F3  = FnKey + "j";
variable Key_Alt_F4  = FnKey + "k";
variable Key_Alt_F5  = FnKey + "l";
variable Key_Alt_F6  = FnKey + "m";
variable Key_Alt_F7  = FnKey + "n";
variable Key_Alt_F8  = FnKey + "o";
variable Key_Alt_F9  = FnKey + "p";
variable Key_Alt_F10 = FnKey + "q";
variable Key_Alt_F11 = FnKey + "\d139";
variable Key_Alt_F12 = FnKey + "\d140";

variable Key_Alt_Up     = ExtKey + "\d152";
variable Key_Alt_Down   = ExtKey + "\d160";
variable Key_Alt_Right  = ExtKey + "\d157";
variable Key_Alt_Left   = ExtKey + "\d155";

variable Key_Alt_Ins    = ExtKey + "\d162";
variable Key_Alt_Del    = ExtKey + "\d163";
variable Key_Alt_Home   = ExtKey + "\d151";
variable Key_Alt_End    = ExtKey + "\d159";
variable Key_Alt_PgUp   = ExtKey + "\d153";
variable Key_Alt_PgDn   = ExtKey + "\d161";

variable Key_Alt_BS  = AltKey + "^H";

% SHIFT keys

variable Key_Shift_F1   = FnKey + "T";
variable Key_Shift_F2   = FnKey + "U";
variable Key_Shift_F3   = FnKey + "V";
variable Key_Shift_F4   = FnKey + "W";
variable Key_Shift_F5   = FnKey + "X";
variable Key_Shift_F6   = FnKey + "Y";
variable Key_Shift_F7   = FnKey + "Z";
variable Key_Shift_F8   = FnKey + "[";
variable Key_Shift_F9   = FnKey + "\\";
variable Key_Shift_F10  = FnKey + "]";
variable Key_Shift_F11  = FnKey + "\d135";
variable Key_Shift_F12  = FnKey + "\d136";

variable Key_Shift_Up   = ExtKey+"1";
variable Key_Shift_Down = ExtKey+"6";
variable Key_Shift_Right= ExtKey+"4";
variable Key_Shift_Left = ExtKey+"3";

variable Key_Shift_Ins  = ExtKey+"8";
variable Key_Shift_Del  = ExtKey+"9";
variable Key_Shift_Home = ExtKey+"0";
variable Key_Shift_End  = ExtKey+"5";
variable Key_Shift_PgUp = ExtKey+"2";
variable Key_Shift_PgDn = ExtKey+"7";
variable Key_Shift_Tab  = "^@^O";
variable Key_Shift_BS   = "^_?";

% Ctrl keys

variable Key_Ctrl_F1 = FnKey + "^";
variable Key_Ctrl_F2 = FnKey + "_";
variable Key_Ctrl_F3 = FnKey + "`";
variable Key_Ctrl_F4 = FnKey + "a";
variable Key_Ctrl_F5 = FnKey + "b";
variable Key_Ctrl_F6 = FnKey + "c";
variable Key_Ctrl_F7 = FnKey + "d";
variable Key_Ctrl_F8 = FnKey + "e";
variable Key_Ctrl_F9 = FnKey + "f";
variable Key_Ctrl_F10   = FnKey + "g";
variable Key_Ctrl_F11   = FnKey + "\d137";
variable Key_Ctrl_F12   = FnKey + "\d138";

variable Key_Ctrl_Up = ExtKey+"\d141";
variable Key_Ctrl_Down  = ExtKey+"\d145";
variable Key_Ctrl_Right = ExtKey+"t";
variable Key_Ctrl_Left  = ExtKey+"s";

variable Key_Ctrl_Ins   = ExtKey+"\d146";
variable Key_Ctrl_Del   = ExtKey+"\d147";
variable Key_Ctrl_Home  = ExtKey+"w";
variable Key_Ctrl_End   = ExtKey+"u";
variable Key_Ctrl_PgUp  = ExtKey+"\d132";
variable Key_Ctrl_PgDn  = ExtKey+"v";

variable Key_Ctrl_BS = "^_@"; %  ??

%}}}
#else           %  UNIX, VMS %{{{

private variable Is_Xjed = is_defined ("x_server_vendor");

private define setkey_via_terminfo (tc, def)
{
   if (Is_Xjed)
     return def;

#ifexists get_termcap_string
   variable s = get_termcap_string (tc);
   if (s != "")
     return s;
#endif
   return def;
}

variable Key_F1         = setkey_via_terminfo ("k1",    "\e[11~");
variable Key_F2         = setkey_via_terminfo ("k2",    "\e[12~");
variable Key_F3         = setkey_via_terminfo ("k3",    "\e[13~");
variable Key_F4         = setkey_via_terminfo ("k4",    "\e[14~");
variable Key_F5         = setkey_via_terminfo ("k5",    "\e[15~");
variable Key_F6         = setkey_via_terminfo ("k6",    "\e[17~");
variable Key_F7         = setkey_via_terminfo ("k7",    "\e[18~");
variable Key_F8         = setkey_via_terminfo ("k8",    "\e[19~");
variable Key_F9         = setkey_via_terminfo ("k9",    "\e[20~");
variable Key_F10        = setkey_via_terminfo ("k;",    "\e[21~");
variable Key_F11        = setkey_via_terminfo ("F1",    "\e[23~");
variable Key_F12        = setkey_via_terminfo ("F2",    "\e[24~");

variable Key_Up         = setkey_via_terminfo ("ku", "\e[A");
variable Key_Down       = setkey_via_terminfo ("kd", "\e[B");
variable Key_Right      = setkey_via_terminfo ("kr", "\e[C");
variable Key_Left       = setkey_via_terminfo ("kl", "\e[D");

variable Key_Ins        = setkey_via_terminfo ("kI", "\e[2~");
variable Key_Del        = setkey_via_terminfo ("kD", "\e[3~");
variable Key_Home       = setkey_via_terminfo ("kh", "\e[1~");
variable Key_End        = setkey_via_terminfo ("@7", "\e[4~");
variable Key_PgUp       = setkey_via_terminfo ("kP", "\e[5~");
variable Key_PgDn       = setkey_via_terminfo ("kN", "\e[6~");

variable Key_BS         = _Backspace_Key;

% Assume that ALT keys are prefixed with ALT_CHAR

variable Key_Alt_F1     = AltKey+Key_F1;
variable Key_Alt_F2     = AltKey+Key_F2;
variable Key_Alt_F3     = AltKey+Key_F3;
variable Key_Alt_F4     = AltKey+Key_F4;
variable Key_Alt_F5     = AltKey+Key_F5;
variable Key_Alt_F6     = AltKey+Key_F6;
variable Key_Alt_F7     = AltKey+Key_F7;
variable Key_Alt_F8     = AltKey+Key_F8;
variable Key_Alt_F9     = AltKey+Key_F9;
variable Key_Alt_F10    = AltKey+Key_F10;
variable Key_Alt_F11    = AltKey+Key_F11;
variable Key_Alt_F12    = AltKey+Key_F12;

variable Key_Alt_Up     = AltKey+Key_Up;
variable Key_Alt_Down   = AltKey+Key_Down;
variable Key_Alt_Right  = AltKey+Key_Right;
variable Key_Alt_Left   = AltKey+Key_Left;

variable Key_Alt_Ins    = AltKey+Key_Ins;
variable Key_Alt_Del    = AltKey+Key_Del;
variable Key_Alt_Home   = AltKey+Key_Home;
variable Key_Alt_End    = AltKey+Key_End;
variable Key_Alt_PgUp   = AltKey+Key_PgUp;
variable Key_Alt_PgDn   = AltKey+Key_PgDn;

variable Key_Alt_BS     = AltKey+Key_BS;

% SHIFT keys.  Do not depend upon these being available.  I cannot find
% any relevant terminfo entries for most of them.  The default values are
% appropriate for Xjed.
variable Key_Shift_F1   = setkey_via_terminfo ("", "\e[11$");
variable Key_Shift_F2   = setkey_via_terminfo ("", "\e[12$");
variable Key_Shift_F3   = setkey_via_terminfo ("", "\e[13$");
variable Key_Shift_F4   = setkey_via_terminfo ("", "\e[14$");
variable Key_Shift_F5   = setkey_via_terminfo ("", "\e[15$");
variable Key_Shift_F6   = setkey_via_terminfo ("", "\e[17$");
variable Key_Shift_F7   = setkey_via_terminfo ("", "\e[18$");
variable Key_Shift_F8   = setkey_via_terminfo ("", "\e[19$");
variable Key_Shift_F9   = setkey_via_terminfo ("", "\e[20$");
variable Key_Shift_F10  = setkey_via_terminfo ("", "\e[21$");
variable Key_Shift_F11  = setkey_via_terminfo ("", "\e[23$");
variable Key_Shift_F12  = setkey_via_terminfo ("", "\e[24$");

variable Key_Shift_Up   = setkey_via_terminfo ("", "\e[a");
variable Key_Shift_Down = setkey_via_terminfo ("", "\e[b");
variable Key_Shift_Right= setkey_via_terminfo ("%i", "\e[c");
variable Key_Shift_Left = setkey_via_terminfo ("#4", "\e[d");

variable Key_Shift_Ins  = setkey_via_terminfo ("#3", "\e[2$");
if (Key_Shift_Ins == "\e2$")
{
   % Work-around rxvt-terminfo bug
   $1 = getenv ("TERM");
   if ($1 != NULL)
     {
        if (is_substr ($1, "rxvt") || is_substr ($1, "screen"))
          Key_Shift_Ins = "\e[2$";
     }
}

variable Key_Shift_Del  = setkey_via_terminfo ("*4", "\e[3$");
variable Key_Shift_Home = setkey_via_terminfo ("#2", "\e[1$");
variable Key_Shift_End  = setkey_via_terminfo ("*7", "\e[4$");
variable Key_Shift_PgUp = setkey_via_terminfo ("", "\e[5$");
variable Key_Shift_PgDn = setkey_via_terminfo ("", "\e[6$");

variable Key_Shift_Tab  = setkey_via_terminfo ("bt", "\e[Z");  % reverse-tab
variable Key_Shift_BS   = setkey_via_terminfo ("", "\e[16$");

% Ctrl keys

variable Key_Ctrl_F1    = setkey_via_terminfo ("", "\e[11^");
variable Key_Ctrl_F2    = setkey_via_terminfo ("", "\e[12^");
variable Key_Ctrl_F3    = setkey_via_terminfo ("", "\e[13^");
variable Key_Ctrl_F4    = setkey_via_terminfo ("", "\e[14^");
variable Key_Ctrl_F5    = setkey_via_terminfo ("", "\e[15^");
variable Key_Ctrl_F6    = setkey_via_terminfo ("", "\e[17^");
variable Key_Ctrl_F7    = setkey_via_terminfo ("", "\e[18^");
variable Key_Ctrl_F8    = setkey_via_terminfo ("", "\e[19^");
variable Key_Ctrl_F9    = setkey_via_terminfo ("", "\e[20^");
variable Key_Ctrl_F10   = setkey_via_terminfo ("", "\e[21^");
variable Key_Ctrl_F11   = setkey_via_terminfo ("", "\e[23^");
variable Key_Ctrl_F12   = setkey_via_terminfo ("", "\e[24^");

variable Key_Ctrl_Up    = setkey_via_terminfo ("", "\e[^A");
variable Key_Ctrl_Down  = setkey_via_terminfo ("", "\e[^B");
variable Key_Ctrl_Right = setkey_via_terminfo ("", "\e[^C");
variable Key_Ctrl_Left  = setkey_via_terminfo ("", "\e[^D");

variable Key_Ctrl_Ins   = setkey_via_terminfo ("", "\e[2^");
variable Key_Ctrl_Del   = setkey_via_terminfo ("", "\e[3^");
variable Key_Ctrl_Home  = setkey_via_terminfo ("", "\e[1^");
variable Key_Ctrl_End   = setkey_via_terminfo ("", "\e[4^");
variable Key_Ctrl_PgUp  = setkey_via_terminfo ("", "\e[5^");
variable Key_Ctrl_PgDn  = setkey_via_terminfo ("", "\e[6^");

%variable Key_Ctrl_Tab  = setkey_via_terminfo ("", "\e[\t^");
variable Key_Ctrl_BS    = setkey_via_terminfo ("", "\e[16^" );

% We no longer need this
private define setkey_via_terminfo ();

%}}}
#endif % not IBMPC_SYSTEM

#ifdef MSWINDOWS
variable NumKey_Div   = NumKey+"q";
variable NumKey_Star  = NumKey+"r";
variable NumKey_Minus = NumKey+"s";
variable NumKey_Plus  = NumKey+"m";
variable NumKey_Enter = NumKey+"M";
variable NumKey_Dot   = ".";
variable NumKey_0     = "0";
variable NumKey_1     = "1";
variable NumKey_2     = "2";
variable NumKey_3     = "3";
variable NumKey_4     = "4";
variable NumKey_5     = "5";
variable NumKey_6     = "6";
variable NumKey_7     = "7";
variable NumKey_8     = "8";
variable NumKey_9     = "9";

#else

variable NumKey_Div   = "/";
variable NumKey_Star  = "*";
variable NumKey_Minus = NumKey+"m";
variable NumKey_Plus  = "+";
variable NumKey_Dot   = NumKey+"n";
variable NumKey_Enter = NumKey+"M";
variable NumKey_0     = NumKey+"p";
variable NumKey_1     = NumKey+"q";
variable NumKey_2     = NumKey+"r";
variable NumKey_3     = NumKey+"s";
variable NumKey_4     = NumKey+"t";
variable NumKey_5     = NumKey+"u";
variable NumKey_6     = NumKey+"v";
variable NumKey_7     = NumKey+"w";
variable NumKey_8     = NumKey+"x";
variable NumKey_9     = NumKey+"y";

#endif

provide ("keydefs");
