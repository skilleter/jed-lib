% ******************************************************************************
% Jenkins pipeline editing mode
% ******************************************************************************

$1 = "pipeline";

create_syntax_table ($1);

define_syntax ("/*", "*/",          '%',        $1);
define_syntax ("//", "",            '%',        $1);
define_syntax ("([{", ")]}",        '(',        $1);
define_syntax ('"',                                             '"',                    $1);
define_syntax ('\'',                                    '\'',           $1);
define_syntax ('\\',                                    '\\',           $1);
define_syntax ("0-9a-zA-Z_-",       'w',                        $1);        % words
define_syntax ("-+0-9a-fA-F.xXL",       '0',                    $1);   % Numbers
define_syntax (",;.?:$@\\",                     ',',                    $1);
define_syntax ('.',                                             '#',                    $1);
define_syntax ('!',                                             '#',                    $1);
define_syntax ("%-+/&*=<>|~^",      '+',                        $1);

set_syntax_flags ($1, 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%

private define setup_dfa_callback (name)
{
    dfa_enable_highlight_cache ("pipelinemode.dfa", name);

    dfa_define_highlight_rule ("\\\\.", "normal", name);
    dfa_define_highlight_rule ("//.*$", "comment", name);
    % dfa_define_highlight_rule ("^.*:", "keyword", name);
    dfa_define_highlight_rule ("\\$\\(.*\\)", "keyword", name);
    dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*\"", "string", name);
    dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*$", "string", name);
    dfa_define_highlight_rule ("'[^']*'", "string", name);
    dfa_define_highlight_rule ("'[^']*$", "string", name);
    dfa_define_highlight_rule ("\\$\\(.*\\)", "string", name);
    dfa_define_highlight_rule ("[\\|&;\\(\\)<>\\:]", "Qdelimiter", name);
    dfa_define_highlight_rule ("[\\[\\]\\*\\?=]", "Qoperator", name);
    dfa_define_highlight_rule ("[A-Za-z_]+",
                               "Knormal", name);
    dfa_define_highlight_rule ("^\t[ \t]*", "region", name);
    % dfa_define_highlight_rule ("^\t.*", "preprocess", name);
    dfa_define_highlight_rule (".", "normal", name);
    dfa_build_highlight_table (name);

%       dfa_define_highlight_rule("^\\.",                                    "PQpreprocess",    name);
%       dfa_define_highlight_rule("#.*" ,                                    "comment",                         name);
%       dfa_define_highlight_rule("[A-Za-z_][A-Za-z_0-9]*",                     "Knormal",                      name);
%       dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?","number",                  name);
%       dfa_define_highlight_rule("$\([A-Z0-9_].\)",                            "string",                       name);
%       dfa_define_highlight_rule("[ \t]+",                                  "normal",                  name);
%       dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]",               "delimiter",               name);
%       dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^\\$\\\\@]",       "operator",                name);
%       dfa_define_highlight_rule("\\$",                                                                                                        "dollar",                       name);
%       dfa_define_highlight_rule("^\t[ \t]*",                                                                          "region",                       name);
%       dfa_build_highlight_table(name);
}
dfa_set_init_callback (&setup_dfa_callback, $1);
%%% DFA_CACHE_END %%%
#endif

!if (keymap_p ($1)) make_keymap ($1);
append_keywords($1, 0,
                "agent", "parameters", "post",
                "triggers", "stages", "options",
                "environment", "branch", "sh", "wrap");

append_keywords($1, 1,
                "import", "def", "new", "pipeline");

append_keywords($1, 2,
                "retry", "build", "job", "filename", "args");

append_keywords($1, 3,
                "any", "string", "anyOf", "choice", "text", "password", "file",
                "credentials");

append_keywords($1, 4,
                "name", "state", "defaultValue", "description", "triggerOnPush",
                "triggerOnMergeRequest", "triggerOnAcceptedMergeRequest",
                "triggerOnNoteRequest", "noteRegex", "pendingBuildName",
                "triggerOnApprovedMergeRequest", "branchFilterType", "true", "false",
                "always", "failure", "success", "steps", "script", "when",
                "triggerOpenMergeRequestOnPush", "value", "choices", "time", "unit",
                "artifacts", "fingerprint");

append_keywords($1, 5,
                "updateGitlabCommitStatus", "gitLabConnection", "gitlab", "dockerfile",
                "stage", "timeout", "parallel", "disableConcurrentBuilds",
                "buildDiscarder", "logRotator", "daysToKeepStr", "artifactDaysToKeepStr",
                "archiveArtifacts");

append_keywords($1, 6,
                "checkout", "changeset", "PATH", "dir");

append_keywords($1, 7,
                "scm", "echo", "ls",
                "if", "isMerge", "env", "allOf",
                "BRANCH_NAME", "contains", "expression");

% ******************************************************************************

define pipeline_mode ()
{
   TAB = 4;
   USE_TABS=0;
   CASE_SEARCH=1;

   use_keymap("pipeline");
   set_mode("pipeline", 4);
   set_comment_info("pipeline", "//", NULL, NULL, 0x0);
   use_syntax_table ("pipeline");
   run_mode_hooks("pipeline_mode_hook");
}
