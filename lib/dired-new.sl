% ******************************************************************************
% TODO: Use fswalk_new()?
% TODO: Implement view, move, rename, delete, updir, re-read, flag backup
% TODO: Use folding to hide/open subdirectories
% TODO: On entry, top-level directory only should be visible
% TODO: First line should be the full directory path
% ******************************************************************************

% ******************************************************************************
% Constants defining the columns where to put stuff on the display
% ******************************************************************************

static variable FILE_COL=3, SIZE_COL=48, ATTR_COL=62;

#ifndef MSDOS OS2 WIN32 IBMPC_SYSTEM
static variable DATE_COL=76;
#else
static variable DATE_COL=68;
#endif

% ******************************************************************************
% Global variables

static variable dt_buffer = "*dirtree*";
static variable dt_info   = "*dir_info*";

static variable dt_help_msg = "[T]ag/Untag, [V]iew, [M]ove, re[N]ame, [D]elete, [Q]uit, CR:open/chdir, Refresh, BKSP=.., [~]-Tag backup files";

static variable dt_line_mark;

static variable dt_entries;

ifnot (is_defined ("FileInfo_Type"))
{
   typedef struct
   {
      dir, filename
   }
   FileInfo_Type;
}

static variable dt_files = @FileInfo_Type;

static variable dt_menu = "[T]ag/Untag, [V]iew, [M]ove, re[N]ame, [D]elete, [Q]uit, CR:open/chdir, Refresh, BKSP=.., [~]-Tag backup files";
% ******************************************************************************
% Configure the keymap

!if (keymap_p (dt_buffer))
   make_keymap (dt_buffer);

definekey ("dirtree_tag_toggle",         "t",        dt_buffer);
definekey ("dirtree_view",               "v",        dt_buffer);
definekey ("dirtree_move",               "m",        dt_buffer);
definekey ("dirtree_rename",             "n",        dt_buffer);
definekey ("dirtree_delete",             "d",        dt_buffer);
definekey ("dirtree_quit",               "q",        dt_buffer);
definekey ("dirtree_find",               "\r",       dt_buffer);
definekey ("dirtree_updir",              Key_BS,     dt_buffer);
definekey ("dirtree_reread_dir",         "r",        dt_buffer);
definekey ("dirtree_open",               "o",        dt_buffer);
definekey ("dirtree_flag_backup",        "~",        dt_buffer);

definekey ("dirtree_up",              Key_Up,        dt_buffer);
definekey ("dirtree_down",            Key_Down,      dt_buffer);
definekey ("dirtree_pgdn",            Key_PgDn,      dt_buffer);
definekey ("dirtree_pgup",            Key_PgUp,      dt_buffer);
definekey ("dirtree_top",             Key_Ctrl_PgUp, dt_buffer);
definekey ("dirtree_bottom",          Key_Ctrl_PgDn, dt_buffer);

% ******************************************************************************

static define human_size(size)
{
   if(size < 1024)
   {
      return sprintf("%d bytes", size);
   }

   size /= 1024;

   if(size < 1024)
   {
      return sprintf("%d KiB", size);
   }

   size /= 1024;

   if(size < 1024)
   {
      return sprintf("%d MiB", size);
   }

   size /= 1024;

   return sprintf("%d GiB", size);
}

% ******************************************************************************

static define update_info_pane(filename)
{
   variable st;
   variable err, status;

   otherwindow();
   sw2buf(dt_info);
   erase_buffer();
   insert(dt_menu);
   insert("\n\n");

   status = stat_file(filename);

   if(length(filename) > SIZE_COL - FILE_COL - 2)
   {
      filename = filename[[0:SIZE_COL - FILE_COL - 2]];
   }

   goto_column(FILE_COL);

   insert(sprintf("%s", filename));

   goto_column(SIZE_COL);

   if(status == NULL)
   {
      insert("ERROR");
   }
   else
   {
      !if(stat_is("dir", status.st_mode))
      {
         insert(sprintf("%12d  ", status.st_size));
      }

      goto_column(ATTR_COL);

#ifndef MSDOS OS2 WIN32 IBMPC_SYSTEM
      if(stat_is("sock", status.st_mode))
         insert("s");
      else if(stat_is("fifo", status.st_mode))
         insert("f");
      else if(stat_is("blk", status.st_mode))
         insert("b");
      else if(stat_is("chr", status.st_mode))
         insert("c");
      else
#endif
      if(stat_is("dir", status.st_mode))
         insert("d");
      else if(stat_is("lnk", status.st_mode))
         insert("l");
      else
         insert("-");

      if(status.st_mode & S_IRUSR) insert("r"); else insert("_");
      if(status.st_mode & S_IWUSR) insert("w"); else insert("_");

#ifndef MSDOS OS2 WIN32 IBMPC_SYSTEM
      if(status.st_mode & S_IXUSR) insert("x"); else insert("_");

      insert("-");

      if(status.st_mode & S_IRGRP) insert("r"); else insert("_");
      if(status.st_mode & S_IWGRP) insert("w"); else insert("_");
      if(status.st_mode & S_IXUSR) insert("x"); else insert("_");

      insert("-");

      if(status.st_mode & S_IXOTH) insert("r"); else insert("_");
      if(status.st_mode & S_IXOTH) insert("w"); else insert("_");
      if(status.st_mode & S_IXOTH) insert("x"); else insert("_");
#endif
      goto_column(DATE_COL);

      insert(sprintf("%s", ctime(status.st_mtime)));
   }

   otherwindow();
}

% ******************************************************************************
% Recentre the screen and move the cursor to the beginning of the line
% ******************************************************************************

static define limit_cursor()
{
   variable max_line = length(dt_entries);
   variable line = what_line();

   emacs_recenter();

   if(line > max_line)
   {
      goto_line(max_line);
      line = max_line;
   }

   bol();
   skip_white();

   update_info_pane(dt_entries[line-1]);
}

% ******************************************************************************

define dirtree_up()
{
   go_up_1();
   limit_cursor();
}

% ******************************************************************************

define dirtree_down()
{
   go_down_1();
   limit_cursor();
}

% ******************************************************************************

define dirtree_pgup()
{
   () = up(window_info('r')-1);
   limit_cursor();
}

% ******************************************************************************

define dirtree_pgdn()
{
   () = down(window_info('r')-1);
   limit_cursor();
}

% ******************************************************************************

define dirtree_top()
{
   bob();
   limit_cursor();
}

% ******************************************************************************

define dirtree_bottom()
{
   eob();
   limit_cursor();
}

% ******************************************************************************

define dirtree_find()
{
   onewindow();
   find_file(dt_entries[what_line()-1]);
   splitwindow();
   sw2buf(dt_buffer);
}

% ******************************************************************************

define dirtree_quit ()
{
   delbuf(dt_buffer);
}

% ******************************************************************************
% Forward declaration of _dirtree to allow recursion

static define _dirtree(files, dirname, wildcard, level);

% ******************************************************************************
% List the directories and files in the specified directory, indented
% as specified.

static define _dirtree(dirname, wildcard, level)
{
   variable entry;

   % Load matching files in the current directory

   foreach(glob(path_concat(dirname, "*")))
   {
      entry = ();

      if(entry != "." && entry != "..")
      {
         if(file_status(entry) == 1)
         {
            insert_spaces(level*4);
            insert(path_basename(entry));
            insert("\n");

            list_append(dt_entries, entry);
         }

      }
   }

   % Recurse recursively

   foreach(glob(path_concat(dirname, "*")))
   {
      entry = ();

      if(entry != "." && entry != "..")
      {
         if(file_status(entry) == 2)
         {
            insert_spaces(level*4);
            insert(path_basename(entry));
            insert("/\n");
            list_append(dt_entries, entry);

            _dirtree(entry, wildcard, level+1);
         }
      }
   }
}

% ******************************************************************************

static define dirtree(dirname, wildcard)
{
   % Can't recurse into a not directory thing

   if(file_status(dirname) != 2)
   {
      flush(dirname + " is not a directory");
      return;
   }

   onewindow();
   splitwindow();
   sw2buf(dt_info);

   while(window_info('r') > 10)
   {
      otherwindow();
      enlargewin();
      otherwindow();
   }

   otherwindow();
   sw2buf(dt_buffer);
   set_buffer_modified_flag(0);
   delbuf(dt_buffer);
   sw2buf(dt_buffer);
   use_keymap (dt_buffer);
   set_mode ("dirtree", 0);

   insert(dir+"\n");
   list_append(dt_entries, dir);

   _dirtree(dirname, wildcard, 1);

   set_buffer_modified_flag(0);
   set_readonly (1);

   flush(sprintf("%d entries", length(dt_entries)));
}

% ******************************************************************************

static define dirtree_point (dirn)
{
   variable max_line = length(dt_entries);

   if (dirn > 0)
      go_down_1 ();
   else if (dirn < 0)
      go_up_1 ();
}

% ******************************************************************************

static define update_dirtree_hook ()
{
   dt_line_mark = create_line_mark (11);
   %message(dt_help_msg);
   dirtree_point(0);
}

% ******************************************************************************

define dired_new()
{
   variable dir;

   % Build a directory tree in the *dirtree* buffer

   (,dir,,) = getbuf_info();

   dt_entries = {};
   dirtree(dir, "*");

   set_buffer_hook ("update_hook", &update_dirtree_hook);
   run_mode_hooks ("dirtree_hook");
   goto_line(1);
}

