%==========================================================================
% rexxmode.sl
%
% REXX editing mode
%
% ----------------------------------------------------------------------------

!if(keymap_p ("REXX"))
        make_keymap ("REXX");

% definekey ("indent_line", "\t", "REXX");
% definekey ("c_newline_and_indent", "\r", "REXX");

% -------------------------------------------------------------------------
% Word characters
% -------------------------------------------------------------------------

define_word ("0-9A-Za-z_");

% -------------------------------------------------------------------------
% Now create and initialize the syntax tables.
% -------------------------------------------------------------------------

create_syntax_table ("REXX");

define_syntax ("'", "", '%', "REXX");
% define_syntax("REM",             "",     '%', "REXX");
define_syntax ("(", ")", '(', "REXX");
define_syntax ('"', '"', "REXX");
define_syntax ("0-9a-zA-Z_", 'w', "REXX");      % words

define_syntax ("-+0-9.", '0', "REXX");  % Numbers

define_syntax (",;.?:_", ',', "REXX");
define_syntax ('#', '#', "REXX");
define_syntax ("%-+/&*=<>|!~^\\", '+', "REXX");

set_syntax_flags ("REXX", 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%
private define setup_dfa_callback (name)
{
        dfa_enable_highlight_cache ("rexxmode.dfa", name);

        dfa_define_highlight_rule ("^[ \t]*#", "PQpreprocess", name);
        dfa_define_highlight_rule ("%.*", "comment", name);
        dfa_define_highlight_rule ("/\\*.*\\*/", "Qcomment", name);
        dfa_define_highlight_rule ("^([^/]|/[^\\*])*\\*/", "Qcomment", name);
        dfa_define_highlight_rule ("/\\*.*", "comment", name);
        dfa_define_highlight_rule ("^[ \t]*\\*+([ \t].*)?$", "comment", name);
        dfa_define_highlight_rule ("[A-Za-z_\\$][A-Za-z_0-9\\$]*", "Knormal", name);
        dfa_define_highlight_rule ("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?", "number", name);
        dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\"", "string", name);
        dfa_define_highlight_rule ("\"([^\"\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*'", "string", name);
        dfa_define_highlight_rule ("'([^'\\\\]|\\\\.)*\\\\?$", "string", name);
        dfa_define_highlight_rule ("[ \t]+", "normal", name);
        dfa_define_highlight_rule ("[\\(\\[{}\\]\\),;\\.\\?:]", "delimiter", name);
        dfa_define_highlight_rule ("[%\\-\\+/&\\*=<>\\|!~\\^]", "operator", name);

        dfa_build_highlight_table (name);
}
dfa_set_init_callback (&setup_dfa_callback, "REXX");
%%% DFA_CACHE_END %%%
#endif

% Type 1 keywords

() = append_keywords ("REXX", 1,
                                                         "address", "arg", "call", "do", "end", "drop", "exit", "if", "then", "else", "interpret", "iterate", "leave", "nop",
                                                         "numeric", "options", "parse", "procedure", "pull", "push", "queue", "return", "say", "select", "when", "otherwise",
                                                         "signal", "trace", "upper");

() = append_keywords ("REXX", 2,
                                                         "abbrev", "abs", "address", "aarg", "b2c", "b2x", "beep", "bitand", "bitchg", "bitclr",
                                                         "bitcomp", "bitor", "bitset", "bittst", "bitxor", "buftype", "c2b", "c2d", "c2x", "cd",
                                                         "chdir", "center", "centre", "changestr", "charin", "charout", "chars", "close", "compare",
                                                         "compress", "condition", "copies", "countstr", "crypt", "datatype", "date", "delstr",
                                                         "delword", "desbuf", "digits", "directory", "d2c", "d2x", "dropbuf", "eof", "errortext",
                                                         "exists", "export", "filespec", "find", "fork", "form", "format", "freespace", "fuzz",
                                                         "gtenv", "getpid", "getspace", "gettid", "hash", "import", "index", "insert", "justify",
                                                         "lastpos", "left", "length", "linein", "lineout", "lines", "lowe", "makebuf", "max",
                                                         "min", "open", "overlay", "poolid", "popen", "pos", "qualify", "queued", "random",
                                                         "randu", "readch", "readln", "reverse", "right", "rxfuncadd", "rxfuncdrop", "rxfuncerrmsg", "rxfuncquery",
                                                         "exqueue", "seek", "show", "sign", "sleep", "sourceline", "space", "state", "storage",
                                                         "stream", "strip", "substr", "subword", "symbol", "time", "trace", "translate", "trim",
                                                         "trunc", "uname", "unixerror", "upper", "userid", "value", "verify", "word", "wordindex",
                                                         "wordlength", "wordpos", "words", "writech", "writeln", "xrange", "x2b", "x2d", "x2c");


() = append_keywords ("REXX", 3, "allocated", "dumptree", "dumpvars", "listleaked", "traceback");
() = append_keywords ("REXX", 4, "");
() = append_keywords ("REXX", 5, "");
() = append_keywords ("REXX", 6, "");
() = append_keywords ("REXX", 7, "");

define rexx_mode ()
{
        variable kmap = "REXX";

        set_abbrev_mode (1);

        set_mode (kmap, 2);
        use_keymap (kmap);
   set_comment_info(kmap, "'", NULL, NULL, 0);
        use_syntax_table (kmap);

%       run_mode_hooks ("rexx_mode_hook");
}

