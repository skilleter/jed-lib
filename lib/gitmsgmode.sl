% ******************************************************************************
% Edit mode for git messages
% ******************************************************************************

define_word ("0-9A-Za-z_");

create_syntax_table ("GITMSG");

define_syntax ("#",                                             "",    '%',     "GITMSG");
define_syntax ("([{",                                   ")]}", '(', "GITMSG");
% define_syntax ('"',                                           '"',                    "GITMSG");
% define_syntax ('\'',                                  '\'',           "GITMSG");
define_syntax ('\\',                                    '\\',           "GITMSG");
define_syntax ("0-9a-zA-Z_:-",      'w',                        "GITMSG");        % words
define_syntax ("-+0-9a-fA-FI.xXL",      '0',                    "GITMSG");   % Numbers
define_syntax (",;.?$@\\",                      ',',                    "GITMSG");
define_syntax ('.',                                             '#',                    "GITMSG");
define_syntax ('!',                                             '#',                    "GITMSG");
define_syntax ("%-+/&*=<>|~^",      '+',                        "GITMSG");
set_syntax_flags ("GITMSG", 4);

#ifdef HAS_DFA_SYNTAX
%%% DFA_CACHE_BEGIN %%%

private define setup_dfa_callback (name)
{
    dfa_enable_highlight_cache ("gitmsgmode.dfa", name);

    dfa_define_highlight_rule ("\\\\.", "normal", name);
    dfa_define_highlight_rule ("#.*$", "comment", name);
    % dfa_define_highlight_rule ("^.*:", "keyword", name);
    dfa_define_highlight_rule ("\\$\\(.*\\)", "keyword", name);
    dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*\"", "string", name);
    dfa_define_highlight_rule ("\"([^\\\\\"]|\\\\.)*$", "string", name);
    dfa_define_highlight_rule ("'[^']*'", "string", name);
    dfa_define_highlight_rule ("'[^']*$", "string", name);
    dfa_define_highlight_rule ("\\$\\(.*\\)", "string", name);
    dfa_define_highlight_rule ("[\\|&;\\(\\)<>\\:]", "Qdelimiter", name);
    dfa_define_highlight_rule ("[\\[\\]\\*\\?=]", "Qoperator", name);
    dfa_define_highlight_rule ("[A-Za-z_]+",
                               "Knormal", name);
    dfa_define_highlight_rule ("^\t[ \t]*", "region", name);
    % dfa_define_highlight_rule ("^\t.*", "preprocess", name);
    dfa_define_highlight_rule (".", "normal", name);
    dfa_build_highlight_table (name);

%       dfa_define_highlight_rule("^\\.",                                    "PQpreprocess",    name);
%       dfa_define_highlight_rule("#.*" ,                                    "comment",                         name);
%       dfa_define_highlight_rule("[A-Za-z_][A-Za-z_0-9]*",                     "Knormal",                      name);
%       dfa_define_highlight_rule("[0-9]+(\\.[0-9]*)?([Ee][\\+\\-]?[0-9]*)?","number",                  name);
%       dfa_define_highlight_rule("$\([A-Z0-9_].\)",                            "string",                       name);
%       dfa_define_highlight_rule("[ \t]+",                                  "normal",                  name);
%       dfa_define_highlight_rule("[\\(\\[{}\\]\\),;\\.\\?:]",               "delimiter",               name);
%       dfa_define_highlight_rule("[%\\-\\+/&\\*=<>\\|!~\\^\\$\\\\@]",       "operator",                name);
%       dfa_define_highlight_rule("\\$",                                                                                                        "dollar",                       name);
%       dfa_define_highlight_rule("^\t[ \t]*",                                                                          "region",                       name);
%       dfa_build_highlight_table(name);
}
dfa_set_init_callback (&setup_dfa_callback, "GITMSG");
%%% DFA_CACHE_END %%%
#endif

!if (keymap_p ("GITMSG")) make_keymap ("GITMSG");

% Commit message keywords

append_keywords("GITMSG", 3, "Change-Description:", "Change-TestProcedure:", "Issue-Id:", "Change-Id:", "Depends-On:");

define gitmsg_mode ()
{
   TAB = 8;
   USE_TABS=1;
   CASE_SEARCH=1;
   use_keymap("GITMSG");
   set_mode("GITMSG", 4);
   use_syntax_table ("GITMSG");
   set_comment_info("GITMSG", "#", NULL, NULL, 0);
   run_mode_hooks("msg_mode_hook");
}

