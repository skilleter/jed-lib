% ******************************************************************************
% bufhist.sl
%
% Traverse a list of buffers visited in historical order - not working perfectly
% as yet as it will get confused if you switch to a new buffer when you are
% part-way through the history list.
% ******************************************************************************

variable BUFFER_HISTORY_LEN = 20;

private variable buffer_history_list        = Mark_Type[BUFFER_HISTORY_LEN]; % The 'snail-trail' of previous locations
private variable buffer_history_used        = 0;                             % Number of used entries in the history
private variable buffer_history_total       = 0;                             % Total number of times that entries have been added
private variable buffer_history_current     = 0;                             % Next entry to use in the history
private variable buffer_history_previous    = -1;                            % Last entry set in the history
private variable buffer_history_navigation  = 0;                             % Index of last entry used when navigating history

private variable buffer_history_last_buffer = "";

private variable buffer_history_debug       = 0;

% ******************************************************************************
% Record the current location in the buffer history
% ******************************************************************************

define buffer_history_record()
{
   error("buffer_history_record() - Not implemented");

   % Update the last mark (if any) if we have simply moved around in the current buffer

   if(buffer_history_last_buffer == whatbuf() and buffer_history_previous != -1)
   {
      if(buffer_history_debug) flush(sprintf("Updating history entry %d in buffer '%s'", buffer_history_previous+1, whatbuf()));

      buffer_history_list[buffer_history_previous] = create_user_mark();
   }
   else
   {
      if(buffer_history_debug) flush(sprintf("Recording history entry %d in buffer '%s'", buffer_history_current+1, whatbuf()));

      buffer_history_list[buffer_history_current] = create_user_mark();

      % Increment the history index and number of stored entries (we store up
      % to BUFFER_HISTORY_LEN entries then they drop off the end of the list)

      buffer_history_total++;
      buffer_history_previous = buffer_history_current;

      buffer_history_current++;
      if(buffer_history_current >= BUFFER_HISTORY_LEN)
      {
         buffer_history_current = 0;
      }

      if(buffer_history_total <= BUFFER_HISTORY_LEN)
      {
         buffer_history_used = buffer_history_total;
      }

      % If we have been navigating around the history and have now moved somewhere
      % else, then reset the navigation index back to the current location

      buffer_history_navigation = buffer_history_current;

      buffer_history_last_buffer = whatbuf();
   }
}

% ******************************************************************************
% Remove the last recorded entry from the buffer history (if we've decided that
% we aren't going anywhere after recording the position)
%
% Note that this isn't properly protected against being called
% ******************************************************************************

define buffer_history_delete()
{
   error("buffer_history_delete() - Not implemented");

   if(buffer_history_total > 0)
   {
      buffer_history_total--;
      buffer_history_current--;

      if(buffer_history_current < 0)
      {
         buffer_history_current = BUFFER_HISTORY_LEN-1;
      }

      buffer_history_previous--;

      if(buffer_history_previous < 0)
      {
         buffer_history_previous = BUFFER_HISTORY_LEN-1;
      }

      if(buffer_history_total < BUFFER_HISTORY_LEN)
      {
         buffer_history_used = buffer_history_total;
      }
   }
}

% ******************************************************************************
% Move back one entry in the buffer history list
% ******************************************************************************

define buffer_history_back()
{
   error("buffer_history_back() - Not implemented");

   variable new_navigation, new_buffer;

   % If this is the first step backwards and the most recent mark isn't in the current
   % file then set one here

   if(buffer_history_current == buffer_history_navigation)
   {
      buffer_history_record();
   }

   % Try to step back to the previous location, handling wrap and making sure
   % that we don't go back too far

   new_navigation = buffer_history_navigation-1;

   if(new_navigation < 0)
   {
      new_navigation = BUFFER_HISTORY_LEN-1;
   }

   if(new_navigation == buffer_history_current or
      new_navigation > buffer_history_used)
   {
      error("At start of history list");
   }

   buffer_history_navigation = new_navigation;

   new_buffer = user_mark_buffer(buffer_history_list[buffer_history_navigation]);

   if(bufferp(new_buffer) == 0)
   {
      error("Buffer '"+new_buffer+"' no longer exists");
   }

   flush(sprintf("Going back to entry #%d, buffer '%s'", new_navigation+1, new_buffer));

   sw2buf (new_buffer);
   goto_user_mark(buffer_history_list[buffer_history_navigation]);
}

% ******************************************************************************
% Move forward one entry in the buffer history list
% ******************************************************************************

define buffer_history_foreward()
{
   error("buffer_history_foreward() - Not implemented");

   variable new_buffer;

   % Don't step past the current entry

   if(buffer_history_navigation == buffer_history_current)
   {
      error("At end of history list");
   }

   % Step forwards with wrappage

   buffer_history_navigation++;

   if(buffer_history_navigation > BUFFER_HISTORY_LEN)
   {
      buffer_history_navigation = 0;
   }

   new_buffer = user_mark_buffer(buffer_history_list[buffer_history_navigation]);

   if(bufferp(new_buffer) == 0)
   {
      error("Buffer '"+new_buffer+"' no longer exists");
   }

   flush(sprintf("Going forewards to entry #%d, buffer '%s'", buffer_history_navigation+1, new_buffer));
   sw2buf (new_buffer);
   goto_user_mark(buffer_history_list[buffer_history_navigation]);
}

provide("bufhist")
