%==========================================================================
% hlpmode.sl
%
% Create and initialize the syntax table for help files
%
%==========================================================================

create_syntax_table ("HLP");

define_syntax ("/*",             "*/",  '%', "HLP");
define_syntax ("([{",            "}])", '(', "HLP");
define_syntax ('"',                     '"', "HLP");
define_syntax ("0-9a-zA-Z_[]",          'w', "HLP");    % words
define_syntax ("", ',', "HLP");
%define_syntax ('#', '#', "HLP");
define_syntax("@$\\%-+/&*=<>|!~^",  '+', "HLP");

append_keywords("HLP", 2,
                "@A", "@B", "@C", "@D", "@E", "@F", "@G", "@H", "@I", "@J", "@K", "@L", "@M",
                "@N", "@O", "@P", "@Q", "@R", "@S", "@T", "@U", "@V", "@W", "@X", "@Y", "@Z",
                "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10","F11","F12",
                "M");

append_keywords("HLP", 1, "EXAMPLE", "SEE", "ALSO", "SYNOPSIS", "USAGE", "DESCRIPTION", "[JED]", "[SLANG]");

define hlp_mode ()
{
        set_mode ("HLP", 0);
        use_syntax_table ("HLP");
   set_comment_info("HLP", NULL, "/*", "*/", 0);
        run_mode_hooks ("hlp_mode_hook");
}
